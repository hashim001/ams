Account and Finance Project Generic using Spring MVC 4, Spring Security, Spring Data JPA, XML Configuration, Maven, JSP, and MySQL.

## Guide
Currently not available. 
## Prerequisites
- JDK 1.8 or later
- Maven 3 or later
- MySQL 5.6 or later

## Stack
- Spring MVC
- Spring Security
- Spring Data JPA
- Maven
- JSP
- MySQL
- TWITTER BOOTSTRAP

## Run
```mvn tomcat7:run```

