<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Lead Management</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${saleLeadMessage}
        <h3 class="heading-main">Sale Leads<span class="addIcon" data-toggle="modal"
                                                         data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h3>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>

                            <th width="20%">Customer</th>
                            <th width="5%">Property Number</th>
                            <th width="10%">Mobile</th>
                            <th width="15%">Email</th>
                            <th width="5%">Created</th>
                            <th width="20%">Remarks</th>
                            <security:authorize access="hasRole('ROLE_MASTER_ADMIN')">
                                <th width="15">Salesman</th>
                                <th width="5%">Action</th>
                            </security:authorize>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${saleLeadList}" var="lead" varStatus="status">

                            <tr>
                                <td>${lead.customer}</td>
                                <td>${lead.apartmentNumber}</td>
                                <td>${lead.mobile}</td>
                                <td>${lead.email}</td>
                                <td><fmt:formatDate type = "date" value = "${lead.created}" /></td>
                                <td>${lead.remarks}</td>
                                <security:authorize access="hasRole('ROLE_MASTER_ADMIN')">
                                    <c:choose>
                                        <c:when test="${lead.salesmanId == 0}">
                                            <td>ADMIN</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>${salesmanMap.get(lead.salesmanId)}</td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td>
                                        <form method="post" id="leadRemove${status.index}"
                                              action="${contextPath}/SaleLead/remove">
                                            <input type="hidden" name="leadId"
                                                   value="${lead.leadId}"/>
                                        </form>
                                        <ul class="list-inline">
                                            <li><a href="#" onclick="remove(${status.index});"><img
                                                    src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                        </ul>
                                    </td>
                                </security:authorize>

                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Add Lead</h2>
            </div>
            <form:form method="POST" modelAttribute="leadForm" action="${contextPath}/SaleLead/addSaleLead">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="customer">
                                <form:label path="customer">Customer Name</form:label>
                                <form:input type="text"  path="customer" autofocus="true" required="true" />
                                <form:errors path="customer"></form:errors>
                            </spring:bind>
                            <spring:bind path="email">
                                <form:label path="email">Email</form:label>
                                <form:input type="email"  path="email" autofocus="true" />
                                <form:errors path="email"></form:errors>
                            </spring:bind>
                            <spring:bind path="mobile">
                                <form:label path="mobile">Mobile</form:label>
                                <form:input type="text"  path="mobile" autofocus="true" required="true" />
                                <form:errors path="mobile"></form:errors>
                            </spring:bind>


                            <security:authorize access="hasRole('ROLE_MASTER_ADMIN')">
                                <spring:bind path="salesmanId">
                                    <form:label path="salesmanId">Salesman</form:label>
                                    <form:select path="salesmanId" autofocus="true" required="true" >
                                        <form:option value="0">NONE</form:option>
                                        <c:forEach items="${salesmanList}" var="salesman">
                                            <form:option value="${salesman.distributorId}">${salesman.name}</form:option>
                                        </c:forEach>
                                    </form:select>
                                </spring:bind>
                            </security:authorize>
                        </div>

                        <div class="col-sm-6">

                            <label> Model:</label>
                            <select name="stockId" id="stockId" >
                                <option value="0">NONE</option>
                                <c:forEach items="${modelList}" var="model">
                                    <option value="${model.stockId}">${model.stockAccount.title}</option>
                                </c:forEach>
                            </select>

                            <spring:bind path="apartmentNumber">
                                <form:label path="apartmentNumber">Apartment Number</form:label>
                                <form:select path="apartmentNumber" autofocus="true" required="true" id = "apartmentNumber">

                                </form:select>
                            </spring:bind>
                            <spring:bind path="created">
                                <form:label path="created">Create Date</form:label>
                                <form:input type="date" path="created" autofocus="true" required="true" value="${currentDate}"></form:input>
                                <form:errors path="created"></form:errors>
                            </spring:bind>

                            <spring:bind path="remarks">
                                <form:label path="remarks">Remarks</form:label>
                                <form:input type="text"  path="remarks" autofocus="true" ></form:input>
                                <form:errors path="remarks"></form:errors>
                            </spring:bind>

                            <spring:bind path="leadId">
                                <form:input type="hidden" path="leadId" id="leadId"
                                            autofocus="true"></form:input>

                            </spring:bind>
                        </div>

                    </div>
                    <div align="center">

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit" onclick="submitForm();return false;">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });

        $('#stockId').change(function () {
            updateInnerList();
        });

    });
    function goBack(){
        window.location = "${contextPath}/home#payroll";
    }

    function remove(id){
        if(confirm("Are you sure you want to remove the Sale Lead?")){
            $('#leadRemove'+id).submit();
        }
    }

    $('#myModal').on('hidden.bs.modal', function () {
        window.location.reload();

    });

    function submitForm() {
        if(!$('#customer').val()){
            alert("Please Enter Customer Name.");
        }else if(!$('#mobile').val()){
            alert("Please Enter Mobile number.");
        }else if($('#apartmentNumber').val() == null){
            alert("Please select a valid Apartment / Shop.");
        }else {
            $('#leadForm').submit();
        }


    }

    function edit(holidayId) {
        $.ajax({
            url: '${contextPath}/PayRoll/editExemption',
            contentType: 'application/json',
            data: {
                holidayId: holidayId

            },
            success: function (response) {
                editForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
    function editForm(response) {
        $("#myModal").modal();
        $('#myModal').find('form').trigger('reset');
        $('#myModal').find('form').find('#month').val(response.month);
        $('#myModal').find('form').find('#created').val(response.created);
        $('#myModal').find('form').find('#year').val(response.year);
        $('#myModal').find('form').find('#count').val(response.count);
        $('#myModal').find('form').find('#reason').val(response.reason);
        $('#myModal').find('form').find('#exemptionId').val(response.exemptionId);

    }


    function updateInnerList() {
        $.ajax({
            url: '${contextPath}/SaleLead/getInternalList',
            contentType: 'application/json',

            data: {
                stockId: $('#stockId').val()

            },
            success: function (response) {
                var data = response,option="";
                if(data){
                    $.each(data,function(key,item){
                        option += "<option value="+key+">"+item+"</option>"
                    });
                }
                $("#apartmentNumber").html(option);

            }
        });
    }
</script>
</body>
</html>