<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Purchase Report</title>


    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>

<div class="container">
    <form method="post" class="form-inline">
        <div class="ledgerReportHeaderFilterSec">
            <h3>Report ${reportType} </h3>
            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label for="fromDate">Start Date :</label>
                    <input type="date" id="fromDate" name="fromDate" class="form-control" value="${fromDate}" />
                    <label for="toDate">End Date :</label>
                    <input type="date" id="toDate" name="toDate" class="form-control" value="${toDate}" />
                    <label>Report Type</label>
                    <select name="reportType" class="form-control">
                        <c:choose>
                            <c:when test="${reportType == 'GATE'}">
                                <option value="GATE" selected>GATE IN</option>
                                <option value="QUALITY">QUALITY ASSURANCE</option>
                                <option value="STORE">STORE</option>
                            </c:when>
                            <c:when test="${reportType == 'QUALITY'}">
                                <option value="GATE" >GATE IN</option>
                                <option value="QUALITY" selected>QUALITY ASSURANCE</option>
                                <option value="STORE">STORE</option>
                            </c:when>
                            <c:otherwise>
                                <option value="GATE" >GATE IN</option>
                                <option value="QUALITY" >QUALITY ASSURANCE</option>
                                <option value="STORE" selected>STORE</option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                    <button type="submit">Apply</button>

                </div>
            </div>

        </div>
    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn"
                    onclick="printDiv()">Print
            </button>
            <p colspan="2" align="right" valign=bottom><font color="#000000">Print on : ${currentDate}</font></p>
        </div>

    </div>
</div>
<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">

                    <table class="table trialreport">
                        <thead>
                        <th>ChallanId</th>
                        <th>Challan Number</th>
                        <th>Date</th>
                        <th>Term</th>
                        <th>P.O</th>
                        <th>Item</th>
                        <th>Qty</th>
                        </thead>
                        <tbody>
                        <c:forEach items="${reportModel}" var="challan">
                            <tr>
                            <td rowspan="${challan.challanProductList.size() + 1}">${challan.challanId}</td>
                            <td rowspan="${challan.challanProductList.size() + 1}">${challan.dcNumber}</td>
                            <td rowspan="${challan.challanProductList.size() + 1}">${challan.date}</td>
                            <td rowspan="${challan.challanProductList.size() + 1}">${challan.deliveryTerm}</td>
                            <td rowspan="${challan.challanProductList.size() + 1}">${challan.challanPurchaseOrder.orderNumber}</td>
                            <c:forEach items="${challan.challanProductList}" var="product">
                                <tr>
                                    <td>${product.itemName}</td>
                                    <td>${product.deliveredQuantity}</td>
                                </tr>
                            </c:forEach>


                            </tr>
                        </c:forEach>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>


    function printDiv() {

        var divToPrint = document.getElementById('printDiv');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '<link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/common.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/booker/bookerSaleSummary.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<table>'+
            '<thead>'+
            '<tr>'+
            '   <td colspan="3"><h1>${distributorName}</h1></td>'+
            '</tr>'+
            '<tr>'+
            '   <td colspan="3"><h2>Booker Sale Report</h2></td>'+
            '</tr>'+
            '<tr>'+
            '   <td style="width: 200px;"></td>'+
            '  <td style="text-align: right;"><span class="bold">Start Date:</span>${fromDate}</td>'+
            ' <td style="text-align: right;"><span class="bold">End Date:</span>${toDate}</td>'+
            '</tr>'+
            '<tr style="text-align: right;">'+
            '   <td colspan="3"><span class="bold">Print Date:</span> ${currentDate}</td>'+
            '</tr>'+
            '</thead>'+
            '</table>'+
            divToPrint.innerHTML +
            '<div class="footer">'+
            '<table>'+
            '<tr>'+
            '<td class="bold">'+
            '<p>Developed & Designed By: Evonexx</p>'+
            ' </td>'+
            ' </tr>'+
            '<tr>'+
            '<td>'+
            '<p>www.evonexx.com / Ph# 0092-21-35293284-6</p>'+
            ' </td>'+
            '  </tr>'+
            ' </table>'+
            '</div>'+
            '</body></html>');

        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 50000);



    }




</script>
</body>
</html>