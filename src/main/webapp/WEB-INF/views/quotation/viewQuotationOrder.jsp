<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Quotations</title>
</head>

<body>
<%@ include file="../header.jsp" %>
<section class="main">
    <div class="container">
        ${wrongRecipientError}
        ${wrongCredentialsError}
        ${emailSuccessful}
        ${imageNotFound}
        <h2 class="heading-main">Quotations<a href="${contextPath}/Quotation/addQuotationOrder"><span
                class="addIcon"
                data-toggle="modal"
                data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></a></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th width="15%">Q-Number</th>
                            <th width="15%">Remarks</th>
                            <th onclick="refreshPage()" width="12%">Q-Date</th>
                            <th width="15%">Customer</th>
                            <th>Status</th>
                            <th width="10%">P-Status</th>
                            <th width="12%">Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${quotationeOrders}" var="quotation" varStatus="quotationStatus">
                            <tr>

                                <td><c:choose>
                                    <c:when test="${quotation.quotationNumber!= ''}">
                                        ${quotation.quotationNumber}
                                    </c:when>
                                    <c:otherwise>
                                        NOT ENTERED
                                    </c:otherwise>
                                </c:choose></td>
                                <td>${quotation.remarks}</td>
                                <td><fmt:formatDate pattern="dd/MM/yyyy" value="${order.orderDate}"/></td>
                                <td>${quotation.customerAccount.title}</td>
                                <td>${quotation.quotationStatus}</td>
                                <td><c:choose>
                                    <c:when test="${quotation.amount> quotation.paymentAmount}">
                                        UNPAID
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${ quotation.paymentAmount > 0}">
                                                PAID
                                            </c:when>
                                            <c:otherwise>
                                                DELETED
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <li>

                                            <a title="View PDF" target="_blank"
                                               href="${contextPath}/Quotation/quotationPdf?quotationId=${quotation.quotationId}"><img
                                                    src="${contextPath}/resources/img/bill.png" alt=""></a></li>
                                        <li>

                                            <a title="Compose Email" target="_blank"
                                               href="${contextPath}/Quotation/emailGenerate?customerCode=${quotation.customerAccount.accountCode}"><img
                                                    src="${contextPath}/resources/img/email.png" alt=""></a></li>
                                    </ul>
                                    <form method="post" id="closeOrder${orderStatus.index}"
                                          action="${contextPath}/SaleTrading/closeOrder?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="orderId" value="${order.orderId}"/>
                                    </form>
                                </td>


                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    function closeQuotation(id) {
        if (confirm("Are you sure, you want to close the Quotation?")) {
            $('#closeOrder' + id).submit();
        }
    }

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[5, "desc"]],
            "pageLength": 7
        });
    });

    function refreshPage() {
        window.location.reload();
    }
    function goBack(){
     window.location = "${contextPath}/home#quotations";
    }

</script>
</body>
</html>