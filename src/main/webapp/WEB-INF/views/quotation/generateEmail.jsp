<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Email Generation</title>
    <style>
        .box {
            margin: 15px auto;
            border: 1px solid;
            width: 650px;
            height: 550px;
        }

        .heading {
            background: #000;
        }

        .heading p {
            color: #fff;
            margin: 0;
            padding: 10px 14px;
            font-size: 16px;
        }

        .form {
            width: 100%;
            margin: 0;
            font-size: 0;
        }

        .form input {
            width: 100%;
            height: 35px;
            border: none;
            border-bottom: 1px solid #b5b4b4;
            padding: 0 15px;
            font-size: 14px
        }

        .form textarea {
            width: 100%;
            height: 350px;
            border: none;
            padding: 5px 15px;
            font-size: 14px;
        }

        .footer {
            border-top: 1px solid #b5b4b4;
            width: 100%;
            text-align: center;
            padding-top: 10px;
        }

        input[type=file] {
            width: 40px;
            height: 28px;
            overflow: hidden;
        }

        input[type="file"]:before {
            content: '\f0c6';
            font-family: FontAwesome;
            background: none;
            border-radius: 30%;
            height: 28px;
            display: inline-block;
            width: 28px;
            font-size: 25px;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="box">
        <div class="heading">
            <p>Message</p>
        </div>
        <form:form method="POST" modelAttribute="emailModel" enctype="multipart/form-data">
        <div class="form">
            <spring:bind path="sender">
                <form:input type="text" path="sender" placeholder="Sender"
                            autofocus="true" readonly="true"></form:input>
                <form:errors path="sender"></form:errors>
            </spring:bind>
            <spring:bind path="recipient">
                <form:input type="text" path="recipient" placeholder="Recipient"
                            autofocus="true"></form:input>
                <form:errors path="recipient"></form:errors>
            </spring:bind>
            <spring:bind path="subject">
                <form:input type="text" path="subject" placeholder="Subject"
                            autofocus="true"></form:input>
                <form:errors path="subject"></form:errors>
            </spring:bind>
            <spring:bind path="emailBody">
                <form:textarea id="editor" type="text" path="emailBody" placeholder="Email Body"
                            autofocus="true"></form:textarea>
                <form:errors path="emailBody"></form:errors>
            </spring:bind>
                <form:input type="hidden"  path="smtpServer" autofocus="true"></form:input>
                <form:input type="hidden"  path="portNumber" autofocus="true"></form:input>
                <form:input type="hidden"  path="password" autofocus="true"></form:input>

        </div>
        <div class="footer">
            <div class="row">
                <div class="col-md-3">
                    <spring:bind path="attachment">
                        <form:input type="file" path="attachment" placeholder="file"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </div>
        </form:form>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
</script>
</body>
</html>