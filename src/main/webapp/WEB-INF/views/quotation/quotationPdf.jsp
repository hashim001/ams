<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quotation - ${quotation.remarks}</title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .container {
            margin: 0 auto;
            padding: 0 15px;
        }

        .width {
            margin: 2px 250px;
        }

        tr > td#dot, #dot2 {
            border-bottom: dotted;
            width: 100%;
        }

        .fullwidth {
            width: 100%;
        }

        .fullwidth .quater td {
            width: 25%
        }

        .bold {
            font-weight: bold;
            font-size: 20px;
            text-transform: uppercase
        }

        .heading {
            text-transform: uppercase;
            font-size: 30px;
            font-weight: bold;
            margin-bottom: 10px;
        }

        table.fulldot {
            width: 100%;
            margin-top: 15px;
        }

        table {
            width: 100%;
        }

        .top .red th {
            border-top: 2px solid red;
            border-bottom: 2px solid red;
            text-align: left;
        }

        .top {
            margin-top: 5px;
            border-spacing: 0;
        }

        .right {
            text-align: right;
            padding-right: 30px;
        }

        .height {
            line-height: 25px
        }

        tr {
            width: 100%;
        }

        td {
            width: 25%;
        }

        .fontred {
            color: red;
            text-transform: uppercase;
            font-weight: bold;
            font-size: 15px;
            align-content: center
        }

        table {
            font-size: 13px
        }

        .bottom {
            position: relative;
            bottom: -30px;
        }

        @media print {
            .width {
                margin: 2px 25px;
            }

            body {
                -webkit-print-color-adjust: exact;
            }

            .fontred {
                color: red;
                text-transform: uppercase;
                font-weight: bold;
                font-size: 15px;
            }

            .bottom {
                position: relative;
                bottom: -100px;
            }

            .top .red th {
                border-top: 2px solid red;
                border-bottom: 2px solid red;
            }
        }

        .deliveryArea {
            text-align: right;

        }

        .deliveryArea td {
            width: 85%;
        }

        .deliveryArea h4 {
            color: #004269;
            margin-bottom: 15px;
        }

        .deliveryArea table {
            float: right;
            border-spacing: 0;
        }

        .deliveryArea table td {
            font-size: 13px;
        }

        .deliveryArea table td:first-child {
            text-align: right;
            padding-right: 15px;
        }

        .deliveryArea table td:last-child {
            border: 1px solid #ddd;
            padding: 2px 10px;
            text-align: left
        }
    </style>
</head>

<body>
<div class="container width" id="content">
    <table>
        <tbody>
        <tr>
            <td>
                <img src="${staticInfo.imageUrl}" alt="img">
                <h5 style="font-family: 'Lucida Handwriting';">
                    <span>${staticInfo.companyAddress}</span><br>
                    <span>${staticInfo.addressSecondary}</span><br>
                    <span>Phone : ${staticInfo.phone}</span><br>
                    <span>Mob: ${staticInfo.mobile}</span>

                </h5>
            </td>
            <td class="deliveryArea" width="50%">
                <h2>Quotation</h2>
                <table>
                    <tr>
                        <td style="font-size: 18px">Quotation#</td>
                        <td style="font-size: 18px; white-space: nowrap;">${quotation.quotationNumber}</td>
                    </tr>
                    <tr>
                        <td style="font-size: 18px">Quotation Date</td>
                        <td style="font-size: 18px; white-space: nowrap;">${quotation.quotationDate}</td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <table class="fullwidth">
                <tbody>
                <tr width="100%" colspan="2">
                    <h3>Messer: <span style="alignment: center"><u>${quotation.customerAccount.title}</u></span></h3>

                </tr>
                </tbody>

            </table>
        </tr>
        <tr>
            <table class="fulldot">
                <tbody>
                <tr>
                    <td id="dot"></td>
                </tr>
                </tbody>
            </table>
        </tr>
        <tr>
            <table class="top">
                <thead>
                <tr class="red">
                    <th class="bold">qty</th>
                    <th class="bold">description</th>
                    <th class="bold">unit price</th>
                    <th class="bold">amount</th>
                </tr>
                </thead>
                <tbody class="height">
                <c:set var="totalAmount" value="${0}"/>
                <c:set var="totalTax" value="${0}"/>
                <c:set var="loopCount" value="${0}"></c:set>
                <c:forEach items="${quotation.quotationProductList}" var="product" varStatus="productStatus">
                    <tr>
                        <td>${product.initialQuantity}</td>
                        <td>${product.itemAccount.title}</td>
                        <td>${product.rate}</td>
                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                              value="${product.rate * (product.initialQuantity)}"/></td>
                        <c:set var="totalAmount"
                               value="${totalAmount + (product.rate * (product.initialQuantity))}"/>
                        <c:set var="totalTax"
                               value="${totalTax + ((product.taxRate / 100) * product.rate * (product.initialQuantity))}"/>
                        <c:set var="loopCount" value="${loopCount + 1}"></c:set>

                    </tr>
                </c:forEach>

                <tr>
                    <td></td>
                    <td></td>
                    <td style="font-weight: bold">SubTotal</td>
                    <td style="font-weight: bold">PKR ${totalAmount}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="font-weight: bold">Sales Tax</td>
                    <td style="font-weight: bold">PKR ${totalTax}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="font-weight: bold">Total</td>
                    <td style="font-weight: bold">PKR ${totalAmount + totalTax}</td>
                </tr>
                </tbody>
            </table>
        </tr>
        <table class="bottom">
            <tbody>
            <tr>
                <td><img src="${contextPath}/resources/img/thank-you.png" width="150" height="150"></td>
            </tr>
            <tr>
                <td class="small">
                    <span class="fontred"> terms & condition</span>
                    ${staticInfo.conditions}
                </td>
            </tr>
            </tbody>
        </table>
        </tbody>
    </table>
</div>
</body>

</html>