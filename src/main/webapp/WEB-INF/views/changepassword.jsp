<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <title>Change Password</title>
</head>
<body>
<%@ include file = "header.jsp" %>

<div class="container">
<div class="row">
    <div class="col-md-4">

    </div>
    <div class="col-md-4">


    <form:form method="POST" modelAttribute="userForm" class="form-signin" >
        <h2 class="form-signin-heading">Enter new credentials</h2>
        <h4>Username : ${userForm.userName}</h4>

        <spring:bind path="userName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="hidden" path="userName" class="form-control" placeholder="Username"
                            autofocus="true"></form:input>
                <form:errors path="userName"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="emailId">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="emailId" class="form-control" placeholder="Email"></form:input>
                <form:errors path="emailId"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="oldPassword">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="oldPassword" class="form-control" placeholder="Enter current Password"></form:input>
                <form:errors path="oldPassword"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="password" class="form-control"
                            placeholder="Enter new password"></form:input>
                <form:errors path="password"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="passwordConfirm">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="passwordConfirm" class="form-control"
                            placeholder="Confirm your password"></form:input>
                <form:errors path="passwordConfirm"></form:errors>
            </div>
        </spring:bind>

        <button class="pull-right" type="submit">Change</button>
    </form:form>
    </div>
    <div class="col-md-4">

    </div>
</div>

</div>

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>
    function goBack(){
     window.location = "${contextPath}/home#settings";
    }
</script>
</body>
</html>