<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Plot Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/select2.min.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<%@ include file="../header.jsp" %>

<div class="container">

    <form method="post" action="${contextPath}/Reports/chassisLedger?${_csrf.parameterName}=${_csrf.token}"
          class="form-inline ">

        <div class="ledgerReportHeaderFilterSec">
            <h3>Plot Report</h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label>Site :</label>
                    <select name="siteId" class="form-control" id="siteId" onchange="updatePlots(this.value);" style="width: 150px">
                        <option value="0">NONE</option>
                        <c:forEach items="${siteList}" var="site">
                            <c:choose>
                                <c:when test="${site.categoryId == currentSite}">
                                    <option value="${site.categoryId}" selected>${site.title}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${site.categoryId}">${site.title}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <label>Plot Number :</label>
                    <select name="chassisId" class="form-control" id="apartmentId" style="width : 400px" >

                        <option value="0">NONE</option>
                        <c:forEach items="${chassisList}" var="apartment">
                            <c:choose>
                                <c:when test="${apartment.apartmentId == currentChassis}">
                                    <option value="${apartment.apartmentId}" selected>${apartment.apartmentNo}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${apartment.apartmentId}">${apartment.apartmentNo}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="ledgerReportHalfSec" id="ledgerReportDataInfo">
                <div class="ledgerReportUserInfo">
                    <p id="chassisNumber">Plot Number : </p>
                </div>
            </div>

        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button type="submit" style="background-color: #292e5a;float: right">Apply</button>
    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn">Print
            </button>
            <p colspan="2" align="right" valign=bottom><font color="#000000">Print on : <fmt:formatDate type = "date" value = "${currentDate}" /></font></p>
        </div>

    </div>


</div>

<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">


                    <table class="table tableLedger">

                        <thead>
                        <tr>
                            <th>V.No</th>
                            <th>Invoice #</th>
                            <th>V.Date</th>
                            <th>Description</th>
                            <th>Cheque No</th>
                            <th width="20%">Remarks</th>
                            <th>Debit</th>
                            <th>Credit</th>
                            <th>Balance</th>
                        </tr>
                        </thead>
                        <tbody>


                        <c:set var="totalDebit" value="${0}"/>
                        <c:set var="totalCredit" value="${0}"/>
                        <c:forEach items="${vouchers}" var="voucher" varStatus="status">
                            <c:choose>
                                <c:when test="${status.index==0}">
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Opening Balance</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <c:set var="ledgerBalance" value="${voucher.balance + voucher.credit - voucher.debit}"/>

                                        <c:choose>
                                            <c:when test="${ledgerBalance > 0}">
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${ledgerBalance}"/> DR
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${ledgerBalance*-1}"/> CR
                                                </td>
                                            </c:otherwise>
                                        </c:choose>

                                    </tr>
                                </c:when>
                            </c:choose>
                            <tr>
                                <td><span title="Warscape"><a role="button" title="${voucher.voucherNumber}"
                                                              onclick="highlightSpan('${voucher.voucherNumber}')">${voucher.voucherNumber}</a></span>
                                </td>
                                <td>${voucher.invoiceNumber}</td>
                                <td><fmt:formatDate type = "date" value = "${voucher.voucherDate}" /></td>
                                <c:choose>

                                    <c:when test="${voucher.voucherType == 'RV' || voucher.voucherType == 'PV'}">
                                        <td>${accountMap.get(voucher.accountCode)}</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>
                                            <table>
                                                <c:forEach items="${voucher.internalVoucherList}"
                                                           var="internalVoucher">

                                                    <tbody>
                                                    <tr>

                                                        <td>${accountMap.get(internalVoucher.accountCode)}</td>
                                                        <td> ${internalVoucher.itemQuantity} </td>
                                                        <td> ${internalVoucher.itemRate} </td>
                                                        <td> ${internalVoucher.credit}</td>


                                                    </tr>
                                                    </tbody>
                                                </c:forEach>
                                            </table>
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td>${voucher.chequeNumber}</td>
                                <td>${voucher.remarks}</td>
                                <c:choose>
                                    <c:when test="${voucher.voucherType == 'J'}">

                                        <c:choose>

                                            <c:when test="${voucher.credit==0.0}">
                                                <td>0</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${voucher.credit}"/></td>
                                            </c:otherwise>

                                        </c:choose>
                                        <c:choose>
                                            <c:when test="${voucher.debit==0.0}">
                                                <td>0</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${voucher.debit}"/></td>
                                            </c:otherwise>

                                        </c:choose>
                                        <c:set var="totalCredit" value="${totalCredit + voucher.debit}"/>
                                        <c:set var="totalDebit" value="${totalDebit + voucher.credit}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${voucher.debit==0.0}">
                                                <td>0</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${voucher.debit}"/></td>
                                            </c:otherwise>

                                        </c:choose>
                                        <c:choose>

                                            <c:when test="${voucher.credit==0.0}">
                                                <td>0</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${voucher.credit}"/></td>
                                            </c:otherwise>

                                        </c:choose>
                                        <c:set var="totalCredit" value="${totalCredit + voucher.credit}"/>
                                        <c:set var="totalDebit" value="${totalDebit + voucher.debit}"/>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${voucher.balance > 0}">
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${voucher.balance}"/> DR
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${voucher.balance*-1}"/> CR
                                        </td>
                                    </c:otherwise>
                                </c:choose>

                            </tr>
                        </c:forEach>

                        </tbody>
                        <tr>
                            <td><br></td>
                            <td><br></td>
                            <td><br></td>
                            <td>Total:</td>
                            <td><br></td>
                            <td><br></td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${totalDebit}"/></td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${totalCredit}"/></td>
                            <td><br></td>
                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- /container -->

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-datepicker.js"></script>
<script src="${contextPath}/resources/js/select.min.js"></script>


<script>
    $(document).ready(function () {
    $('#apartmentId').select2();
    $('#chassisNumber').html($('#chassisNumber').html().concat($('#apartmentId').find(":selected").text()));
        $('#printBtn').click(function () {
            printDiv();

        });
    })
    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            ' <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + ledgerReportDataInfo.innerHTML + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 50000);

    }
    function goBack(){
     window.location = "${contextPath}/home#reports";
    }
    function updatePlots(categoryId) {
        $.ajax({
            url: '${contextPath}/Apartment/getApartmentList',
            contentType: 'application/json',
            data: {
                categoryId: categoryId
            },
            success: function (response) {
                $("#apartmentId").empty();
                var model = document.getElementById('apartmentId');
                var modelOpt = document.createElement('option');
                modelOpt.innerHTML = 'NONE';
                modelOpt.value = 0;
                model.appendChild(modelOpt);
                Object.keys(response).forEach(function (key) {
                    var modelOpt = document.createElement('option');
                    modelOpt.innerHTML = response[key];
                    modelOpt.value = key;
                    model.appendChild(modelOpt);
                });
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }

        });
    }
</script>

</body>
</html>