<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Detailed Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/select2.min.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<%@ include file="../header.jsp" %>

<div class="container">

    <form method="post" action="${contextPath}/Reports/detailedGeneralReport?${_csrf.parameterName}=${_csrf.token}"
          class="form-inline pull-right">

        <div class="ledgerReportHeaderFilterSec">
            <h3>Detailed Report</h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label> From:</label>
                    <input type="date" name="dateFrom" class="form-control "
                           value="${fromDate}"/>
                    <label> To:</label>
                    <input type="date" name="dateTo" class="form-control "
                           value="${toDate}"/>
                    <label> Account :</label>
                    <select name="accountCode" id="accountCode" class="form-control " style="width: 250px" >

                        <option value="">NONE</option>
                        <c:forEach items="${accounts}" var="account">
                            <c:choose>
                                <c:when test="${account.accountCode == currentAccount}">
                                    <option value="${account.accountCode}" selected>${account.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${account.accountCode}">${account.title}</option>


                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="ledgerReportHalfSec" id="ledgerReportDataInfo">
                <div class="ledgerReportUserInfo">
                    <p>Account Ledger : ${accountMap.get(currentAccount)}</p>
                </div>
                <div class="ledgerReportDateSec">
                    <p>Date From : <fmt:formatDate type = "date" value = "${fromDate}" /> To : <fmt:formatDate type = "date" value = "${toDate}" /></p>
                </div>
            </div>

        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button type="submit" style="background-color: #292e5a;float: right">Apply</button>
    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn">Print
            </button>
            <input type="hidden" value="${accountMap.get(currentAccount)}" id="ledgerName">
            <button class="btn btn-success" style="background-color: #292e5a" id="btnExport" onclick="fnExcelReport('Detailed General Report');">Excel
            </button>
            <p colspan="2" align="right" valign=bottom><font color="#000000">Print on : <fmt:formatDate type = "date" value = "${currentDate}" /></font></p>
        </div>

    </div>


</div>

<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">


                    <table class="table tableLedger" id="dataTable">

                        <thead>
                        <tr>
                            <th>V.No</th>
                            <th>Invoice #</th>
                            <th>V.Date</th>
                            <th>Description</th>
                            <th>Cheque No</th>
                            <th width="20%">Remarks</th>
                            <th>Debit</th>
                            <th>Credit</th>
                            <th>Balance</th>
                        </tr>
                        </thead>
                        <tbody>


                        <c:set var="totalDebit" value="${0}"/>
                        <c:set var="totalCredit" value="${0}"/>
                        <c:forEach items="${vouchers}" var="voucher" varStatus="status">
                            <c:choose>
                                <c:when test="${status.index==0}">
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Opening Balance</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <c:set var="ledgerBalance" value="${voucher.balance + voucher.credit - voucher.debit}"/>

                                        <c:choose>
                                            <c:when test="${ledgerBalance > 0}">
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${ledgerBalance}"/> DR
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${ledgerBalance*-1}"/> CR
                                                </td>
                                            </c:otherwise>
                                        </c:choose>

                                    </tr>
                                </c:when>
                            </c:choose>
                            <tr>
                                <td><span title="Warscape"><a role="button" title="${voucher.voucherNumber}"
                                                              onclick="highlightSpan('${voucher.voucherNumber}')">${voucher.voucherNumber}</a></span>
                                </td>
                                <td>${voucher.invoiceNumber}</td>
                                <td><fmt:formatDate type = "date" value = "${voucher.voucherDate}" /></td>
                                <c:choose>

                                    <c:when test="${voucher.voucherType == 'RV' || voucher.voucherType == 'PV'}">
                                        <td>${accountMap.get(voucher.accountCode)}</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>
                                            <table>
                                                <c:forEach items="${voucher.internalVoucherList}"
                                                           var="internalVoucher">

                                                    <tbody>
                                                    <tr>

                                                        <td>${accountMap.get(internalVoucher.accountCode)}</td>
                                                        <td> ${internalVoucher.itemQuantity} </td>
                                                        <td> ${internalVoucher.itemRate} </td>
                                                        <td> ${internalVoucher.credit}</td>
                                                        <td> ${internalVoucher.chassisNo}</td>

                                                    </tr>
                                                    </tbody>
                                                </c:forEach>
                                            </table>
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td>${voucher.chequeNumber}</td>
                                <td>${voucher.remarks}</td>
                                <c:choose>
                                    <c:when test="${voucher.voucherType == 'J'}">

                                        <c:choose>

                                            <c:when test="${voucher.credit==0.0}">
                                                <td>0</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${voucher.credit}"/></td>
                                            </c:otherwise>

                                        </c:choose>
                                        <c:choose>
                                            <c:when test="${voucher.debit==0.0}">
                                                <td>0</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${voucher.debit}"/></td>
                                            </c:otherwise>

                                        </c:choose>
                                        <c:set var="totalCredit" value="${totalCredit + voucher.debit}"/>
                                        <c:set var="totalDebit" value="${totalDebit + voucher.credit}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${voucher.debit==0.0}">
                                                <td>0</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${voucher.debit}"/></td>
                                            </c:otherwise>

                                        </c:choose>
                                        <c:choose>

                                            <c:when test="${voucher.credit==0.0}">
                                                <td>0</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                      value="${voucher.credit}"/></td>
                                            </c:otherwise>

                                        </c:choose>
                                        <c:set var="totalCredit" value="${totalCredit + voucher.credit}"/>
                                        <c:set var="totalDebit" value="${totalDebit + voucher.debit}"/>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${voucher.balance > 0}">
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${voucher.balance}"/> DR
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${voucher.balance*-1}"/> CR
                                        </td>
                                    </c:otherwise>
                                </c:choose>

                            </tr>
                        </c:forEach>

                        </tbody>
                        <tr>
                            <td><br></td>
                            <td><br></td>
                            <td><br></td>
                            <td>Total:</td>
                            <td><br></td>
                            <td><br></td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${totalDebit}"/></td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${totalCredit}"/></td>
                            <td><br></td>
                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- /container -->

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-datepicker.js"></script>
<script src="${contextPath}/resources/js/select.min.js"></script>
<script src="${contextPath}/resources/js/export-excel.js"></script>
<script>
    function getFirstTwoLetter(vouucherNumber) {
        var firstTwoLetters = spanTitle.substring(0, 2);
        return firstTwoLetters;
    }

    function highlightSpan(spanTitle) {
        var FirstTwoLetters = spanTitle.substring(0, 2);

        if (FirstTwoLetters == 'SL') {
            window.open("${contextPath}/Sales/viewSalesVoucher?id=" + spanTitle);

        } else if (FirstTwoLetters == 'RV') {
            window.open("${contextPath}/Account/createReceiptVoucher?id=" + spanTitle);
        } else if (FirstTwoLetters == 'LP') {
            window.open("${contextPath}/Purchase/viewPurchaseVoucher?id=" + spanTitle);
        } else if (FirstTwoLetters == 'PV') {
            window.open("${contextPath}/Account/createPaymentVoucher?id=" + spanTitle);

        }

    }

    function getParameterByName(name, url) {
        if (!url)
            url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results)
            return null;
        if (!results[2])
            return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $(document).ready(function () {
        $('#accountCode').select2();
        if (getParameterByName('id')) {
            $('html,body').animate({
                    scrollTop: $("#" + getParameterByName('id')).offset().top
                },
                'slow');
        }
        $('#printBtn').click(function () {
            printDiv();

        });


    })

</script>
<script>


    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            ' <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + ledgerReportDataInfo.innerHTML + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 50000);

    }
    function goBack(){
        window.location = "${contextPath}/home#reports";
    }
</script>

</body>
</html>