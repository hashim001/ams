<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Current Stock Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">


</head>

<body>

<%@ include file="../header.jsp" %>

<div class="container">

    <form method="post" action="${contextPath}/Report/currentApartmentsReport"
          class="form-inline" id="form">

        <div class="ledgerReportHeaderFilterSec">
            <h3>Current Stock Report</h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                   <label>Status:</label>
                    <select name="status" class="form-control " id="status">

                        <option value="">NONE</option>
                        <c:forEach items="${statusList}" var="status">
                            <c:choose>
                                <c:when test="${status == currentStatus}">
                                    <option value="${status}" selected>${status}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${status}">${status}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                   <label>Account:</label>
                    <select name="stockId" class="form-control " id="stock">

                        <option value="0">NONE</option>
                        <c:forEach items="${stocks}" var="stock">
                            <c:choose>
                                <c:when test="${stock.stockId == currentStock}">
                                    <option value="${stock.stockId}" selected>${stock.stockAccount.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${stock.stockId}">${stock.stockAccount.title}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                     <button type="submit">Apply</button>
                </div>
            </div>

            <div class="ledgerReportHalfSec" id="ledgerReportDataInfo">
                <div class="ledgerReportUserInfo">
                    <p>Apartment : ${accountMap.get(currentAccount)}</p>
                </div>
            </div>

        </div>
    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn">Print
            </button>
            <p colspan="2" align="right" valign=bottom><font color="#000000">Print on : ${currentDate}</font></p>
        </div>

    </div>


</div>

<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">


                    <table class="table tableLedger">

                        <thead>
                        <tr>
                            <th>S No.</th>
                            <th>Apartment Number</th>
                            <th>Customer</th>
                            <th>Type</th>
                            <td>Amount</td>
                            <td>Status</td>
                        </tr>
                        </thead>
                        <tbody>
                         <c:set var="total" value="${0}"/>
                          <c:set var="sno" value="${1}"/>
                        <c:forEach items="${apartments}" var="apartment" varStatus="status">
                            <tr>
                                <td>${sno}</td>
                                <td>${apartment.apartmentNo}</td>
                                <c:choose>
                                <c:when test="${apartment.customerAccount>0}">
                                <td>${accountMap.get(apartment.customerAccount)}</td>
                                </c:when>
                                <c:otherwise>
                                <td>In Stock</td>
                                </c:otherwise>
                                </c:choose>
                                <td>${apartment.stock.name}</td>
                                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${apartment.sellingPrice}"/></td>
                                <c:choose>
                                    <c:when test="${apartment.hold == 'true'}">
                                        <td>ON HOLD</td>
                                    </c:when>
                                    <c:when test="${apartment.customerAccount>0}">
                                        <td>SOLD</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>AVAILABLE</td>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="total" value="${total+apartment.sellingPrice}"/>
                             <c:set var="sno" value="${sno+1}"/>
                            </tr>
                            </c:forEach>
                                     <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${total}"/></td>
                                        <td></td>
                                    </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- /container -->

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function () {
        $('#printBtn').click(function () {
            printDiv();
        });
    })

        $('#category').change(function () {
            $('#account').val(0);
            $('#form').submit();
        });

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '  <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + ledgerReportDataInfo.innerHTML + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 50000);

    }

    function goBack(){
     window.location = "${contextPath}/home#reports";
    }
</script>

</body>
</html>