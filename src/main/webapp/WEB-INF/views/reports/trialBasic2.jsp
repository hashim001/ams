<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trial Balance Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<div class="container">
    <h2>Trial Balance</h2>

    <form method="post" action="${contextPath}/Reports/trialBalanceBasic?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-right">

        <label> Level :</label>
        <select name="level" class="form-control" onchange="this.form.submit()">

            <c:forEach items="${levels}" var="level">
                <c:choose>
                    <c:when test="${level == currentLevel}">
                        <option value="${level}" selected>Level ${level}</option>

                    </c:when>
                    <c:otherwise>
                        <option value="${level}" >Level ${level}</option>

                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <label> To Date :</label>
        <input type="date"  name="dateTo"   class="form-control dateField" value="${toDate}"  onchange="this.form.submit()" />
        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>


    </form>
    <button class="btn btn-success" id = "printBtn" onclick="printDiv()">Print</button>

</div>
<div class="container" id = "printDiv">
    <h3>Trial Balance</h3>
    <table cellspacing="0" border="0">
        <colgroup span="13" width="64"></colgroup>
        <tr>
            <td colspan=13 height="20" align="center" valign=bottom><font color="#000000">Trial Balance - Datewise</font></td>
        </tr>
        <tr>
            <td colspan=13 height="20" align="center" valign=bottom><font color="#000000">Date To : ${toDate}</font>
            </td>
        </tr>
        <tr>
            <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
        </tr>
        <tr>
            <td height="20" align="left" valign=bottom><font color="#000000">Print on :</font></td>
            <td colspan="2" align="left" valign=bottom><font color="#000000">${currentDate}</font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000">Page : a/zz</font></td>
        </tr>
        <tr>
            <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
        </tr>
        <tr>

            <td style="border-bottom: 1px solid #000000" colspan=9 align="left" valign=bottom><font color="#000000">Account Title</font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="right" valign=bottom><font color="#000000">Debit</font></td>
            <td align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="right" valign=bottom><font color="#000000">Credit</font></td>
        </tr>



            <tr>
                <td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
            </tr>
        <c:set var="totalDebit" value="${0}"/>
        <c:set var="totalCredit" value="${0}"/>
        <c:forEach items="${trailModel}" var="trail">
            <c:set var="totalCredit" value="${totalCredit + trail.credit}" />
            <c:set var="totalDebit" value="${totalDebit + trail.debit}" />
            <tr>

                <td colspan="8" align="left" valign=bottom><font color="#000000">${trail.accountTitle}</font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <c:choose>
                    <c:when test="${trail.debit != 0.0}">
                        <td align="right" valign=bottom><font color="#000000"><fmt:formatNumber type="number" maxFractionDigits="2" value="${trail.debit}"/></font></td>
                    </c:when>
                    <c:otherwise>
                        <td align="right" valign=bottom><font color="#000000"></font></td>
                    </c:otherwise>
                </c:choose>
                <td align="left" valign=bottom><font color="#000000"><br></font></td>
                <c:choose>
                    <c:when test="${trail.credit != 0.0 }">
                        <td align="right" valign=bottom><font color="#000000"><fmt:formatNumber type="number" maxFractionDigits="2" value="${-1 * trail.credit}"/></font></td>
                    </c:when>
                    <c:otherwise>
                        <td align="right" valign=bottom><font color="#000000"></font></td>
                    </c:otherwise>
                </c:choose>

            </tr>
        </c:forEach>
            <tr>
                <td style="border-bottom: 1px solid #000000" height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
                <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            </tr>
        <tr>
            <td style="border-bottom: 1px solid #000000" height="20" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000">Total:</font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="right" valign=bottom><font color="#000000">
                <fmt:formatNumber type="number" maxFractionDigits="2" value="${totalDebit}"/>
            </font></td>
            <td style="border-bottom: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
            <td style="border-bottom: 1px solid #000000" align="right" valign=bottom><font color="#000000">
                <fmt:formatNumber type="number" maxFractionDigits="2" value="${-1*totalCredit}"/>
            </font></td>
        </tr>

    </table>

</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function() {

    });

    function printDiv()
    {

        var divToPrint=document.getElementById('printDiv');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '\t\n' +
            '</head>\n<body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

    }
</script>
</body>
</html>