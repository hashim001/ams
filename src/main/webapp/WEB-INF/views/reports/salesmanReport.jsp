<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Salesman Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<%@ include file="../header.jsp" %>

<div class="container">

    <form method="post" action="${contextPath}/Salesman/salesmanReport?${_csrf.parameterName}=${_csrf.token}"
          class="form-inline pull-right">

        <div class="ledgerReportHeaderFilterSec">
            <h3>Salesman Report</h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label> Select Salesman :</label>
                    <select name="salesmanId" style="width:300px" class="form-control " onchange="this.form.submit()">

                        <option value="">NONE</option>
                        <c:forEach items="${salesmanList}" var="salesman">
                            <c:choose>
                                <c:when test="${salesman.distributorId == currentSalesman}">
                                    <option value="${salesman.distributorId}" selected>${salesman.name}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${salesman.distributorId}">${salesman.name}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                   <label> Cost Centre :</label>
                    <select name="costCentreCode" style="width:250px"  class="form-control " onchange="this.form.submit()">

                        <option value="">NONE</option>
                        <c:forEach items="${costCentres}" var="costCentre">
                            <c:choose>
                                <c:when test="${costCentre.accountCode == currentCostCentre}">
                                    <option value="${costCentre.accountCode}" selected>${costCentre.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${costCentre.accountCode}">${costCentre.title}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="ledgerReportHalfSec" id="ledgerReportDataInfo">
                <div class="ledgerReportUserInfo">
                    <p>Salesman Ledger</p>
                </div>
            </div>

        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn">Print
            </button>
        </div>

    </div>


</div>

<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">


                    <table class="table tableLedger">
                        <thead>
                        <tr>
                            <th>Salesman</th>
                            <th>Date</th>
                            <th>Days</th>
                            <th>Customer</th>
                            <th>Plot Number</th>
                            <th>Model No</th>
                            <th>Commission Amount</th>
                            <th>Balance</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${distributors}" var="distributor" varStatus="status">
                         <c:set var="totalBalance" value="${0}"/>
                         <tr>
                        <td>${distributor.name}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                       <td></td>
                        </tr>
                        <c:forEach items="${distributor.apartmentList}" var="apartment">
                        <c:choose>
                        <c:when test="${apartment.sellingPrice - apartment.amountPaid != 0}">
                         <tr>
                         <td></td>
                         <td><fmt:formatDate type = "date" value = "${apartment.date}"/></td>
                         <td>${apartment.days}</td>
                         <td>${accountMap.get(apartment.customerAccount)}</td>
                         <td>${apartment.apartmentNo}</td>
                         <td>${apartment.stock.name}</td>
                         <td>${apartment.commissionAmount}</td>
                         <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${apartment.sellingPrice - apartment.amountPaid}"/></td>
                         </tr>
                         <c:set var="totalBalance" value="${totalBalance + apartment.sellingPrice - apartment.amountPaid}"/>
                        </c:when>
                        </c:choose>
                        </c:forEach>
                         <tr>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${totalBalance}"/></td>
                         </tr>
                         </c:forEach>
                       </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- /container -->

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-datepicker.js"></script>
<script src="${contextPath}/resources/js/jquery-3.2.1.min.js"></script>
<script>

    $(document).ready(function () {
        $('#printBtn').click(function () {
            printDiv();

        });

    })
</script>
<script>

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '  <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + ledgerReportDataInfo.innerHTML + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 50000);

    }
    function goBack(){
     window.location = "${contextPath}/home";
    }
</script>

</body>
</html>