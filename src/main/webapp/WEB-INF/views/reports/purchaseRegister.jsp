<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Purchase Register</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<%@ include file="../header.jsp" %>

<div class="container">

    <form method="post" action="${contextPath}/Reports/purchaseRegister/?${_csrf.parameterName}=${_csrf.token}"
          class="form-inline pull-right">

        <div class="ledgerReportHeaderFilterSec">
            <h3>Purchase Register</h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label> From Date :</label>
                    <input type="date" name="dateFrom" class="form-control "
                           value="${fromDate}"/>
                    <label> To Date :</label>
                    <input type="date" name="dateTo" class="form-control "
                           value="${toDate}"/>
                    <label> Account :</label>
                    <select name="accountCode" class="form-control " >

                        <option value="">NONE</option>
                        <c:forEach items="${accounts}" var="account">
                            <c:choose>
                                <c:when test="${account.accountCode == currentAccount}">
                                    <option value="${account.accountCode}" selected>${account.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${account.accountCode}">${account.title}</option>


                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <label> Cost Centre :</label>
                    <select name="costCentreCode" class="form-control ">

                        <option value="">NONE</option>
                        <c:forEach items="${costCentres}" var="costCentre">
                            <c:choose>
                                <c:when test="${costCentre.accountCode == currentCostCentre}">
                                    <option value="${costCentre.accountCode}" selected>${costCentre.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${costCentre.accountCode}">${costCentre.title}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="ledgerReportHalfSec" id="ledgerReportDataInfo">
                <div class="ledgerReportUserInfo">
                    <p>Account : ${accountMap.get(currentAccount)}</p>
                </div>
                <div class="ledgerReportDateSec">
                    <p>Date From : <fmt:formatDate type = "date" value = "${fromDate}" /> To : <fmt:formatDate type = "date" value = "${toDate}" /></p>
                </div>
            </div>

        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button type="submit" style="background-color: #292e5a;float: right">Apply</button>
    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn">Print
            </button>
            <p colspan="2" align="right" valign=bottom><font color="#000000">Print on : <fmt:formatDate type = "date" value = "${currentDate}" /></font></p>
        </div>

    </div>


</div>

<section class="main">
    <div class="container" id="printDiv">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">


                    <table class="table tableLedger">

                        <thead>
                        <tr>
                            <th>Order No</th>
                            <th>Order Date</th>
                            <th>Description</th>
                            <th>Vendor</th>
                            <th>Sub Total</th>
                            <th>GST</th>
                            <td>Grand Total</th>
                        </tr>
                        </thead>
                        <tbody>

                        <c:forEach items="${purchaseOrderModel}" var="order" varStatus="status">
                        <c:set var="totalAmount" value="${0}"/>
                        <c:set var="totalTax" value="${0}"/>
                        <tr>
                        <td>${order.orderNumber}</td>
                        <td>${order.orderDate}</td>
                        <td>
                        <c:forEach items="${order.orderProductList}" var="product" varStatus="productStatus">
                        ${product.itemAccount.title}
                        ${product.initialQuantity - product.quantity}
                        ${product.rate}
                        <fmt:formatNumber type="number" maxFractionDigits="2"
                               value="${product.rate * (product.initialQuantity - product.quantity)}"/>
                         </br>
                        <c:set var="totalAmount"
                             value="${totalAmount + (product.rate * (product.initialQuantity - product.quantity))}"/>
                        <c:set var="totalTax"
                             value="${totalTax + ((product.taxRate / 100) * product.rate * (product.initialQuantity - product.quantity))}"/>
                        </c:forEach>
                        </td>
                        <td>${accountMap.get(order.supplierCode)}</td>
                            <td style="font-weight: bold"><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                            value="${totalAmount}"/></td>
                            <td style="font-weight: bold"><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                            value="${totalTax}"/></td>
                            <td style="font-weight: bold"><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                            value="${totalAmount + totalTax}"/></td>
                        </tr>
                        </c:forEach>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- /container -->

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-datepicker.js"></script>
<script>

        $('#printBtn').click(function () {
            printDiv();
    })

</script>
<script>


    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            ' <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + ledgerReportDataInfo.innerHTML + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 50000);

    }
    function goBack(){
     window.location = "${contextPath}/home#register";
    }
</script>

</body>
</html>