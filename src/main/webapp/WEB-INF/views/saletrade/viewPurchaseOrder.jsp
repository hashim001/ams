<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Orders</title>
</head>

<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">

        <h2 class="heading-main">Purchase Orders (Sale)<a href="${contextPath}/SaleTrading/addPurchaseOrder"><span
                class="addIcon"
                data-toggle="modal"
                data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></a></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th width="15%">O-Number</th>
                            <th width="5%">Final</th>
                            <th width="15%">Remarks</th>
                            <th width="12%">O-Date</th>
                            <th width="12%">D-Date</th>
                            <th width="15%">Customer</th>
                            <th>Status</th>
                            <th width="10%">P-Status</th>
                            <th width="12%">Action</th>

                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    function closeOrder(id) {
        if (confirm("Are you sure, you want to close the Order?")) {
            $('#closeOrder' + id).submit();
        }
    }

    $(document).ready(function () {
        $('#dataTable').DataTable({
            searchDelay: 900,
            "processing": true,
            "serverSide": true,
            "order": [[2, "desc"]],
            "pageLength": 7,
            "ajax": "${contextPath}/SalePO/json"
        });
    });
    function refreshPage() {
        window.location.reload();
    }

    function goBack(){
     window.location = "${contextPath}/home#tradingSale";
    }




</script>
</body>
</html>