<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bill Invoice - ${purchaseOrder.remarks}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style>
        body {
            font-family: "Open Sans";
        }

        table.mainTable {
            width: 100%;
            max-width: 800px;
            margin: 0 auto;
        }

        .tableHeaderSec thead td {
            vertical-align: top
        }

        .tableHeaderSec thead h4 {
            margin: 0;
            padding: 0;
            display: block;
        }

        .tableHeaderSec thead h4 span {
            margin: 0;
            padding: 5px 0;
            display: block;
            font-weight: normal;
            font-size: 14px;
        }

        .tableHeaderSec thead .logoArea {
            margin-top: 10px;
        }

        .tableHeaderSec thead .logoArea img {
            max-width: 100%;
        }

        .deliveryArea {
            text-align: right;
        }

        .deliveryArea h4 {
            color: #004269;
            margin-bottom: 15px;
        }

        .deliveryArea table {
            float: right;
            border-spacing: 0;
        }

        .deliveryArea table td {
            font-size: 13px;
        }

        .deliveryArea table td:first-child {
            text-align: right;
            padding-right: 15px;
        }

        .deliveryArea table td:last-child {
            border: 1px solid #ddd;
            padding: 2px 10px;
            text-align: left
        }

        .orderDetailArea {
            border-spacing: 0
        }

        .orderDetailArea > td {
            padding-top: 15px;
        }

        .orderDetailArea h6 {
            margin: 0;
            padding: 0;
            display: block;
            background: #004269;
            padding: 5px 10px;
            color: #fff;
            font-size: 13px;
        }

        .orderDetailArea table {
            width: 100%;
            border-spacing: 0
        }

        .orderDetailArea table td {
            font-size: 13px;
            padding: 3px 10px;
            border: 1px solid #004269;
            border-top: 0
        }

        table.mainTable.productDescTableSec {
            margin-top: 15px;
            border-spacing: 0
        }

        .productDescTableSec th {
            background: #004269;
            padding: 5px 10px;
            color: #fff;
            font-size: 13px;
            text-align: left
        }

        .productDescTableSec tr:nth-child(even) {
            background: #ececec;
        }

        .productDescTableSec tr td {
            padding: 5px 10px;
            font-size: 12px;
            border: 1px solid #004269;
            border-top: 0;
            border-right: 0;
        }

        .productDescTableSec tr td:last-child {
            border-right: 1px solid #004269;
        }

        .productDescTableSec tr.tfoot td:first-child {
            border-bottom: 0;
            border-left: 0;
            background: #fff;
        }

        .productDescTableSec tr.tfoot td:nth-child(2) {
            text-transform: uppercase
        }

        .footerDetailSec {
            font-size: 13px;
            text-align: center;
        }

        .footerDetailSec p {
            margin: 1px 0;
        }

        .footerDetailSec h2 {
            margin: 15px 0 0 0;
        }

        .mainTable.subTotalTableSec {
            width: 340px;
            float: right;
            border-spacing: 0;
        }

        .mainTable.subTotalTableSec td {
            padding: 5px 10px;
            font-size: 12px;
            border: 1px solid #004269;
            border-top: 0;
            border-right: 0;
        }

        .mainTable.subTotalTableSec td:last-child {
            border-right: 1px solid #004269;
        }

        .mainTable.subTotalTableSec tr:nth-child(even) {
            background: #ececec;
        }

        @media print {
            body {
                -webkit-print-color-adjust: exact !important;
            }
        }

    </style>
</head>
<body>
<table style="width: 100%">
    <tbody>

    <tr>
        <td colspan="10">
            <table class="tableHeaderSec mainTable">
                <thead>
                <tr>
                    <td width="50%">
                        <div class="logoArea">
                            <img src="${staticInfo.imageUrl}" alt="img">
                        </div>
                        <h4 style="font-family: 'Lucida Handwriting';">
                            <span>${staticInfo.companyAddress}</span>
                            <span>${staticInfo.addressSecondary}</span>
                            <span>Phone : ${staticInfo.phone}</span>
                            <span>Mob: ${staticInfo.mobile}</span>

                        </h4>

                    </td>
                    <td class="deliveryArea" width="50%">
                        <h2>Sale Invoice</h2>
                        <table>
                            <tr>
                                <td style="font-size: 18px">Date</td>
                                <td style="font-size: 18px;white-space: nowrap;">${purchaseOrder.orderBill.billDate}</td>
                            </tr>
                            <tr>
                                <td style="font-size: 18px">Invoice #</td>
                                <td style="font-size: 18px; white-space: nowrap;">${purchaseOrder.orderBill.billNumber}</td>
                            </tr>
                            <tr>
                                <td style="font-size: 18px">NTN #</td>
                                <td style="font-size: 18px; white-space: nowrap;">${staticInfo.nationalTaxNumber}</td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="2">
                        <h3>Messer: <span
                                style="alignment: center"><u>${purchaseOrder.customerAccount.title}</u></span></h3>
                        <div><strong style="float: left">DC: ${dcList}  </strong> <span class="pull-right"
                                                                                        style="float: right"><strong>Purchase Order: ${purchaseOrder.orderNumber}</strong></span>
                        </div>

                    </td>
                </tr>
                </thead>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="10">
            <table class="mainTable productDescTableSec">
                <thead>
                <tr>
                    <th width="10%">Item#</th>
                    <th>Description</th>
                    <th width="10%">Quantity</th>
                    <th width="15%">Unit Price</th>
                    <th width="15%">Total</th>
                </tr>
                </thead>
                <tbody>
                <c:set var="totalAmount" value="${0}"/>
                <c:set var="totalTax" value="${0}"/>
                <c:set var="loopCount" value="${0}"></c:set>
                <c:forEach items="${purchaseOrder.orderProductList}" var="product" varStatus="productStatus">
                    <c:choose>
                        <c:when test="${(product.initialQuantity - product.quantity) > 0}">
                            <tr>
                                <td>${productStatus.index + 1}</td>
                                <td>${product.itemAccount.title}</td>
                                <td>${product.initialQuantity - product.quantity}</td>
                                <td>${product.rate}</td>
                                <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                      value="${product.rate * (product.initialQuantity - product.quantity)}"/></td>
                                <c:set var="totalAmount"
                                       value="${totalAmount + (product.rate * (product.initialQuantity - product.quantity))}"/>
                                <c:set var="totalTax"
                                       value="${totalTax + ((product.taxRate / 100) * product.rate * (product.initialQuantity - product.quantity))}"/>
                                <c:set var="loopCount" value="${loopCount + 1}"></c:set>
                            </tr>
                        </c:when>
                    </c:choose>
                </c:forEach>
                <c:forEach begin="${loopCount}" end="11">
                    <tr style="height: 26.55px">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </c:forEach>

                <tr class="tfoot">
                    <td colspan="3"></td>
                    <td><strong>subtotal</strong></td>
                    <td><strong>PKR <fmt:formatNumber type="number" maxFractionDigits="2"
                                                      value="${totalAmount}"/></strong></td>
                </tr>
                <tr class="tfoot">
                    <td colspan="3"></td>
                    <td><strong>GST</strong></td>
                    <td><strong>PKR <fmt:formatNumber type="number" maxFractionDigits="2" value="${totalTax}"/></strong>
                    </td>
                </tr>
                <tr class="tfoot">
                    <td colspan="3"></td>
                    <td><strong>Grand Total</strong></td>
                    <td><strong>PKR <fmt:formatNumber type="number" maxFractionDigits="2"
                                                      value="${totalAmount + totalTax}"/></strong></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="10">
            <table class="mainTable footerDetailSec">
                <tr>
                <tr>
                    <td>

                        <h2>Thanks you for your business!</h2>
                        <p>Should you have any enquiries concerning this delivery note, feel free to contact.</p>
                        <hr>
                        <p>${staticInfo.companyAddress},${staticInfo.addressSecondary}</p>
                        <p>Mob: ${staticInfo.mobile} Email: ${company-email} Web: ${company-website}</p>
                    </td>
                </tr>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>


<p style="page-break-after: always;">&nbsp;</p>
<p style="page-break-before: always;">&nbsp;</p>
<%--For GST invoice --%>
<c:choose>
    <c:when test="${staticInfo.salesTaxNumber!=null&&staticInfo.salesTaxNumber!=''}">
        <table class="mainTable tableHeaderSec">
            <tbody>
            <tr>
                <h2 style="text-align: center"><u>Sale Tax Invoice</u></h2>
                <td width="50%">

                </td>
                <td class="deliveryArea" width="50%">

                    <table>
                        <tr>
                            <td style="font-size: 18px">Date</td>
                            <td style="font-size: 18px">${purchaseOrder.orderBill.billDate}</td>
                        </tr>

                        <tr>
                            <td style="font-size: 18px">Invoice #</td>
                            <td style="font-size: 18px">${purchaseOrder.orderBill.billNumber}</td>
                        </tr>


                    </table>
                </td>
            </tr>
            <tr class="orderDetailArea">
                <td width="50%">
                    <h6>Buyer Details</h6>
                    <table style="text-align: center">
                        <tr style="height: 26.55px">
                            <td>
                                Buyer's Name: ${purchaseOrder.customerAccount.title}
                            </td>
                        </tr>
                        <tr style="height: 26.55px">

                            <td>
                                Buyer's Address: ${purchaseOrder.customerAccount.stockCustomer.address}
                            </td>
                        </tr>
                        <tr style="height: 26.55px">

                            <td>
                                Buyer's Telephone #:${purchaseOrder.customerAccount.stockCustomer.landline}
                            </td>
                        </tr>
                        <tr style="height: 26.55px">

                            <td>
                                Buyer's STRN #: ${purchaseOrder.customerAccount.stockCustomer.saletaxNumber}
                            </td>
                        </tr>
                        <tr style="height: 26.55px">

                            <td>
                                Buyer's NTN #: ${purchaseOrder.customerAccount.stockCustomer.nationalTaxNumber}
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <h6>Supplier Details</h6>
                    <table style="text-align: center">
                        <tr style="height: 26.55px">
                            <td>
                                Supplier's Name: ${staticInfo.companyName}
                            </td>
                        </tr>
                        <tr style="height: 26.55px">

                            <td>
                                Supplier's Address: ${staticInfo.companyAddress},${staticInfo.addressSecondary}
                            </td>
                        </tr>
                        <tr style="height: 26.55px">

                            <td>
                                Supplier's Telephone # : ${staticInfo.phone}
                            </td>
                        </tr>
                        <tr style="height: 26.55px">

                            <td>
                                Supplier's STRN #: ${staticInfo.salesTaxNumber}
                            </td>
                        </tr>
                        <tr style="height: 26.55px">

                            <td>
                                Supplier's NTN #: ${staticInfo.nationalTaxNumber}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <table class="mainTable">
            <tr>
                <td>
                    <table class="mainTable productDescTableSec">
                        <thead>
                        <tr>
                            <th width="10%">Item#</th>
                            <th>Description</th>
                            <th width="10%">Quantity</th>
                            <th width="15%">Unit Price</th>
                            <th width="15%">Exclusive Tax</th>
                            <th>Tax Rate</th>
                            <th>Tax</th>
                            <th>Inclusive Tax</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="totalAmount" value="${0}"/>
                        <c:set var="totalTax" value="${0}"/>
                        <c:set var="loopCount" value="${0}"></c:set>
                        <c:forEach items="${purchaseOrder.orderProductList}" var="product" varStatus="productStatus">
                            <c:choose>
                                <c:when test="${(product.initialQuantity - product.quantity) > 0}">
                                    <tr style="height: 26.55px">
                                        <td>${productStatus.index + 1}</td>
                                        <td>${product.itemAccount.title}</td>
                                        <td>${product.initialQuantity - product.quantity}</td>
                                        <td>${product.rate}</td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${product.rate * (product.initialQuantity - product.quantity)}"/></td>
                                        <td>${product.taxRate}%</td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${((product.taxRate / 100) * product.rate * (product.initialQuantity - product.quantity))}"/></td>
                                        <td><fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${((product.taxRate / 100) * product.rate * (product.initialQuantity - product.quantity)) + (product.rate * (product.initialQuantity - product.quantity))}"/></td>
                                        <c:set var="totalAmount"
                                               value="${totalAmount + (product.rate * (product.initialQuantity - product.quantity))}"/>
                                        <c:set var="totalTax"
                                               value="${totalTax + ((product.taxRate / 100) * product.rate * (product.initialQuantity - product.quantity))}"/>
                                        <c:set var="loopCount" value="${loopCount + 1}"></c:set>
                                    </tr>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <c:forEach begin="${loopCount}" end="11">
                            <tr style="height: 26.55px">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="font-weight: bold">TOTAL:</td>
                            <td style="font-weight: bold"><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                            value="${totalAmount}"/></td>
                            <td></td>
                            <td style="font-weight: bold"><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                            value="${totalTax}"/></td>
                            <td style="font-weight: bold"><fmt:formatNumber type="number" maxFractionDigits="2"
                                                                            value="${totalAmount + totalTax}"/></td>
                        </tr>
                        </tbody>
                        <tfoot>


                        </tfoot>
                    </table>
                </td>
            </tr>
        </table>

    </c:when>
</c:choose>
<table class="mainTable footerDetailSec">
</table>
</body>
</html>
