<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create Voucher</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">
    ${voucherAddSuccess}


    <h2 class="form-signin-heading">Recipe Calculator</h2>
     <form:form method="POST" modelAttribute="recipeForm" id="formid" Class="form-inline formSaleVoucher formWidthDef">
    <div class="container">
        <form class="form-inline formSaleVoucher" style="margin-bottom: 15px; ">
            <div class="pull-left">
                <div class="form-Row">
                    <spring:bind path="recipeNumber">
                        <form:label path="recipeNumber">Recipe#</form:label>
                        <form:input type="text" readonly="true" cssStyle="width: 100px" path="recipeNumber"
                                    cssClass="form-control" value="${recipeNumber}"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
            </div>
                <label>Stock:</label>
                <form:select style="width: 200px"  path="stockId" name="account" class="form-control" id="supplierAccount">
                 <form:option value="0">NIL</form:option>
                    <c:forEach items="${stocks}" var="stock">
                        <form:option value="${stock.stockId}">${stock.stockAccount.title}
                        </form:option>
                    </c:forEach>
                </form:select>
                    <spring:bind path="turnoutTime">
                        <form:label path="turnoutTime">Turnout Time</form:label>
                        <form:input type="number"  cssStyle="width: 100px" path="turnoutTime"
                                    cssClass="form-control" value="0.0"
                                    autofocus="true"></form:input>
                    </spring:bind>

    </div>

        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>

                    <th>Raw Material</th>
                    <th>Measure Unit</th>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Amount</th>


                </tr>
                </thead>
                <tbody>
                <c:forEach items="${recipeForm.recipeMaterialList}" varStatus="productStatus">

                    <tr>
                        <td>
                            <form:select path="recipeMaterialList[${productStatus.index}].transitionCode"
                                       class="form-control rawMaterial" id ="stockAccountCode${productStatus.index}" onchange="getMaterialValues(this.value,${productStatus.index});">
                                <form:option value="0">NIL</form:option>
                                <c:forEach items="${rawMaterials}" var="rm">
                                     <form:option value="${rm.accountCode}-${rm.rawMaterialId}">${rm.name}</form:option>
                                </c:forEach>
                            </form:select>
                        </td>
                        <td>
                            <form:input type="text" id="measureUnit${productStatus.index}"
                                        path="recipeMaterialList[${productStatus.index}].measureUnit"
                                        class="form-control quantity" readonly="true"
                                        autofocus="true"></form:input>
                        </td>
                        <td>
                            <form:input type="text" id="innerQuantity${productStatus.index}"
                                        path="recipeMaterialList[${productStatus.index}].quantity" class="form-control quantity"
                                        onblur="calculateAmount(${productStatus.index})"
                                        autofocus="true" value="0.00"></form:input>
                        </td>
                        <td>
                            <form:input type="text" id="innerRate${productStatus.index}"
                                        path="recipeMaterialList[${productStatus.index}].rate" class="form-control rate"
                                        onblur="calculateAmount(${productStatus.index})"
                                        autofocus="true" value="0.00"></form:input>
                        </td>
                        <td>
                            <form:input type="text" id="innerAmount${productStatus.index}"
                                        path="recipeMaterialList[${productStatus.index}].amount" class="form-control amountTotal"
                                        onblur="calculateQuantity(${productStatus.index})"
                                        autofocus="true" value="0.00"></form:input>
                        </td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">

        <div class="totalBillSec">
            <p>Total Rs <span>${total}</span></p>
        </div>
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>
    var selectedAccountList = [];
    $(document).ready(function () {

        $('#supplierAccount').change(function () {
            $('.supplierAccountNumber').val($('#supplierAccount').val());

        });


        $(".amountTotal").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });

        $(".quantity").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });


        $(".rate").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });

        function calculateAmountTotal() {
            var netTotal = 0;
            $(".amountTotal").each(function (index, ele) {
                netTotal += parseFloat($(ele).val());
            });
            console.log("total", netTotal);
            return netTotal.toFixed(2);
        }


    });

    // get rate of specific item
    function getMaterialValues(accountCode, id) {
       getMeasureUnit(id);
        /*****MakeUniqueDropdown******/
        makeUniqueDropdown(accountCode, id);

    }
    function makeUniqueDropdown(accountCode, id){
    $(".rawMaterial").each(function(index,iElement){
                var iteratedVal = $(iElement).val();
                var matchedAccountIndex = selectedAccountList.findIndex(function(iteratedAccountList){
                       return iteratedAccountList.rowid==id
                });

                //Add values in the selectedAccountList array
                if(iteratedVal!="0"){
                    if(matchedAccountIndex!=-1){
                        selectedAccountList.splice(matchedAccountIndex,1);
                        selectedAccountList.push({"rowid":id, "val":accountCode});
                    }
                    else{
                        selectedAccountList.push({"rowid":id, "val":accountCode});
                    }
                }else{
                    selectedAccountList.splice(matchedAccountIndex,1);
                    selectedAccountList.push({"rowid":id, "val":accountCode});
                }

                //UpdateDropdown Option Value

                var val = $(iElement).val(),rowid=index;

                //OptionElements Loop
                $(iElement).find("option").each(function(optionIndex,optionElement){

                       //selectedAccountList Loop
                       $.each(selectedAccountList,function(accountIndex,accountRow){
                            var optionVal = $(optionElement).attr("value"),
                                filteredMatchedOption = selectedAccountList.filter(function(iteratedAccountList){
                                      return iteratedAccountList.val==optionVal;
                                });
                            if(filteredMatchedOption.length>0){
                                 if(rowid!=filteredMatchedOption[0].rowid && filteredMatchedOption[0].val!="0"){
                                    $(optionElement).addClass("hide");
                                 }
                            }
                            else{
                                 $(optionElement).removeClass("hide");
                            }
                       });

                });

            });
    }
    function getMeasureUnit(id) {
        $.ajax({
            // Request to Stock Controller
            url: '${contextPath}/Stock/ajax/getProductInfo',
            contentType: 'application/json',
            data: {
                rawId: $('#stockAccountCode' + id).val()

            },

            success: function (response) {
                $('#measureUnit' + id).val(response[0]);
                 $('#innerRate' + id).val(response[1]);
             $('#innerRate' + id).trigger('blur');
            }
        });

    }
    function calculateAmount(id) {
        var amount = $('#innerRate' + id).val() * $('#innerQuantity' + id).val();
        $('#innerAmount' + id).val(amount.toFixed(2));
    }

    function calculateQuantity(id) {
        var quantity = $('#innerAmount' + id).val() / $('#innerRate' + id).val();
        $('#innerQuantity' + id).val(quantity.toFixed(2));

    }
    function goBack(){
     window.location = "${contextPath}/Recipe/recipeListing";
    }

</script>
</body>
</html>
