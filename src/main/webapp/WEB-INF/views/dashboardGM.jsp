<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/dashboard.css">
    <script src="${contextPath}/resources/js/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
    <title>Dashboard</title>
</head>
<body>

<section class="dashboardHeaderSec">
    <section class="logoSec">
        <c:choose>
            <c:when test="${imageUrl==''}">
                <a href="${contextPath}/home"><img src="${contextPath}/resources/img/logo.png" alt="logo"></a>
            </c:when>
            <c:otherwise>
                <a href="${contextPath}/home"><img src="${imageUrl}" alt="logo"></a>
            </c:otherwise>
        </c:choose>
    </section>
    ${paymentResponse}
</section>


<section class="dashboradLeftMenuContent">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <sec:authorize access="hasRole('ROLE_GM')">
            <li role="presentation"><a href="#home" aria-controls="profile" role="tab"
                                       data-toggle="tab">Home</a></li>

            </li>
           <li role="presentation"><a href="#rawMaterialLedger" aria-controls="profile" role="tab"
                               data-toggle="tab">Raw Material Ledger</a>
            </li>
            <li role="presentation"><a href="#challanFile" aria-controls="profile" role="tab"
                                       data-toggle="tab">Challan</a>

            </li>

            <li role="presentation"><a href="#purchaseRequest" aria-controls="profile" role="tab"
                                       data-toggle="tab">Purchase Request</a>

            </li>

            <li role="presentation"><a href="#store" aria-controls="profile" role="tab"
                                       data-toggle="tab">Inventory Control</a>

            </li>

            <li role="presentation"><a href="#settings" aria-controls="profile" role="tab"
                                       data-toggle="tab">Settings</a>

            </li>

            <li role="presentation"><a href="#" onclick="logout()" aria-controls="profile" role="tab" data-toggle="tab">Logout</a>
            </li>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_STORE')">
            <li role="presentation"><a href="#home" aria-controls="profile" role="tab"
                                       data-toggle="tab">Home</a></li>

            </li>
           <li role="presentation"><a href="#stock" aria-controls="profile" role="tab"
                               data-toggle="tab">Material/Stock Master Files</a>
            </li>
           <li role="presentation"><a href="#rawMaterialLedger" aria-controls="profile" role="tab"
                               data-toggle="tab">Raw Material Ledger</a>
            </li>
            <li role="presentation"><a href="#challanFile" aria-controls="profile" role="tab"
                                       data-toggle="tab">Challan</a>

            </li>
            <li role="presentation"><a href="#store" aria-controls="profile" role="tab"
                                       data-toggle="tab">Inventory Control</a>

            </li>
            <li role="presentation"><a href="#" onclick="logout()" aria-controls="profile" role="tab" data-toggle="tab">Logout</a>
            </li>
        </sec:authorize>


        <sec:authorize access="hasRole('ROLE_REQUEST_MAKER')">
            <li role="presentation"><a href="#home" aria-controls="profile" role="tab"
                                       data-toggle="tab">Home</a></li>

            </li>
            <li role="presentation"><a href="#purchaseRequest" aria-controls="profile" role="tab"
                                       data-toggle="tab">Purchase Request</a>

            </li>
            <li role="presentation"><a href="#qualitycontrol" aria-controls="profile" role="tab"
                                       data-toggle="tab">Quality Control</a>

            </li>
            <li role="presentation"><a href="#challanFile" aria-controls="profile" role="tab"
                                       data-toggle="tab">Challan</a>

            </li>
            <li role="presentation"><a href="#" onclick="logout()" aria-controls="profile" role="tab" data-toggle="tab">Logout</a>
            </li>
        </sec:authorize>

        <sec:authorize access="hasAnyRole('ROLE_PURCHASE_MILL','ROLE_PURCHASE_HO')">
            <li role="presentation"><a href="#home" aria-controls="profile" role="tab"
                                       data-toggle="tab">Home</a></li>

            </li>
           <li role="presentation"><a href="#vendor" aria-controls="profile" role="tab"
                                      data-toggle="tab">Vendors</a>
            </li>
           <li role="presentation"><a href="#billTransactions" aria-controls="profile" role="tab"
                                      data-toggle="tab">Bill</a>

           </li>
            <li role="presentation"><a href="#tradingFile" aria-controls="profile" role="tab" data-toggle="tab">Purchase
                Trading</a>
            </li>
            <li role="presentation"><a href="#" onclick="logout()" aria-controls="profile" role="tab" data-toggle="tab">Logout</a>
            </li>
        </sec:authorize>

        <sec:authorize access="hasRole('ROLE_GATE')">
            <li role="presentation"><a href="#home" aria-controls="profile" role="tab"
                                       data-toggle="tab">Home</a></li>

            </li>
            <li role="presentation"><a href="#tradingFile" aria-controls="profile" role="tab" data-toggle="tab">Purchase
                Trading</a>
            </li>
            <li role="presentation"><a href="#challanFile" aria-controls="profile" role="tab"
                                       data-toggle="tab">Challan</a>

            </li>
            <li role="presentation"><a href="#" onclick="logout()" aria-controls="profile" role="tab" data-toggle="tab">Logout</a>
            </li>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_QA')">
            <li role="presentation"><a href="#home" aria-controls="profile" role="tab"
                                       data-toggle="tab">Home</a></li>

            </li>
            <li role="presentation"><a href="#qualitycontrol" aria-controls="profile" role="tab"
                                       data-toggle="tab">Quality Control</a>

            </li>
            <li role="presentation"><a href="#challanFile" aria-controls="profile" role="tab"
                                       data-toggle="tab">Challan</a>

            </li>
            <li role="presentation"><a href="#" onclick="logout()" aria-controls="profile" role="tab" data-toggle="tab">Logout</a>
            </li>
        </sec:authorize>
    </ul>

    <!-- Tab panes -->
    <section class="tab-content">
        <section role="tabpanel" class="tab-pane active" id="home">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Home</h3>
            </section>
            <section class="dashboardGraphContent">
                <section class="row">

                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                </section>
            </section>
        </section>

        <section role="tabpanel" class="tab-pane fade" id="tradingFile">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Purchase Trading</h3>
            </section>
            <section class="dashboradNavBoxContent">

                <section class="dashboradNavBoxRow">

                    <ul>
                        <sec:authorize access="hasAnyRole('ROLE_PURCHASE_MILL','ROLE_PURCHASE_HO')">
                            <li><a href="${contextPath}/Trading/viewPurchaseOrders">Purchase Orders</a></li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_GATE')">
                            <li><a href="${contextPath}/Trading/viewGrnOrders">Gate Received</a></li>
                        </sec:authorize>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane fade" id="rawMaterialLedger">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Raw Material Ledger</h3>
            </section>
            <section class="dashboradNavBoxContent">

                <section class="dashboradNavBoxRow">

                    <ul>
                        <li><a href="${contextPath}/Report/rawMaterialLedger">Raw Material Ledger</a></li>
                        <li><a href="${contextPath}/Report/rawMaterialReport">Raw Material Report</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
       <section role="tabpanel" class="tab-pane fade" id="vendor">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Manage Vendor</h3>
            </section>
            <section class="dashboradNavBoxContent">

                <section class="dashboradNavBoxRow">

                    <ul>
                    <li><a href="${contextPath}/StockVendor/addVendor">Add Vendor</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
  <section role="tabpanel" class="tab-pane" id="stock">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Material/Stock Master Files</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/RawMaterialStock/addStockRawMaterial">Add Raw Material</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="register">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>SL/LP Register</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/Reports/saleRegister">Sale Register</a></li>
                        <li><a href="${contextPath}/Reports/purchaseRegister">Purchase Register</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane fade" id="challanFile">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Challan</h3>
            </section>
            <section class="dashboradNavBoxContent">

                <section class="dashboradNavBoxRow">

                    <ul>
                        <li><a href="${contextPath}/Challan/viewChallanList">Factory Challans</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="settings">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Settings</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/staticinfo">Static Info</a></li>
                        <li><a href="${contextPath}/changePassword">Change Password</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane fade" id="billTransactions">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Bill Transactions</h3>
            </section>
            <section class="dashboradNavBoxContent">

                <section class="dashboradNavBoxRow">

                    <ul>

                        <li><a href="${contextPath}/Bill/addBill">Received Bill</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="purchaseRequest">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Request Management</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/Request/viewRequests">Requests</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="qualitycontrol">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Quality Control</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/Quality/orderListing">Check List</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="store">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Inventory Control</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <%--                        <li><a href="${contextPath}/Store/order">Request</a></li>
                                                <li><a href="${contextPath}/Store/orderListing">Store Requests</a></li>--%>
                        <li><a href="${contextPath}/Request/viewRequests">Requests</a></li>
                        <li><a href="${contextPath}/Store/clearance">Receivable</a></li>
                        <li><a href="${contextPath}/PurchaseReturn/viewReturnVoucher">Returns</a></li>
                        <li><a href="${contextPath}/Gin/ginListing">GIN List</a></li>
                         <li><a href="${contextPath}/Reject/rejectListing">Reject List</a></li>
                        <sec:authorize access="hasRole('ROLE_STORE')">
                          <li><a href="${contextPath}/SubDepartment/addSubDepartment">Sub Departments</a></li>
                        </sec:authorize>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>

    </section>
    <form action="${contextPath}/logout" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</section>
<script>
    $(document).ready(function () {
        $(function () {
            var hash = window.location.hash;
            console.log($('ul.nav a[href="' + hash + '"]'));
            console.log($('ul.nav a[href="' + hash + '"]'));
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            $('.nav-tabs a').click(function (e) {
                $(this).tab('show');
                var scrollmem = $('body').scrollTop();
                window.location.hash = this.hash;
                $('html,body').scrollTop(scrollmem);
            });
        });

    });

    function logout() {
        $('#logoutForm').submit();
    }
</script>


</body>
</html>

