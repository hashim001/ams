<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bill Payment</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="">
    ${paymentResponse}
    <!-- Modal -->
    <div class="modal fade modal-small modal-bg-o" id="myModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
                <div class="modal-header">
                    <span data-dismiss="modal" onclick="refreshPage()" aria-label="Close" aria-hidden="true"
                          class="closeFixed">&times;</span>
                    <h2 class="modal-title" id="myModalLabel">Multi Bill Payment</h2>
                </div>

                <form:form method="POST" modelAttribute="voucherForm" id="paymentForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">

                            <spring:bind path="voucherNumber">

                                <form:label path="voucherNumber">Voucher Number</form:label>
                                <form:input id="voucherNumber" type="text" path="voucherNumber" class="form-control"
                                            readonly="true"
                                            autofocus="true" value="${voucherNumber}"></form:input>
                                <form:errors path="voucherNumber"></form:errors>

                            </spring:bind>

                            <spring:bind path="bankAccount">

                                <form:label path="bankAccount">Bank</form:label>
                                <form:select cssClass="form-control" path="bankAccount">

                                    <c:forEach items="${bankDetailAccounts}" var="account">
                                        <form:option value="${account.accountCode}">${account.title}</form:option>
                                    </c:forEach>
                                </form:select>
                            </spring:bind>
                            <spring:bind path="accountCode">
                                <form:label path="accountCode">Vendor Account</form:label>
                                <form:select cssClass="form-control" path="accountCode" id="vendorAccountCode">
                                    <form:option value="0">-- NIL --</form:option>
                                    <c:forEach items="${vendorAccounts}" var="account">
                                        <form:option value="${account.accountCode}">${account.title}</form:option>
                                    </c:forEach>
                                </form:select>
                            </spring:bind>
                            <div id="billSelectDiv">


                            </div>
                            <spring:bind path="referenceVoucher">
                                <form:input cssClass="form-control" id="referenceVoucher" type="hidden"
                                            path="referenceVoucher"
                                            autofocus="true"></form:input>
                            </spring:bind>

                            <spring:bind path="taxAccount">
                                <form:label path="taxAccount">Tax Account</form:label>
                                <form:select cssClass="form-control" path="taxAccount" id="taxDrop">
                                    <form:option value="">NIL</form:option>
                                    <c:forEach items="${taxationAccounts}" var="account">
                                        <form:option value="${account.accountCode}">${account.title}</form:option>
                                    </c:forEach>
                                </form:select>
                            </spring:bind>

                            <div id="taxForm" style="display: none">

                                <label>Select Account Type </label>
                                <select id="taxRate">
                                    <option id="filer" value="">Filer</option>
                                    <option id="non-filer" value="">Non-filer</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <spring:bind path="costCentre">
                                <form:label path="costCentre">Cost Centre</form:label>
                                <form:select cssClass="form-control" path="costCentre">
                                    <form:option value="">NIL</form:option>
                                    <c:forEach items="${costCentres}" var="centre">
                                        <form:option value="${centre.accountCode}">${centre.title}</form:option>
                                    </c:forEach>
                                </form:select>
                            </spring:bind>

                            <spring:bind path="billAmount">

                                <form:label path="billAmount">Bill Amount</form:label>
                                <form:input cssClass="form-control" id="billAmount" type="text" path="billAmount"
                                            placeholder="Bill Amount"
                                            autofocus="true" value="0.00" readonly="true"></form:input>
                                <form:errors path="billAmount"></form:errors>

                            </spring:bind>
                            <input type="hidden" id="maxBillAmount"/>
                            <spring:bind path="voucherDate">

                                <form:label path="voucherDate">Voucher Date</form:label>
                                <form:input cssClass="form-control" type="date" path="voucherDate"
                                            autofocus="true" value="${currentDate}" required="required"></form:input>
                                <form:errors path="voucherDate"></form:errors>

                            </spring:bind>
                            <spring:bind path="chequeNumber">

                                <form:label path="chequeNumber">Cheque Number</form:label>
                                <form:input cssClass="form-control" type="text" path="chequeNumber"
                                            placeholder="Cheque Number"
                                            autofocus="true"></form:input>
                                <form:errors path="chequeNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="taxAmount">

                                <form:label path="taxAmount">Tax Amount</form:label>
                                <form:input cssClass="form-control" id="taxAmount" type="text" path="taxAmount"
                                            placeholder="Tax Amount"
                                            autofocus="true" value="0.00" readonly="true"></form:input>
                                <form:errors path="taxAmount"></form:errors>

                            </spring:bind>
                            <spring:bind path="debit">

                                <form:label path="debit">Net Payment</form:label>
                                <form:input cssClass="form-control" type="text" path="debit" placeholder="Net Payment"
                                            id="netPayment"
                                            autofocus="true" value="0.00" readonly="true"></form:input>
                                <form:errors path="debit"></form:errors>

                            </spring:bind>
                        </div>
                    </div>
                    <spring:bind path="remarks">

                        <form:label path="remarks">Remarks</form:label>
                        <form:textarea cssClass="form-control" type="text" path="remarks"
                                       placeholder="Enter Remarks if any."
                                       autofocus="true"></form:textarea>
                        <form:errors path="remarks"></form:errors>

                    </spring:bind>


                </div>

                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button onclick="submitPayment(); return false;">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
                </form:form>
</section>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function () {
        $('#myModal').modal();
        updateNetAmount();

        $('#taxDrop').change(function () {
            $.ajax({
                url: '${contextPath}/Account/ajax',
                contentType: 'application/json',

                data: {
                    taxAccountId: $('#taxDrop').val()
                },
                success: function (response) {

                    if ($('#taxDrop').val() == '') {
                        $('#taxForm').css("display", "none");
                        $('#taxAmount').val("0.00");
                        $('#netPayment').val($('#billAmount').val());

                    } else {
                        $('#filer').val(response[0]);
                        $('#non-filer').val(response[1]);
                        $('#filer').text('Filer - ' + response[0] + '%');
                        $('#non-filer').text('Non-Filer - ' + response[1] + '%');
                        $('#taxForm').css("display", "block");

                        var currentTaxRate = $('#taxRate').val();
                        var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
                        $('#taxAmount').val(taxAmount.toFixed(2));
                        $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));
                    }
                }
            });
        });

        $('#taxRate').change(function () {
            var currentTaxRate = $('#taxRate').val();
            var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
            $('#taxAmount').val(taxAmount.toFixed(2));
            $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));

        });

        $('#billAmount').blur(function () {
            updateNetAmount();
        });

        // Call for bill List
        $('#vendorAccountCode').change(function () {
            $('#billAmount').val(0.0);
            updateNetAmount();

            $.ajax({
                url: '${contextPath}/Bill/generateBillList',
                contentType: 'application/json',

                data: {
                    accountCode: $('#vendorAccountCode').val(),
                    isPurchase: true
                },
                success: function (response) {

                    // alert(response);
                    $("#billSelectDiv").html(response);

                }
            });
        });


    });
    $('#myModal').on('hidden.bs.modal', function () {
        window.location = "${contextPath}/home";
    })

    function updateNetAmount() {

        if ($('#taxDrop').val() != '') {
            var currentTaxRate = $('#taxRate').val();
            var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
            $('#taxAmount').val(taxAmount.toFixed(2));
            $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));
        } else {
            $('#netPayment').val($('#billAmount').val());
        }
    }

    function submitPayment() {
        if ($('#netPayment').val() != 0) {
            $('#paymentForm').submit();
        } else {
            alert("Net Payment cannot be 0.");
        }

    }


    function multipleCall() {

        var mySelections = [];
        $('#billMultiSelect option').each(function (i) {
            if (this.selected == true) {
                mySelections.push(this.value);
            }
        });


        if (mySelections.length > 0) {
            $.ajax({

                url: '${contextPath}/Bill/getMultiDetail',
                contentType: 'application/json',
                data: {
                    billList: mySelections,
                    isPurchase: true
                },
                success: function (response) {
                    $('#billAmount').val(parseFloat(response[0]).toFixed(2));
                    $('#referenceVoucher').val(response[2]);
                    $('#maxBillAmount').val(parseFloat(response[0]).toFixed(2));
                    updateNetAmount();
                }
            });
        }
    }

    function remove(id) {
        if (confirm("Are you sure, you want to delete the voucher?")) {
            $('#voucherRemove' + id).submit();
        }
    }

    function goBack() {
        window.location = "${contextPath}/home#billTransactions";
    }

</script>

</body>
</html>
