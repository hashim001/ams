<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Bill</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>


<section class="main">
    <div class="container">

        <div class="chartAcc">
            <div class="row">
                <div class="col-sm-4">
                    <h2 class="heading-light">Receive Bill</h2>
                    <div class="inputContainer">
                        <form:form method="POST" modelAttribute="billForm" id="billForm">
                        <div class="">
                            <spring:bind path="orderId">

                                <form:label path="orderId">Purchase Order</form:label>
                                <form:select path="orderId"  id="orderDrop" >
                                    <form:option value="0">--NIL--</form:option>
                                    <c:forEach items="${orders}" var="order" >
                                        <form:option value="${order.orderId}">${order.orderNumber}</form:option>
                                    </c:forEach>
                                </form:select>

                            </spring:bind>
                            <spring:bind path="billNumber">

                                <form:label path="billNumber">Bill Number</form:label>
                                <form:input type="text" path="billNumber"  placeholder="Bill Number" required="true"></form:input>
                                <form:errors path="billNumber"></form:errors>

                            </spring:bind>

                            <spring:bind path="amount">

                                <form:label path="amount">Bill Amount</form:label>
                                <form:input type="number" path="amount"  id="billAmount" placeholder="Bill Amount"></form:input>
                                <form:errors path="amount"></form:errors>

                            </spring:bind>
                            <spring:bind path="POAmount">

                                <form:label path="POAmount">P.O Amount</form:label>
                                <form:input type="text" path="POAmount"  id="poAmount" readonly="true"></form:input>
                                <form:errors path="POAmount"></form:errors>

                            </spring:bind>
                            <spring:bind path="billDate">

                                <form:label path="billDate">Bill Date</form:label>
                                <form:input type="Date" path="billDate" required="required" value="${currentDate}"></form:input>
                                <form:errors path="billDate"></form:errors>
                            </spring:bind>


                            </form:form>
                            <button onclick="submitBill(); return false;">submit</button>
                        </div>

                    </div>
                </div>
                <div class="col-sm-8">
                    <h2 class="heading-light">Bills</h2>
                    <div class="">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="10%">Purchase Order</th>
                                <th width="10%">Bill Number</th>
                                <th width="25%">Bill Amount</th>
                                <th width="25%">Received Amount</th>
                                <th width="15%">Date</th>
                                <th width="15%">Status</th>


                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${bills}" var="bill" varStatus="status">

                                <tr>
                                    <td>${bill.billPurchaseOrder.orderNumber}</td>
                                    <td>${bill.billNumber}</td>
                                    <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${bill.amount}"/></td>
                                    <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${bill.receivedAmount}"/></td>
                                    <td>${bill.billDate}</td>
                                    <td><c:choose>
                                        <c:when test="${bill.amount == bill.receivedAmount}">
                                            PAID
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${bill.receivedAmount > 0.0}">
                                                    PARTIAL
                                                </c:when>
                                                <c:otherwise>
                                                    UNPAID
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $('#orderDrop').change(function() {


            $.ajax({
                url : '${contextPath}/Bill/getBillAmount',
                contentType: 'application/json',

                data : {
                    orderId : $('#orderDrop').val()
                },
                success : function(response) {
                    $('#poAmount').val(response);

                }
            });
        });


    });

    function submitBill() {
        if(parseInt($('#poAmount').val()) >= parseInt($('#billAmount').val())){
            $('#billForm').submit();
        }else{
            alert("Bill Amount cannot be greater than PO Amount. Please recheck.")
            $('#billAmount').val(0.0);
        }
    }
    function goBack(){
     window.location = "${contextPath}/home#billTransactions";
    }


</script>

</body>
</html>
