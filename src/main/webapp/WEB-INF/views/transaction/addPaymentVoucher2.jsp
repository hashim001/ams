<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <title>Payment Voucher</title>
</head>
<body>
<%@ include file = "../header.jsp" %>

<section class="main">
    <div class="container">
        <h2 class="heading-main">Payment Voucher <span class="addIcon" data-toggle="modal" data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Voucher Number </br> Voucher Date</th>
                            <th>Bank</br> Cheque Number </th>
                            <th>Account</br> Cost Centre</th>
                            <th>Tax Account </br> Remarks</th>
                            <th>Bill Amount </br> Tax Amount</th>
                            <th>Net Payment</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${paymentVouchers}" var="voucher" varStatus="voucherStatus">
                        <tr>
                            <td>${voucher.voucherNumber} </br>${voucher.voucherDate}</td>
                            <td>${accountMap.get(voucher.bankAccount)} </br>${voucher.chequeNumber}</td>
                            <td>${accountMap.get(voucher.accountCode)} </br>${costCentreMap.get(voucher.costCentre)}</td>
                            <td>${accountMap.get(voucher.taxAccount)} </br>${voucher.remarks}</td>
                            <td>${voucher.billAmount} </br>${voucher.taxAmount}</td>
                            <td>${voucher.debit}</td>
                            <td><form method="post" id = "voucherRemove${voucherStatus.index}" action="${contextPath}/Account/deleteVoucher?${_csrf.parameterName}=${_csrf.token}">
                                <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                                <input  type="hidden" name="voucherNumber" value="${voucher.voucherNumber}"/>
                                <input  type="hidden" name="redirectUrl" value="Account/createPaymentVoucher"/>
                            </form>
                                <ul class="list-inline">
                                    <li><a href="#" onclick="remove(${voucherStatus.index});"><img src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                </ul>
                            </td>
                        </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Client Detail</h2>
            </div>

        <form:form method="POST" modelAttribute="voucherForm">
            <div class="modal-body">
                <spring:bind path="voucherNumber">

                        <form:label path="voucherNumber">Voucher Number</form:label>
                        <form:input id ="voucherNumber" type="text" path="voucherNumber" class="form-control" readonly="true"
                                    autofocus="true" value = "${voucherNumber}"></form:input>
                        <form:errors path="voucherNumber"></form:errors>

                </spring:bind>

                <spring:bind path="bankAccount">

                        <form:label path="bankAccount">Bank</form:label>
                        <form:select path="bankAccount" >

                            <c:forEach items="${bankDetailAccounts}" var="account" >
                                <form:option value="${account.accountCode}">${account.title}</form:option>
                            </c:forEach>
                        </form:select>


                </spring:bind>

                <spring:bind path="accountCode">

                        <form:label path="accountCode">Account</form:label>
                        <form:select path="accountCode"  >

                            <c:forEach items="${accounts}" var="account" >
                                <form:option value="${account.accountCode}">${account.title}
                                    <c:choose>
                                        <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                        </c:when>
                                    </c:choose></form:option>
                            </c:forEach>
                        </form:select>


                </spring:bind>

                <spring:bind path="costCentre">

                        <form:label path="costCentre">Cost Centre</form:label>
                        <form:select path="costCentre"  >
                            <form:option value="">NIL</form:option>
                            <c:forEach items="${costCentres}" var="centre" >
                                <form:option value="${centre.accountCode}">${centre.title}</form:option>
                            </c:forEach>
                        </form:select>


                </spring:bind>
                <spring:bind path="taxAccount">

                        <form:label path="taxAccount">Tax Account</form:label>
                        <form:select path="taxAccount"  id="taxDrop">
                            <form:option value="">NIL</form:option>
                            <c:forEach items="${taxationAccounts}" var="account" >
                                <form:option value="${account.accountCode}">${account.title}</form:option>
                            </c:forEach>
                        </form:select>


                </spring:bind>
                <div  id="taxForm" style="display: none" >

                    <label>Select Account Type </label>
                    <select id = "taxRate">
                        <option id = "filer" value="">Filer</option>
                        <option id= "non-filer" value="">Non-filer</option>

                    </select>
                </div>

                <spring:bind path="billAmount">

                        <form:label path="billAmount">Bill Amount</form:label>
                        <form:input id ="billAmount" type="text" path="billAmount"  placeholder="Bill Amount"
                                    autofocus="true" value = "0.00"></form:input>
                        <form:errors path="billAmount"></form:errors>

                </spring:bind>
                <spring:bind path="voucherDate">

                        <form:label path="voucherDate">Voucher Date</form:label>
                        <form:input type="date" path="voucherDate"
                                    autofocus="true" value = "${currentDate}" required="required"></form:input>
                        <form:errors path="voucherDate"></form:errors>

                </spring:bind>
                <spring:bind path="chequeNumber">

                        <form:label path="chequeNumber">Cheque Number</form:label>
                        <form:input type="text" path="chequeNumber"  placeholder="Cheque Number"
                                    autofocus="true"></form:input>
                        <form:errors path="chequeNumber"></form:errors>

                </spring:bind>
                <spring:bind path="taxAmount">

                        <form:label path="taxAmount">Tax Amount</form:label>
                        <form:input id ="taxAmount" type="text" path="taxAmount"  placeholder="Tax Amount"
                                    autofocus="true" value = "0.00" readonly="true" ></form:input>
                        <form:errors path="taxAmount"></form:errors>

                </spring:bind>
                <spring:bind path="debit">

                        <form:label path="debit">Net Payment</form:label>
                        <form:input type="text" path="debit"  placeholder="Net Payment" id="netPayment"
                                    autofocus="true" value = "0.00" readonly="true"></form:input>
                        <form:errors path="debit"></form:errors>

                </spring:bind>
                <spring:bind path="remarks">

                        <form:label path="remarks">Remarks</form:label>
                        <form:textarea type="text" path="remarks"  placeholder="Enter Remarks if any."
                                       autofocus="true"></form:textarea>
                        <form:errors path="remarks"></form:errors>

                </spring:bind>


            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <button type="submit" >Submit</button>
                    </div>
                    <div class="col-sm-6 text-center">
                        <button type="button" data-dismiss="modal">Exit</button>
                    </div>
                </div>
            </div>
        </form:form>
        </div>
    </div>
</div>

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function() {
        $('#taxDrop').change(function() {


            $.ajax({
                url : '${contextPath}/Account/ajax',
                contentType: 'application/json',

                data : {
                    taxAccountId : $('#taxDrop').val()
                },
                success : function(response) {



                    if($('#taxDrop').val() == ''){
                        $('#taxForm').css("display","none");
                        $('#taxAmount').val("0.00");
                        $('#netPayment').val($('#billAmount').val());

                    }else{
                        $('#filer').val(response[0]);
                        $('#non-filer').val(response[1]);
                        $('#filer').text('Filer - '+response[0]+ '%');
                        $('#non-filer').text('Non-Filer - '+response[1]+ '%');
                        $('#taxForm').css("display","block");

                        var currentTaxRate = $('#taxRate').val();
                        var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
                        $('#taxAmount').val(taxAmount.toFixed(2));
                        $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));
                    }

                }
            });
        });

        $('#taxRate').change(function() {
            var currentTaxRate = $('#taxRate').val();
            var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
            $('#taxAmount').val(taxAmount.toFixed(2));
            $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));

        });

        $('#billAmount').blur(function() {
            if($('#taxDrop').val() != '') {
                var currentTaxRate = $('#taxRate').val();
                var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
                $('#taxAmount').val(taxAmount.toFixed(2));
                $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));
            }else{
                $('#netPayment').val($('#billAmount').val());
            }
        });

    });

    function remove(id) {
        if(confirm("Are you sure, you want to delete the voucher?")) {
            $('#voucherRemove' + id).submit();
        }
    }

</script>
</body>
</html>