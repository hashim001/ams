<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create Voucher</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/select2.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">
    ${voucherAddSuccess}

    <h2 class="form-signin-heading">Journal Voucher</h2>
    <div class="container hide">
        <form class="form-inline formSaleVoucher" style="margin-bottom: 15px; ">
            <div class="pull-left">
                <div class="form-Row">
                    <label>Invoice #</label>
                    <input id="invoiceNumber" type="text" style="width: 150px" class="form-control" onblur="updateInvoiceValues(this.value);"
                           autofocus="true" value="${invoiceNumber}"/>
                    <label>Voucher #</label>
                    <input id="voucherNumber" type="text" style="width: 150px" class="form-control" readonly="readonly"
                           autofocus="true" value="${voucherNumber}"/>
                </div>

            </div>
            <label>Date : </label>
            <input type="date" style="width: 160px" class="form-control " id="currentDate"
                   autofocus="true" value="${currentDate}" required="required"/>


            <label>Total Credit : </label>
            <input type="text" style="width: 150px" class="form-control " id="totalCredit"
                   autofocus="true" value="0.00" readonly="true"/>


            <label>Total Debit : </label>
            <input type="text" style="width: 150px" class="form-control " id="totalDebit"
                   autofocus="true" value="0.00" readonly="true"/>


        </form>

    </div>


    <form:form method="POST" modelAttribute="voucherList" id="formid">
        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>
                    <th width="10%">Project</th>
                    <th width="20%">Account Name</th>
                    <th width="10%">Debit</th>
                    <th width="10%">Credit</th>
                    <th width="15%">Remarks</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${voucherList.voucherList}" var="vourchers" varStatus="status">

                    <tr>

                        <form:input id="voucherNumber${status.index}" type="hidden" path="voucherList[${status.index}].voucherNumber"
                                    class="form-control"
                                    autofocus="true" value="${voucherNumber}"/>
                        <form:input id="invoiceNumber${status.index}" cssClass="invoiceNumber" type="hidden" path="voucherList[${status.index}].invoiceNumber"
                                    class="form-control"
                                    autofocus="true" value="${invoiceNumber}"/>
                        <td>

                            <form:select id="projectCode${status.index}" path="voucherList[${status.index}].projectCode" class="form-control">

                                <form:option value="0">NIL</form:option>
                                <c:forEach items="${projectList}" var="project">
                                    <form:option value="${project.accountCode}">${project.title}</form:option>
                                </c:forEach>
                            </form:select>
                        </td>

                        <td>

                            <form:select id="accountCode${status.index}" path="voucherList[${status.index}].accountCode" class="form-control accountCode">

                                <form:option value="0">NIL</form:option>
                                <c:forEach items="${accounts}" var="account">
                                    <form:option value="${account.accountCode}">${account.title}
                                        <c:choose>
                                            <c:when test="${account.parentName != 'NA' }"> - ${account.parentName}
                                            </c:when>
                                        </c:choose></form:option>
                                </c:forEach>
                            </form:select>
                        </td>
                        <td>

                            <form:input type="text" path="voucherList[${status.index}].debit"
                                        class="form-control debitList" id="debitList${status.index}"
                                        onblur="findTotalDebit()"
                                        autofocus="true" value="${vourchers.debit}"></form:input>

                        </td>
                        <td>
                            <form:input type="text" path="voucherList[${status.index}].credit" id="creditList${status.index}"
                                        class="form-control creditList" onblur="findTotalCredit()"
                                        autofocus="true" value="${vourchers.credit}"></form:input>

                        </td>
                        <td>
                            <form:textarea type="text" path="voucherList[${status.index}].remarks" class="form-control"
                                           placeholder="Enter Remarks if any."
                                           autofocus="true" value="${vourchers.remarks}"></form:textarea>

                        </td>

                        <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].voucherDate"
                                    class="form-control theDate"
                                    autofocus="true" value="${currentDate}" required="required"></form:input>


                    </tr>
                </c:forEach>
                </tbody>

            </table>
        </div>
        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/select.min.js"></script>

<script>

    $(document).ready(function () {
        $('.accountCode').select2({"width": "100%"});
        $(".container").removeClass("hide");

            $('.debitList').each(function (index, item) {
              $('#debitList'+index).val(parseFloat($('#debitList'+index).val()).toFixed(1));
              $('#creditList'+index).val(parseFloat($('#creditList'+index).val()).toFixed(1));
            });
        findTotalCredit();
        findTotalDebit();
        $('#currentDate').change(function () {
            $('.theDate').val($('#currentDate').val());

        });
        $('#formid').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                alert("Enter key not Allowed.Please use submit button");
                e.preventDefault();
                return false;
            }
        });
    });


    function findTotalDebit() {
        var arr = $('.debitList');
        var tot = 0;
        for (var i = 0; i < arr.length; i++) {

            tot += parseFloat(arr[i].value);
        }

        document.getElementById('totalDebit').value = tot;
        validation();
    }

    function findTotalCredit() {
        var arr = $('.creditList');
        var tot = 0;
        for (var i = 0; i < arr.length; i++) {

            tot += parseFloat(arr[i].value);
        }

        document.getElementById('totalCredit').value = tot;
        validation();
    }

    function validation() {
        if (document.getElementById('totalCredit').value == document.getElementById('totalDebit').value) {

            $('#btn_submit').prop('disabled',false)
        }
        else {

            $('#btn_submit').prop('disabled', true)
        }

    }
    function goBack(){
       history.back();
    }
    function updateInvoiceValues(invoiceNumber) {
        $('.invoiceNumber').val(invoiceNumber);
    }
</script>
</body>
</html>
