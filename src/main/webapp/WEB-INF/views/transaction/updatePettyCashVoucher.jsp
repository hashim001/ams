<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create Voucher</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">
    ${voucherAddSuccess}

    <h2 class="form-signin-heading">Payment Petty Cash</h2>
    <div class="container">
        <form class="form-inline formSaleVoucher" style="margin-bottom: 15px; ">
            <div class="pull-left">
                <div class="form-Row">
                    <label>Voucher Number :</label>
                    <input id="voucherNumber" type="text" style="width: 150px" class="form-control" readonly="true"
                           autofocus="true" value="${voucherNumber}"/>
                </div>

            </div>
            <input type="hidden" id="totalPayment" value="${total}"/>
            <input type="hidden" id="bankAccountCode" value="${currentBankAccountCode}"/>
                <label>Bank Account:</label>
                <select name="account" class="form-control" id="bankAccount">
                        <option value="0">NIL</option>
                    <c:forEach items="${bankAccounts}" var="account" varStatus="status">
                    <c:choose>
                    <c:when test="${account.accountCode==currentBankAccountCode}">
                    <option value="${account.accountCode}" selected>${account.title}</option>
                    </c:when>
                    <c:otherwise>
                    <option value="${account.accountCode}">${account.title}</option>
                    </c:otherwise>
                    </c:choose>
                    </c:forEach>
                </select>

            <label>Date : </label>
            <input type="date" style="width: 160px" class="form-control " id="currentDate"
                   autofocus="true" value="${currentDate}" required="required"/>
              <br style="clear: both;">
            <label>Total : </label>
            <input type="text" style="width: 150px" class="form-control " id="total"
                   autofocus="true" value="0" readonly="true"/>
            <label>Total Debit : </label>
            <input type="text" style="width: 150px" class="form-control "  id="totalDebit"
                   autofocus="true" value="0.00" readonly="true" onblur="changeTotal()"/>
            <label>Remaining : </label>
            <input type="text" style="width: 150px" class="form-control "  id="remaining"
                   autofocus="true" value="0.00" readonly="true" />

        </form>

    </div>


    <form:form method="POST" modelAttribute="voucherList" id="formid">
        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>

                    <th>Account Name</th>
                    <th width="15%">Amount</th>
                    <th width="25%">Remarks</th>


                </tr>
                </thead>
                <tbody>
                <c:forEach items="${voucherList.voucherList}" varStatus="status">

                    <tr>

                        <form:input id="voucherNumber" type="hidden" path="voucherList[${status.index}].voucherNumber"
                                    class="form-control"
                                    autofocus="true" value="${voucherNumber}"></form:input>

                        <td>

                            <form:select path="voucherList[${status.index}].accountCode" class="form-control">

                                <form:option value="0">NIL</form:option>
                                <c:forEach items="${accounts}" var="account">
                                    <form:option value="${account.accountCode}">${account.title}
                                        <c:choose>
                                            <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                            </c:when>
                                        </c:choose></form:option>
                                </c:forEach>
                            </form:select>
                        </td>
                        <td>
                            <form:input type="text" path="voucherList[${status.index}].debit" id="debitList${status.index}"
                                        class="form-control debitList"
                                        onblur="findTotalDebit()"
                                        autofocus="true" value=""></form:input>


                        </td>
                        <td>
                            <form:textarea type="text" path="voucherList[${status.index}].remarks" class="form-control"
                                           placeholder="Enter Remarks if any."
                                           autofocus="true"></form:textarea>

                        </td>

                        <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].voucherDate"
                                    class="form-control theDate"
                                    autofocus="true" value="${currentDate}" required="required"></form:input>
                       <form:input type="hidden" path="voucherList[${status.index}].bankVoucher"
                                    class="form-control bankAccountNumber"
                                    autofocus="true"></form:input>


                    </tr>
                </c:forEach>
                </tbody>

            </table>
        </div>
        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>
        $('#bankAccount').change(function () {
          onChangeBankAccount();
        });
     function onChangeBankAccount(){
            $('.bankAccountNumber').val($('#bankAccount').val());
        $.ajax({
            url: '${contextPath}/Account/ajax/changeBalance',
            contentType: 'application/json',
            data: {
                bankAccount: $('#bankAccount').val(),
            },

            success: function (response) {
            console.log($('#bankAccountCode').val())
            if ($('#bankAccount').val()==$('#bankAccountCode').val()){
               $('#total').val((parseFloat(response)+parseFloat($('#totalPayment').val())).toFixed(1));
            }
            else{
                $('#total').val(parseFloat(response).toFixed(1));
                }
                   changeTotal();
            }
        });
     }
     $(document).ready(function () {
            $('.debitList').each(function (index, item) {
            $('#debitList'+index).val(parseFloat($('#debitList'+index).val()).toFixed(1));
            });
         onChangeBankAccount();
         findTotalDebit();

        $('#currentDate').change(function () {
            $('.theDate').val($('#currentDate').val());

        });
        $('#formid').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                alert("Enter key not Allowed.Please use submit button");
                e.preventDefault();
                return false;
            }
        });
    });

    function changeTotal(){
        $('#remaining').val(($('#total').val()-$('#totalDebit').val()).toFixed(1));

    }
    function findTotalDebit() {
        var arr = $('.debitList');
        var tot = 0;
        for (var i = 0; i < arr.length; i++) {

            tot += parseFloat(arr[i].value);
        }

        document.getElementById('totalDebit').value = tot;

    }

    function goBack(){
       window.location = "${contextPath}/Account/viewPettyCashVouchers";
    }
</script>
</body>
</html>
