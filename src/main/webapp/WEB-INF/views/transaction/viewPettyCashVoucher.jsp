<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Payment Petty Cash Vouchers</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        ${voucherAddSuccess}
        <h2 class="heading-main">Payment Petty Cash Vouchers<a href="${contextPath}/Account/addPettyCash"><span class="addIcon"
                                                                                                      data-toggle="modal"
                                                                                                      data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></a></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Voucher Number</th>
                            <th>Voucher Date</th>
                            <th>Account</th>
                            <th>Amount</th>
                            <th width="10%">Action</th>

                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>

    function remove(id) {
        if (confirm("Are you sure, you want to delete the voucher?")) {
            $('#voucherRemove' + id).submit();
        }
    }

    $(document).ready(function () {
        $('#dataTable').DataTable({
            searchDelay: 900,
            "processing": true,
            "serverSide": true,
            "order": [[2, "desc"]],
            "pageLength": 10,
            "ajax": "${contextPath}/PettyCash/json"
        });
    });

    function goBack(){
     window.location = "${contextPath}/home#transactionFile";
    }

</script>
</body>
</html>