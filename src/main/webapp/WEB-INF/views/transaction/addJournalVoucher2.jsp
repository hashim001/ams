<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <title>Journal Voucher</title>
</head>
<body>
<%@ include file = "../header.jsp" %>

<section class="main">
    <div class="container">
        <h2 class="heading-main">Journal Voucher <span class="addIcon" data-toggle="modal" data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Voucher Number </th>
                            <th>Voucher Date </th>
                            <th>Account</th>
                            <th>Remarks</th>
                            <th>Net Payment</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${journalVouchers}" var="voucher">
                            <tr>
                                <td>${voucher.voucherNumber}</td>
                                <td>${voucher.voucherDate}</td>
                                <td>${accountMap.get(voucher.accountCode)}</td>
                                <td>${voucher.remarks}</td>
                                <td>${voucher.credit + voucher.debit}</td>
                                <td>
                                    <ul class="list-inline">
                                        <li><a href="#"><img src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                        <li><a href="#"><img src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                        <li><a href="#"><img src="${contextPath}/resources/img/recycle.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Client Detail</h2>
            </div>
            <div class="modal-body">
                <input type="text" placeholder="Name">
                <input type="text" placeholder="Address">
                <input type="text" placeholder="Phone Number">
                <input type="text" placeholder="Contact Person">
                <input type="text" placeholder="Mobile Number">
                <input type="text" placeholder="NTN">
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <button type="button" data-dismiss="modal">Submit</button>
                    </div>
                    <div class="col-sm-6 text-center">
                        <button type="button">Exit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>