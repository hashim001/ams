<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bank Payment Voucher</title>
</head>
<style>
    body{
        margin: 0;padding: 0;max-width: 950px;width: 100%;margin: 0 auto;
    }
    *{
        margin: 0;padding: 0;box-sizing: border-box;
    }
    table{width: 100%;margin-top: 20px;border-collapse: collapse;border-spacing: 0;}
    img{height: 150px;}
    .upper{text-transform: uppercase;}
    .input{text-align: left;padding-left: 15px;}
    .nav{text-align: center;border: 1px solid;border-bottom: 4px solid;}
    .nav>td{font-weight: bold;}
    .entry>tr>td:first-child{border-left: 1px solid;}
    .nav>td,.entry>tr>td{padding: 8px 5px;border-right: 1px solid;}
    .entry{border-bottom: 1px solid;text-align: left;}
    .total{border-top: 1px solid;text-align: left;}
    tfoot>tr>td{padding-top: 65px;}
    .dotted{border-bottom: 1px dotted;font-weight: bold;margin-right: 50px;}
    .box{border: 1px dotted;padding-bottom: 25px;width: 150px;height: 100px;}
    .name{text-transform: uppercase;border-bottom: 1px solid;margin-right:35px;padding-left: 5px;}
    .sign{margin-top: 100px;}
    table.sign p{border-top: 1px solid;margin: 0 10px;text-align: center;}
    tr.last>td>p{margin: 70px 0 50px;}
    @media print{
        tbody td{font-size: 14px}
        h1{font-size: 22px}
        .upper{font-size: 15px;}
        h3{font-size: 18px;}


    }
</style>
<body>
    <table>
        <thead>
            <th style="float:left; " >
                <img src="${imageUrl}" alt="logo">
            </th>
            <th>
                <h1 style="white-space:nowrap">${companyName}</h1> <br>
                <p class="upper"></p> <br>
                <h3>Bank Payment Voucher</h3>
            </th>
            <th style="text-align: left;    padding-left: 55px;">
               Date: <br> Voucher# <br> Chq# / Slip#
            </th>
            <th class="input">
                ${voucherDate} <br> ${voucherNumber} <br> ${chequeNo}
            </th>
        </thead>
        <tbody class="entry">
            <tr class="nav">
                <td style="    width: 300px;" colspan="2">Account Name / Remarks</td>
                <td style="min-width:140px">Debit</td>
                <td style="min-width:140px">Credit</td>
            </tr>
            <c:set var="total" value="0"/>
                <c:forEach items="${voucherList}" var="voucher">

            <tr>

                <td colspan="2">${accountMap.get(voucher.accountCode)}</td>
                <c:choose>
                 <c:when test="${voucher.credit==0}">
                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${voucher.debit}"/></td>
                <td></td>
                <c:set var="total" value="${total+voucher.debit}"/>
                 </c:when>
                 <c:otherwise>
                 <td></td>
                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${voucher.credit}"/></td>
                 </c:otherwise>
                </c:choose>
            </tr>
           </c:forEach>

               <tr>
               <td colspan="2">${remarks}</td>
               <td></td>
               <td></td>
               <tr>
            <tr class="total">
                <td colspan="2"><h3 style="text-align: center">TOTAL</h3></td>
                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${total}"/></td>
                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${total}"/></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <table>
                    <tr>
                        <td  colspan="1">Prepared By:<span class="name">${user}</span></td>
                        <td style="width: 490px"></td>
                        <td style="text-align: center;">Recieved By:</td>
                        <td class="box"></td>
                    </tr>
                </table>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="sign">
                        <tr>
                            <td>
                                <p>Accountant</p>
                            </td>
                            <td>
                                <p>Chief Financial Officer</p>
                            </td>
                            <td>
                                <p>Executive Director / Chairman</p>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </tfoot>
    </table>
</body>
</html>