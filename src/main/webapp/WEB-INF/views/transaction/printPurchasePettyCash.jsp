<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Purchase Petty Cash Voucher</title>
</head>
<style>
    body{
        margin: 0;padding: 0;max-width: 950px;width: 100%;margin: 0 auto;
    }
    *{
        margin: 0;padding: 0;box-sizing: border-box;
    }
    table{width: 100%;margin-top: 20px;border-collapse: collapse;border-spacing: 0;}
    img{height: 150px;}
    .upper{text-transform: uppercase;}
    .input{text-align: left;padding-left: 15px;}
    .nav{text-align: center;border: 1px solid;border-bottom: 4px solid;}
    .nav>td{font-weight: bold;}
    .entry>tr>td:first-child{border-left: 1px solid;}
    .nav>td,.entry>tr>td{padding: 8px 5px;border-right: 1px solid;}
    .entry{border-bottom: 1px solid;text-align: left;}
    .total{border-top: 1px solid;text-align: left;    border-bottom: 1px solid;}
    tfoot>tr>td{padding-top: 65px;}
    .dotted{border-bottom: 1px dotted;font-weight: bold;margin-right: 50px;}
    .box{border: 1px dotted;padding-bottom: 25px;width: 150px;height: 100px;}
    .name{text-transform: uppercase;border-bottom: 1px solid;margin-right:35px;padding-left: 5px;}
    .sign{margin-top: 100px;}
    table.sign p{border-top: 1px solid;margin: 0 10px;text-align: center;}
    tr.last>td>p{margin: 70px 0 50px;}
    @media print{
        tbody td{font-size: 14px}
        h1{font-size: 22px}
        .upper{font-size: 15px;}
        h3{font-size: 18px;}


    }
    .table-entry tbody tr{
        line-height: 31px;
    }
    .table-entry tbody tr>td{
        padding: 0 10px;
    }
    .table-entry{border: 1px solid;}
    .table-entry tbody tr>td{
        border-left: 1px solid;
        border-right: 1px solid;
    }
</style>
<body>
    <table>
        <thead>
            <th>
                <p class="upper"></p> <br>
                <h3>Purchase Petty Cash</h3>
                <h4>Bank Account - ${accountMap.get(bankAccount)}</h4>
            </th>
            <th style="text-align: left;    padding-left: 55px;">
               Date: <br> Voucher# <br> Opening:
            </th>
            <th class="input">
                ${voucherDate} <br> ${voucherNumber} <br> <fmt:formatNumber type="number" maxFractionDigits="2" value="${openingBalance+total}"/>

            </th>
        </thead>
        </table>
        <table class="table-entry">
        <thead class="entry">
            <tr class="nav">
                <td style="    width: 300px;" colspan="1">Account Name</td>
                <td style="    width: 300px;" colspan="2">Remarks</td>
                <td style="min-width:140px">Amount</td>
            </tr>
         </thead>
         <tbody>
                <c:forEach items="${voucherList}" var="voucher">

            <tr>
                <c:choose>
                 <c:when test="${voucher.credit==0}">
                <td style="width: 300px" colspan="1">${accountMap.get(voucher.accountCode)}</td>
                <td style="    width: 300px;" colspan="2">${voucher.remarks}</td>
                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${voucher.debit}"/></td>
                </c:when>
                </c:choose>

            </tr>
           </c:forEach>
            <tr class="total">
                <td style="width: 300px" colspan="1"></td>
                <td style="    width: 300px;" colspan="2"><h3 style="text-align: center">Total</h3></td>
                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${total}"/></td>
            </tr>
            <tr class="total">
                <td style="width: 300px" colspan="1"></td>
                <td style="    width: 300px;" colspan="2"><h3 style="text-align: center">Closing Balance</h3></td>
                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${openingBalance}"/></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <table>
                    <tr>
                        <td  colspan="1">Prepared By:<span class="name">${user}</span></td>
                        <td style="width: 490px"></td>
                        <td style="text-align: center;">Recieved By:</td>
                        <td class="box"></td>
                    </tr>
                </table>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="sign">
                        <tr>
                            <td>
                                <p>Accountant</p>
                            </td>
                            <td>
                                <p>Chief Financial Officer</p>
                            </td>
                            <td>
                                <p>Executive Director / Chairman</p>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </tfoot>
    </table>
</body>
</html>