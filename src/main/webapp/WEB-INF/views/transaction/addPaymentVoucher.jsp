<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/select2.min.css">
    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>Payment Voucher</title>
</head>
<style>
.select2-container {
    width: 100% !important;
    padding: 0;
}
</style>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        <h2 class="heading-main">Payment Voucher <span class="addIcon addForm" data-toggle="modal"
                                                       data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th width="10%">V-No </br> V-Date</th>
                            <th width="15%">Bank</br> Chq-No</th>
                            <th width="20%">Acc</br> Cost Centre</th>
                            <th width="20%">Tax Acc</br> Remarks</th>
                            <th width="15%">Voucher Amt</br> Tax Amt</th>
                            <th width="10%">Net Payment</th>
                            <th width="10%">Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" onclick="refreshPage()" aria-label="Close" aria-hidden="true"
                      class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Payment Voucher</h2>
            </div>
            <form:form method="post" modelAttribute="voucherForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="voucherNumber">

                                <form:label path="voucherNumber">Voucher Number</form:label>
                                <form:input id="voucherNumber" type="text" path="voucherNumber" readonly="true"
                                            autofocus="true" value="${voucherNumber}"></form:input>
                                <form:errors path="voucherNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="voucherDate">

                                <form:label path="voucherDate">Voucher Date</form:label>
                                <form:input type="date" path="voucherDate"
                                            autofocus="true" value="${currentDate}" required="required"></form:input>
                                <form:errors path="voucherDate"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankAccount">

                                <form:label path="bankAccount">Bank</form:label>
                                <form:select path="bankAccount">
                                    <c:forEach items="${bankDetailAccounts}" var="account">
                                       <form:option value="${account.accountCode}">${account.title}</form:option>
                                     </c:forEach>
                                </form:select>


                            </spring:bind>

                            <spring:bind path="accountCode">

                                <form:label path="accountCode">Account</form:label>
                                <form:select path="accountCode" id="accountAmount" >

                                    <c:forEach items="${accounts}" var="account">
                                        <form:option value="${account.accountCode}">${account.title}
                                            <c:choose>
                                                <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                                </c:when>
                                            </c:choose></form:option>
                                    </c:forEach>
                                </form:select>
                            </spring:bind>
                            <spring:bind path="chequeNumber">

                                <form:label path="chequeNumber">Cheque Number</form:label>
                                <form:input type="text" path="chequeNumber" placeholder="Cheque Number"
                                            autofocus="true"></form:input>
                                <form:errors path="chequeNumber"></form:errors>

                            </spring:bind>
                        </div>
                        <div class="col-sm-6">
                            <spring:bind path="costCentre">

                                <form:label path="costCentre">Cost Centre</form:label>
                                <form:select path="costCentre">
                                    <form:option value="">NIL</form:option>
                                    <c:forEach items="${costCentres}" var="centre">
                                        <form:option value="${centre.accountCode}">${centre.title}</form:option>
                                    </c:forEach>
                                </form:select>


                            </spring:bind>
                            <spring:bind path="billAmount">

                                <form:label path="billAmount">Voucher Amount</form:label>
                                <form:input id="billAmount" type="number" path="billAmount" placeholder="Voucher Amount"
                                            autofocus="true" value="0.00" required = "true" ></form:input>
                                <form:errors path="billAmount"></form:errors>

                            </spring:bind>
                            <spring:bind path="taxAccount">

                                <form:label path="taxAccount">Tax Rate</form:label>
                                <form:select path="taxAccount" id="taxDrop">

                                    <form:option value="">NIL</form:option>
                                    <c:forEach items="${taxationAccounts}" var="taxationAccount" varStatus="status">
                                        <form:option
                                                value="${taxationAccount.accountCode}">${taxationAccount.title}</form:option>
                                    </c:forEach>

                                </form:select>
                            </spring:bind>
                            <div id="taxForm" style="display: none">
                                <spring:bind path="taxRate">
                                    <form:label path="taxRate">Select Account Type </form:label>
                                    <input type="hidden" value="" id="taxRateID"></input>
                                    <form:select path="taxRate" id="taxRate">
                                        <c:choose>
                                            <c:when test="${taxDetail.size()>0}">
                                                <option id="filer" value="${taxDetail.get(0).filerAmount}">Filer
                                                </option>
                                                <option id="non-filer" value="${taxDetail.get(0).nonFilerAmount}">
                                                    Non-filer
                                                </option>
                                            </c:when>
                                        </c:choose>
                                    </form:select>
                                </spring:bind>
                            </div>
                            <spring:bind path="taxAmount">

                                <form:label path="taxAmount">Tax Amount</form:label>
                                <form:input id="taxAmount" type="text" path="taxAmount" placeholder="Tax Amount"
                                            autofocus="true" value="0.00" readonly="true"></form:input>
                                <form:errors path="taxAmount"></form:errors>

                            </spring:bind>
                            <spring:bind path="debit">

                                <form:label path="debit">Net Payment</form:label>
                                <form:input type="text" path="debit" placeholder="Net Payment" id="netPayment"
                                            autofocus="true" value="0.00" readonly="true"></form:input>
                                <form:errors path="debit"></form:errors>

                            </spring:bind>
                        </div>


                    </div>
                    <div align="center">
                        <spring:bind path="remarks">
                            <div align="center">
                                <form:label path="remarks">Remarks</form:label></div>
                            <form:textarea cssStyle="width: 700px" type="text" path="remarks"
                                           placeholder="Enter Remarks if any."
                                           autofocus="true"></form:textarea>
                            <form:errors path="remarks"></form:errors>

                        </spring:bind>
                    </div>
                    <div id="balance" style="display: none"></div>
                    <div id="limit" style="display: none"></div>
                </div>

                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="button" onclick="addVoucherForm()" id="btn_submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" onclick="refreshPage()">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${contextPath}/resources/js/select.min.js"></script>
<script>
    var form_action = "edit";
    var forChassis = "false";
    $(document).ready(function () {
    $('#apartment').select2();

        $('#taxDrop').change(function () {
            changeTaxAmount(0);
        });

        $(".addForm").click(function () {
            form_action = 'add';
        });

        $(".editForm").click(function () {
            form_action = 'edit';
        });


        $('#taxRate').change(function () {
            var currentTaxRate = $('#taxRate').val();
            taxRateID = currentTaxRate;
            var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
            $('#taxAmount').val(taxAmount.toFixed(2));
            $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));

        });

        $('#billAmount').blur(function () {
            if ($('#taxDrop').val() != '') {
                var currentTaxRate = $('#taxRate').val();
                var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
                $('#taxAmount').val(taxAmount.toFixed(2));
                $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));
                var balance = (+$('#balance').val() + +$('#netPayment').val()).toFixed(2);
            } else {
                $('#netPayment').val($('#billAmount').val());
                var balance = (+$('#balance').val() + +$('#netPayment').val()).toFixed(2);
            }
            $('#balance').html(balance);
            limitValidation();
        });

    });
        $('#accountAmount').change(function (e,data) {
        if(data){
         forChassis = "true";
        }else{
            forChassis = "false";
        }
        console.log("data",data);
            getBalanceAmount($('#accountAmount').val());
        });
    function changeTaxAmount(defaultTaxRate) {

        $.ajax({
            url: '${contextPath}/Account/ajax',
            contentType: 'application/json',

            data: {
                taxAccountId: $('#taxDrop').val()
            },
            success: function (response) {


                if ($('#taxDrop').val() == '') {
                    $('#taxForm').css("display", "none");
                    $('#taxAmount').val("0.00");
                    $('#netPayment').val($('#billAmount').val());

                }
                else if (defaultTaxRate == 0){
                    $('#filer').val(response[0]);
                    $('#non-filer').val(response[1]);
                    $('#filer').text('Filer - ' + response[0] + '%');
                    $('#non-filer').text('Non-Filer - ' + response[1] + '%');
                    $('#taxForm').css("display", "block");

                    var currentTaxRate = $('#taxRate').val();
                    var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
                    $('#taxAmount').val(taxAmount.toFixed(2));
                    $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));
                }
                else{
                    $('#filer').val(response[0]);
                    $('#non-filer').val(response[1]);
                    $('#filer').text('Filer - ' + response[0] + '%');
                    $('#non-filer').text('Non-Filer - ' + response[1] + '%');
                    $('#taxForm').css("display", "block");
                    $('#taxRate').val(defaultTaxRate);
                    var currentTaxRate = $('#taxRate').val();
                    var taxAmount = $('#billAmount').val() * (currentTaxRate / 100);
                    $('#taxAmount').val(taxAmount.toFixed(2));
                    $('#netPayment').val(($('#billAmount').val() - taxAmount.toFixed(2)).toFixed(2));

                }


            }
        });
    }

    function remove(id) {
        if (confirm("Are you sure, you want to delete the voucher?")) {
            $('#voucherRemove' + id).submit();
        }
    }

    function getParameterByName(name, url) {
        if (!url)
            url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results)
            return null;
        if (!results[2])
            return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $(document).ready(function () {
        console.log('test')
        if (getParameterByName('id')) {
            $('html,body').animate({
                    scrollTop: $("#" + getParameterByName('id')).offset().top
                },
                'slow');
        }

    });
    $(document).ready(function () {
        $('#dataTable').DataTable({
            searchDelay: 900,
            "processing": true,
            "serverSide": true,
            "order": [[2, "desc"]],
            "pageLength": 10,
            "ajax": "${contextPath}/Payments/json"
        });
    });

    function addVoucherForm() {
/*        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $(document).ajaxSend(function (e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });*/
        var fd = $("#voucherForm").serialize();
        $.ajax({
            type: 'POST',
            async: false,
            url: '${contextPath}/Account/ajax/addVoucher',
            data: fd + "&prefix=BP",
            timeout: 600000,
            success: function (data) {
                console.log(form_action);
                if (form_action == "edit") {
                    refreshPage();
                }
                else {
                    refreshPage();
                }
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }

  function updateForm(voucherNumber) {
        $.ajax({
            url: '${contextPath}/Account/ajax/getAllChassisList',
            contentType: 'application/json',
            success: function (chassisMap) {
                $('#apartment').empty();
                 var chassisSelect = document.getElementById('apartment');
                    var modelOpt = document.createElement('option');
                    modelOpt.innerHTML = 'NIL';
                    modelOpt.value = 0;
                    chassisSelect.appendChild(modelOpt);
                 Object.keys(chassisMap).forEach(function (key) {
                    var modelOpt = document.createElement('option');
                    modelOpt.innerHTML = chassisMap[key];
                    modelOpt.value = key;
                    chassisSelect.appendChild(modelOpt);
                });
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
        $('#myModal').find('form').trigger('reset');
        $('#taxForm').hide();
        $('#voucherNumber').val(voucherNumber);
    }

    function refreshPage() {
        window.location.reload();
    }

    function edit(voucherNumber) {
        console.log("hello");
        $.ajax({
            url: '${contextPath}/Account/ajax/editVoucher',
            contentType: 'application/json',
            data: {
                voucherId: voucherNumber

            },
            success: function (response) {
                editForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }

    function editForm(response) {
        $("#myModal").modal();
        $('#myModal').find('form').trigger('reset');
        $('#myModal').find('form').find('#voucherNumber').val(response.voucherNumber);
        $('#myModal').find('form').find('#bankAccount').val(response.bankAccount);
        $('#myModal').find('form').find('#accountAmount').val(response.accountCode);
        $('#myModal').find('form').find('#costCentre').val(response.costCentre);
        $('#myModal').find('form').find('#taxDrop').val(response.taxAccount);
        $('#myModal').find('form').find('#billAmount').val(response.billAmount);
        $('#myModal').find('form').find('#voucherDate').val(response.voucherDate);
        $('#myModal').find('form').find('#chequeNumber').val(response.chequeNumber);
        $('#myModal').find('form').find('#taxAmount').val(response.taxAmount);
        $('#myModal').find('form').find('#debit').val(response.debit);
        $('#myModal').find('form').find('#remarks').val(response.remarks);
        $('#apartment').val(response.apartmentNumber).trigger('change.select2');
        $('#billAmount').trigger("blur");
        changeTaxAmount(response.taxRate);

    }

    function getBalanceAmount(accountCode) {
        console.log("hello");
        $.ajax({
            url: '${contextPath}/Account/ajax/getAmountBalance',
            contentType: 'application/json',
            data: {
                accountCode: accountCode,
                prefix: "BP"

            },
            success: function (response) {
                $('#balance').val(response[0]);
                $('#balance').text(response[0]);
                $('#limit').val(response[1]);
                $('#limit').text(response[1]);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
  function getSalesman(accountCode) {
        console.log("success");
        $.ajax({
            url: '${contextPath}/Account/ajax/getSalesman',
            contentType: 'application/json',
            data: {
                accountCode: accountCode
            },
            success: function (salesmanList) {
                $("#salesman").empty();
                 var salesmanSelect = document.getElementById('salesman');
                for(salesman in salesmanList){
                    var salesmanOpt = document.createElement('option');
                    salesmanOpt.innerHTML =  salesmanList[salesman].name;
                    salesmanOpt.value =  salesmanList[salesman].distributorId;
                    salesmanSelect.appendChild(salesmanOpt);
                }
                if(salesmanList.length >0){
                    $('#salesmanDiv').css("display", "block");
                     getModels($('#salesman').val());
                }
                else{
                    $('#salesmanDiv').css("display", "none");
                    $('#makerDiv').css("display", "none");
                    $("#apartment").val(0).change();
                }


            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }

     function getModels(salesman) {
        $.ajax({
            url: '${contextPath}/Account/ajax/getStockListForSalesman',
            contentType: 'application/json',
            data: {
                salesman: salesman,
                accountCode: $('#accountAmount').val()
            },
            success: function (stockMap) {
                $("#itemName").empty();

                 var itemSelect = document.getElementById('itemName');
                 Object.keys(stockMap).forEach(function (key) {
                    var modelOpt = document.createElement('option');
                    value = stockMap[key]
                    modelOpt.innerHTML = value;
                    modelOpt.value = key;
                    itemSelect.appendChild(modelOpt);
                });
                console.log(Object.keys(stockMap).length);
                if(Object.keys(stockMap).length >0){
                    $('#makerDiv').css("display", "block");
                    getChassisList($('#itemName').val());
                }
                else{
                    $('#makerDiv').css("display", "none");
                }
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
      function getChassisList(stockId) {
        $.ajax({
            url: '${contextPath}/Account/ajax/getChassisListForSalesman',
            contentType: 'application/json',
                data: {
                   stockId: stockId,
               accountCode: $('#accountAmount').val(),
               salesman: $('#salesman').val()

            },
            success: function (chassisMap) {
                $('#apartment').empty();
                 var chassisSelect = document.getElementById('apartment');
                 Object.keys(chassisMap).forEach(function (key) {
                    var modelOpt = document.createElement('option');
                    value = chassisMap[key]
                    modelOpt.innerHTML = value;
                    modelOpt.value = key;
                    chassisSelect.appendChild(modelOpt);
                });
                $("#billAmount").val(0.00).trigger('blur') ;
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
     function getChassisDetail() {
       if($('#accountAmount').val()!=$('#suzukiAccount').val()){
       //getChassisListForSelectedChassis();
       }
    }
    function getChassisListForSelectedChassis(){
     $.ajax({
                url: '${contextPath}/Account/ajax/getChassisDetail',
                contentType: 'application/json',
                data: {
                   apartmentNo: $('#apartment').val()

                },
                success: function (apartment) {

                    $("#salesman").empty();
                    $("#itemName").empty();

                    if(apartment.apartmentId==0){
                         $('#salesmanDiv').css("display", "none");
                         $('#makerDiv').css("display", "none");
                    }
                    else{
                    var accountSelect = document.getElementById('accountAmount');
                     var salesmanSelect = document.getElementById('salesman');
                     var modelSelect = document.getElementById('itemName');
                        var salesmanOpt = document.createElement('option');
                        salesmanOpt.innerHTML = apartment.salesmanName;
                        salesmanOpt.value = apartment.distributorId;
                        salesmanSelect.appendChild(salesmanOpt);

                        var modelOpt = document.createElement('option');
                        modelOpt.innerHTML = apartment.itemName;
                        modelOpt.value = apartment.stockId;
                        modelSelect.appendChild(modelOpt);
                         forChassis = "true";
                        $("#accountAmount").val(apartment.customerAccount).trigger('change', {'forChassis':true}) ;
                        $("#billAmount").val(apartment.sellingPrice).trigger('blur') ;
                         $('#salesmanDiv').css("display", "block");
                         $('#makerDiv').css("display", "block");
                     }


                },
                error: function (e) {

                    console.log("ERROR : ", e);

                }
            });

    }
    function limitValidation() {

        console.log(parseInt($('#balance').html()));
        console.log(parseInt($('#limit').html()));
        if (parseInt($('#balance').html()) > parseInt($('#limit').html()) && parseInt($('#limit').html()) != 0) {
            alert("OnHold amount exceeds for this Vendor. OnHold amount was "+ ($('#limit').html()) );
            $('#btn_submit').prop('disabled', true)
        }
        else {
            $('#btn_submit').prop('disabled', false)
        }

    }

    function goBack(){
    window.location = "${contextPath}/home#transactionFile";
    }

</script>
</body>
</html>