<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Payment Plan - Floor</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">

</head>

<body>

<%@ include file="../header.jsp" %>

<div class="container">
    <form method="get" action="${contextPath}/Plan/viewFloorPlan"
          class="form-inline">

        <div class="ledgerReportHeaderFilterSec">
            <h3>Payment Plan </h3>

            <div class="ledgerReportHeaderFilterBlockSp">
                <div class="ledgerReportHeaderFilterBlock">
                    <label> Select Model :</label>
                    <select name="stockId" style="width:350px" class="form-control ">

                        <option value="0">NONE</option>
                        <c:forEach items="${modelList}" var="flatModel">
                            <c:choose>
                                <c:when test="${flatModel.stockId == currentStockId}">
                                    <option value="${flatModel.stockId}"
                                            selected>${flatModel.stockAccount.title}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${flatModel.stockId}">${flatModel.stockAccount.title}</option>

                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <label> Floor :</label>
                    <select name="floor" style="width:300px" class="form-control ">

                        <option value="">NONE</option>
                        <c:forEach items="${floorList}" var="floor">
                            <c:choose>
                                <c:when test="${floor == currentFloor}">
                                    <option value="${floor}" selected>${floor}</option>

                                </c:when>
                                <c:otherwise>
                                    <option value="${floor}">${floor}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <input type="submit" class="btn btn-primary pull-right" value="Apply"/>
            </div>

        </div>

    </form>
    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn">Print
            </button>
        </div>

    </div>


</div>

<section class="main">
    <div class="container" id="printDiv">
        <div class="row" style="page-break-before: always">
            <div class="col-sm-12" style="text-align: center">
                <img src="${contextPath}/resources/img/Building.jpg" width="100%" height="140%"/>
                <h1>Company</h1>
                <h3>DG KHAN</h3>
                <h4>Payment Plan for floor : ${currentFloor}</h4>
            </div>
        </div>
        <div class="row" style="page-break-before: always">
            <div class="col-sm-12" style="text-align: center">
                <img src="${contextPath}/resources/img/BuildingNight.jpg" width="100%" height="140%"/>
                <h1>Company</h1>
                <h3>DG KHAN</h3>
                <h4>Payment Plan for floor : ${currentFloor}</h4>
            </div>
        </div>

        <c:forEach items="${PaymentPlanModelList}" var="paymentPlan">
            <div class="row" style="page-break-before: always">
                <div class="col-sm-12">
                    <div class="col-md-6" style="width: 60% ; float: left">
                        <h3> Payment Plan for floor : ${currentFloor}</h3>
                        <h4>Model : ${paymentPlan.apartment.stock.stockAccount.title}</h4>
                        <h4>Net Price : Rs <fmt:formatNumber type="number" maxFractionDigits="2" value="${paymentPlan.apartment.sellingPrice}"/></h4>
                        <h4>No. of Rooms : ${paymentPlan.apartment.rooms} BEDROOM(s)</h4>
                    </div>
                    <div class="col-md-6" style="width: 40% ; float: left">
                        <img src="${contextPath}/resources/img/bahria.jpg" class="pull-right"/>
                    </div>
                    <h3> Down Payments</h3>
                    <div class="main-table">
                        <table class="table tableLedger">
                            <thead>
                            <tr>
                                <th width="60%">Title</th>
                                <th width="20%">Due Date</th>
                                <th width="20%">Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:set var="totalBalance" value="${0}"/>
                            <c:forEach items="${paymentPlan.downPaymentList}" var="downPayment" varStatus="status">
                                <tr>
                                    <td>${downPayment.title}</td>
                                    <c:choose>
                                    <c:when test="${status.index == 0}">
                                        <td>Current Month</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>Next Month</td>
                                    </c:otherwise>
                                    </c:choose>
                                    <td>Rs. <fmt:formatNumber type="number" maxFractionDigits="2"
                                                              value="${downPayment.amount}"/> /=
                                    </td>
                                </tr>
                                <c:set var="totalBalance" value="${totalBalance + downPayment.amount}"/>
                            </c:forEach>
                            <tr>
                                <td style="font-weight: bold"> Total :</td>
                                <td></td>
                                <td>Rs. <fmt:formatNumber type="number" maxFractionDigits="2" value="${totalBalance}"/>
                                    /=
                                </td>
                            </tr>

                            </tbody>

                        </table>
                    </div>
                    <h3> Installments </h3>
                    <div class="main-table">
                        <table class="table tableLedger">
                            <thead>
                            <tr>
                                <th width="60%">Title</th>
                                <th width="20%">Due Date</th>
                                <th width="20%">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>${paymentPlan.installmentMessage}</td>
                                <td>10th of Quarter</td>
                                <td>Rs. <fmt:formatNumber type="number" maxFractionDigits="2" value="${paymentPlan.installmentTotal}"/> /=
                                </td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                    <div id="imageDiv">
                        <img src="${paymentPlan.imageUrl}" alt="Model Image" height="65%" width="100%"/>
                    </div>
                    <div style="margin-top: 20px">
                        <p>Note : Utility deployment payments are not included and will be taken at the time of
                            deployment.</p>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</section>


<!-- /container -->

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-datepicker.js"></script>
<script>

    $(document).ready(function () {
        $('#printBtn').click(function () {
            printDiv();

        });

    })
</script>
<script>

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '  <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 50000);

    }

    function goBack() {
        window.location = "${contextPath}/home";
    }
</script>

</body>
</html>