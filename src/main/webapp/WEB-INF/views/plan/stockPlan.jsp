<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Flat Payment Plan</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">
    ${duplicateOrderNumber}
    <h3 class="form-signin-heading">Payment Plan - ${paymentPlan.planStock.stockAccount.title}</h3>
    <form:form method="POST" modelAttribute="paymentPlan" id="formid" Class="form-inline formSaleVoucher formWidthDef">
        <div class="container">
            <div class="pull-left">
                <div class="form-Row">
                    <spring:bind path="title">
                        <form:label path="title">Plan Title</form:label>
                        <form:input type="text" cssStyle="width: 420px" path="title" required="true"
                                    cssClass="form-control" placeholder="Payment Plan title"
                                    autofocus="true"></form:input>
                    </spring:bind>

                </div>
            </div>

            <div class="pull-left" style="margin-left: 40px">
                <spring:bind path="installmentRate">
                    <form:label path="installmentRate">Installment Rate</form:label>
                    <form:input cssStyle="width: 150px" type="number" path="installmentRate" cssClass="form-control"
                                autofocus="true" step ="any"  min="0" max="100" ></form:input>
                </spring:bind>
            </div>

        </div>

        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>

                    <th width="5%">Check</th>
                    <th>Title</th>
                    <th>Down Payment Rate(%)</th>
                    <th>Order</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${paymentPlan.paymentPlanDownList}" var="plan" varStatus="planStatus">
                    <tr>
                        <td>
                            <form:checkbox  autofocus="true" path="paymentPlanDownList[${planStatus.index}].check" cssClass="form-control"></form:checkbox>
                        </td>
                        <td>
                            <form:input path="paymentPlanDownList[${planStatus.index}].title" type="text" id="title${productStatus.index}"
                                        class="form-control" placeholder="Down Payment title (if Any)"></form:input>
                        </td>
                        <td>
                            <form:input type="number"
                                        id="itemCode${productStatus.index}"
                                        path="paymentPlanDownList[${planStatus.index}].rate"
                                        class="form-control"  step ="any"  min="0" max="100"></form:input>
                        </td>
                        <td>
                            <form:input type="number" required="true"
                                        path="paymentPlanDownList[${planStatus.index}].order"
                                        class="form-control" max="7" min="0" ></form:input>

                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {
    });


    function goBack(){
        window.location = "${contextPath}/Stock/addStock";
    }



</script>
</body>
</html>
