<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Payment Plan - ${paymentPlan.apartment.apartmentNo}</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<%@ include file="../header.jsp" %>

<div class="container">

    <h2>Payment Plan</h2>

    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn">Print
            </button>
        </div>

    </div>


</div>

<section class="main">
    <div class="container" id="printDiv">
        <div class="row" style="page-break-after: always">
            <div class="col-sm-12" style="text-align: center">
                <img src="${contextPath}/resources/img/Building.jpg" width="100%" height="140%"/>
                <h1>Company</h1>
                <h3>DG KHAN</h3>
                <h4>Payment Plan for Apartment : ${paymentPlan.apartment.apartmentNo}</h4>
            </div>
        </div>
        <div class="row" style="page-break-after: always">
            <div class="col-sm-12" style="text-align: center">
                <img src="${contextPath}/resources/img/BuildingNight.jpg" width="100%" height="140%"/>
                <h1>Company</h1>
                <h3>DG KHAN</h3>
                <h4>Payment Plan for Apartment : ${paymentPlan.apartment.apartmentNo}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-md-6" style="width: 60% ; float: left">
                    <h4>Apartment : ${paymentPlan.apartment.apartmentNo}</h4>
                    <h4>Model : ${paymentPlan.apartment.stock.stockAccount.title}</h4>
                    <h4>Sale Date : <fmt:formatDate type="date" value="${paymentPlan.apartment.date}"/></h4>
                    <h4>Net Price : Rs <fmt:formatNumber type="number" maxFractionDigits="2" value="${paymentPlan.apartment.sellingPrice}"/></h4>
                    <h4>Floor : ${paymentPlan.apartment.floor}</h4>
                    <h4>No. of Rooms : ${paymentPlan.apartment.rooms} BEDROOM(s)</h4>
                    <h4>Customer : ${paymentPlan.customer.title}</h4>
                    <h3>Down Payments</h3>
                </div>
                <div class="col-md-6" style="width: 40% ; float: left">
                    <img src="${contextPath}/resources/img/bahria.jpg" class="pull-right"/>
                </div>
                <div class="main-table">
                    <table class="table tableLedger">
                        <thead>
                        <tr>
                            <th width="60%">Title</th>
                            <th width="20%">Due Date</th>
                            <th width="20%">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="totalBalance" value="${0}"/>
                        <c:forEach items="${paymentPlan.downPaymentList}" var="downPayment" varStatus="status">
                            <tr>
                                <td>${downPayment.title}</td>
                                <td><fmt:formatDate type="date" value="${downPayment.dueDate}"/></td>
                                <td>Rs. <fmt:formatNumber type="number" maxFractionDigits="2" value="${downPayment.amount}"/> /=</td>
                            </tr>
                            <c:set var="totalBalance" value="${totalBalance + downPayment.amount}"/>
                        </c:forEach>
                        <tr>
                            <td style="font-weight: bold"> Total :</td>
                            <td></td>
                            <td>Rs. <fmt:formatNumber type="number" maxFractionDigits="2" value="${totalBalance}"/> /=</td>
                        </tr>

                        </tbody>

                    </table>
                </div>
                <h3> Installments </h3>
                <div class="main-table">
                    <table class="table tableLedger">
                        <thead>
                        <tr>
                            <th width="60%">Title</th>
                            <th width="20%">Due Date</th>
                            <th width="20%">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="totalBalance" value="${0}"/>
                        <c:forEach items="${paymentPlan.installmentList}" var="installment" varStatus="status">
                            <tr>
                                <td>${installment.title}</td>
                                <td><fmt:formatDate type="date" value="${installment.dueDate}"/></td>
                                <td>Rs. <fmt:formatNumber type="number" maxFractionDigits="2" value="${installment.amount}"/> /=</td>
                            </tr>
                            <c:set var="totalBalance" value="${totalBalance + installment.amount}"/>
                        </c:forEach>
                        <tr>
                            <td style="font-weight: bold"> Total :</td>
                            <td></td>
                            <td>Rs. <fmt:formatNumber type="number" maxFractionDigits="2" value="${totalBalance}"/> /=</td>
                        </tr>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        <div id="imageDiv" style="page-break-before: always">
            <img src="${paymentPlan.imageUrl}" alt="Model Image" height="65%" width="100%"/>
        </div>
    </div>
</section>


<!-- /container -->

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap-datepicker.js"></script>
<script>

    $(document).ready(function () {
        $('#printBtn').click(function () {
            printDiv();

        });

    })
</script>
<script>

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');
        var ledgerReportDataInfo = document.getElementById('ledgerReportDataInfo');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '  <link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 50000);

    }

    function goBack() {
        window.location = "${contextPath}/home";
    }
</script>

</body>
</html>