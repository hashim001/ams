<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Bank Details</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        ${bankRemoveSuccess}
        ${AccountAddSuccess}
        <h2 class="heading-main">Bank Master Files<span class="addIcon" data-toggle="modal"
                  data-target="#myModal2"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Bank Name </br> Branch Name</th>
                            <th>Bank Account Number</br> Branch Code</th>
                            <th>Contact Person</br> Mobile Number</th>
                            <th>Landline#</th>
                            <th>Bank URL</br> Email Address</th>
                            <th>Bank NTN</br> opening Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${bankDetailList}" var="bank" varStatus="status">
                            <tr>
                                <td>${bank.account.title} </br>${bank.bankBranch}</td>
                                <td>${bank.bankAccount} </br>${bank.branchCode}</td>
                                <td>${bank.bankContactPerson} </br>${bank.mobileNumber}</td>
                                <td>${bank.bankLandline}</td>
                                <td>${bank.url} </br>${bank.emailAddress}</td>
                                <td>${bank.bankNtn} </br>${bank.accountOpeningDate}</td>
                                <td>
                                    <form method="post" id="bankRemove${status.index}"
                                          action="${contextPath}/Account/removeBank?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="accountCode"
                                               value="${bank.account.accountCode}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a title="Edit" href="#" data-toggle="modal" data-target="#myModal"
                                               onclick="edit(${bank.account.accountCode})"><img
                                                src="${contextPath}/resources/img/editIcon.png" alt=""></a></li>
                                        <li><a title="Delete" href="#" onclick="remove(${status.index})"><img src="${contextPath}/resources/img/deleteIcon.png" alt=""></a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Bank Details</h2>
            </div>
            <form:form method="POST" modelAttribute="accountForm" action="${contextPath}/AccountSupport/addBankAccount">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="title">
                                <form:label path="title">Account Title</form:label>
                                <form:input type="text" path="title" placeholder="Account Title"
                                            autofocus="true" required="true" ></form:input>
                                <form:errors path="title"></form:errors>

                            </spring:bind>
                            <spring:bind path="parentCode">

                                <form:input type="hidden" path="parentCode" value="${bankHead}"
                                            autofocus="true"></form:input>
                                <form:errors path="parentCode"></form:errors>

                            </spring:bind>

                            <spring:bind path="openingBalance">
                                <form:label path="openingBalance">Opening Balance </form:label>
                                <form:input id="openingBalance" onblur="toggleAccountType()" type="text"
                                            path="openingBalance" placeholder="Opening Balance"></form:input>
                                <form:errors path="openingBalance"></form:errors>

                            </spring:bind>


                            <label>Select Account Type </label>
                            <select id="accountType">
                                <option id="credit" value="credit">Credit</option>
                                <option id="debit" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingBalanceLast">

                                <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                <form:input id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text"
                                            path="openingBalanceLast"
                                            placeholder="Last Opening Balance"></form:input>
                                <form:errors path="openingBalanceLast"></form:errors>

                            </spring:bind>
                            <label>Select Account Type Last </label>
                            <select id="accountTypeLast">
                                <option id="creditLast" value="credit">Credit</option>
                                <option id="debitLast" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingCredit">
                                <form:input type="hidden" path="openingCredit" id="openingCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="openingDebit">

                                <form:input type="hidden" path="openingDebit" id="openingDebit"
                                            value="0.0"></form:input>

                            </spring:bind>

                            <spring:bind path="accountCodeForEdit">

                                <form:input id="accountCode" type="hidden" path="accountCodeForEdit"
                                            value="0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningCredit">
                                <form:input type="hidden" path="lastOpeningCredit" id="lastOpeningCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningDebit">

                                <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="level">

                                <form:input id="level" type="hidden" path="level"
                                            value="0"></form:input>

                            </spring:bind>

                            <spring:bind path="openingDate">
                                <form:label path="openingDate">Opening Date</form:label>
                                <form:input type="Date" path="openingDate" value="${currentDate}"
                                            required="required"></form:input>
                                <form:errors path="openingDate"></form:errors>

                            </spring:bind>


                            <spring:bind path="bankDetails.bankAccount">

                                <form:label path="bankDetails.bankAccount">Bank Account</form:label>
                                <form:input id="bankAccount" type="text" path="bankDetails.bankAccount" placeholder="Bank Account Number"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankAccount"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankDetails.bankBranch">

                                <form:label path="bankDetails.bankBranch">Branch Name</form:label>
                                <form:input id="bankBranch" type="text" path="bankDetails.bankBranch" placeholder="Branch Name"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankBranch"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankDetails.bankContactPerson">

                                <form:label path="bankDetails.bankContactPerson">Contact Person</form:label>
                                <form:input id="bankContactPerson" type="text" path="bankDetails.bankContactPerson"
                                            placeholder="Contact Person"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankContactPerson"></form:errors>

                            </spring:bind>
                        </div>

                        <div class="col-sm-6">
                            <spring:bind path="bankDetails.mobileNumber">

                                <form:label path="bankDetails.mobileNumber">Mobile Number</form:label>
                                <form:input id="mobileNumber" type="text" path="bankDetails.mobileNumber"
                                            placeholder="Mobile Number"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.mobileNumber"></form:errors>

                            </spring:bind>

                            <spring:bind path="bankDetails.bankLandline">

                                <form:label path="bankDetails.bankLandline">Landline#</form:label>
                                <form:input id="bankLandline" type="text" path="bankDetails.bankLandline"
                                            placeholder="Landline number"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankLandline"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankDetails.emailAddress">

                                <form:label path="bankDetails.emailAddress">Email Address</form:label>
                                <form:input id="emailAddress" type="text" path="bankDetails.emailAddress"
                                            placeholder="Email Address"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.emailAddress"></form:errors>

                            </spring:bind>
                        </div>
                        <div class="col-sm-6">

                            <spring:bind path="bankDetails.scanForm">

                                <form:label path="bankDetails.scanForm">Scan Copy</form:label>
                                <form:input id="scanForm" type="text" path="bankDetails.scanForm" placeholder="Scan Copy"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.scanForm"></form:errors>

                            </spring:bind>

                            <spring:bind path="bankDetails.accountOpeningDate">

                                <form:label path="bankDetails.accountOpeningDate">Opening Date</form:label>
                                <form:input id="accountOpeningDate" type="date" path="bankDetails.accountOpeningDate" value="${currentDate}"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.accountOpeningDate"></form:errors>

                            </spring:bind>

                            <spring:bind path="bankDetails.branchCode">

                                <form:label path="bankDetails.branchCode">Branch Code</form:label>
                                <form:input id="branchCode" type="text" path="bankDetails.branchCode" placeholder="Branch Code"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.branchCode"></form:errors>

                            </spring:bind>
                            <spring:bind path="bankDetails.bankNtn">

                                <form:label path="bankDetails.bankNtn">Bank NTN</form:label>
                                <form:input id="bankNtn" type="text" path="bankDetails.bankNtn" placeholder="Bank NTN"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.bankNtn"></form:errors>

                            </spring:bind>

                            <spring:bind path="bankDetails.url">

                                <form:label path="bankDetails.url">Bank URL</form:label>
                                <form:input id="url" type="text" path="bankDetails.url" placeholder="Bank URL"
                                            autofocus="true"></form:input>
                                <form:errors path="bankDetails.url"></form:errors>

                            </spring:bind>

                        </div>

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function () {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function () {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function () {

            toggleAccountTypeLast();
        });
    });

    function toggleAccountType() {

        if ($('#accountType').val() == "credit") {

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        } else if ($('#accountType').val() == "debit") {

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if ($('#accountTypeLast').val() == "credit") {

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        } else if ($('#accountTypeLast').val() == "debit") {

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }


    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[1, "desc"]],
            "pageLength": 7
        });

    });

    function remove(id) {
        if (confirm("Are you sure, you want to delete?")) {
            $('#bankRemove' + id).submit();
        }


    }

    function goBack(){
     window.location = "${contextPath}/home#masterFile";
    }
       function setAccountType(response)
        {
    		    if(response.accountDto.openingCredit<response.accountDto.openingDebit){
    		   $('#accountType').val('debit');
    		    }
    		    else{
    		    $('#accountType').val('credit');
    		    }

        }
            function edit(accountCode) {

                $.ajax({
                    url: '${contextPath}/Stock/ajax/editBankDetails',
                    contentType: 'application/json',

                    data: {
                        accountCode: accountCode

                    },
                    success: function (response) {
                        editForm(response);
                         setAccountType(response);
                    },

                    error: function (e) {

                        console.log("ERROR : ", e);

                    }
                });
            }
    		    function editForm(response) {
    		    var openingBalance ;
    		    if(response.accountDto.openingCredit>response.accountDto.openingDebit){
    		    openingBalance = response.accountDto.openingCredit;
    		    }
    		    else{
    		    openingBalance = response.accountDto.openingDebit;
    		    }
                 $('#myModal2').modal();
                 $('#myModal2').find('form').trigger('reset');
                 $('#myModal2').find('form').find('#title').val(response.accountDto.title);
                 $('#myModal2').find('form').find('#openingDate').val(response.accountDto.openingDate);
                 $('#myModal2').find('form').find('#openingCredit').val(response.accountDto.openingCredit);
                 $('#myModal2').find('form').find('#openingDebit').val(response.accountDto.openingDebit);
                 $('#myModal2').find('form').find('#lastOpeningCredit').val(response.accountDto.lastOpeningCredit);
                 $('#myModal2').find('form').find('#openingBalanceLast').val(response.accountDto.openingBalanceLast);
                 $('#myModal2').find('form').find('#openingBalance').val(openingBalance);
                 $('#myModal2').find('form').find('#parentCode').val(response.accountDto.parentCode);
                 $('#myModal2').find('form').find('#accountCode').val(response.accountDto.accountCode);
                 $('#myModal2').find('form').find('#mobile').val(response.mobile);
                 $('#myModal2').find('form').find('#landline').val(response.landline);
                 $('#myModal2').find('form').find('#taxStatus').val(response.taxStatus);
                 $('#myModal2').find('form').find('#bankAccount').val(response.bankAccount);
                 $('#myModal2').find('form').find('#bankBranch').val(response.bankBranch);
                 $('#myModal2').find('form').find('#bankContactPerson').val(response.bankContactPerson);
                 $('#myModal2').find('form').find('#mobileNumber').val(response.mobileNumber);
                 $('#myModal2').find('form').find('#bankLandline').val(response.bankLandline);
                 $('#myModal2').find('form').find('#emailAddress').val(response.emailAddress);
                 $('#myModal2').find('form').find('#authorizedPersons').val(response.authorizedPersons);
                 $('#myModal2').find('form').find('#scanForm').val(response.scanForm);
                 $('#myModal2').find('form').find('#accountOpeningDate').val(response.accountOpeningDate);
                 $('#myModal2').find('form').find('#branchCode').val(response.branchCode);
                 $('#myModal2').find('form').find('#bankNtn').val(response.bankNtn);
                 $('#myModal2').find('form').find('#url').val(response.url);
                 $('#myModal2').find('form').find('#level').val(response.accountDto.level);

            }


</script>
</body>
</html>