<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Manage Report</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../navigation.jsp" %>

<div class="container">
    ${AccountNoteAddSuccess}
    ${accountNoteRemoveSuccess}

    <div class="row">
        <div class="col-md-6">
            <form:form method="POST" modelAttribute="AccountReportForm" class="form-signin">
            <h3 class="form-signin-heading">Account Report ${AccountReportForm.title}</h3>
            <spring:bind path="title">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:label path="title">Heading title</form:label>
                    <form:input type="text" path="title" class="form-control"
                                autofocus="true"></form:input>
                    <form:errors path="title"></form:errors>
                </div>
            </spring:bind>


            <spring:bind path="reportType">
                <div class="form-group">
                    <form:label path="reportType">Report Type</form:label>
                    <form:select path="reportType" class="form-control" >

                        <form:option value="ProfitAndLoss">Profit And Loss</form:option>
                        <form:option value="BalanceSheet">Balance Sheet</form:option>

                    </form:select>

                </div>
            </spring:bind>

        </div>
        <div class="col-md-6 form-signin">
            <spring:bind path="printSeq">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:label path="printSeq">Printing Sequence</form:label>
                    <form:input type="text" path="printSeq" class="form-control" placeholder="Printing Sequence"
                                autofocus="true"></form:input>
                    <form:errors path="printSeq"></form:errors>
                </div>
            </spring:bind>

            <spring:bind path="date">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <form:label path="date">Creation Date</form:label>
                    <form:input type="Date" path="date" class="form-control" value = "${currentDate}" required = "required"></form:input>
                    <form:errors path="date"></form:errors>
                </div>
            </spring:bind>

            <spring:bind path="subTotal">
                <form:label path="subTotal">Is Sub-Total</form:label>
                <form:checkbox path="subTotal"  />
            </spring:bind>

            <spring:bind path="total">
                <form:label path="total">Is Total</form:label>
                <form:checkbox path="total"  />
            </spring:bind>

            <spring:bind path="working">
                <form:label path="working">Is Working</form:label>
                <form:checkbox path="working"  />
            </spring:bind>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Commit</button>
            </form:form>
        </div>


    </div>
</div>
<div class="container">
    <h3>Report Notes</h3>

    <table border="1px black" class="table table-condensed" >
        <thead>
        <tr>
            <th width="25%">Heading</th>
            <th width="25%">Add Subheading</th>
            <th>Sub-headings</th>


        </tr>
        </thead>
        <tbody>
        <c:forEach items="${AccountReportForm.accountNoteList}" var="note" varStatus="status">

            <tr>
                <td><form:input cssStyle="width: 100%" type="text" path="AccountReportForm.accountNoteList[${status.index}].heading"   autofocus="true"></form:input>
                </td>
                <td >
                    <form method="post" action="${contextPath}/User/addReportNoteSubHead?${_csrf.parameterName}=${_csrf.token}" >
                        <select name="subId" style="width: 80%;">
                            <c:forEach items="${subHeadList}" var="subHead" >
                                <option value="${subHead.subId}">${subHead.heading}</option>
                            </c:forEach>
                        </select>
                        <input  type="hidden" name="noteId" value="${note.noteId}"/>
                        <input  type="hidden" name="reportId" value="${AccountReportForm.stageId}"/>
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <button class="btn btn-xs btn-primary" type="submit">Add</button>
                    </form></br>
                </td>

                <td ><c:forEach items="${note.noteSubHeadings}" var="noteSubhead">
                    <form method="post" action="${contextPath}/User/removeReportNoteSubHead?${_csrf.parameterName}=${_csrf.token}" >
                            ${noteSubhead.heading}<input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <input  type="hidden" name="subId" value="${noteSubhead.subId}"/>
                        <input  type="hidden" name="noteId" value="${note.noteId}"/>
                        <input class="btn btn-xs btn-danger pull-right" type="submit" value="Remove"/>
                    </form></br>
                </c:forEach>
                </td>



            </tr>
        </c:forEach>

        <tr>

            <td>
                <form method="POST" action="${contextPath}/User/addReportNote?${_csrf.parameterName}=${_csrf.token}">

                    <input type="text" name="heading"  placeholder="Heading" />

                    <input  type="hidden" name="reportId" value="${AccountReportForm.stageId}"/>
                    <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                    <input class="btn btn-xs btn-primary pull-right" type="submit" value="Add" />
                </form>
            </td>
            <td></td>
            <td></td>

        </tr>

        </tbody>
    </table>

</div>


<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function() {

    });



</script>
</body>
</html>
