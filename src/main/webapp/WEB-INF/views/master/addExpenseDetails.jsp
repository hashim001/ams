<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Expense</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${expenseAddSuccess}
        ${expenseRemoveSuccess}
        ${AccountAddSuccess}
        <h2 class="heading-main">Expense<span class="addIcon" data-toggle="modal"
                  data-target="#myModal2"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${expenses}" var="expense" varStatus="expenseStatus">

                            <tr>
                                <td>${expense.expenseAccount.title}</td>
                                <td>${expense.description}</td>
                                <td>
                                    <form method="post" id="expenseRemove${expenseStatus.index}"
                                          action="${contextPath}/Account/removeExpense?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="accountCode"
                                               value="${expense.expenseAccount.accountCode}"/>
                                    </form>
                                        <%--<form method="post" id="customerEdit${expenseStatus.index}"--%>
                                        <%--action="${contextPath}/Stock/editStock?${_csrf.parameterName}=${_csrf.token}">--%>
                                        <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
                                        <%--<input type="hidden" name="stockId" value="${item.stockId}"/>--%>
                                        <%--</form>--%>
                                    <ul class="list-inline">
                                        <li><a href="#" onclick="edit(${expense.expenseAccount.accountCode});"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                        <li><a href="#" onclick="remove(${expenseStatus.index});"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Expense Details</h2>
            </div>
            <form:form method="POST" modelAttribute="accountForm" action="${contextPath}/AccountSupport/addExpenseAccount">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="title">
                                <form:label path="title">Account Title</form:label>
                                <form:input type="text" path="title" placeholder="Account Title"
                                            autofocus="true"></form:input>
                                <form:errors path="title"></form:errors>

                            </spring:bind>
                            <spring:bind path="parentCode">

                                <form:input type="hidden" path="parentCode" value="${expenseHead}"
                                            autofocus="true"></form:input>
                                <form:errors path="parentCode"></form:errors>

                            </spring:bind>

                            <spring:bind path="openingBalance">
                                <form:label path="openingBalance">Opening Balance </form:label>
                                <form:input id="openingBalance" onblur="toggleAccountType()" type="text"
                                            path="openingBalance" placeholder="Opening Balance"></form:input>
                                <form:errors path="openingBalance"></form:errors>

                            </spring:bind>


                            <label>Select Account Type </label>
                            <select id="accountType">
                                <option id="credit" value="credit">Credit</option>
                                <option id="debit" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingBalanceLast">

                                <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                <form:input id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text"
                                            path="openingBalanceLast"
                                            placeholder="Last Opening Balance"></form:input>
                                <form:errors path="openingBalanceLast"></form:errors>

                            </spring:bind>

                            <spring:bind path="openingCredit">
                                <form:input type="hidden" path="openingCredit" id="openingCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="openingDebit">

                                <form:input type="hidden" path="openingDebit" id="openingDebit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="level">

                                <form:input id="level" type="hidden" path="level"
                                            value="0"></form:input>

                            </spring:bind>
                            <spring:bind path="accountCodeForEdit">

                                <form:input id="accountCode" type="hidden" path="accountCodeForEdit"
                                            value="0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningCredit">
                                <form:input type="hidden" path="lastOpeningCredit" id="lastOpeningCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningDebit">

                                <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit"
                                            value="0.0"></form:input>

                            </spring:bind>

                        </div>

                        <div class="col-sm-6">
                            <label>Select Account Type Last </label>
                            <select id="accountTypeLast">
                                <option id="creditLast" value="credit">Credit</option>
                                <option id="debitLast" value="debit">Debit</option>

                            </select>
                            <spring:bind path="openingDate">
                                <form:label path="openingDate">Opening Date</form:label>
                                <form:input type="Date" path="openingDate" value="${currentDate}"
                                            required="required"></form:input>
                                <form:errors path="openingDate"></form:errors>

                            </spring:bind>

                            <spring:bind path="expenseDetail.description">

                                <form:label path="expenseDetail.description">Description</form:label>
                                <form:input id="description" type="text" path="expenseDetail.description" placeholder="Description"
                                            autofocus="true"></form:input>
                                <form:errors path="expenseDetail.description"></form:errors>

                            </spring:bind>

                            <spring:bind path="expenseDetail.expenseNtn">

                                <form:label path="expenseDetail.expenseNtn">NTN #</form:label>
                                <form:input id="expenseNtn" type="text" path="expenseDetail.expenseNtn" placeholder="NTN"
                                            autofocus="true"></form:input>
                                <form:errors path="expenseDetail.expenseNtn"></form:errors>

                            </spring:bind>


                        </div>

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>

    $(document).ready(function () {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function () {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function () {

            toggleAccountTypeLast();
        });
    });

    function toggleAccountType() {

        if ($('#accountType').val() == "credit") {

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        } else if ($('#accountType').val() == "debit") {

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if ($('#accountTypeLast').val() == "credit") {

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        } else if ($('#accountTypeLast').val() == "debit") {

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }



    function remove(id) {
        if (confirm("Are you sure, you want to delete?")) {
            $('#expenseRemove' + id).submit();
        }

    }

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });
    function goBack(){
     window.location = "${contextPath}/home#masterFile";
    }
       function setAccountType(response)
        {
    		    if(response.accountDto.openingCredit<response.accountDto.openingDebit){
    		   $('#accountType').val('debit');
    		    }
    		    else{
    		    $('#accountType').val('credit');
    		    }

        }
            function edit(accountCode) {

                $.ajax({
                    url: '${contextPath}/Stock/ajax/editExpense',
                    contentType: 'application/json',

                    data: {
                        accountCode: accountCode

                    },
                    success: function (response) {
                        editForm(response);
                         setAccountType(response);
                    },

                    error: function (e) {

                        console.log("ERROR : ", e);

                    }
                });
            }
    		    function editForm(response) {
    		    var openingBalance ;
    		    if(response.accountDto.openingCredit>response.accountDto.openingDebit){
    		    openingBalance = response.accountDto.openingCredit;
    		    }
    		    else{
    		    openingBalance = response.accountDto.openingDebit;
    		    }
                 $('#myModal2').modal();
                 $('#myModal2').find('form').trigger('reset');
                 $('#myModal2').find('form').find('#title').val(response.accountDto.title);
                 $('#myModal2').find('form').find('#openingDate').val(response.accountDto.openingDate);
                 $('#myModal2').find('form').find('#openingCredit').val(response.accountDto.openingCredit);
                 $('#myModal2').find('form').find('#openingDebit').val(response.accountDto.openingDebit);
                 $('#myModal2').find('form').find('#lastOpeningCredit').val(response.accountDto.lastOpeningCredit);
                 $('#myModal2').find('form').find('#openingBalanceLast').val(response.accountDto.openingBalanceLast);
                 $('#myModal2').find('form').find('#openingBalance').val(openingBalance);
                 $('#myModal2').find('form').find('#parentCode').val(response.accountDto.parentCode);
                 $('#myModal2').find('form').find('#accountCode').val(response.accountDto.accountCode);
                 $('#myModal2').find('form').find('#mobile').val(response.mobile);
                 $('#myModal2').find('form').find('#expenseNtn').val(response.expenseNtn);
                 $('#myModal2').find('form').find('#description').val(response.description);
                 $('#myModal2').find('form').find('#level').val(response.accountDto.level);
            }




</script>
</body>
</html>
