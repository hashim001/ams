<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Update Return</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">


    <h2 class="form-signin-heading">Purchase Return</h2>
    <div class="container">
        <form class="form-inline formSaleVoucher" style="margin-bottom: 15px; ">
            <div class="pull-left">
                <div class="form-Row">
                    <label>Voucher Number :</label>
                    <input id="voucherNumber" type="text" class="form-control" readonly="true"
                           autofocus="true" value="${voucherNumber}"/></br>
                </div>
            </div>
            <div class="customerInfoSec">
                <label> Supplier Account :</label>
                <select name="account" class="form-control" id="supplierAccount">
                    <c:forEach items="${accounts}" var="account">
                        <c:choose>
                            <c:when test="${account.accountCode == currentVendorAccountCode}">
                                <option value="${account.accountCode}" selected >${account.title}
                                    <c:choose>
                                        <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                        </c:when>
                                    </c:choose>
                                </option>
                            </c:when>
                            <c:otherwise>
                                <option value="${account.accountCode}">${account.title}
                                    <c:choose>
                                        <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                        </c:when>
                                    </c:choose>
                                </option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>


            <div class="form-Row">
                <label>Date : </label>
                <input style="display: none" type="date" class="form-control " id="billDate"
                       autofocus="true" value="${currentDate}" required="required"/>
                <input type="date" class="form-control " id="currentDate"
                       autofocus="true" value="${currentDate}" required="required"/>
            </div>
        </form>

    </div>


    <form:form method="POST" modelAttribute="voucherList" id="formid">
        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>
                    <th>Check</th>
                    <th>Item</th>
                    <th width="15%">Measure Unit</th>
                    <th width="15%">Quantity</th>
                    <th width="15%">Rate</th>
                    <th width="25%">Amount</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${voucherList.voucherList}" var="vouchers" varStatus="status">

                    <tr>

                        <form:input id="voucherNumber" type="hidden" path="voucherList[${status.index}].voucherNumber"
                                    class="form-control"
                                    autofocus="true" value="${voucherNumber}"></form:input>
                        <form:input type="hidden" path="voucherList[${status.index}].billNumber"
                                    class="form-control theBillNumber"
                                    autofocus="true"></form:input>
                        <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].billDate"
                                    class="form-control theBillDate"
                                    autofocus="true" value="${currentDate}" required="required"></form:input>
                        <form:input type="hidden" path="voucherList[${status.index}].accountCode"
                                    class="form-control supplierAccountNumber"
                                    autofocus="true"></form:input>
                        <td>
                            <form:checkbox  cssClass="form-control" autofocus="true" path="voucherList[${status.index}].check" ></form:checkbox>
                        </td>

                        <td>
                            <form:input type="hidden" id ="transitionCode${status.index}" path="voucherList[${status.index}].transitionCode" class="form-control"
                                         onblur="updateValues(this.value,${status.index});" />

                            <form:input type="text" id ="innerItemName${status.index}" path="voucherList[${status.index}].itemName" class="form-control"
                                        onblur="updateValues(this.value,${status.index});" />


                        </td>
                        <td>
                            <input type="text" class="form-control" id="innerMU${status.index}"
                                   autofocus="true" value="null" readonly="true" disabled/>
                        </td>

                        <td>
                            <form:input type="text" id="innerQuantity${status.index}"
                                        path="voucherList[${status.index}].itemQuantity" class="form-control quantity"
                                        onblur="calculateAmount(${status.index})"
                                        autofocus="true" ></form:input>
                            <form:input type="hidden" id="innerMaxQuantity${status.index}"
                                        path="voucherList[${status.index}].balance"
                                        autofocus="true" ></form:input>


                            <form:input type="hidden" id="innerProductId${status.index}"
                                        path="voucherList[${status.index}].productId"
                                        autofocus="true" ></form:input>
                        </td>
                        <td>
                            <form:input type="text" id="innerRate${status.index}"
                                        path="voucherList[${status.index}].itemRate" class="form-control rate"
                                        onblur="calculateAmount(${status.index})" readonly="true"
                                        autofocus="true" ></form:input>
                        </td>
                        <td>
                            <form:input type="text" id="innerAmount${status.index}"
                                        path="voucherList[${status.index}].credit" class="form-control amountTotal"
                                        onblur="calculateQuantity(${status.index})"
                                        autofocus="true" ></form:input>
                        </td>


                        <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].voucherDate"
                                    class="form-control theDate"
                                    autofocus="true" value="${currentDate}" required="required"></form:input>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">

        <div class="totalBillSec">
            <p>Total Rs <span>${total}</span></p>
        </div>
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {
        $('.amountTotal').each(function (index, item) {
            $('#innerAmount'+index).val(parseFloat($('#innerAmount'+index).val()).toFixed(1));
        });

        var netTotal = calculateAmountTotal();
        $(".totalBillSec span").html(netTotal);

        $('.theDate').val($('#currentDate').val());
        $('.theBillDate').val($('#billDate').val());
        $('.theBillNumber').val($('#billNumber').val());
        $('.supplierAccountNumber').val($('#supplierAccount').val());


        $('#currentDate').change(function () {
            $('.theDate').val($('#currentDate').val());

        });

        $('#billDate').change(function () {
            $('.theBillDate').val($('#billDate').val());

        });
        $('#billNumber').change(function () {
            $('.theBillNumber').val($('#billNumber').val());

        });


        $(".amountTotal").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });

        $(".quantity").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });


        $(".rate").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });

        function calculateAmountTotal() {
            var netTotal = 0;
            $(".amountTotal").each(function (index, ele) {
                netTotal += parseFloat($(ele).val());
            });
            console.log("total", netTotal);
            return netTotal.toFixed(2);
        }


    });
        $('#supplierAccount').change(function () {
            $('.supplierAccountNumber').val($('#supplierAccount').val());

        });
    // get rate of specific item
    function updateValues(accountCode, id) {
        if (accountCode == 0) {
            $('#innerRate' + id).val('0.0');
            $('#innerMU' + id).val('null');
        } else {
            $('#innerRate' + id).val($('#rate' + accountCode + id).text());
            $('#innerMU' + id).val($('#measureUnit' + accountCode + id).text());
        }
    }

    function calculateAmount(id) {
        var amount = $('#innerRate' + id).val() * $('#innerQuantity' + id).val();
        $('#innerAmount' + id).val(amount.toFixed(2));
         validateAmount(id);
    }

    function calculateQuantity(id) {
        var quantity = $('#innerAmount' + id).val() / $('#innerRate' + id).val();
        $('#innerQuantity' + id).val(quantity.toFixed(2));

    }
    function validateAmount(id) {
        if (parseInt($('#innerQuantity' + id).val()) > parseInt($('#innerMaxQuantity' + id).val()) ) {
            alert("Return Quantity cannot be greater than Purchase Quantity : " + parseInt($('#innerMaxQuantity' + id).val()));
            $('#innerQuantity' + id).val(0.00);
        }

    }

    function goBack(){
        window.location = "${contextPath}/PurchaseReturn/viewReturnVoucher";

    }

</script>
</body>
</html>
