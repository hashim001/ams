<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Purchase Order</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>
<div class="container">


    <h2 class="form-signin-heading">Purchase Order</h2>
    <div class="container">

        <div>
            <label>Order Number :${purchaseOrder.orderNumber}</label>
        </div>
        <div>
            <label>Supplier : ${purchaseOrder.supplierAccount.title}</label>
        </div>
        <div>
            <label>Remarks :${purchaseOrder.remarks}</label>
        </div>
        <div>
            <label>Order Status :${purchaseOrder.orderStatus}</label>
        </div>
        <span class="pull-right">
            <label>Order Date :${purchaseOrder.orderDate}</label></br>
            <label>Delivery Date :${purchaseOrder.deliveryDate}</label>

        </span>





    </div>
    <div class="col-sm-12">
        <div class="main-table">
            <table class="table">
                <thead>
                <tr>
                    <th>Item Account</th>
                    <th width="15%">Measure Unit</th>
                    <th width="10%">Order Quantity</th>
                    <th width="10%">Balance Quantity</th>
                    <th width="10%">Rate</th>
                    <th width="10%">Tax Rate(%)</th>
                    <th width="15%">Amount</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${purchaseOrder.orderProductList}"  var="product" varStatus="productStatus">

                    <tr>

                        <td>${product.name}</td>
                        <td>${product.measureUnit}</td>
                        <td>${product.initialQuantity}</td>
                        <td>${product.quantity}</td>
                        <td>${product.rate}</td>
                        <td>${product.taxRate}</td>
                        <td>${product.initialQuantity * product.rate}</td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>



</script>
</body>
</html>
