<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GRN - Delivery</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>
<div class="container">
    ${voucherAddSuccess}


    <h2 class="form-signin-heading">Factory Goods</h2>
    <div class="container">
        <form class="form-inline" style="margin-bottom: 15px; ">
            <label>Delivery Number :</label>
            <input  type="text" class="form-control" readonly="true"
                    autofocus="true" value="${voucherListOrder.deliveryNumber}"/></br>
            <label> Supplier :</label>
            <input  type="text" class="form-control" readonly="true"
                   autofocus="true" value = "${purchaseOrder.supplierAccount.title}" /></br>
            <label> Supplier DC :</label>
            <input  type="text" class="form-control"  id="vendorDC"/></br>

            <span class="pull-right">
        <label>Voucher Date  : </label>
        <input type="date" class="form-control " id="currentDate"
               autofocus="true" value = "${currentDate}" required="required" /></br>
        <label>Delivery Date  : </label>
        <input type="date" class="form-control "
                               autofocus="true" value = "${purchaseOrder.deliveryDate}" required="required" readonly ="true" />
        <label>Purchase Order  : </label>
        <input type="text" class="form-control "
               autofocus="true" value = "${purchaseOrder.orderNumber}"  readonly ="true" />
            </span>
        </form>





    </div>


    <form:form method="POST" modelAttribute="voucherListOrder" id="formid">
        <table class="table table-condensed">
            <thead>
            <tr>

                <th width="30%">Item Account</th>
                <th width="30%">Measure Unit</th>
                <th width="20%">Request Quantity</th>
                <th width="20%">Receipt Quantity</th>



            </tr>
            </thead>
            <tbody>
            <c:forEach items="${voucherListOrder.voucherList}"  var="voucher" varStatus="status">

                <tr>
                    <form:input  type="hidden" path="voucherList[${status.index}].accountCode" class="form-control"
                                 autofocus="true" ></form:input>
                    <form:input  type="hidden" path="voucherList[${status.index}].itemAccount" class="form-control"
                                 autofocus="true" ></form:input>

                    <td>
                        <input  type="text" value="${voucher.itemName}" class="form-control"
                                     autofocus="true" readonly="true" />
                    </td>
                    <td>
                        <form:input type="text"  path="voucherList[${status.index}].measureUnit" class="form-control"
                                    autofocus="true"  readonly="true"></form:input>
                    </td>
                    <td>
                        <form:input type="text" id ="requestQuantity${status.index}" path="voucherList[${status.index}].requestQuantity" class="form-control"
                                    autofocus="true" readonly="true" ></form:input>
                    </td>
                    <td>
                        <form:input type="text" id ="itemQuantity${status.index}" path="voucherList[${status.index}].itemQuantity" class="form-control" onblur="validateAmount(${status.index})"
                                    autofocus="true" value = "0.00" ></form:input>
                    </td>


                    <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].voucherDate" class="form-control theDate"
                                autofocus="true" value = "${currentDate}" required="required"></form:input>

                </tr>
            </c:forEach>
            </tbody>
        </table>
        <form:input  type="hidden" path="vendorDC" class="form-control" id="vendorDcNumber"
                     autofocus="true" ></form:input>
        <button class="pull-right" id="btn_submit" type="submit" >Submit</button>
    </form:form>



</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function() {

        $('.theDate').val($('#currentDate').val());


        $('#currentDate').change(function() {
            $('.theDate').val($('#currentDate').val());

        });

        $('#vendorDcNumber').val($('#vendorDC').val());


        $('#vendorDC').blur(function() {
            $('#vendorDcNumber').val($('#vendorDC').val());

        });
    });


    function validateAmount(id) {

        if (parseInt($('#itemQuantity' + id).val()) > parseInt($('#requestQuantity' + id).val())) {
            alert("Receipt Quantity cannot be greater than request Quantity.");
            $('#itemQuantity' + id).val(0.00);
        }

    }
    function goBack(){
     window.location = "${contextPath}/Trading/viewGrnOrders";
    }



</script>
</body>
</html>
