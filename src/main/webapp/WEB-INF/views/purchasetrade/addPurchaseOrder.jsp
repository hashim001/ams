<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create Purchase Order</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">
${duplicateOrderNumber}
    <h2 class="form-signin-heading">Purchase Order</h2>
    <form:form method="POST" modelAttribute="orderForm" id="formid" Class="form-inline formSaleVoucher formWidthDef">
        <div class="container">
            <div class="pull-left">
                <div class="form-Row">
                    <spring:bind path="orderNumber">
                        <form:label path="orderNumber">O.No</form:label>
                        <form:input type="text" cssStyle="width: 120px" path="orderNumber" readonly="true"
                                    cssClass="form-control"
                                    autofocus="true"></form:input>
                    </spring:bind>

                </div>
            </div>
                <spring:bind path="supplierCode">
                    <form:label path="supplierCode">Item Supplier</form:label>
                    <form:select style="width: 220px" path="supplierCode" id="supplierAccount"
                                 cssClass="form-control">
                        <c:forEach items="${vendorAccounts}" var="account">
                            <form:option value="${account.accountCode}">${account.title}
                                <c:choose>
                                    <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                    </c:when>
                                </c:choose>
                            </form:option>
                        </c:forEach>
                    </form:select>
                </spring:bind>
                <spring:bind path="requestId">
                    <form:label path="requestId">Request #</form:label>
                    <form:select style="width: 150px"  path="requestId" id="requestId"
                                 cssClass="form-control">
                        <form:option value="0">NIL</form:option>
                        <c:forEach items="${purchaseRequests}" var="purchaseRequest">
                            <form:option value="${purchaseRequest.requestId}">${purchaseRequest.requestNumber}</form:option>
                        </c:forEach>
                    </form:select>
                </spring:bind>
                <spring:bind path="remarks">
                    <form:label path="remarks">Remarks</form:label>
                    <form:input cssStyle="width: 150px" type="text" path="remarks" cssClass="form-control"
                                autofocus="true"></form:input>
                </spring:bind>
                <spring:bind path="orderDate">
                    <form:label path="orderDate">Order Date </form:label>
                    <form:input cssStyle="width: 155px" type="date" path="orderDate" value="${currentDate}" cssClass="form-control"
                                autofocus="true"></form:input>
                </spring:bind>
                 <div class="pull-left">
                <spring:bind path="poType">
                    <form:label path="poType">PO Type</form:label>
                    <form:select style="width: 220px" path="poType" cssClass="form-control">
                            <form:option value="LPO">LPO</form:option>
                            <form:option value="IPO">IPO</form:option>
                    </form:select>
                </spring:bind>
                </div>
            <div style="display: none" class="form-Row">
                <spring:bind path="deliveryDate">
                    <form:label path="deliveryDate">Delivery Date</form:label>
                    <form:input type="date" path="deliveryDate" value="${currentDate}" cssClass="form-control"
                                autofocus="true"></form:input>
                </spring:bind>
            </div>
            <div class="totalBillSec">
                <div class="customerInfoSec"> <p style="font-size: large">Total Without GST Rs <span id="totalWithoutTax"></span></p></div>
                <div class="customerInfoSec">  <p style="font-size: large">Total Tax Rs <span id="taxTotal"></span></p></div>
                <div class="customerInfoSec"> <p style="font-size: large">Total Rs <span id="totalBill"></span></p></div>
            </div>


        </div>

        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>
                    <th>Check</th>
                    <th>Item Account</th>
                    <th>Order Quantity</th>
                    <th>Rate</th>
                    <th>Tax Rate(%)</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${orderForm.orderProductList}" var="product" varStatus="productStatus">
                    <tr>
                    <td>
                      <form:checkbox  autofocus="true" path="orderProductList[${productStatus.index}].checkItem" id="checkItem${productStatus.index}"></form:checkbox>
                    </td>
                        <td>
                            <form:input path="orderProductList[${productStatus.index}].name" type="text" id="itemName${productStatus.index}"
                                         class="form-control accounts" readonly="true" value="Nil"></form:input>
                        </td>
                        <td style="display: none">
                            <form:input type="number"
                                         id="itemCode${productStatus.index}"
                                         path="orderProductList[${productStatus.index}].itemCode"
                                         class="form-control accounts" readonly="true" ></form:input>
                            <form:input type="number"
                                        id="accountCode${productStatus.index}"
                                        path="orderProductList[${productStatus.index}].accountCode"
                                        class="form-control accounts" readonly="true" ></form:input>
                        </td>
                        <td>
                            <form:input type="number" id="innerInitialQuantity${productStatus.index}"
                                        path="orderProductList[${productStatus.index}].initialQuantity"
                                        class="form-control quantity"
                                        onblur="updateValues(${productStatus.index})"
                                        autofocus="true" value="0.0"></form:input>

                        </td>
                        <td style="display: none">
                            <form:input type="text" id="innerQuantity${productStatus.index}"
                                        path="orderProductList[${productStatus.index}].quantity" readonly="true"
                                        class="form-control" onblur="calculateAmount(${productStatus.index})"
                                        autofocus="true" value="0.0"></form:input>

                            <input type="text" id="requestQuantity${productStatus.index}" value="0.0"></input>

                            <form:input type="text" id="innerStageQuantity${productStatus.index}"
                                        path="orderProductList[${productStatus.index}].stageQuantity" readonly="true"
                                        class="form-control" onblur="calculateAmount(${productStatus.index})"
                                        autofocus="true" value="0.0"></form:input>
                            <form:input type="text" id="innerFinalQuantity${productStatus.index}"
                                        path="orderProductList[${productStatus.index}].finalQuantity" readonly="true"
                                        class="form-control" onblur="calculateAmount(${productStatus.index})"
                                        autofocus="true" value="0.0"></form:input>
                        </td>
                        <td>
                            <form:input type="number" id="innerRate${productStatus.index}"
                                        path="orderProductList[${productStatus.index}].rate" class="form-control rate"
                                        onblur="calculateAmountWithoutTax(${productStatus.index})"
                                        autofocus="true" value="0.0"></form:input>
                        </td>
                        <td>
                            <select class="form-control" id="taxMap${productStatus.index}"
                                    onchange="taxMapCalculator(${productStatus.index},this.value,this);">

                                <c:forEach items="${taxMap}" var="tax">
                                    <option value="${tax.key}">${tax.value}</option>
                                </c:forEach>
                            </select>
                            <form:input type="hidden" id="innerTaxRate${productStatus.index}"
                                        path="orderProductList[${productStatus.index}].taxRate"
                                        class="form-control"
                                        onblur="calculateAmount(${productStatus.index})"
                                        autofocus="true" value="${defaultTaxRate}"></form:input>
                            <form:input type="hidden" id="innerTaxAccount${productStatus.index}"
                                        path="orderProductList[${productStatus.index}].taxAccount" class="form-control"
                                        onblur="calculateAmount(${productStatus.index})"
                                        autofocus="true" value="${defaultTaxAccount}"></form:input>
                        </td>
                        <td>
                            <form:input type="text" id="innerAmount${productStatus.index}"
                                        path="orderProductList[${productStatus.index}].amount" readonly="true"
                                        class="form-control amountTotal"
                                        autofocus="true" value="0.0"></form:input>
                        </td>
                        <input type="hidden" id="innerAmountWithoutTax${productStatus.index}"
                               class="form-control amountWithoutTax"
                               autofocus="true" value="0.0">
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {
        $(".amountTotal").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $("#totalBill").html(netTotal);
        });
        $(".rate").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $("#totalBill").html(netTotal);
            var Total = calculateWithoutTaxTotal();
            $("#totalWithoutTax").html(Total);
            var taxTotal = (netTotal - Total).toFixed(2);
            $("#taxTotal").html(taxTotal);

        });
        $(".quantity").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $("#totalBill").html(netTotal);
            var withoutTaxTotal = calculateWithoutTaxTotal();
            $("#totalWithoutTax").html(withoutTaxTotal);
            var taxTotal = (netTotal - withoutTaxTotal).toFixed(2);
            $("#taxTotal").html(taxTotal);

        });


        function calculateAmountTotal() {
            var netTotal = 0;
            $(".amountTotal").each(function (index, ele) {
                netTotal += parseFloat($(ele).val());
            });
            return netTotal.toFixed(2);
        }

        $(".amountWithoutTax").on('blur', function (e) {
            var netTotal = calculateWithoutTaxTotal();
            $("#totalWithoutTax").html(netTotal);
        });

        function calculateWithoutTaxTotal() {
            var netTotal = 0;
            $(".amountWithoutTax").each(function (index, ele) {
                netTotal += parseFloat($(ele).val());
            });
            return netTotal.toFixed(2);
        }

    });

    // get rate of specific item
    function updateValues(id) {
        $('#innerQuantity' + id).val($('#innerInitialQuantity' + id).val());
        $('#innerStageQuantity' + id).val($('#innerInitialQuantity' + id).val());
        $('#innerFinalQuantity' + id).val($('#innerInitialQuantity' + id).val());
        calculateAmount(id);
        calculateAmountWithoutTax(id);
        validateAmount(id);
    }

    function calculateAmount(id) {
        var taxAmount = $('#innerRate' + id).val() * $('#innerQuantity' + id).val() * ($('#innerTaxRate' + id).val() / 100);
        var amount = ($('#innerRate' + id).val() * $('#innerQuantity' + id).val()) + taxAmount;
        $('#innerAmount' + id).val(amount.toFixed(2));

    }

    function calculateAmountWithoutTax(id) {
        document.getElementById('checkItem' + id).checked = true;
        var taxAmount = $('#innerRate' + id).val() * $('#innerQuantity' + id).val();
        $('#innerAmountWithoutTax' + id).val(taxAmount);
        calculateAmount(id);

    }

    function taxMapCalculator(id, rateAccount, dropdownElement) {
        var rate = rateAccount.substring(rateAccount.indexOf("-") + 1);
        var accountCode = rateAccount.substring(0, rateAccount.indexOf("-"));

        $('#innerTaxRate' + id).val(rate);
        $('#innerTaxAccount' + id).val(accountCode);
        calculateAmountWithoutTax(id);
        $('#innerTaxRate' + id).closest("tr").find(".rate").trigger("blur");
        $('#innerTaxRate' + id).closest("tr").find(".quantity").trigger("blur");
    }
    function goBack(){
     window.location = "${contextPath}/Trading/viewPurchaseOrders";
    }
       $('#requestId').change(function(){
           $.ajax({
                   url: '${contextPath}/Trading/getProductList',
                   contentType: 'application/json',
                   data: {
                       requestId: $('#requestId').val(),

                  },
                   success: function (response) {

                      populateProductList(response);
                   },
                   error: function (e) {

                       console.log("ERROR : ", e);

                   }
               });

    });
    function populateProductList(requestProducts){
    var index = 0;
         $(".quantity").val('0.0');
         $(".accounts").val('Nil');
    for(requestProduct in requestProducts){
        if(requestProducts[requestProduct].approvedQuantity>0){
            console.log(requestProducts[requestProduct].itemCode);
        $('#itemCode' + index).val(requestProducts[requestProduct].itemCode);
        $('#accountCode' + index).val(requestProducts[requestProduct].accountCode);
        $('#itemName' + index).val(requestProducts[requestProduct].name);
        $('#innerInitialQuantity' + index).val(requestProducts[requestProduct].approvedQuantity);
        $('#requestQuantity' + index).val(requestProducts[requestProduct].approvedQuantity);
        $('#innerInitialQuantity' + index).trigger("blur");
       index++;
       }
    }
    }
    function validateAmount(id) {
        if (parseInt($('#innerInitialQuantity' + id).val()) > parseInt($('#requestQuantity' + id).val())) {
            alert("PO Quantity cannot be greater than Request Quantity : " + parseInt($('#requestQuantity' + id).val()));
            $('#innerInitialQuantity' + id).val(0.00);
        }

    }

</script>
</body>
</html>
