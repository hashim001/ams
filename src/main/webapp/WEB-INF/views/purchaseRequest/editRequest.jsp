<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Purchase Request</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">


    <h2 class="form-signin-heading">Purchase Request</h2>

    ${requestMessage}
    <form:form method="POST" modelAttribute="requestForm" id="formid"
               Class="form-inline formSaleVoucher formWidthDef">
        <div class="container">
            <h4> <strong>Department</strong> : ${employeeDepartment}</h4>
            <div class="pull-left">
                <div class="form-Row">
                    <spring:bind path="requestNumber">
                        <form:label path="requestNumber">Request Number</form:label>
                        <form:input type="text" cssStyle="width: 155px" path="requestNumber"
                                    cssClass="form-control" readonly="true"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
            </div>
            <div  class="customerInfoSec">
                <spring:bind path="purchaseType">
                    <form:label path="purchaseType">Request Type</form:label>
                    <form:select cssStyle="width: 155px" path="purchaseType"  cssClass="form-control">
                        <form:option value="MILL">MILL</form:option>
                        <form:option value="HO">HO</form:option>
                    </form:select>
                </spring:bind>
            </div>
            <div  class="customerInfoSec">
                <spring:bind path="remarks">
                    <form:label path="remarks">Description</form:label>
                    <form:input type="text" cssStyle="width: 155px" path="remarks"
                                cssClass="form-control" readonly="true"
                                autofocus="true"></form:input>
                </spring:bind>
            </div>
            <div class="customerInfoSec">
                <spring:bind path="requestDate">
                    <form:label path="requestDate">Request Date </form:label>
                    <form:input cssStyle="width: 155px" type="date" path="requestDate"
                                cssClass="form-control" readonly="true"
                                autofocus="true"></form:input>
                </spring:bind>
            </div>
            <div  class="customerInfoSec">
                <spring:bind path="dueDate">
                    <form:label path="dueDate">Due Date </form:label>
                    <form:input cssStyle="width: 155px" type="date" path="dueDate" cssClass="form-control"
                                autofocus="true"></form:input>
                </spring:bind>
            </div>


        </div>


        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>

                    <th>Item Account</th>
                    <th>Measure Unit</th>
                    <th>Request Quantity</th>
                    <th>Remarks</th>
                    <sec:authorize access="hasAnyRole('ROLE_STORE','ROLE_GM')">
                        <th>Approved Qty</th>
                        <th>Approved</th>
                    </sec:authorize>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestForm.requestProductList}" var="product" varStatus="productStatus">

                    <tr>
                        <td>
                            <form:select path="requestProductList[${productStatus.index}].transitionCode"
                                         class="form-control" id ="stockAccountCode${productStatus.index}" onchange="getMeasureUnit(${productStatus.index});">
                                <form:option value="0">NIL</form:option>
                                <c:forEach items="${rawMaterial}" var="rm">
                                    <form:option value="${rm.accountCode}-${rm.rawMaterialId}">${rm.name}</form:option>
                                </c:forEach>
                            </form:select>
                        </td>
                        <td>
                            <form:input type="text" id="measureUnit${productStatus.index}"
                                        path="requestProductList[${productStatus.index}].measureUnit"
                                        class="form-control quantity" readonly="true"
                                        autofocus="true"></form:input>
                        </td>
                        <td>
                            <sec:authorize access="hasAnyRole('ROLE_REQUEST_MAKER')">
                            <form:input type="number" id="innerInitialQuantity${productStatus.index}"
                                        path="requestProductList[${productStatus.index}].quantity"
                                        class="form-control quantity"
                                        autofocus="true" ></form:input>
                            </sec:authorize>
                            <sec:authorize access="hasAnyRole('ROLE_STORE','ROLE_GM')">
                                <form:input type="number" id="innerInitialQuantity${productStatus.index}"
                                            path="requestProductList[${productStatus.index}].quantity"
                                            class="form-control quantity" readonly="true"
                                            autofocus="true" ></form:input>
                            </sec:authorize>
                        </td>

                        <td>
                            <form:input type="text" path="requestProductList[${productStatus.index}].remarks"
                                        class="form-control quantity"
                                        autofocus="true" placeholder="Remarks( if any )" ></form:input>
                        </td>

                        <sec:authorize access="hasAnyRole('ROLE_STORE','ROLE_GM')">
                            <td>
                                <form:input type="number" id="innerApprovedQuantity${productStatus.index}"
                                            path="requestProductList[${productStatus.index}].approvedQuantity"
                                            class="form-control quantity" onchange="validateAmount(${productStatus.index});"
                                            autofocus="true"></form:input>
                            </td>

                            <td>
                                <form:checkbox  path="requestProductList[${productStatus.index}].approved"
                                            class="form-control quantity"
                                            autofocus="true"  ></form:checkbox>
                            </td>
                        </sec:authorize>
                    </tr>

                    <form:input type="hidden" id ="productName${productStatus.index}" path="requestProductList[${productStatus.index}].name" ></form:input>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    function getMeasureUnit(id) {
        $.ajax({
            // Request to Stock Controller
            url: '${contextPath}/Stock/ajax/getMeasureUnit',
            contentType: 'application/json',
            data: {
                rawId: $('#stockAccountCode' + id).val()

            },

            success: function (response) {
                $('#measureUnit' + id).val(response);
            }
        });

        var generatedId = "stockAccountCode" + id;
        console.log($("#"+ generatedId +" option:selected").text());
        $("#productName" + id).val($("#"+ generatedId +" option:selected").text());
    }

    function validateAmount(id) {

        if (parseInt($('#innerApprovedQuantity' + id).val()) > parseInt($('#innerInitialQuantity' + id).val())) {
            alert("Approved Quantity cannot be greater than request Quantity.");
            $('#innerApprovedQuantity' + id).val(0.00);
        }

    }

    function goBack(){
        window.location = "${contextPath}/Request/viewRequests";
    }
</script>
</body>
</html>
