<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Purchase Request</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>
<div class="container">


    <h2 class="form-signin-heading">Purchase Request</h2>
    <div class="container">

        <div>
            <label>Request Id :${purchaseRequest.requestId}</label>
        </div>
        <div>
            <label>User : ${purchaseRequest.user}</label>
        </div>
        <div>
            <label>Remarks :${purchaseRequest.remarks}</label>
        </div>
        <div>
            <label>Status :${purchaseRequest.status}</label>
        </div>
        <span class="pull-right">
            <label>Request Date : <fmt:formatDate type = "date" value = "${purchaseRequest.requestDate}" /></label></br>
            <label>Due Date : <fmt:formatDate type = "date" value = "${purchaseRequest.dueDate}" /></label>

        </span>





    </div>
    <div class="col-sm-12">
        <div class="main-table">
            <table class="table">
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Account</th>
                    <th width="10%">Request Quantity</th>
                    <th width="10%">Approved Quantity</th>
                    <th width="40%">Remarks</th>
                    <th width="10%">Status</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${purchaseRequest.requestProductList}"  var="product" varStatus="productStatus">

                    <tr>
                        <td>${product.name}</td>
                        <td>${product.accountCode}</td>
                        <td>${product.quantity}</td>
                        <td>${product.approvedQuantity}</td>
                        <td>${product.remarks}</td>
                        <c:choose>
                            <c:when test="${product.approved}">
                                <c:choose>
                                    <c:when test="${product.approvedQuantity == 0.0}">
                                        <td>REJECTED</td>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${product.quantity > product.approvedQuantity}">
                                                <td>PARTIAL APPROVAL</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td>COMPLETE</td>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <td>PENDING</td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>
    function goBack(){
        window.location = "${contextPath}/Request/viewRequests";
    }
</script>
</body>
</html>
