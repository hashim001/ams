<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">
    <title>Store Orders</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<section class="main">
    <div class="container">
        ${voucherUpdateSuccess}
        <h2 class="heading-main">Purchase Requests</h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Request Code</th>
                            <th>Create Date</th>
                            <th>Update Date</th>
                            <th width="10%">Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${storeOrders}" var="storeOrder" varStatus="status">
                        <tr>
                        <td>${storeOrder.requestCode}</td>
                        <td>${storeOrder.createdDate}</td>
                        <td>${storeOrder.updatedDate}</td>
                        <td>
                        <form method="post" id="approve${status.index}" action="${contextPath}/Store/approve">
                               <input type="hidden" name="orderId" value="${storeOrder.storeOrderId}"/></form>
                        <ul class="list-inline">
                           <li><a title="View" target="_blank" href= "${contextPath}/Store/viewOrder?orderId=${storeOrder.storeOrderId}"><img src= "${contextPath}/resources/img/viewIcon.png"
                           alt="" width="20" height="20"></a></li>
                            <c:choose>
                                <c:when test="${storeOrder.status == 'LOCKED'}">
                                    <li><a title="Edit" href="${contextPath}/Store/order?orderId=${storeOrder.storeOrderId}" ><img src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                </c:when>
                            </c:choose>
                            <li><a title="Approve" href="#" onclick="approve(${status.index});"><img src="${contextPath}/resources/img/recycle.png" alt=""></a></li>

                           </ul>
                        </td>
                        </tr>

                        </c:forEach>


                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>



    function approve(id) {

        if (confirm("Are you sure, you want to Approve the Store Request?")) {
            $('#approve' + id).submit();
        }
    }

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });


    function goBack() {
        window.location = "${contextPath}/home#store";
    }

</script>
</body>
</html>