<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Store Request</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">


    <h2 class="form-signin-heading">Store Request</h2>

    ${requestMessage}
    <form:form method="POST" modelAttribute="storeOrder" id="formid"
               Class="form-inline formSaleVoucher formWidthDef">
        <div class="container">
            <div class="pull-left">
                <div class="form-Row">
                    <spring:bind path="requestCode">
                        <form:label path="requestCode">Request Number</form:label>
                        <form:input type="text" cssStyle="width: 155px" path="requestCode"
                                    cssClass="form-control" readonly="true"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
            </div>
            <div class="customerInfoSec">
                <spring:bind path="createdDate">
                    <form:label path="createdDate">Date </form:label>
                    <form:input cssStyle="width: 155px" type="date" path="createdDate" value="${currentDate}"
                                cssClass="form-control"
                                autofocus="true"></form:input>
                </spring:bind>
            </div>
        </div>


        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Item Account</th>
                    <th>Department</th>
                    <th width="20%">Remarks</th>
                    <th>Request Quantity</th>
                    <th>Approved Quantity</th>
                    <th>Is Approved</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${storeOrder.requestProductList}" var="product" varStatus="productStatus">

                    <tr>
                        <td>
                            <form:input type="text"
                                        path="requestProductList[${productStatus.index}].name"
                                        class="form-control quantity" readonly="true"
                                        autofocus="true" ></form:input>
                        </td>

                        <td>
                            <form:input type="number"
                                        path="requestProductList[${productStatus.index}].accountCode"
                                        class="form-control quantity" readonly="true"
                                        autofocus="true" ></form:input>
                        </td>
                        <td>
                            <input type="text" class="form-control quantity"  disabled
                                         value="${product.purchaseRequest.employeeDepartment.name}" />
                        </td>
                        <td>
                            <form:input type="text"
                                        path="requestProductList[${productStatus.index}].remarks"
                                        class="form-control quantity" readonly="true"
                                        autofocus="true"></form:input>
                        </td>
                        <td>
                            <form:input type="number" id="innerInitialQuantity${productStatus.index}"
                                        path="requestProductList[${productStatus.index}].quantity"
                                        class="form-control quantity" readonly="true"
                                        autofocus="true"></form:input>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${storeOrder.redirectTo == 'GM'}">
                                    <form:input type="number"
                                                path="requestProductList[${productStatus.index}].approvedQuantity"
                                                class="form-control quantity"
                                                autofocus="true" ></form:input>
                                </c:when>
                                <c:otherwise>
                                    <form:input type="number"
                                                path="requestProductList[${productStatus.index}].approvedQuantity"
                                                class="form-control quantity"
                                                autofocus="true" value="${product.quantity}"></form:input>
                                </c:otherwise>
                            </c:choose>

                        </td>

                        <td>
                            <form:checkbox
                                        path="requestProductList[${productStatus.index}].approved"
                                        class="form-control quantity"></form:checkbox>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">
    </form:form>

    <input type="hidden" value="${storeOrder.redirectTo}"  id="redirectTo"/>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    function goBack(){
        if($('#redirectTo').val() == 'GM'){
            window.location = "${contextPath}/Store/gmOrderListing";
        }else{
            window.location = "${contextPath}/Store/orderListing";
        }

    }
</script>
</body>
</html>
