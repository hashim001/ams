<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Saleman</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${vendorAddSuccess}
        ${distributorRemoveSuccess}
        ${AccountAddSuccess}
        <h2 class="heading-main">Salesmen
            <span class="addIcon" data-toggle="modal"
                  data-target="#myModal2"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${distributors}" var="distributor" varStatus="distributorStatus">

                            <tr>
                                <td>${distributor.name}</td>
                                <td>${distributor.address}</td>
                                <td>${distributor.mobile}</td>
                                <td>${distributor.email}</td>
                                <td>${distributor.date}</td>
                                <td>
                                    <form method="post" id="distributorRemove${distributorStatus.index}"
                                          action="${contextPath}/Distributor/removeDistributor">
                                        <input type="hidden" name="distributorId"
                                               value="${distributor.distributorId}"/>
                                    </form>

                                    <ul class="list-inline">
                                            <%--                                        <li><a href="${contextPath}/Distributor/addRider?distributorId=${distributorId}" ><img
                                                                                            src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                                                                    <li><a href="${contextPath}/Distributor/addVendor?distributorId=${distributorId}" ><img
                                                                                            src="${contextPath}/resources/img/edit.png" alt=""></a></li>--%>
                                        <li><a href="#" onclick="edit(${distributor.distributorId});"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                        <li><a href="#" onclick="remove(${distributorStatus.index})"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" onclick="resetForm();" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Salesman Details</h2>
            </div>
            <form:form method="POST" modelAttribute="salesmanForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="name">

                                <form:label path="name">Salesman Name</form:label>
                                <form:input type="text" path="name" placeholder="Name " required="true"></form:input>
                                <form:errors path="name"></form:errors>

                            </spring:bind>

                            <spring:bind path="mobile">

                                <form:label path="mobile">Mobile #</form:label>
                                <form:input type="text" path="mobile" placeholder="Mobile#"
                                            autofocus="true"></form:input>
                                <form:errors path="mobile"></form:errors>

                            </spring:bind>

                        </div>

                        <div class="col-sm-6">
                            <spring:bind path="email">

                                <form:label path="email">Email</form:label>
                                <form:input type="email" path="email" placeholder="Email"
                                            autofocus="true"></form:input>
                                <form:errors path="email"></form:errors>

                            </spring:bind>
                           <div style="display: none">
                            <spring:bind path="date">

                                <form:label path="date">Registration Date #</form:label>
                                <form:input type="date" path="date" value="${currentDate}"
                                            autofocus="true" required="true"></form:input>
                                <form:errors path="date"></form:errors>

                            </spring:bind>
                            </div>

                            <spring:bind path="distributorId">
                                <form:input type="hidden" path="distributorId" autofocus="true"></form:input>

                            </spring:bind>
                        </div>

                    </div>
                    <div align="center">
                        <spring:bind path="address">
                            <form:label path="address">Address</form:label>
                            <form:textarea type="text" path="address" placeholder="Address"
                                           autofocus="true" style="width: 70%;"></form:textarea>
                            <form:errors path="address"></form:errors>
                        </spring:bind>

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal" onclick="resetForm();">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>

    $(document).ready(function () {

    });


    function remove(id) {
        if (confirm("Are you sure, you want to delete salesman?")) {
            $('#distributorRemove' + id).submit();
        }
    }

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });


    function edit(distributorId) {
        $.ajax({
            url: '${contextPath}/Distributor/editDistributor',
            contentType: 'application/json',
            data: {
                distributorId: distributorId

            },
            success: function (response) {
                editForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }

    function editForm(response) {
        $("#myModal2").modal();
        $('#myModal2').find('form').trigger('reset');
        $('#myModal2').find('form').find('#name').val(response.name);
        $('#myModal2').find('form').find('#email').val(response.email);
        $('#myModal2').find('form').find('#address').val(response.address);
        $('#myModal2').find('form').find('#date').val(response.date);
        $('#myModal2').find('form').find('#mobile').val(response.mobile);
        $('#myModal2').find('form').find('#distributorId').val(response.distributorId);
    }

    function resetForm() {
        $('#myModal2').find('form').trigger('reset');
        $('#myModal2').find('form').find('#distributorId').val(null);

    }

</script>
</body>
</html>
