<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Salesman Commission</title>


    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/style.css" rel="stylesheet">

</head>

<body>
<%@ include file="../header.jsp" %>

<div class="container">


        <div class="ledgerReportHeaderFilterSec">

        </div>

    <div align="right">
        <div class="pdfLinkSec">
            <button class="btn btn-success" style="background-color: #292e5a" id="printBtn"
                    onclick="printDiv()">Print
            </button>

        </div>

    </div>

</div>
<section class="main">
    <div class="container" id="printDiv">
        <h3>Salesman Commission</h3>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">

                    <table class="table trialreport">
                        <thead>
                        <th>Salesman</th>
                        <th>Date Applicable</th>
                        <th>Apartment Type</th>
                        <th>Apartment Number</th>
                        <th>Amount Due</th>
                        </thead>
                        <tbody>
                        <c:set var="total" value="${0.0}"/>

                        <c:forEach items="${apartmentList}" var="apartment">
                            <c:set var="total" value="${total + apartment.commissionAmount}"/>
                            <tr>
                                <td>${distributorMap.get(apartment.distributorId)}</td>
                                <td><fmt:formatDate type = "date" value = "${apartment.commissionDue}" /></td>
                                <td>${apartment.stock.stockAccount.title}</td>
                                <td>${apartment.apartmentNo}</td>
                                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${apartment.commissionAmount}"/></td>

                            </tr>
                        </c:forEach>

                        </tbody>
                        <tr style="font-weight: bold">
                            <td>Total:</td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Rs . <fmt:formatNumber type="number" maxFractionDigits="2" value="${total}"/></td>

                        </tr>
                        </tbody>
                    </table>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function () {

        updateName();
    });

    function printDiv() {

        var divToPrint = document.getElementById('printDiv');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><head>\n' +
            '\t\n' +
            '\n' +
            '\t\n' +
            '\t<style type="text/css">\n' +
            '\t\tbody,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:small }\n' +
            '\t\ta.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } \n' +
            '\t\ta.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } \n' +
            '\t\tcomment { display:none;  } \n' +
            '\t</style>\n' +
            '<link href="${contextPath}/resources/css/ledgerreport-style.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">' +
            '<link href="${contextPath}/resources/css/style.css" rel="stylesheet">' +
            '</head>\n<body onload="window.print()">' +
            '<img src="${imageUrl}" alt="img" width="200" height="80">' + divToPrint.innerHTML +
            '</body></html>');

        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 50000);


    }

    function updateName() {
        $("#distributorName").html($("#distributor_name option:selected").text());
    }


</script>
</body>
</html>