<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Universal Distributor</title>
</head>
<style>
    body {
        margin: 0;
        padding: 0;
        padding-top: 10px;
        height: auto;
        text-transform: capitalize;
        position: relative;
    }

    html {
        height: auto;
    }

    .container {
        margin: 0 auto;
        max-width: 1000px;
        padding-bottom: 50px;
        position: static;
        left: 0;
        right: 0;
    }

    table {
        width: 100%;
        border-spacing: 0;
    }

    .right {
        float: right;
        font-size: 15px;
    }

    .right td:nth-of-type(2) {
        padding-left: 5px;
    }

    .entry-table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    .entry-table thead, .entry-table tbody, .entry-table tfoot {
        border: 1px solid #000;
    }

    .entry-table tr td {
        line-height: 35px;
        text-align: center;
        font-size: 12px;
        padding: 5px 0;
    }

    .stamp {
        float: right;
        position: relative;
        font-size: 12px
    }

    .stamp td::before {
        position: absolute;
        content: "";
        width: 100%;
        border: 1px solid;
        bottom: 48px;
    }

    .stamp p {
        padding: 12px;
    }

    .inputUppercase td:nth-of-type(2) {
        text-transform: uppercase
    }

    .less-height {
        line-height: 16px;
        font-size: 12px;
    }

    .footer {
        margin: 0 auto;;
        border-top: 1px solid;
        position: absolute;
        top: 100%;
        left: 0;
        right: 0;
        text-align: center;
        font-size: 12px;
        max-width: 1000px
    }

    .bold {
        font-weight: bold;
    }

    .entry-table .bold {
        background: #fff;
        color: #000;
    }

    .entry-table .bold td, tr.borders td {
        padding: 0;
        line-height: 20px
    }

    .footer table tr td {
        line-height: 13px;
    }

    .footer p {
        margin: 4px 0;
    }

    span {
        font-size: 17px;
        position: relative;
        top: 2.5px;
        left: -5px;
    }

    .signature {
        float: right;
        width: 100%;
        position: relative;
        top: 31px
    }

    .signature td {
        float: right;
        padding-right: 65px;
    }

    .amount {
        float: right;
        width: 100%;;
        position: relative;
        top: 50px
    }

    table.inputUppercase {
        width: 70%;
        font-size: 15px;
    }

    th {
        font-size: 22px;
    }

    .description {
        width: 100%;
    }

    .description td {
        width: 50%;
        font-size: 12px;
        padding: 0 15px;
        text-align: left;
        box-sizing: border-box;
    }

    .description td:nth-of-type(2) {
        position: relative;
        top: -10px;
        padding-right: 0;
    }

    .right {
        text-align: right;
    }

    .borders {
        font-weight: bold;
    }

    .pagebreak {
        page-break-before: always;
    }

    @media print {
        body {
            -webkit-print-color-adjust: exact;
        }

        .footer {
            bottom: 0px
        }
    }
</style>
<body>
<div class="container">
        <table>
            <thead>
            <tr>
                <th><h3 style="text-align:left;">${companyName}</h3></th>
                <td style="text-align:right">
                    <table class="less-height">
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="3"><h3 style="margin:0">Sale Invoice</h3></th>
            </tr>
            </thead>
        </table>
        <table>
            <tbody>
            <tr>
                <td>
                    <table class="inputUppercase">
                        <tbody>
                        <tr>
                            <td>custumer code</td>
                            <td>${accountCode}</td>
                        </tr>
                        <tr>
                            <td>custumer name</td>
                            <td>${accountMap.get(accountCode)}</td>
                        </tr>
                        <tr>
                            <td style="height: 17px"></td>
                        </tr>
                        <tr>
                            <td>order number</td>
                            <td>${voucherNumber}</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
                <td style="float: right;width:70%;">
                    <table class="right">
                        <tbody>
                        <tr>
                            <td style="height: 27px"></td>
                        </tr>
                        <tr>
                            <td>Voucher Date</td>
                            <td>${voucherDate}</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <table class="entry-table">
            <c:set var="count" value="0"/>
            <c:set var="totalGrossAmount" value="0"/>
            <c:set var="totalNetAmount" value="0"/>
            <thead>

            <tr class="bold">
                <td width="5%">S.No</td>
                <td width="35%">description</td>
                <td width="5%">qty</td>
                <td width="10%">rate</td>
                <td width="8%">net amount</td>

            </tr>
            </thead>
            <c:set var="sno" value="1"/>


            <tbody>
            <c:forEach items="${voucherList}" var="voucher" >
                <c:choose>
                    <c:when test="${voucher.credit==0}">

                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td>${sno}</td>
                            <td>${accountMap.get(voucher.accountCode)}</td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="0"
                                                  value="${voucher.itemQuantity}"/></td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="0" value="${voucher.itemRate}"/></td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="0" value="${voucher.credit}"/></td>
                            <c:set var="totalNetAmount" value="${totalNetAmount+voucher.credit}"/>
                            <c:set var="count" value="${count+1}"/>

                        </tr>
                    </c:otherwise>
                </c:choose>
                <c:set var="sno" value="${sno+1}"/>
                <%--                    <c:choose>
                                        <c:when test="${sno%11 == 0}">
                                            <div class="pagebreak"> </div>
                                        </c:when>
                                    </c:choose>--%>
            </c:forEach>
            <tr class="borders" style="border-top: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;">
                <td>no of items : ${count}</td>
                <td>total amount ----------------- <span>></span> Rs</td>
                <td></td>
                <td></td>
                <td><fmt:formatNumber type="number" maxFractionDigits="0" value="${totalNetAmount}"/></td>
            </tr>
            </tbody>
        </table>
        <table class="line-gaps">
            <tr class="signature">
                <td>Signature</td>
            </tr>
            <tr class="stamp">
                <td style="padding-top: 75px;"><p>For <b>${companyName}</b></p></td>
            </tr>
        </table>
</div>

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${contextPath}/resources/js/numberToWords.js"></script>
<script>
    $(document).ready(function () {
        $("td[id^='netNumericAmount']").each(function(i,e){
            console.log(cleanNumber($(e).html()));
            $("#wordingAmountSpan" + i).text(getAmount(cleanNumber($(e).html())));
        });
    });

    function getAmount(numericAmount) {
        return numberToWords.toWords(numericAmount);
    }
    function cleanNumber(num) {
        num = num + ' ';
        if (num.indexOf("(") > -1) {
            num = num.replace("(", "-");
        }
        num = Number(num.replace(/\D+$/g, "").replace(/[,| |\$]/g, '').replace(/[,| |\%]/g, ''));
        return isNaN(num) ? 0 : num;
    }

    function numberFormat(number, dp){
        if (isNaN(number))
            number = 0;

        if (dp == "" || typeof dp == "undefined") {
            dp = 0;
        }
        var a;
        if (parseFloat(number) < 0) {
            a = Number(number).toFixed(dp).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/-/, "($") + ")";

        } else {
            a = Number(number).toFixed(dp).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        return a;
    }

</script>
</body>
</html>