<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create Voucher</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/select2.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">
    ${voucherAddSuccess}

    <h2 class="form-signin-heading">Sales Voucher</h2>
    <form:form method="POST" modelAttribute="voucherList" id="formid">
        <div class="container">

            <div class="row">
                <div class="container">
                    <div class="col-md-2">
                        <div>
                            <label>Voucher Number</label>
                            <form:input id="voucherNumber" readonly="true" path="deliveryNumber" class="form-control"
                                        autofocus="true" value="${voucherNumber}"></form:input>
                        </div>
                        <div>
                            <label>Date</label>
                            <input style="display: none" type="date" class="form-control " id="billDate"
                                   autofocus="true" value="${currentDate}" required="required"/>
                            <form:input type="date" path="createDate" class="form-control " id="currentDate"
                                        autofocus="true" value="${currentDate}" required="required"/>
                        </div>
                        <div>
                            <label>Salesman :</label>
                            <select name="salesman" class="form-control" id="salesman" required="required">
                                <c:forEach items="${salesmanList}" var="salesman">
                                    <option value="${salesman.distributorId}">${salesman.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div>
                            <label>Customer Name</label>

                            <form:select path="customerAccountCode" id="customerAccountCode" onchange="getCustomerDetail(3);" class="form-control">
                            <form:option value="0">NIL</form:option>
                            <c:forEach items="${customerAccounts}" var="account">
                           <form:option value="${account.accountCode}">${account.title}
                            <c:choose>
                                <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                </c:when>
                            </c:choose>
                           </form:option>
                           </c:forEach>
                            </form:select>
                        </div>
                        <div>
                            <label>Email</label>
                            <form:input path="email" type="email" id="email" class="form-control" requried="true" ></form:input>
                        </div>
                        <div>
                            <label>LANDLINE</label>
                            <form:input path="landline" id="landline" type="text" class="form-control" readonly="true" ></form:input>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div>
                            <label>MOBILE Format(03xx-xxxxxxx)</label>
                            <form:input path="mobile" id="mobile" maxlength="12" onKeyup="insertHifensForMobile();" onchange="getCustomerDetail(1);" type="text" class="form-control" requried="true" ></form:input>
                        </div>
                        <div>
                            <label>CNIC Format(xxxx-xxxxxxx-x)</label>
                            <form:input path="cnic" id="cnic" onKeyup="insertHifensForCnic();" maxlength="15" onchange="getCustomerDetail(2);" type="text" class="form-control" requried="true" ></form:input>
                        </div>
                        <div>
                            <label>Tax Detail</label>
                            <form:select path="taxStatus" class="form-control" id="taxStatus" >
                                <form:option value="NON-FILER">NON-FILER</form:option>
                                <form:option value="FILER">FILER</form:option>
                            </form:select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div>
                            <label>Address</label>
                            <form:textarea path="address"  id="address" cssStyle="height: 92px;" class="form-control" ></form:textarea>
                        </div>
                        <div class="pull-right" style="margin-top: 10%;">
                            <button id="searchBtn" onclick="getCustomerDetail(); return false;" >Search</button>
                            <button id="resetBtn" onclick="resetForm(); return false;" >Reset</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>


        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>
                    <th width="18%">Model</th>
                    <th width="8%">Apartment No</th>
                    <th width="20%">Description</th>
                    <th width="12%">Rate</th>
                    <th width="10%">Commission Amount</th>
                    <th width="12%">Amount</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${voucherList.voucherList}" varStatus="status">

                    <tr>

                        <form:input id="voucherNumber" type="hidden" path="voucherList[${status.index}].voucherNumber"
                                    class="form-control"
                                    autofocus="true" value="${voucherNumber}"></form:input>
                        <form:input type="hidden" path="voucherList[${status.index}].billNumber"
                                    class="form-control theBillNumber"
                                    autofocus="true"></form:input>
                        <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].billDate"
                                    class="form-control theBillDate"
                                    autofocus="true" value="${currentDate}" required="required"></form:input>
                        <td>
                            <form:select path="voucherList[${status.index}].itemAccount" class="form-control stock"
                                         onchange="updateStock(this.value,${status.index})" id="stock${status.index}">
                                <form:option value="0">NIL</form:option>
                                <c:forEach items="${stockAccounts}" var="account">
                                    <form:option
                                            value="${account.accountCode}">${account.accountStock.name} </form:option>
                                </c:forEach>
                            </form:select>
                        </td>
                        <td>
                            <form:select id="chassisNo${status.index}" path="voucherList[${status.index}].apartmentNo"
                                         class="form-control chassisNo" onchange="getSelectedChassis(${status.index})">
                                <form:option value="0">NIL</form:option>
                            </form:select>
                        </td>
                        <td>
                            <input type="text" readonly="true" class="form-control description" id="innerDescription${status.index}"
                                   autofocus="true"></input>
                        </td>
                        <td>
                            <form:input type="number" id="innerRate${status.index}"
                                        path="voucherList[${status.index}].itemRate" class="form-control rate"
                                        onblur="calculateAmount(${status.index})"
                                        autofocus="true" value="0.00"></form:input>
                        </td>
                        <td>
                            <form:input type="number" id="commissionAmount${status.index}"
                                        path="voucherList[${status.index}].commissionAmount" class="form-control commissionAmount"
                                        onblur="calculateAmount(${status.index})" autofocus="true"
                                        value="0.00"></form:input>

                        </td>
                        <td>
                            <form:input readonly="true" type="number" id="innerAmount${status.index}"
                                        path="voucherList[${status.index}].credit" class="form-control amountTotal"
                                        autofocus="true" value="0.00"></form:input>

                        </td>

                        <form:input type="hidden" path="voucherList[${status.index}].salesmanId"
                                    class="form-control salesmanId"
                                    autofocus="true"></form:input>
                        <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].voucherDate"
                                    class="form-control theDate"
                                    autofocus="true" value="${currentDate}" required="required"></form:input>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <form:input id="discountAmount" type="hidden" path="discountAmount" value="0.0"></form:input>
        <form:checkbox id="doPrint" cssStyle="display: none" path="doPrint" value="false"></form:checkbox>
        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">

        <div class="totalBillSec">
            <p>Total Rs <span>${total}</span></p>
        </div>
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/select.min.js"></script>

<script>

    $(document).ready(function () {

        $('#customerAccountCode').select2();
        $('.theDate').val($('#currentDate').val());
        $('.theBillDate').val($('#billDate').val());
        $('.theBillNumber').val($('#billNumber').val());
        $('.salesmanId').val($('#salesman').val());
        $('#currentDate').change(function () {
            $('.theDate').val($('#currentDate').val());

        });

        $('#billDate').change(function () {
            $('.theBillDate').val($('#billDate').val());

        });
        $('#billNumber').change(function () {
            $('.theBillNumber').val($('#billNumber').val());

        });
            $('#salesman').change(function () {
                $('.salesmanId').val($('#salesman').val());

            });

        $(".amountTotal").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            var balance = (+$('#balance').val() + +netTotal).toFixed(2);
            $('#balance').html(balance);
            $(".totalBillSec span").html(netTotal);
            $("#side_sale").html(netTotal);
            $("#side_total").html(netTotal);
            limitValidation();
        });

        $(".rate").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            var balance = (+$('#balance').val() + +netTotal).toFixed(2);
            $('#balance').html(balance);
            $(".totalBillSec span").html(netTotal);
            $("#side_sale").html(netTotal);
            $("#side_total").html(netTotal);
            limitValidation();
        });

        // For Discount Flat Operations
        $("#discountValue").change(function () {
            $("#discountRate").val(0.0);
            parseFloat(($("#discountAmount").val($("#discountValue").val()) ));
            parseFloat(($("#side_discount").html($("#discountAmount").val())));
            $("#side_total").html($("#side_sale").html() - $("#discountAmount").val());
        });

        // For Discount Rate Operations
        $("#discountRate").change(function () {
            $("#discountValue").val(parseFloat(calculateAmountTotal() * ($("#discountRate").val() / 100 )).toFixed(2));
            $("#discountAmount").val($("#discountValue").val());
            $("#side_discount").html($("#discountAmount").val());
            ($("#side_total").html($("#side_sale").html() - $("#discountAmount").val()));
        });

        function calculateAmountTotal() {
            var netTotal = 0;
            $(".amountTotal").each(function (index, ele) {
                netTotal += parseFloat($(ele).val());
            });
            console.log("total", netTotal);
            return netTotal.toFixed(2);
        }

    });
    function calculateAmount(id) {
        var amount = parseFloat($('#innerRate' + id).val() ); //+ parseFloat($('#commissionAmount' + id).val());
         $('#innerAmount' + id).val(amount);
          $('#innerAmount' + id).trigger('blur');

    }

    function getCustomerDetail(searchCase) {

        var mobile = $('#mobile').val();
        var cnic = $('#cnic').val();
        var customerAccountCode = $('#customerAccountCode').val();
        if(searchCase == 1){
            cnic = '';
        }else if(searchCase == 2){
            mobile = '';
        }
        else{
           cnic = '';
           mobile = '';
        }
        $.ajax({
            url: '${contextPath}/Account/getCustomerDetails',
            contentType: 'application/json',
            data: {
                mobile:mobile,
                cnic: cnic,
                customerAccountCode: customerAccountCode
            },

            success: function (response) {
                if(response) {
                    $('#address').val(response.address);
                    $('#mobile').val(response.mobile);
                    $('#cnic').val(response.cnic);
                    $('#landline').val(response.landline);
                    $('#taxStatus').val(response.taxStatus);
                    $('#email').val(response.emailAddress);
                    $('#customerAccountCode').val(response.accountCode);
                    //$('#customerAccountCode').val(response.accountCode).trigger('change.select2');

                }

            }
        });

    }

    function resetForm() {
        $('#address').val('');
        $('#mobile').val('');
        $('#cnic').val('');
        $('#landline').val('');
        $('#email').val('');
        $('#customerAccountCode').val(null);
    }

    function limitValidation() {

        console.log(parseInt($('#limit').html()));
        if (parseInt($('#balance').html()) > parseInt($('#limit').html()) && parseInt($('#limit').html()) != 0) {
            alert("Limit exceeded for this Customer. Limit was " + ($('#limit').html()));
            $('#btn_submit').prop('disabled', true)
        }
        else {

            $('#btn_submit').prop('disabled', false)
        }

    }

    function submit_save() {

        $("#formid").submit();
    }

    function submit_print() {

        document.getElementById("doPrint").checked = true;
        $("#formid").submit();
    }

    function goBack() {
        window.location = "${contextPath}/Sales/viewSalesVoucher";
    }

    function updateStock(accountCode, id) {
        $.ajax({
            url: '${contextPath}/Account/ajax/getApartmentList',
            contentType: 'application/json',
            data: {
                accountCode: accountCode,
                isUpdate: false
            },
            success: function (response) {
                var value;
                $("#chassisNo" + id).empty();
                var model = document.getElementById('chassisNo' + id);
                Object.keys(response).forEach(function (key) {
                    var modelOpt = document.createElement('option');
                    value = response[key];
                    modelOpt.innerHTML = value;
                    modelOpt.value = key;
                    model.appendChild(modelOpt);
                });
                getSelectedChassis(id);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }

        });
    }

    function getSelectedChassis(id) {
        $.ajax({
            url: '${contextPath}/Account/ajax/getSelectedApartment',
            contentType: 'application/json',
            data: {
                apartmentId: $("#chassisNo" + id).val()
            },
            success: function (response) {
                $("#innerDescription" + id).val(response.description);
                $("#innerRooms" + id).val(response.rooms);
                $("#innerFloor" + id).val(response.floor);
                $("#innerRate" + id).val(response.sellingPrice);
                $("#innerRate" + id).trigger('blur');
                $("#innerAmount" + id).trigger('blur');
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }

        });

    }
function insertHifensForMobile(){
  if($('#mobile').val().toString().length==4){
  var res = $('#mobile').val().concat('-');
  $('#mobile').val(res);
  }
}
function insertHifensForCnic(){
  if($('#cnic').val().toString().length==5||$('#cnic').val().toString().length==13){
  var res = $('#cnic').val().concat('-');
  $('#cnic').val(res);
  }
}
</script>
</body>
</html>
