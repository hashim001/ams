<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Delivery Challan - ${challan.description}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style>
        .main-table{
            width: 750px;
            margin: 0 auto;
            box-sizing: border-box;
            font-size: 15px ;border-spacing: 0;
        }
        table>thead{ width: 100%; }
        table >tbody { width: 100%; }
        table >tbody tr {width: 100%; }
        table p{
            display: inline-block;
            min-width: 50px;
            border-bottom: 1px dotted;
            padding: 0 3px;
            max-width: 125px;
			line-height: 20px;
    margin: 0;
    overflow: hidden;
    position: relative;
    top: 4px;
        }
        table.signs p{
            text-align: center;
            border-top: 1px solid;
            margin: 0 5px ;
            margin-top: 70px;
            border-bottom: 0;

        }
        tfoot>tr>td:last-child{
            text-align: right;
        }
        tfoot>tr:first-child>td{
            padding-top: 45px;
        }
        tfoot>tr>td{
            padding-top: 35px;
        }
    </style>
</head>
<body>
    <table class="main-table">
        <thead>
            <tr>
                <th colspan="4" style="text-align: left; font-size: 20px; white-space: nowrap; ">Company Name</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td >S: #<p></p> </td>
            <td colspan="3" style="text-align: center;padding: 25px 75px;"><p style="border: 2px solid;margin: 0;padding: 10px 0; min-width: 100%;">GOOD REJECTION NOTE</p></td>
            <td style="padding-left: 25px">White = Purchase Dept. <br> Yellow = Main Gate <br> Green  = Store</td>
        </tr>
        <tr style="line-height: 50px;">
            <td colspan="2">Supplier - <p style="max-width: 100%;">${challan.challanPurchaseOrder.supplierAccount.title}</p> </td>
            <td >Ch/bill# <p>${challan.dcNumber}</p></td>
            <td >Veh# <p></p> </td>
            <td >Ch. Date: <p><fmt:formatDate type = "date" value = "${challan.challanPurchaseOrder.orderDate}" /></p></td>
        </tr>
        <tr style="line-height: 50px;">
            <td >Supplier DC # <p> ${challan.vendorDc} </p> </td>
            <td >P.O # <p>${challan.challanPurchaseOrder.orderNumber}</p></td>
              <td colspan="2">PR# <p style="font-size:12px">${challan.challanPurchaseOrder.poPurchaseRequest.requestNumber} </p></td>
            <td >Receiving Date: <p><fmt:formatDate type = "date" value ="${challan.date}"/></p> </td>
        </tr>
        <tr>
            <td width="100%; " colspan="5">
                <table style="border: 1px solid; border-spacing: 0; width: 100%; text-align: center; ">
                    <tbody>
                        <tr>
                            <td style="border: 1px solid;">S.#</td>
                            <td style="border: 1px solid;">Description</td>
                            <td style="border: 1px solid;">Unit</td>
                            <td style="border: 1px solid;">Qty</td>
                            <td style="border: 1px solid;">Rate</td>
                            <td style="border: 1px solid;">Amount</td>
                            <td style="padding: 0">
                                <table style=" border:1px solid; border-spacing: 0; border-collapse: collapse; width: 100%;  ">
                                    <tbody>
                                    <tr><td style="border: 1px solid;" colspan="2">Previous</td></tr>
                                    <tr>
                                        <td style="border: 1px solid;">Rate</td>
                                        <td style="border: 1px solid;">Date</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td style="border: 1px solid;">Remarks</td>
                        </tr>
               <c:set var="count" value="1" ></c:set>
                <c:forEach items="${challan.challanProductList}" var="product" varStatus="productStatus">
                    <c:choose>
                        <c:when test="${product.deliveredQuantity > 0}">
                            <tr>
                                <td>${productStatus.index + 1}</td>
                                <td>${product.itemName}</td>
                                <td></td>
                                <td>${product.deliveredQuantity}</td>
                                <td>${product.unitPrice}</td>
                                <td>${product.deliveredQuantity*product.unitPrice}</td>
                                <c:set var="count" value="${count + 1}" ></c:set>
                            </tr>
                        </c:when>
                    </c:choose>
                </c:forEach>

                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5" style="width: 100%">
                <table class="signs" style="width: 100%">
                    <tbody>
                    <tr>
                        <td>
                            <p>Store manager</p>
                        </td>
                        <td>
                            <p>H.O.D</p>
                        </td>
                        <td>
                            <p>Purchase Dept</p>
                        </td>
                        <td>
                            <p>Account</p>
                        </td>
                        <td>
                            <p>G M Admin</p>
                        </td>
                        <td>
                            <p>G M Plant</p>
                        </td>
                        <td>
                            <p>Director</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>



        </tbody>
        <tfoot>
        <tr>
            <td colspan="3">Rejected Material _ _ _ _ _ _ _ _ _ _ _ _</td>
            <td colspan="2" style="text-align: right; ">Returned By _ _ _ _ _ _ _ _ _ _ _ _</td>
        </tr>
        <tr>
            <td colspan="3" >Gate Pass # _ _ _ _ _ _ _ _ _ _ _ _</td>
            <td colspan="2" style="text-align: right; ">Dated: _ _ _ _ _ _ _ _ _ _ _ _</td>
        </tr>
        </tfoot>
    </table>


</body>
</html>