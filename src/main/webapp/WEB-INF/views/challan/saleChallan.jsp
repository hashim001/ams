<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Delivery Challan - ${challan.description}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style>
        body{font-family: "Open Sans";}
        table.mainTable{width: 100%;max-width: 800px;margin: 0 auto;}
        .tableHeaderSec td{vertical-align: top}
        .tableHeaderSec h4{margin: 0;padding: 0;display: block;}
        .tableHeaderSec h4 span{margin: 0;padding: 5px 0;display: block;font-weight: normal;font-size: 14px;}
        .tableHeaderSec .logoArea{margin-top: 10px;}
        .tableHeaderSec .logoArea img{max-width: 100%;}

        .deliveryArea{text-align: right;}
        .deliveryArea h4{color: #004269;margin-bottom: 15px;}
        .deliveryArea table{float: right;border-spacing: 0;}
        .deliveryArea table td{font-size: 13px;}
        .deliveryArea table td:first-child{text-align: right;padding-right: 15px;}
        .deliveryArea table td:last-child{border: 1px solid #ddd;padding: 2px 10px;text-align: left}

        .orderDetailArea{border-spacing: 0}
        .orderDetailArea > td{padding-top: 15px;}
        .orderDetailArea h6{margin: 0;padding: 0;display: block;background: #004269;padding: 5px 10px;color: #fff;font-size: 13px;}
        .orderDetailArea table{width: 100%;border-spacing: 0}
        .orderDetailArea table td{font-size: 13px;padding: 3px 10px;border: 1px solid #004269;border-top: 0}

        table.mainTable.productDescTableSec{margin-top: 15px;border-spacing: 0}
        .productDescTableSec th{background: #004269;padding: 5px 10px;color: #fff;font-size: 13px;text-align: left}
        .productDescTableSec tr:nth-child(even){background: #ececec;}
        .productDescTableSec tr td{padding: 5px 10px;font-size: 12px;border: 1px solid #004269;border-top: 0;border-right: 0;}
        .productDescTableSec tr td:last-child{border-right: 1px solid #004269;}
        .productDescTableSec tfoot tr td:first-child{border-bottom: 0;border-left: 0;background: #fff;}
        .productDescTableSec tfoot tr td:nth-child(2){text-transform: uppercase}
        .footerDetailSec{font-size: 13px;text-align: center;}
        .footerDetailSec p{margin: 1px 0;}
        .footerDetailSec h2{margin: 15px 0 0 0;}

        .mainTable.subTotalTableSec{width: 340px;float: right;border-spacing: 0;}
        .mainTable.subTotalTableSec td{padding: 5px 10px;font-size: 12px;border: 1px solid #004269;border-top: 0;border-right: 0;}
        .mainTable.subTotalTableSec td:last-child{border-right: 1px solid #004269;}
        .mainTable.subTotalTableSec tr:nth-child(even){background: #ececec;}

        @media print {
            body{-webkit-print-color-adjust: exact !important;}
        }

    </style>
</head>
<body>
<table class="mainTable tableHeaderSec">
    <tbody>
    <tr>
        <td width="50%">
            <div class="logoArea">
                <img src="${staticInfo.imageUrl}" alt="img">
            </div>
            <h4 style="font-family: 'Lucida Handwriting';">
                <span>${staticInfo.companyAddress}</span>
                <span>${staticInfo.addressSecondary}</span>
                <span>Phone : ${staticInfo.phone}</span>
                <span>Mob: ${staticInfo.mobile}</span>

            </h4>

        </td>
        <td class="deliveryArea" width="50%">
            <h3>Delivery Challan</h3>
            <table>
                <tr>
                    <td style="font-size: 18px">Date</td>
                    <td style="font-size: 18px; white-space: nowrap;">${challan.challanPurchaseOrder.orderDate}</td>
                </tr>
                <tr>
                    <td style="font-size: 18px">DC#</td>
                    <td style="font-size: 18px; white-space: nowrap;">${challan.dcNumber}</td>
                </tr>


            </table>
        </td>
    </tr>
    <tr>
        <td width="100%">
            <h3>Messer:   <span style="alignment: center;white-space:  nowrap;">${challan.challanPurchaseOrder.customerAccount.title}</span></h3>
            <h3>Purchase Order:    <span>${challan.challanPurchaseOrder.orderNumber}</span></h3>
        </td>
    </tr>
    </tbody>
</table>
<table class="mainTable">
    <tr>
        <td>
            <table class="mainTable productDescTableSec">
                <thead>
                <tr>
                    <th width="10%">Item#</th>
                    <th>Description</th>
                    <th width="20%">Quantity</th>

                </tr>
                </thead>
                <tbody>

                <c:set var="loopCount" value="${0}" ></c:set>
                <c:forEach items="${challan.challanProductList}" var="product" varStatus="productStatus">
                    <c:choose>
                        <c:when test="${product.deliveredQuantity > 0}">
                            <tr style="height: 26.55px">
                                <td>${productStatus.index + 1}</td>
                                <td>${product.itemName}</td>
                                <td>${product.deliveredQuantity}</td>
                                <c:set var="loopCount" value="${loopCount + 1}" ></c:set>
                            </tr>
                        </c:when>
                    </c:choose>
                </c:forEach>
                <c:forEach begin="${loopCount}" end="14" >
                    <tr style="height: 26.55px">
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                </c:forEach>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </td>
    </tr>
</table>
<table class="mainTable footerDetailSec">
    <tr>
        <td>
            <h2>Thanks you for your business!</h2>
            <p>Should you have any enquiries concerning this delivery note, feel free to contact.</p>
            <hr>
            <p>${staticInfo.companyAddress},${staticInfo.addressSecondary}</p>
            <p>Mob: ${staticInfo.mobile} Email: ${company-email} Web: ${company-website}</p>
        </td>
    </tr>
</table>



</body>
</html>
