<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Stock</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>

<div class="container">
    ${AccountAddSuccess}


    <section class="main">
        <div class="container">
            <div class="chartAcc">
                <div class="row">
                    <div class="col-sm-6">
                        <h2 class="heading-light">Add Stock Details</h2>
                        <div class="inputContainer">
                            <form:form method="POST" modelAttribute="accountForm">
                            <div class="inputContainer">
                                <spring:bind path="title">

                                    <form:input type="text" path="title" placeholder="Account Title"
                                                autofocus="true"></form:input>
                                    <form:errors path="title"></form:errors>

                                </spring:bind>
                                <spring:bind path="parentCode">

                                    <form:input type="hidden" path="parentCode" value="${stockHead}"
                                                autofocus="true"></form:input>
                                    <form:errors path="parentCode"></form:errors>

                                </spring:bind>

                                <spring:bind path="openingBalance">
                                    <form:label path="openingBalance">Opening Balance </form:label>
                                    <form:input id="openingBalance" onblur="toggleAccountType()" type="text"
                                                path="openingBalance" placeholder="Opening Balance"></form:input>
                                    <form:errors path="openingBalance"></form:errors>

                                </spring:bind>


                                <label>Select Account Type </label>
                                <select id="accountType">
                                    <option id="credit" value="credit">Credit</option>
                                    <option id="debit" value="debit">Debit</option>

                                </select>

                                <spring:bind path="openingBalanceLast">

                                    <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                    <form:input id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text"
                                                path="openingBalanceLast"
                                                placeholder="Last Opening Balance"></form:input>
                                    <form:errors path="openingBalanceLast"></form:errors>

                                </spring:bind>
                                <label>Select Account Type Last </label>
                                <select id="accountTypeLast">
                                    <option id="creditLast" value="credit">Credit</option>
                                    <option id="debitLast" value="debit">Debit</option>

                                </select>

                                <spring:bind path="openingCredit">
                                    <form:input type="hidden" path="openingCredit" id="openingCredit"
                                                value="0.0"></form:input>

                                </spring:bind>
                                <spring:bind path="openingDebit">

                                    <form:input type="hidden" path="openingDebit" id="openingDebit"
                                                value="0.0"></form:input>

                                </spring:bind>
                                <spring:bind path="lastOpeningCredit">
                                    <form:input type="hidden" path="lastOpeningCredit" id="lastOpeningCredit"
                                                value="0.0"></form:input>

                                </spring:bind>
                                <spring:bind path="lastOpeningDebit">

                                    <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit"
                                                value="0.0"></form:input>

                                </spring:bind>

                                <spring:bind path="openingDate">

                                    <form:input type="Date" path="openingDate" value="${currentDate}"
                                                required="required"></form:input>
                                    <form:errors path="openingDate"></form:errors>

                                </spring:bind>

                                <button type="submit">submit</button>

                            </div>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputContainer">



                            <spring:bind path="accountStock.categoryId">

                                <form:label path="accountStock.categoryId">Stock Category</form:label>
                                <form:select path="accountStock.categoryId">
                                    <c:forEach items="${categories}" var="category">
                                        <form:option value="${category.categoryId}">${category.title}</form:option>
                                    </c:forEach>
                                </form:select>


                            </spring:bind>

                            <spring:bind path="accountStock.description">


                                <form:label path="accountStock.description">Description</form:label>
                                <form:input type="text" path="accountStock.description" placeholder="Description"></form:input>
                                <form:errors path="accountStock.description"></form:errors>

                            </spring:bind>

                            <spring:bind path="accountStock.quantity">

                                <form:label path="accountStock.quantity">Current Quantity</form:label>
                                <form:input type="text" path="accountStock.quantity" placeholder="Quantity"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.quantity"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.openingQuantity">

                                <form:label path="accountStock.openingQuantity">Opening Quantity</form:label>
                                <form:input type="text" path="accountStock.openingQuantity" placeholder="Opening Quantity"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.openingQuantity"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.cost">

                                <form:label path="accountStock.cost">Cost/Unit</form:label>
                                <form:input type="text" path="accountStock.cost" placeholder="Cost"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.cost"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.sellingPrice">

                                <form:label path="accountStock.sellingPrice">Selling Price/Unit</form:label>
                                <form:input type="text" path="accountStock.sellingPrice" placeholder="Selling Price"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.sellingPrice"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.unitMeasure">

                                <form:label path="accountStock.unitMeasure">Measuring Unit</form:label>
                                <form:input type="text" path="accountStock.unitMeasure" placeholder="Measuring Unit"
                                            autofocus="true"></form:input>
                                <form:errors path="accountStock.unitMeasure"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.itemDate">

                                <form:label path="accountStock.itemDate">Item Date</form:label>
                                <form:input type="Date" path="accountStock.itemDate" value="${currentDate}" required="required"></form:input>
                                <form:errors path="accountStock.itemDate"></form:errors>

                            </spring:bind>
                        </div>



                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function () {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function () {

            toggleAccountTypeLast();
        });
    });

    function toggleAccountType() {

        if ($('#accountType').val() == "credit") {

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        } else if ($('#accountType').val() == "debit") {

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if ($('#accountTypeLast').val() == "credit") {

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        } else if ($('#accountTypeLast').val() == "debit") {

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }


</script>
</body>
</html>
