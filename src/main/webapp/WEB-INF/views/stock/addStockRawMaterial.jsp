<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
        <!-- default header name is X-CSRF-TOKEN -->
        <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <title>Add Raw Material</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${stockAddSuccess}
        ${stockEditSuccess}
        ${stockRemoveSuccess}
        ${AccountAddSuccess}

            <h2 class="heading-main">Raw Material Items
            <span class="addIcon addForm" data-toggle="modal"
                  data-target="#myModal2"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTableStock">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Purchase Rate</th>
                            <th width="23%">Category</th>
                            <th>Max Level</th>
                            <th>Min Level</th>
                            <th>Measuring Unit</th>
                            <th>Opening Quantity</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" onclick="refreshPage()" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Raw Material Detail</h2>
            </div>
            <form:form method="POST" modelAttribute="rawMaterialForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">

                            <form:input type="hidden" path="rawMaterialId" ></form:input>
                            <spring:bind path="name">
                                <form:label path="name">Material Name</form:label>
                                <form:input type="text" path="name" placeholder="Title" autofocus="true" required="true" ></form:input>
                                <form:errors path="name"></form:errors>
                            </spring:bind>

                            <spring:bind path="categoryId">

                                <form:label path="categoryId">Stock Category</form:label>
                                <form:select id="category" path="categoryId" required="true" >
                                    <c:forEach items="${categories}" var="category">
                                        <form:option value="${category.categoryId}">${category.title}</form:option>
                                    </c:forEach>
                                </form:select>


                            </spring:bind>
                            <spring:bind path="unitMeasure">

                                <form:label path="unitMeasure">Measuring Unit</form:label>
                                <form:input id="measureUnit" type="text" path="unitMeasure" placeholder="Measuring Unit"
                                            autofocus="true" required="true" ></form:input>
                                <form:errors path="unitMeasure"></form:errors>

                            </spring:bind>

                            <spring:bind path="itemDate">

                                <form:label path="itemDate">Item Date</form:label>
                                <form:input id="itemDate" type="Date" path="itemDate" value="${currentDate}" required="required"></form:input>
                                <form:errors path="itemDate"></form:errors>

                            </spring:bind>
                            <spring:bind path="cost">

                                <form:label path="cost">Last Purchase Rate</form:label>
                                <form:input id="cost" type="text" path="cost" placeholder="LAST PURCHASE RATE"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="cost"></form:errors>

                            </spring:bind>
                            <spring:bind path="openingQuantity">
                                 <form:label path="openingQuantity">Opening Quantity</form:label>
                                <form:input id="openingQuantity"  path="openingQuantity" placeholder="Opening Quantity"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="openingQuantity"></form:errors>

                            </spring:bind>

                        </div>

                        <div class="col-sm-6">
                            <spring:bind path="openingBalance">

                                <form:label path="openingBalance">Opening Balance</form:label>
                                <form:input id="openingBalance" type="text" path="openingBalance" placeholder="Opening Balance"
                                            autofocus="true" value="0.0" required ="true" ></form:input>
                                <form:errors path="openingBalance"></form:errors>

                            </spring:bind>
                            <spring:bind path="description">
                                <form:input  type="hidden" id="description" path="description" placeholder="Description"></form:input>
                                <form:errors path="description"></form:errors>

                            </spring:bind>

                            <spring:bind path="quantity">
                                <form:input  type="hidden"  id="quantity"  path="quantity" placeholder="Quantity"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="quantity"></form:errors>

                            </spring:bind>
                           <spring:bind path="lastPurchaseDate">

                                <form:label path="lastPurchaseDate">Last Purchase Date</form:label>
                                <form:input id="itemDate" type="Date" path="lastPurchaseDate" value="${currentDate}" required="required"></form:input>
                                <form:errors path="lastPurchaseDate"></form:errors>

                            </spring:bind>
                            <spring:bind path="itemLocation">

                                <form:label path="itemLocation">Item Location</form:label>
                                <form:input id="itemLocation" type="text" path="itemLocation" placeholder="Item Location"
                                            autofocus="true" value=""></form:input>
                                <form:errors path="itemLocation"></form:errors>

                            </spring:bind>
                            <spring:bind path="reorderLevel">

                                <form:label path="reorderLevel">Reorder Level</form:label>
                                <form:input id="reorderLevel" type="text" path="reorderLevel" placeholder="Reorder Level"
                                            autofocus="true" value="0"></form:input>
                                <form:errors path="reorderLevel"></form:errors>

                            </spring:bind>
                            <spring:bind path="maxLevel">
                                <form:label path="maxLevel">Max Level</form:label>
                                <form:input type="text" path="maxLevel" placeholder="Max Level"
                                            autofocus="true"  value="0.0"></form:input>
                                <form:errors path="maxLevel"></form:errors>

                            </spring:bind>
                            <spring:bind path="minLevel">
                                <form:label path="minLevel">Min Level</form:label>
                                <form:input type="text" path="minLevel" placeholder="Min Level"
                                            autofocus="true"  value="0.0"></form:input>
                                <form:errors path="minLevel"></form:errors>

                            </spring:bind>

                        </div>

                    </div>
                    <div align="center">

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit" onclick="addRawMaterialForm(); return false;">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" onclick="refreshPage()">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>
var form_action = "edit";
    $(document).ready(function () {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function () {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function () {

            toggleAccountTypeLast();
        });
    });
            $(".addForm").click(function () {
                form_action = 'add';
            });

    function toggleAccountType() {

        if ($('#accountType').val() == "credit") {

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        } else if ($('#accountType').val() == "debit") {

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if ($('#accountTypeLast').val() == "credit") {

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        } else if ($('#accountTypeLast').val() == "debit") {

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }


    function remove(id) {
        if(confirm("Are you sure, you want to delete the Stock Item?")) {
            $('#stockRemove' + id).submit();
        }

    }
    function addRawMaterialForm() {
/*        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $(document).ajaxSend(function (e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });*/
        var fd = $("#rawMaterialForm").serialize();
        $.ajax({
            type: 'POST',
            async: false,
            url: '${contextPath}/RawMaterialStock/addStockRawMaterialAccount',
            data: fd,
            timeout: 600000,
            success: function (data) {
            console.log(form_action)
                if (form_action == "edit") {
                    refreshPage();
                }
                else {
                    addForm(data);
                }
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }

    function addForm(data) {
        $('#myModal2').find('form').trigger('reset');

    }



    $(document).ready(function () {
        $('#dataTableStock').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[0, "desc"]],
            "pageLength": 7,
            "ajax": "${contextPath}/RawMaterialStock/json"
        });
    });
          function setAccountType(response)
        {
    		    if(response.accountDto.openingCredit<response.accountDto.openingDebit){
    		   $('#accountType').val('debit');
    		    }
    		    else{
    		    $('#accountType').val('credit');
    		    }

        }
        function edit(accountCode) {

              console.log(accountCode);
                $.ajax({
                    url: '${contextPath}/RawMaterialStock/ajax/editStockRawMaterial',
                    contentType: 'application/json',

                    data: {
                        rawId: accountCode

                    },
                    success: function (response) {
                        editForm(response);
                        setAccountType(response);
                    },

                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });
            }
			  function editForm(response) {
/*    		    var openingBalance ;
    		    if(response.accountDto.openingCredit>response.accountDto.openingDebit){
    		    openingBalance = response.accountDto.openingCredit;
    		    }
    		    else{
    		    openingBalance = response.accountDto.openingDebit;
    		    }*/
                 $('#myModal2').modal();
                 $('#myModal2').find('form').trigger('reset');
                  $('#myModal2').find('form').find('#rawMaterialId').val(response.rawMaterialId);
                 $('#myModal2').find('form').find('#name').val(response.name);
                 $('#myModal2').find('form').find('#measureUnit').val(response.unitMeasure);
                 $('#myModal2').find('form').find('#itemDate').val(response.itemDate);
                  $('#myModal2').find('form').find('#description').val(response.description);
                 $('#myModal2').find('form').find('#category').val(response.categoryDto.categoryId);
                 $('#myModal2').find('form').find('#quantity').val(response.quantity);
                 $('#myModal2').find('form').find('#openingQuantity').val(response.openingQuantity);
                 $('#myModal2').find('form').find('#cost').val(response.cost);
                 $('#myModal2').find('form').find('#sellingPrice').val(response.sellingPrice);
                 $('#myModal2').find('form').find('#minLevel').val(response.minLevel);
                 $('#myModal2').find('form').find('#maxLevel').val(response.maxLevel);
                 $('#myModal2').find('form').find('#itemLocation').val(response.itemLocation);
                 $('#myModal2').find('form').find('#reorderLevel').val(response.reorderLevel);
                 $('#myModal2').find('form').find('#openingBalance').val(response.openingBalance);

            }
    function goBack(){
     window.location = "${contextPath}/home#stock";
    }
        function refreshPage() {
            window.location.reload();
        }

</script>
</body>
</html>
