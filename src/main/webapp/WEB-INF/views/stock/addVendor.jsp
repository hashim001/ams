<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Vendor</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${vendorAddSuccess}
        ${vendorRemoveSuccess}
            ${AccountAddSuccess}
        <h2 class="heading-main">Vendors<span class="addIcon" data-toggle="modal"
                  data-target="#myModal2"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTableStock">
                        <thead>
                        <tr>
                            <th width="18%">Name</th>
                            <th width="25%">Address</th>
                            <th width="10%">Mobile</th>
                            <th width="10%">CNIC</th>
                            <th width="10%">NTN</th>
                            <th width="10%">LandLine</th>
                            <th width="10%">SaleTax #</th>
                            <th width="7%">Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Vendor Details</h2>
            </div>
            <form:form method="POST" modelAttribute="accountForm" action="${contextPath}/StockVendor/addVendorAccount">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="title">
                                <form:label path="title">Vendor Name</form:label>
                                <form:input type="text" path="title" placeholder="Vendor Name"
                                            autofocus="true"></form:input>
                                <form:errors path="title"></form:errors>

                            </spring:bind>
                            <spring:bind path="parentCode">

                                <form:input type="hidden" path="parentCode" value="${vendorHead}"
                                            autofocus="true"></form:input>
                                <form:errors path="parentCode"></form:errors>

                            </spring:bind>

                            <spring:bind path="stockVendor.mobile">


                                <form:label path="stockVendor.mobile">Mobile Number</form:label>
                                <form:input type="text" id="mobile" path="stockVendor.mobile" placeholder="Mobile #"></form:input>
                                <form:errors path="stockVendor.mobile"></form:errors>

                            </spring:bind>

                            <spring:bind path="stockVendor.landline">

                                <form:label path="stockVendor.landline">Landline #</form:label>
                                <form:input type="text" id="landline" path="stockVendor.landline" placeholder="LandLine#"
                                            autofocus="true"></form:input>
                                <form:errors path="stockVendor.landline"></form:errors>

                            </spring:bind>

                            <spring:bind path="stockVendor.cnic">

                                <form:label path="stockVendor.cnic">CNIC</form:label>
                                <form:input type="text"  id="cnic" path="stockVendor.cnic" placeholder="CNIC"
                                            autofocus="true"></form:input>
                                <form:errors path="stockVendor.cnic"></form:errors>

                            </spring:bind>
                            <spring:bind path="stockVendor.saletaxNumber">

                                <form:label path="stockVendor.saletaxNumber">Sales Tax #</form:label>
                                <form:input type="text" id="saletaxNumber" path="stockVendor.saletaxNumber" placeholder="Sales Tax #"
                                            autofocus="true"></form:input>
                                <form:errors path="stockVendor.saletaxNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="stockVendor.nationalTaxNumber">

                                <form:label path="stockVendor.nationalTaxNumber">NTN #</form:label>
                                <form:input type="text" id="nationalTaxNumber" path="stockVendor.nationalTaxNumber" placeholder="NTN"
                                            autofocus="true"></form:input>
                                <form:errors path="stockVendor.nationalTaxNumber"></form:errors>

                            </spring:bind>
                            <spring:bind path="stockVendor.emailAddress">

                                <form:label path="stockVendor.emailAddress">Email Address</form:label>
                                <form:input type="text" id="emailAddress" path="stockVendor.emailAddress" placeholder="Email Address"
                                            autofocus="true"></form:input>
                                <form:errors path="stockVendor.emailAddress"></form:errors>

                            </spring:bind>
                        </div>

                        <div class="col-sm-6">
                           <spring:bind path="stockVendor.contactPerson">
                                <form:label path="stockVendor.contactPerson">Contact Person</form:label>
                                <form:input id="contactPerson" type="text"
                                            path="stockVendor.contactPerson" placeholder="Contact Person"></form:input>
                                <form:errors path="stockVendor.contactPerson"></form:errors>

                            </spring:bind>
                           <spring:bind path="openingBalance">
                                <form:label path="openingBalance">Opening Balance </form:label>
                                <form:input id="openingBalance" onblur="toggleAccountType()" type="text"
                                            path="openingBalance" placeholder="Opening Balance"></form:input>
                                <form:errors path="openingBalance"></form:errors>

                            </spring:bind>

                            <spring:bind path="openingBalanceLast">

                                <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                <form:input id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text"
                                            path="openingBalanceLast"
                                            placeholder="Last Opening Balance"></form:input>
                                <form:errors path="openingBalanceLast"></form:errors>

                            </spring:bind>
                            <label>Select Account Type Last </label>
                            <select id="accountTypeLast">
                                <option id="creditLast" value="credit">Credit</option>
                                <option id="debitLast" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingCredit">
                                <form:input type="hidden" path="openingCredit" id="openingCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="openingDebit">

                                <form:input type="hidden" path="openingDebit" id="openingDebit"
                                            value="0.0"></form:input>

                            </spring:bind>
                          <spring:bind path="level">

                                <form:input id="level" type="hidden" path="level"
                                            value="0"></form:input>

                            </spring:bind>
                          <spring:bind path="accountCodeForEdit">

                                <form:input id="accountCode" type="hidden" path="accountCodeForEdit"
                                            value="0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningCredit">
                                <form:input type="hidden" path="lastOpeningCredit" id="lastOpeningCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="lastOpeningDebit">

                                <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit"
                                            value="0.0"></form:input>

                            </spring:bind>

                            <spring:bind path="openingDate">
                                <form:label path="openingDate">Opening Date</form:label>
                                <form:input type="Date" path="openingDate" value="${currentDate}"
                                            required="required"></form:input>
                                <form:errors path="openingDate"></form:errors>

                            </spring:bind>

                            <spring:bind path="stockVendor.taxStatus">

                                <form:label path="stockVendor.taxStatus">Tax Status</form:label>
                                <form:select id="taxStatus" path="stockVendor.taxStatus">

                                    <form:option value="FILER">Filer</form:option>
                                    <form:option value="NONFILER">Non-Filer</form:option>

                                </form:select>

                            </spring:bind>

                        </div>

                    </div>
                    <div align="center">
                        <spring:bind path="stockVendor.address">

                            <form:label path="stockVendor.address">Address</form:label>
                            <form:textarea id="address" cssStyle="width: 700px" type="text" path="stockVendor.address" placeholder="Address"
                                           autofocus="true"></form:textarea>
                            <form:errors path="stockVendor.address"></form:errors>

                        </spring:bind>

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>

    $(document).ready(function () {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function () {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function () {

            toggleAccountTypeLast();
        });
    });

    function toggleAccountType() {

        if ($('#accountType').val() == "credit") {

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        } else if ($('#accountType').val() == "debit") {

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if ($('#accountTypeLast').val() == "credit") {

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        } else if ($('#accountTypeLast').val() == "debit") {

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }

    function remove(id) {
        if (confirm("Are you sure, you want to delete?")) {
            $('#vendorRemove' + id).submit();
        }
    }

    function edit(id) {
        //$('#vendorEdit'+ id).submit();
    }

    $(document).ready(function () {
        $('#dataTableStock').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[0, "desc"]],
            "pageLength": 7,
            "ajax": "${contextPath}/StockVendor/json"
        });
    });

    function goBack(){
     window.location = "${contextPath}/home#stock";
    }
        function setAccountType(response)
            {
        		    if(response.accountDto.openingCredit<response.accountDto.openingDebit){
        		   $('#accountType').val('debit');
        		    }
        		    else{
        		    $('#accountType').val('credit');
        		    }

            }

                function edit(accountCode) {

                    $.ajax({
                        url: '${contextPath}/StockVendor/ajax/editVendor',
                        contentType: 'application/json',

                        data: {
                            accountCode: accountCode

                        },
                        success: function (response) {
                            editForm(response);
                             setAccountType(response);
                        },

                        error: function (e) {

                            console.log("ERROR : ", e);

                        }
                    });
                }
        		    function editForm(response) {
        		    var openingBalance ;
        		    if(response.accountDto.openingCredit>response.accountDto.openingDebit){
        		    openingBalance = response.accountDto.openingCredit;
        		    }
        		    else{
        		    openingBalance = response.accountDto.openingDebit;
        		    }
                     $('#myModal2').modal();
                     $('#myModal2').find('form').trigger('reset');
                     $('#myModal2').find('form').find('#title').val(response.accountDto.title);
                     $('#myModal2').find('form').find('#openingDate').val(response.accountDto.openingDate);
                     $('#myModal2').find('form').find('#openingCredit').val(response.accountDto.openingCredit);
                     $('#myModal2').find('form').find('#openingDebit').val(response.accountDto.openingDebit);
                     $('#myModal2').find('form').find('#lastOpeningCredit').val(response.accountDto.lastOpeningCredit);
                     $('#myModal2').find('form').find('#openingBalanceLast').val(response.accountDto.openingBalanceLast);
                     $('#myModal2').find('form').find('#openingBalance').val(openingBalance);
                     $('#myModal2').find('form').find('#parentCode').val(response.accountDto.parentCode);
                     $('#myModal2').find('form').find('#parentName').val(response.accountDto.parentName);
                     $('#myModal2').find('form').find('#totalBalanceAmount').val(response.accountDto.totalBalanceAmount);
                     $('#myModal2').find('form').find('#accountCode').val(response.accountDto.accountCode);
                     $('#myModal2').find('form').find('#description').val(response.description);
                     $('#myModal2').find('form').find('#mobile').val(response.mobile);
                     $('#myModal2').find('form').find('#landline').val(response.landline);
                     $('#myModal2').find('form').find('#saletaxNumber').val(response.saletaxNumber);
                     $('#myModal2').find('form').find('#nationalTaxNumber').val(response.nationalTaxNumber);
                     $('#myModal2').find('form').find('#discountRate').val(response.discountRate);
                     $('#myModal2').find('form').find('#taxStatus').val(response.taxStatus);
                     $('#myModal2').find('form').find('#address').val(response.address);
                     $('#myModal2').find('form').find('#address').val(response.address);
                     $('#myModal2').find('form').find('#cnic').val(response.cnic);
                     $('#myModal2').find('form').find('#emailAddress').val(response.emailAddress);
                     $('#myModal2').find('form').find('#contactPerson').val(response.contactPerson);
                     $('#myModal2').find('form').find('#level').val(response.accountDto.level);

                }

</script>
</body>
</html>
