<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Site</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${stockCategoryAddSuccess}
        ${categoryRemoveSuccess}
        <h2 class="heading-main">Sites <span class="addIcon" data-toggle="modal"
                                                        data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
            <button class="btn btn-success" style="background-color: #292e5a" onClick="window.open('${contextPath}/Stock/printStockCategory');">Print</button>
            <button class="btn btn-success" style="background-color: #292e5a" onClick="window.open('${contextPath}/Stock/printCategoryWithHead');">Print With Head</button>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Site Title</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${categories}" var="category" varStatus="status">

                            <tr>
                                <td>${category.title}</td>

                                <td>
                                    <form method="post" id="categoryRemove${status.index}"
                                          action="${contextPath}/Stock/removeCategory?${_csrf.parameterName}=${_csrf.token}">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="categoryId"
                                               value="${category.categoryId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a href="#" onclick="edit(${category.categoryId})"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                        <li><a href="#" onclick="remove(${status.index})"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel">Add Site</h2>
            </div>

            <form:form method="POST" modelAttribute="categoryForm">
                <div class="modal-body">
                    <spring:bind path="title">

                        <form:label path="title">Title</form:label>
                        <form:input type="text" path="title" placeholder="Category Title"
                                    autofocus="true" required ="true" ></form:input>
                        <form:errors path="title"></form:errors>
                    </spring:bind>

                    <spring:bind path="accountCode">

                        <form:label path="accountCode">Master Account</form:label>
                        <form:select  path="accountCode" >
                            <form:option value="0">-- NIL --</form:option>
                            <c:forEach items="${accounts}" var="account">
                                <form:option value="${account.accountCode}">${account.title} - ${account.parentName}</form:option>
                            </c:forEach>
                        </form:select>

                    </spring:bind>
                </div>
                          <spring:bind path="category">
                                <form:input type="hidden" path="category"
                                            value="0"></form:input>
                            </spring:bind>

                <div class="modal-footer" style="background-color: #1f2d5c" >
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>
    function remove(id) {
        if (confirm("Are you sure, you want to delete?")) {
            $('#categoryRemove' + id).submit();
        }
    }
    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });
            function edit(categoryId) {

                $.ajax({
                    url: '${contextPath}/Stock/ajax/editCategory',
                    contentType: 'application/json',

                    data: {
                        categoryId: categoryId

                    },
                    success: function (response) {
                        editForm(response);
                    },

                    error: function (e) {

                        console.log("ERROR : ", e);

                    }
                });
            }
    		    function editForm(response) {
                 $('#myModal').modal();
                 $('#myModal').find('form').trigger('reset');
                 $('#myModal').find('form').find('#title').val(response.title);
                 $('#myModal').find('form').find('#category').val(response.categoryId);
                 $('#myModal').find('form').find('#accountCode').val(response.accountCode);

            }
       function goBack(){
        window.location = "${contextPath}/home#stock";
       }
       function print(){

       window.location = "${contextPath}/Stock/printStockCategory";
       }
</script>
</body>
</html>
