<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Plot Phase</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${stockAddSuccess}
        ${stockEditSuccess}
        ${stockRemoveSuccess}
        ${AccountAddSuccess}

            <h2 class="heading-main">Plot Phase<span class="addIcon" data-toggle="modal"
                  data-target="#myModal2"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTableStock">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th width="30%">Description</th>
                            <th>Site</th>
                            <th>Selling Price</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h2 class="modal-title" id="myModalLabel2">Phase Details</h2>
            </div>
            <form:form method="POST" modelAttribute="accountForm" action="${contextPath}/AccountSupport/addStockAccount">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <spring:bind path="title">
                                <form:label path="title">Phase Title</form:label>
                                <form:input id="title" type="text" path="title" placeholder="Phase Name"
                                            autofocus="true" required = "true" ></form:input>
                                <form:errors path="title"></form:errors>
                            </spring:bind>
                            <div style="display: none;">
                            <spring:bind path="openingBalance">
                                <form:label path="openingBalance">Opening Balance </form:label>
                                <form:input id="openingBalance" onblur="toggleAccountType()" type="text"
                                            path="openingBalance" placeholder="Opening Balance"></form:input>
                                <form:errors path="openingBalance"></form:errors>

                            </spring:bind>
                            </div>
                            <div style="display: none;">
                            <label>Select Account Type </label>
                            <select id="accountType">
                                <option id="credit" value="credit">Credit</option>
                                <option id="debit" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingBalanceLast">

                                <form:label path="openingBalanceLast">Opening Balance Last </form:label>
                                <form:input id="openingBalanceLast" onblur="toggleAccountTypeLast()" type="text"
                                            path="openingBalanceLast"
                                            placeholder="Last Opening Balance"></form:input>
                                <form:errors path="openingBalanceLast"></form:errors>

                            </spring:bind>
                            <label>Select Account Type Last </label>
                            <select id="accountTypeLast">
                                <option id="creditLast" value="credit">Credit</option>
                                <option id="debitLast" value="debit">Debit</option>

                            </select>

                            <spring:bind path="openingCredit">
                                <form:input type="hidden" path="openingCredit" id="openingCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="openingDebit">

                                <form:input type="hidden" path="openingDebit" id="openingDebit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            </div>
                            <input id="accountCode" name="accountCodeForEdit" type="hidden" value="0">
                            <spring:bind path="lastOpeningCredit">
                                <form:input type="hidden" path="lastOpeningCredit" id="lastOpeningCredit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <div style="display: none;">
                            <spring:bind path="lastOpeningDebit">

                                <form:input type="hidden" path="lastOpeningDebit" id="lastOpeningDebit"
                                            value="0.0"></form:input>

                            </spring:bind>
                            <spring:bind path="level">

                                <form:input id="level" type="hidden" path="level"
                                            value="0"></form:input>

                              </spring:bind>
                            </div>
                            <spring:bind path="openingDate">
                                <form:label path="openingDate">Register Date</form:label>
                                <form:input id="openingDate" type="Date" path="openingDate" value="${currentDate}"
                                            required="required"></form:input>
                                <form:errors path="openingDate"></form:errors>

                            </spring:bind>

                            <spring:bind path="accountStock.description">


                                <form:label path="accountStock.description">Description</form:label>
                                <form:input id="description" type="text" path="accountStock.description" placeholder="Description"></form:input>
                                <form:errors path="accountStock.description"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.categoryId">

                                <form:label path="accountStock.categoryId">Site</form:label>
                                <form:select id="category" path="accountStock.categoryId">
                                    <c:forEach items="${categories}" var="category">
                                        <form:option value="${category.categoryId}">${category.title}</form:option>
                                    </c:forEach>
                                </form:select>
                            </spring:bind>
                            <div style="display: none;">
                            <spring:bind path="accountStock.quantity">

                                <form:label path="accountStock.quantity">Current Quantity</form:label>
                                <form:input id="quantity" type="text" path="accountStock.quantity" placeholder="Quantity"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.quantity"></form:errors>

                            </spring:bind>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div style="display: none;">
                            <spring:bind path="accountStock.unitMeasure">


                                <form:label path="accountStock.unitMeasure">Unit Measure</form:label>
                                <form:input id="measureUnit" type="text" path="accountStock.unitMeasure" placeholder="Unit Measure"></form:input>
                                <form:errors path="accountStock.unitMeasure"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.openingQuantity">

                                <form:label path="accountStock.openingQuantity">Opening Quantity</form:label>
                                <form:input id="openingQuantity" type="number" path="accountStock.openingQuantity" placeholder="Opening Quantity"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.openingQuantity"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.cost">

                                <form:label path="accountStock.cost">Cost/Unit</form:label>
                                <form:input id="cost" type="text" path="accountStock.cost" placeholder="Cost"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.cost"></form:errors>

                            </spring:bind>
                            </div>
                            <spring:bind path="accountStock.sellingPrice">

                                <form:label path="accountStock.sellingPrice">Selling Price/Unit</form:label>
                                <form:input id="sellingPrice" type="number" path="accountStock.sellingPrice" placeholder="Selling Price"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.sellingPrice"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.commissionAmount">

                                <form:label path="accountStock.commissionAmount">Commission Amount</form:label>
                                <form:input id="commissionAmount" type="number" path="accountStock.commissionAmount" placeholder="Selling Price"
                                            autofocus="true" value="0.0"></form:input>
                                <form:errors path="accountStock.commissionAmount"></form:errors>

                            </spring:bind>
                            <spring:bind path="accountStock.makerId">

                                <form:label path="accountStock.makerId">Make</form:label>
                                <form:select id="makerId" path="accountStock.makerId">
                                 <c:forEach items="${makers}" var="maker">
                                        <form:option value="${maker.makerId}">${maker.makerName}</form:option>
                                </c:forEach>
                                </form:select>
                            </spring:bind>
                            <div style="display: none;">
                            <spring:bind path="accountStock.itemDate">

                                <form:label path="accountStock.itemDate">Register Date</form:label>
                                <form:input id="itemDate" type="Date" path="accountStock.itemDate" value="${currentDate}" required="required"></form:input>
                                <form:errors path="accountStock.itemDate"></form:errors>

                            </spring:bind>
                            </div>
                            <spring:bind path="accountStock.enginePrefix">


                                <form:label path="accountStock.enginePrefix">Plot Prefix</form:label>
                                <form:input id="enginePrefix" type="text" path="accountStock.enginePrefix" placeholder="Plot Prefix"></form:input>
                                <form:errors path="accountStock.enginePrefix"></form:errors>

                            </spring:bind>
                            <div style="display: none;">
                            <spring:bind path="accountStock.chassisPrefix">

                                <form:label path="accountStock.chassisPrefix">Chassis Prefix</form:label>
                                <form:input id="chassisPrefix" type="text" path="accountStock.chassisPrefix" placeholder="Chassis Prefix"></form:input>
                                <form:errors path="accountStock.chassisPrefix"></form:errors>

                            </spring:bind>
                            </div>

                        </div>

                    </div>
                    <div align="center">

                    </div>
                </div>
                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</div>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>

          function setAccountType(response)
        {
    		    if(response.accountDto.openingCredit<response.accountDto.openingDebit){
    		   $('#accountType').val('debit');
    		    }
    		    else{
    		    $('#accountType').val('credit');
    		    }

        }

    $(document).ready(function () {

        toggleAccountType();
        toggleAccountTypeLast();


        $('#accountType').change(function () {

            toggleAccountType();
        });
        $('#accountTypeLast').change(function () {

            toggleAccountTypeLast();
        });
    });

    function toggleAccountType() {

        if ($('#accountType').val() == "credit") {

            $('#openingCredit').val($('#openingBalance').val());
            $('#openingDebit').val('0.0');
        } else if ($('#accountType').val() == "debit") {

            $('#openingDebit').val($('#openingBalance').val());
            $('#openingCredit').val('0.0');
        }
    }

    function toggleAccountTypeLast() {

        if ($('#accountTypeLast').val() == "credit") {

            $('#lastOpeningCredit').val($('#openingBalanceLast').val());
            $('#lastOpeningDebit').val('0.0');
        } else if ($('#accountTypeLast').val() == "debit") {

            $('#lastOpeningDebit').val($('#openingBalanceLast').val());
            $('#lastOpeningCredit').val('0.0');
        }
    }


    function remove(id) {
        if(confirm("Are you sure, you want to delete the Stock Item?")) {
            $('#stockRemove' + id).submit();
        }

    }



    $(document).ready(function () {
        $('#dataTableStock').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[0, "desc"]],
            "pageLength": 7,
            "ajax": "${contextPath}/Stock/json"
        });
    });
    function goBack(){
     window.location = "${contextPath}/home#stock";
    }
     function edit(accountCode) {

                $.ajax({
                    url: '${contextPath}/Stock/ajax/editStock',
                    contentType: 'application/json',

                    data: {
                        accountCode: accountCode

                    },
                    success: function (response) {
                        editForm(response);
                        setAccountType(response);
                    },

                    error: function (e) {

                        console.log("ERROR : ", e);

                    }
                });
            }
			  function editForm(response) {
			  console.log(response);
    		    var openingBalance ;
    		    if(response.accountDto.openingCredit>response.accountDto.openingDebit){
    		    openingBalance = response.accountDto.openingCredit;
    		    }
    		    else{
    		    openingBalance = response.accountDto.openingDebit;
    		    }
                 $('#myModal2').modal();
                 $('#myModal2').find('form').trigger('reset');
                 $('#myModal2').find('form').find('#title').val(response.accountDto.title);
                 $('#myModal2').find('form').find('#openingDate').val(response.accountDto.openingDate);
                 $('#myModal2').find('form').find('#openingCredit').val(response.accountDto.openingCredit);
                 $('#myModal2').find('form').find('#openingDebit').val(response.accountDto.openingDebit);
                 $('#myModal2').find('form').find('#lastOpeningCredit').val(response.accountDto.lastOpeningCredit);
                 $('#myModal2').find('form').find('#openingBalanceLast').val(response.accountDto.openingBalanceLast);
                 $('#myModal2').find('form').find('#openingBalance').val(openingBalance);
                 $('#myModal2').find('form').find('#parentName').val(response.accountDto.parentName);
                 $('#myModal2').find('form').find('#totalBalanceAmount').val(response.accountDto.totalBalanceAmount);
                 $('#myModal2').find('form').find('#accountCode').val(response.accountDto.accountCode);
                 $('#myModal2').find('form').find('#description').val(response.description);
                 $('#myModal2').find('form').find('#category').val(response.categoryDto.categoryId);
                 $('#myModal2').find('form').find('#quantity').val(response.quantity);
                 $('#myModal2').find('form').find('#openingQuantity').val(response.openingQuantity);
                 $('#myModal2').find('form').find('#cost').val(response.cost);
                 $('#myModal2').find('form').find('#sellingPrice').val(response.sellingPrice);
                 $('#myModal2').find('form').find('#measureUnit').val(response.unitMeasure);
                 $('#myModal2').find('form').find('#itemDate').val(response.itemDate);
                 $('#myModal2').find('form').find('#level').val(response.accountDto.level);
                 $('#myModal2').find('form').find('#makerId').val(response.makerId);
                 $('#myModal2').find('form').find('#commissionAmount').val(response.commissionAmount);
                 $('#myModal2').find('form').find('#enginePrefix').val(response.enginePrefix);
                 $('#myModal2').find('form').find('#chassisPrefix').val(response.chassisPrefix);

            }
</script>
</body>
</html>
