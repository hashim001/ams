<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Purchase Indent</title>
    <style>
        body{
        margin: 0;padding: 0;max-width: 950px;width: 100%;margin: 0 auto;
    }
    *{
        margin: 0;padding: 0;box-sizing: border-box;
    }
    table{width: 100%;margin-top: 20px;border-collapse: collapse;border-spacing: 0;}
    img{height: 150px;}
    h2,h3{text-transform: uppercase;}
    thead>tr>th{text-align: left;}
    .nav>td{text-align: center;font-weight: bold;text-transform: capitalize;}
    tbody>tr>td{border: 1px solid;}
    .box{display: inline-block;width: 40px;height: 20px;background: transparent;border: 1px solid;margin-right: 10px;}
    .require>th{text-align: center;padding-top: 10px;}
    .depart>th{padding: 30px 0 15px;}
    .border{border-bottom: 1px solid;display: inline-block;}
    .foot>tbody>tr>td{border: 0;padding: 20px 0 10px;}
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="7">
                    <h2 style="text-align:center">Company Name</h2>
                </th>
            </tr>
            <tr>
                <th colspan="7">
                    <h3 style="text-align:center">Purchase Indent</h3>
                </th>
                <th>Indent No.</th>
                <th>${voucherNumber}</th>
            </tr>

            <tr class="require">
                <th colspan="2" style="text-align: left;">REQUIREMENT</th>
                <th><span class="box"></span>Monthly</th>
                <th><span class="box"></span>Urgent</th>
                <th colspan="3"><span class="box"></span>Ordinary</th>
                <th>Date:</th>
                <th>${voucherDate}</th>
            </tr>
            <tr class="depart">
                <th colspan="2">
                    Department:
                </th>
                <th>
                    <p class="border">as</p>
                </th>
                <th collection-repeat="6"></th>
            </tr>
        </thead>
        <tbody>
            <tr class="nav">
                <td>S#</td>
                <td>Item Code </td>
                <td style="width: 220px">Description</td>
                <td>Required Delievery <br>period</td>
                <td>Stock in Hand</td>
                <td>Quantity <br>Required</td>
                <td>Quantity <br>purcahse</td>
                <td>Delieverd <br>Date</td>
                <td>Remark</td>
            </tr>
             <c:set var="count" value="1"/>
            <c:forEach items="${voucherList}" var="voucher">
            <tr>
             <c:choose>
             <c:when test="${voucher.itemAccount>0}">
                <td>${count}</td>
                <td>${voucher.accountCode}</td>
                <td>${accountMap.get(voucher.accountCode)}</td>
                <td></td>
                <td></td>
                <td>${voucher.itemQuantity}</td>
                <td>${voucher.itemQuantity}</td>
                <td>${voucher.voucherDate}</td>
                <td>${voucher.remarks}</td>
              <c:set var="count" value="${count+1}"/>
               </c:when>
               </c:choose>
            </tr>
            </c:forEach>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="10">
                    <table class="foot">
                        <tr>
                            <td>S.O ____________________</td>
                            <td style="text-align: center">H.O.D. _______________________</td>
                            <td style="text-align: right">G.M Lt Col Raheel Aslam Director ___________________</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tfoot>
    </table>
</body>
</html>