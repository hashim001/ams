    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Update Voucher</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">


    <h2 class="form-signin-heading">Purchase Voucher</h2>
    <div class="container">
        <form class="form-inline formSaleVoucher" style="margin-bottom: 15px; ">
            <div class="pull-left">
                <div class="form-Row">
                    <label>Voucher Number :</label>
                    <input id="voucherNumber" type="text" class="form-control" readonly="true"
                           autofocus="true" value="${voucherNumber}"/></br>
                </div>
            </div>
            <div class="customerInfoSec">
                <label> Supplier Account :</label>
                <select name="account" class="form-control" id="supplierAccount">
                    <c:forEach items="${accounts}" var="account">
                        <c:choose>
                            <c:when test="${account.accountCode == currentVendorAccountCode}">
                                <option value="${account.accountCode}" selected >${account.title}
                                    <c:choose>
                                        <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                        </c:when>
                                    </c:choose>
                                </option>
                            </c:when>
                            <c:otherwise>
                                <option value="${account.accountCode}">${account.title}
                                    <c:choose>
                                        <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                        </c:when>
                                    </c:choose>
                                </option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <option value="${cashInHand}">CASH IN HAND</option>
                </select>
            </div>


            <div class="form-Row">
                <label>Date : </label>
                <input style="display: none" type="date" class="form-control " id="billDate"
                       autofocus="true" value="${currentDate}" required="required"/>
                <input type="date" class="form-control " id="currentDate"
                       autofocus="true" value="${currentDate}" required="required"/>
            </div>
            <div class="customerInfoSec"><p id="name"></p></div>
            <div class="customerInfoSec"><p id="address"></p></div>
            <div class="customerInfoSec"><p id="mobile"></p></div>
            <div class="customerInfoSec"><p id="cnic"></p></div>
            <div class="customerInfoSec"><p id="landline"></p></div>

        </form>

    </div>


    <form:form method="POST" modelAttribute="voucherList" id="formid">
        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
			     <tr>
                    <th width="15%">Model</th>
                    <th width="15%">Chassis No</th>
                    <th width="8%">Color</th>
                    <th width="10%">Engine No</th>
                    <th width="10%">Transmission</th>
                    <th width="8%">Year</th>
                    <th width="12%">Rate</th>
                    <th width="15%">Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${voucherList.voucherList}" var="vouchers" varStatus="status">

                    <tr>

                        <form:input id="voucherNumber" type="hidden" path="voucherList[${status.index}].voucherNumber"
                                    class="form-control"
                                    autofocus="true" value="${voucherNumber}"></form:input>
                        <form:input type="hidden" path="voucherList[${status.index}].billNumber"
                                    class="form-control theBillNumber"
                                    autofocus="true"></form:input>
                        <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].billDate"
                                    class="form-control theBillDate"
                                    autofocus="true" value="${currentDate}" required="required"></form:input>
                        <form:input type="hidden" path="voucherList[${status.index}].accountCode"
                                    class="form-control supplierAccountNumber"
                                    autofocus="true"></form:input>

						 <td>
                            <form:select  path="voucherList[${status.index}].itemAccount" class="form-control stock" onchange="updateValues(this.value,${status.index})" id="stock${status.index}">
                                <form:option value="0">NIL</form:option>
                           <c:forEach items="${stockAccounts}" var="account">
                            <form:option value="${account.accountCode}">${account.accountStock.name} </form:option>
                           </c:forEach>
                            </form:select>
                        </td>
                        <c:forEach items="${stockAccounts}" var="account">
                        <span style="display: none"
                              id="rate${account.accountCode}${status.index}">${account.accountStock.cost}</span>
                        <span style="display: none"
                              id="chassisPrefix${account.accountCode}${status.index}">${account.accountStock.chassisPrefix}</span>
                        <span style="display: none"
                              id="enginePrefix${account.accountCode}${status.index}">${account.accountStock.enginePrefix}</span>
                        </c:forEach>
			            <td>
                            <form:input type="text" id="chassisNo${status.index}" path="voucherList[${status.index}].chassisNo" class="form-control chassisNo" autofocus="true" value=""></form:input>
                        </td>
			            <td>
                            <form:input type="text" id="color${status.index}" path="voucherList[${status.index}].color" class="form-control color"  autofocus="true" value=""></form:input>
                        </td>
			            <td>
                            <form:input type="text" id="engineNo${status.index}" path="voucherList[${status.index}].engineNo" class="form-control engineNo" autofocus="true" value=""></form:input>
                        </td>
						 <td>
                            <form:select  path="voucherList[${status.index}].transmission" class="form-control transmission" id="transmission${status.index}">
                                <form:option value="Automatic">Automatic</form:option>
                                <form:option value="Manual">Manual</form:option>
                            </form:select>
                        </td>
			            <td>
                            <form:select id="year${status.index}" path="voucherList[${status.index}].year" class="form-control year">
                           <c:forEach items="${yearList}" var="year">
                            <form:option value="${year}">${year}</form:option>
                           </c:forEach>
                            </form:select>
                        </td>
                        <td>
                            <form:input type="text" id="innerRate${status.index}" readonly="true"
                                        path="voucherList[${status.index}].itemRate" class="form-control rate"
                                        onblur="calculateAmount(${status.index})"
                                        autofocus="true" value="${vouchers.itemRate}"></form:input>
                        </td>
                        <td>
                            <form:input type="text" id="innerAmount${status.index}"
                                        path="voucherList[${status.index}].debit" class="form-control amountTotal"
                                        onblur="calculateQuantity(${status.index})"
                                        autofocus="true" value="${vouchers.billAmount}"></form:input>
                        </td>


                        <form:input type="date" cssStyle="display: none" path="voucherList[${status.index}].voucherDate"
                                    class="form-control theDate"
                                    autofocus="true" value="${currentDate}" required="required"></form:input>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">

        <div class="totalBillSec">
            <p>Total Rs <span>${total}</span></p>
        </div>
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {
       $('.amountTotal').each(function (index, item) {
       $('#innerAmount'+index).val(parseFloat($('#innerAmount'+index).val()).toFixed(1));
       });

        var netTotal = calculateAmountTotal();
        $(".totalBillSec span").html(netTotal);

        getCustomerDetail();
        $('#supplierAccount').change(function () {
            getCustomerDetail();
        });

        $('.theDate').val($('#currentDate').val());
        $('.theBillDate').val($('#billDate').val());
        $('.theBillNumber').val($('#billNumber').val());
        $('.supplierAccountNumber').val($('#supplierAccount').val());


        $('#currentDate').change(function () {
            $('.theDate').val($('#currentDate').val());

        });

        $('#billDate').change(function () {
            $('.theBillDate').val($('#billDate').val());

        });
        $('#billNumber').change(function () {
            $('.theBillNumber').val($('#billNumber').val());

        });

        $('#supplierAccount').change(function () {
            $('.supplierAccountNumber').val($('#supplierAccount').val());

        });


        $(".amountTotal").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });


        $(".rate").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });

        function calculateAmountTotal() {
            var netTotal = 0;
            $(".amountTotal").each(function (index, ele) {
                netTotal += parseFloat($(ele).val());
            });
            console.log("total", netTotal);
            return netTotal.toFixed(2);
        }


    });

    // get rate of specific item
    function updateValues(accountCode, id) {
        if (accountCode == 0) {
            $('#innerRate' + id).val('0.0');
        } else {
            $('#chassisNo' + id).val($('#chassisPrefix' + accountCode + id).text());
            $('#engineNo' + id).val($('#enginePrefix' + accountCode + id).text());
            $('#innerRate' + id).val($('#rate' + accountCode + id).text());
        }
         $('#innerRate' + id).trigger('blur');
    }

    function calculateAmount(id) {
        var amount = parseFloat($('#innerRate' + id).val() );
         $('#innerAmount' + id).val(amount);
         $('#innerAmount' + id).trigger('blur');
    }

    function getCustomerDetail() {
        $.ajax({
            url: '${contextPath}/Account/ajax/getCustomerInfo',
            contentType: 'application/json',
            data: {
                accountCode: $('#supplierAccount').val(),
                isPurchase: true

            },

            success: function (response) {
                $('#name').val(response[0]);
                $('#name').text(response[0]);
                $('#address').val(response[1]);
                $('#address').text(response[1]);
                $('#mobile').val(response[2]);
                $('#mobile').text(response[2]);
                $('#cnic').val(response[3]);
                $('#cnic').text(response[3]);
                $('#landline').val(response[4]);
                $('#landline').text(response[4]);
            }
        });

    }
    function goBack(){
     window.location = "${contextPath}/Purchase/viewPurchaseVoucher";
    }
</script>
</body>
</html>
