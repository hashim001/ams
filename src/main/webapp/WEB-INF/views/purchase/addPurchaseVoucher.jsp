<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Plot</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">
    ${voucherAddSuccess}


    <h2 class="form-signin-heading">Add Plot</h2>
    <div class="container">


    </div>


    <form:form method="POST" modelAttribute="voucherList" id="formid">
        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>
                    <th width="10%">Model</th>
                    <th width="18%">Description</th>
                    <th width="10%">Plot No</th>
                    <th width="15%">Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${voucherList.voucherList}" varStatus="status">

                    <tr>
                        <td>
                            <form:select path="voucherList[${status.index}].itemAccount" class="form-control stock"
                                         onchange="updateValues(this.value,${status.index})" id="stock${status.index}">
                                <form:option value="0">NIL</form:option>
                                <c:forEach items="${stockAccounts}" var="account">
                                    <form:option
                                            value="${account.accountCode}">${account.accountStock.name} </form:option>
                                </c:forEach>
                            </form:select>
                        </td>
                        <c:forEach items="${stockAccounts}" var="account">
                        <span style="display: none"
                              id="rate${account.accountCode}${status.index}">${account.accountStock.sellingPrice}</span>
                            <span style="display: none"
                                  id="chassisPrefix${account.accountCode}${status.index}">${account.accountStock.chassisPrefix}</span>
                            <span style="display: none"
                                  id="enginePrefix${account.accountCode}${status.index}">${account.accountStock.enginePrefix}</span>
                        </c:forEach>
                        <td>
                            <form:input type="text" id="color${status.index}" path="voucherList[${status.index}].remarks"
                                        class="form-control color" autofocus="true" value=""></form:input>
                        </td>
                        <td>
                            <form:input type="text" id="engineNo${status.index}"
                                        path="voucherList[${status.index}].apartmentNo" class="form-control engineNo"
                                        autofocus="true" value=""></form:input>
                        </td>
                        <td>
                            <form:input type="number" id="innerAmount${status.index}"
                                        path="voucherList[${status.index}].debit" class="form-control amountTotal" step="any"
                                        autofocus="true" value="0.00"></form:input>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">

        <div class="totalBillSec">
            <p>Total Worth Rs <span>${total}</span></p>
        </div>
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>
    var selectedAccountList = [];
    $(document).ready(function () {


        $(".amountTotal").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });

        $(".rate").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $(".totalBillSec span").html(netTotal);
            console.log("keydown", netTotal);
        });

        function calculateAmountTotal() {
            var netTotal = 0;
            $(".amountTotal").each(function (index, ele) {
                netTotal += parseFloat($(ele).val());
            });
            console.log("total", netTotal);
            return netTotal.toFixed(2);
        }


    });

    // get rate of specific item
    function updateValues(accountCode, id) {

        /*****MakeUniqueDropdown******/
        makeUniqueDropdown(accountCode, id);


        if (accountCode == 0) {
            $('#innerRate' + id).val('0.0');
        } else {
            $('#chassisNo' + id).val($('#chassisPrefix' + accountCode + id).text());
            $('#engineNo' + id).val($('#enginePrefix' + accountCode + id).text());
            $('#innerAmount' + id).val($('#rate' + accountCode + id).text());
        }
        $('#innerAmount' + id).trigger('blur');
    }

    function makeUniqueDropdown(accountCode, id) {
        $(".rawMaterial").each(function (index, iElement) {
            var iteratedVal = $(iElement).val();
            var matchedAccountIndex = selectedAccountList.findIndex(function (iteratedAccountList) {
                return iteratedAccountList.rowid == id
            });

            //Add values in the selectedAccountList array
            if (iteratedVal != "0") {
                if (matchedAccountIndex != -1) {
                    selectedAccountList.splice(matchedAccountIndex, 1);
                    selectedAccountList.push({"rowid": id, "val": accountCode});
                }
                else {
                    selectedAccountList.push({"rowid": id, "val": accountCode});
                }
            } else {
                selectedAccountList.splice(matchedAccountIndex, 1);
                selectedAccountList.push({"rowid": id, "val": accountCode});
            }

            //UpdateDropdown Option Value

            var val = $(iElement).val(), rowid = index;

            //OptionElements Loop
            $(iElement).find("option").each(function (optionIndex, optionElement) {

                //selectedAccountList Loop
                $.each(selectedAccountList, function (accountIndex, accountRow) {
                    var optionVal = $(optionElement).attr("value"),
                        filteredMatchedOption = selectedAccountList.filter(function (iteratedAccountList) {
                            return iteratedAccountList.val == optionVal;
                        });
                    if (filteredMatchedOption.length > 0) {
                        if (rowid != filteredMatchedOption[0].rowid && filteredMatchedOption[0].val != "0") {
                            $(optionElement).addClass("hide");
                        }
                    }
                    else {
                        $(optionElement).removeClass("hide");
                    }
                });

            });

        });
    }

    function calculateAmount(id) {
        var amount = parseFloat($('#innerRate' + id).val());
        $('#innerAmount' + id).val(amount);
        $('#innerAmount' + id).trigger('blur');
    }



    function goBack() {
        window.location = "${contextPath}/home#stock";
    }
</script>
</body>
</html>
