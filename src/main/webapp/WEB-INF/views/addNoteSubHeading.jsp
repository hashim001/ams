<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Sub Heading</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "navigation.jsp" %>

<div class="container">
    ${accountNoteSubHeadSuccess}
    ${subHeadRemoveSuccess}

    <div class="row">
        <div class="col-md-4">
            <form:form method="POST" modelAttribute="noteSubForm" class="form-signin">
                <h2 class="form-signin-heading">Add Note Subheading</h2>
                <spring:bind path="heading">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="heading">Heading</form:label>
                        <form:input type="text" path="heading" class="form-control" placeholder="Note Heading"
                                    autofocus="true"></form:input>
                        <form:errors path="heading"></form:errors>
                    </div>
                </spring:bind>

                <spring:bind path="level">
                    <div class="form-group">
                        <form:label path="level">Level</form:label>
                        <form:select path="level" class="form-control" >
                            <form:option value="1">1</form:option>
                            <form:option value="2">2</form:option>
                            <form:option value="3">3</form:option>
                            <form:option value="4">4</form:option>
                        </form:select>
                    </div>
                </spring:bind>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Add</button>
            </form:form>
        </div>
        <div class="col-md-2"></div>


    </div>
</div>
<div class="container">
    <h3>Note Subheadings</h3>

    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Heading</th>
            <th>Level</th>
            <th>Accounts</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${noteSubHeadings}" var="subHead" varStatus="status">

            <tr>
                <td>${subHead.heading}</td>
                <td>${subHead.level}</td>
                <td><c:forEach items="${subHead.accounts}" var="account">
                    ${account.title}</br>
                </c:forEach></td>
                <td ><a class="btn btn-xs btn-primary pull-left" href="${contextPath}/Account/addSubHeadAccount?subId=${subHead.subId}">Manage</a>
                    <form method="post" action="${contextPath}/Account/removeNoteSubHead?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-left">
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <input  type="hidden" name="subId" value="${subHead.subId}"/>
                        <input  type="hidden" name="noteId" value="${noteId}"/>
                        <input class="btn btn-xs btn-danger" type="submit" value="Remove"/>
                    </form></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>


<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function() {

    });



</script>
</body>
</html>
