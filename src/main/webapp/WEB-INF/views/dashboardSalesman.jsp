<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <title>Dashboard</title>
    <style>
        .container {
            position: relative;
            text-align: center;
            color: white;
        }

        .bottom-left {
            position: absolute;
            bottom: 8px;
            left: 16px;
        }

        .top-left {
            position: absolute;
            top: 8px;
            left: 16px;
        }

        .top-right {
            position: absolute;
            top: 8px;
            right: 16px;
        }

        .bottom-right {
            position: absolute;
            bottom: 8px;
            right: 16px;
        }

        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container">



        <div class="container-dash">
            <div class="logoDash-container" style="    margin: 3em 0 0 0;padding-bottom: 3em;position: relative">

               <img style="    position: absolute;width: 100%;top: 0;left: 0;"
                     src="${contextPath}/resources/img/logo-dash.jpg" class="center-block img-responsive" alt="">
                <div style="position: relative;padding: 0 205px 0 270px;font-size: 22px;line-height: 68px; font-weight: bolder;font-size: xx-large">${riderName}</div>

            </div>
            <div class="padding-close-container">


                <div class="box-square ">
                    <div class="box-square-content pGreen">
                        <a href="${contextPath}/Apartment/viewApartmentList">
                            <img src="${contextPath}/resources/img/d11.png" alt="">
                            <span>APARTMENT LIST</span>
                        </a>
                    </div>
                </div>

                <div class="box-square">
                    <div class="box-square-content Dgrey">
                        <a href="${contextPath}/Report/holdApartmentReport">
                            <img src="${contextPath}/resources/img/d11.png" alt="">
                            <span>HOLD APARTMENT REPORT</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content orange">
                        <a href="${contextPath}/SaleLead/addSaleLead">
                            <img src="${contextPath}/resources/img/d7.png" alt="">
                            <span>SALE LEADS</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content pGreen">
                        <a href="${contextPath}/Report/currentApartmentsReport">
                            <img src="${contextPath}/resources/img/d11.png" alt="">
                            <span>APARTMENT REPORT</span>
                        </a>
                    </div>
                </div>
                <div class="box-square">
                    <div class="box-square-content mergenda">
                        <a href="${contextPath}/logout" >
                            <img src="${contextPath}/resources/img/d5.png" alt="">
                            <span>LOGOUT</span>
                        </a>
                    </div>
                </div>
            </div>

            <form action="${contextPath}/logout" method="post" id = "logoutForm">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </div>
    </div>
</div>


<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>
    function logout() {
        $("#logoutForm").submit();
    }

</script>
</body>
</html>