<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Departments</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${deptAddSuccess}
            ${deptRemoveSuccess}
        <h2 class="heading-main">Departments <span class="addIcon" data-toggle="modal"
                                                        data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Department Title</th>
                            <th>Location</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${departments}" var="dept" varStatus="status">

                            <tr>
                                <td>${dept.name}</td>
                                <td>${dept.location}</td>
                                <td>
                                    <form method="post" id="deptRemove${status.index}"
                                          action="${contextPath}/Department/removeDept">
                                        <input type="hidden" name="deptId"
                                               value="${dept.depId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a href="#" onclick="edit(${dept.depId});"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                        <li><a href="#" onclick="remove(${status.index});"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h3 class="modal-title" id="myModalLabel">Add Department</h3>
            </div>

            <form:form method="POST" modelAttribute="deptForm">
                <div class="modal-body">
                    <spring:bind path="name">

                        <form:label path="name">Title</form:label>
                        <form:input type="text" path="name" placeholder="Dept Title" id="name"
                                    autofocus="true" required="true" ></form:input>
                        <form:errors path="name"></form:errors>

                    </spring:bind>
                    <spring:bind path="depCode">

                        <form:label path="depCode">Dept Code</form:label>
                        <form:input type="text" path="depCode" placeholder="Dept Code" id="depCode"
                                    autofocus="true" required="true" ></form:input>
                        <form:errors path="depCode"></form:errors>

                    </spring:bind>
                            <spring:bind path="location">

                                <form:label path="location">Location</form:label>
                                <form:select path="location" id="location" required="true">

                                    <c:forEach items="${locations}" var="location">
                                        <form:option value="${location.locationName}">${location.locationName}</form:option>
                                    </c:forEach>
                                </form:select>
                     </spring:bind>


                    <spring:bind path="deptGm">

                        <form:label path="deptGm">Dept GM</form:label>
                        <form:select path="deptGm" id="deptGM">
                            <form:option value="0">-- NIL --</form:option>
                            <c:forEach items="${gmList}" var="gm">
                                <form:option value="${gm.auth_id}">${gm.userName}</form:option>
                            </c:forEach>
                        </form:select>
                    </spring:bind>

                    <spring:bind path="deptAccount">
                        <form:label path="deptAccount">Dept Account</form:label>
                        <form:select path="deptAccount" id="deptAccount">
                            <form:option value="0">-- NIL --</form:option>
                            <c:forEach items="${accounts}" var="account">
                                <form:option value="${account.accountCode}">${account.title} - ${account.parentName}</form:option>
                            </c:forEach>
                        </form:select>
                    </spring:bind>


                    <spring:bind path="depId">
                        <form:input type="hidden" path="depId" placeholder="Dept Code" id="depId"
                                    autofocus="true"></form:input>

                    </spring:bind>
                </div>

                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });

    function goBack(){
        window.location = "${contextPath}/home#masterFile";
    }

    function remove(id){
        if(confirm("Are you sure you want to remove the Department")){
            $('#deptRemove'+ id).submit();
        }
    }

    function edit(deptId) {
     console.log("hello");
        $.ajax({
            url: '${contextPath}/Department/editDept',
            contentType: 'application/json',
            data: {
                deptId: deptId

            },
            success: function (response) {
                editForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
     function editForm(response) {
            $("#myModal").modal();
            $('#myModal').find('form').trigger('reset');
            $('#myModal').find('form').find('#name').val(response.name);
            $('#myModal').find('form').find('#depCode').val(response.depCode);
            $('#myModal').find('form').find('#depId').val(response.depId);
            $('#myModal').find('form').find('#deptGM').val(response.deptGm);
            $('#myModal').find('form').find('#deptAccount').val(response.deptAccount);
            $('#myModal').find('form').find('#location').val(response.location);
        }

</script>
</body>
</html>
