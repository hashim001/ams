<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Models</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${locationAddSuccess}
        ${locationRemoveSuccess}
        <h2 class="heading-main">Models <span class="addIcon" data-toggle="modal"
                                                        data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Model ID</th>
                            <th>Model Name</th>
                            <th>Make Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${models}" var="model" varStatus="status">

                            <tr>
                                <td>${model.modelId}</td>
                                <td>${model.modelName}</td>
                                <td>${model.maker.makerName}</td>
                                <td>
                                    <form method="post" id="modelRemove${status.index}"
                                          action="${contextPath}/model/removeModel">
                                        <input type="hidden" name="modelId"
                                               value="${model.modelId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a href="#" onclick="edit(${model.modelId});"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h3 class="modal-title" id="myModalLabel">Add Model</h3>
            </div>

            <form:form method="POST" modelAttribute="modelForm">
                <div class="modal-body">
                    <spring:bind path="modelName">

                        <form:label path="modelName">Model</form:label>
                        <form:input type="text" path="modelName" placeholder="Model Name" id="modelName"
                                    autofocus="true" required="true" ></form:input>
                        <form:errors path="modelName"></form:errors>

                    </spring:bind>
                    <spring:bind path="showroomCommission">

                        <form:label path="showroomCommission">Showroom Commission</form:label>
                        <form:input type="number" path="showroomCommission" placeholder="Showroom Commission" id="showroomCommission"
                                    autofocus="true" required="true" ></form:input>
                        <form:errors path="showroomCommission"></form:errors>

                    </spring:bind>
                    <spring:bind path="salesmanCommission">

                        <form:label path="salesmanCommission">Salesman Commission</form:label>
                        <form:input type="number" path="salesmanCommission" placeholder="Salesman Commission" id="salesmanCommission"
                                    autofocus="true" required="true" ></form:input>
                        <form:errors path="salesmanCommission"></form:errors>

                    </spring:bind>
                  <spring:bind path="makerId">

                                <form:label path="makerId"></form:label>
                                <form:select path="makerId" id="makerId">
                                    <c:forEach items="${makers}" var="maker">
                                        <form:option value="${maker.makerId}">${maker.makerName}</form:option>
                                    </c:forEach>
                                </form:select>
                     </spring:bind>
                    <spring:bind path="modelId">
                        <form:input type="hidden" path="modelId" id="modelId"
                                    autofocus="true"></form:input>

                    </spring:bind>
                </div>

                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });
    function goBack(){
        window.location = "${contextPath}/home#masterFile";
    }

    function remove(id){
        if(confirm("Are you sure you want to remove the Model")){
            $('#modelRemove'+id).submit();
        }
    }

    function edit(modelId) {
     console.log("hello");
        $.ajax({
            url: '${contextPath}/Maker/editModel',
            contentType: 'application/json',
            data: {
                modelId: modelId

            },
            success: function (response) {
                editForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
     function editForm(response) {
            $("#myModal").modal();
            $('#myModal').find('form').trigger('reset');
            $('#myModal').find('form').find('#modelName').val(response.modelName);
            $('#myModal').find('form').find('#modelId').val(response.modelId);
            $('#myModal').find('form').find('#makerId').val(response.makerId);
            $('#myModal').find('form').find('#salesmanCommission').val(response.salesmanCommission);
            $('#myModal').find('form').find('#showroomCommission').val(response.showroomCommission);
        }
</script>
</body>
</html>
