<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Makers</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${locationAddSuccess}
        ${locationRemoveSuccess}
        <h2 class="heading-main">Makers <span class="addIcon" data-toggle="modal"
                                                        data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Make ID</th>
                            <th>Make Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${makers}" var="maker" varStatus="status">

                            <tr>
                                <td>${maker.makerId}</td>
                                <td>${maker.makerName}</td>
                                <td>
                                    <form method="post" id="makerRemove${status.index}"
                                          action="${contextPath}/Maker/removeMaker">
                                        <input type="hidden" name="makerId"
                                               value="${maker.makerId}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a href="#" onclick="edit(${maker.makerId});"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h3 class="modal-title" id="myModalLabel">Add Make</h3>
            </div>

            <form:form method="POST" modelAttribute="makerForm">
                <div class="modal-body">
                    <spring:bind path="makerName">

                        <form:label path="makerName">Make</form:label>
                        <form:input type="text" path="makerName" placeholder="Make Name" id="makerName"
                                    autofocus="true" required="true" ></form:input>
                        <form:errors path="makerName"></form:errors>

                    </spring:bind>

                    <spring:bind path="makerId">
                        <form:input type="hidden" path="makerId" id="makerId"
                                    autofocus="true"></form:input>

                    </spring:bind>
                </div>

                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });
    function goBack(){
        window.location = "${contextPath}/home#masterFile";
    }

    function remove(id){
        if(confirm("Are you sure you want to remove the Location")){
            $('#makerRemove'+id).submit();
        }
    }

    function edit(makerId) {
     console.log("hello");
        $.ajax({
            url: '${contextPath}/Maker/editMaker',
            contentType: 'application/json',
            data: {
                makerId: makerId

            },
            success: function (response) {
                editForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
     function editForm(response) {
            $("#myModal").modal();
            $('#myModal').find('form').trigger('reset');
            $('#myModal').find('form').find('#makerName').val(response.makerName);
            $('#myModal').find('form').find('#makerId').val(response.makerId);
        }
</script>
</body>
</html>
