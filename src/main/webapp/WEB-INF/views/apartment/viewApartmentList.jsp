<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Plots</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${ApartmentAddSuccess}
        ${apartmentRemoveSuccess}
            <h2 class="heading-main">Plots<a href="${contextPath}/Purchase/createPurchaseVoucher"><span class="addIcon"><i><img
                    src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></a></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Plot Number</th>
                            <th>Customer</th>
                            <th>Type</th>
                            <td>Amount</td>
                            <td>Status</td>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${apartmentList}" var="apartment" varStatus="status">

                            <tr>
                                <td>${apartment.apartmentNo}</td>
                                <c:choose>
                                <c:when test="${apartment.customerAccount>0}">
                                <td>${accountMap.get(apartment.customerAccount)}</td>
                                </c:when>
                                <c:otherwise>
                                <td>In Stock</td>
                                </c:otherwise>
                                </c:choose>
                                <td>${apartment.stock.name}</td>
                                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${apartment.sellingPrice}"/></td>
                                <c:choose>
                                    <c:when test="${apartment.hold == 'true'}">
                                        <td>ON HOLD</td>
                                    </c:when>
                                    <c:when test="${apartment.sold == 'true'}">
                                        <td>SOLD</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>AVAILABLE</td>
                                    </c:otherwise>
                                </c:choose>
                                <td>
                                    <form method="post" id="apartmentRemove${status.index}"
                                          action="${contextPath}/Apartment/removeApartment">
                                        <input type="hidden" name="apartmentId"
                                               value="${apartment.apartmentId}"/>
                                    </form>
                                    <ul class="list-inline">
                                     <sec:authorize access="hasRole('ROLE_MASTER_ADMIN')">
                                        <li><a title="edit" href="#" onclick="edit(${apartment.apartmentId});"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                     </sec:authorize>
                                     <sec:authorize access="hasRole('ROLE_SALESMAN')">
                                        <li><a title="hold" href="#" onclick="holdApartment(${apartment.apartmentId});"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                     </sec:authorize>
                                        <c:if test="${apartment.customerAccount == 0}">
                                            <li><a href="#" onclick="remove(${status.index})"><img
                                                    src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                        </c:if>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- /container -->
<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" onclick="refreshPage()" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h3 class="modal-title" id="myModalLabel">Edit Plot</h3>
            </div>

            <form:form method="POST" modelAttribute="apartmentForm" action="${contextPath}/Apartment/editApartment">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                    <spring:bind path="apartmentNo">

                        <form:label path="apartmentNo">Plot Number</form:label>
                        <form:input type="text" path="apartmentNo" placeholder="Plot No" id="apartmentNo"
                                    autofocus="true" required="true" ></form:input>
                        <form:errors path="apartmentNo"></form:errors>
                    </spring:bind>
                    <spring:bind path="stockId">
                                <form:label path="stockId">Model</form:label>
                                <form:select id="stockId" path="stockId">
                                    <c:forEach items="${stockList}" var="stock">
                                        <form:option value="${stock.stockId}">${stock.name}</form:option>
                                    </c:forEach>
                                </form:select>
                   </spring:bind>
                    <spring:bind path="hold">
                        <form:label path="hold">On Hold</form:label>
                        <form:checkbox cssClass="form-control" path="hold" id="hold"></form:checkbox>
                        <form:errors path="hold"></form:errors>

                    </spring:bind>
                    <div id="customerDiv" style="display: none">
                    <spring:bind path="customerName">

                        <form:label path="customerName">Customer Name</form:label>
                        <form:input type="text" path="customerName" placeholder="Customer Name" id="customerName" ></form:input>
                        <form:errors path="customerName"></form:errors>
                    </spring:bind>
                    <spring:bind path="mobile">

                        <form:label path="mobile">Mobile</form:label>
                        <form:input type="text" path="mobile" maxlength="12" onKeyup="insertHifensForMobile();" placeholder="Mobile" id="mobile" ></form:input>
                        <form:errors path="mobile"></form:errors>
                    </spring:bind>
                    <spring:bind path="holdDate">

                        <form:label path="holdDate" style="display: none">Hold Date</form:label>
                            <form:input type="date"  path="holdDate" style="display: none" placeholder="Hold Date" id="holdDate" value="${currentDate}"></form:input>
                        <form:errors path="holdDate"></form:errors>
                    </spring:bind>
                            <spring:bind path="salesmanId">

                                <form:label path="salesmanId">Agent Name</form:label>
                                <form:select path="salesmanId">
                                    <c:forEach items="${salesmanList}" var="salesman">
                                        <form:option value="${salesman.distributorId}">${salesman.name}</form:option>
                                    </c:forEach>
                                </form:select>
                            </spring:bind>
                    </div>
                    </div>
                        <div class="col-sm-6">
                    <spring:bind path="sellingPrice">

                        <form:label path="sellingPrice">Amount</form:label>
                        <form:input type="number" path="sellingPrice" placeholder="Amount" id="sellingPrice"
                                    autofocus="true" required="true" ></form:input>
                        <form:errors path="sellingPrice"></form:errors>

                    </spring:bind>
                    <spring:bind path="description">

                        <form:label path="description">Description</form:label>
                        <form:input type="text" path="description" placeholder="Description" id="description"
                                    autofocus="true" ></form:input>
                        <form:errors path="description"></form:errors>

                    </spring:bind>
                            <spring:bind path="sold">
                                <form:label path="sold">Sold</form:label>
                                <form:checkbox cssClass="form-control" path="sold" id="sold"></form:checkbox>
                                <form:errors path="sold"></form:errors>

                            </spring:bind>
                    <div id="customerDiv2" style="display: none">
                    <spring:bind path="onHoldDays">

                        <form:label path="onHoldDays">OnHold Days</form:label>
                        <form:input type="number" path="onHoldDays" placeholder="OnHold Days" id="onHoldDays" ></form:input>
                        <form:errors path="onHoldDays"></form:errors>

                    </spring:bind>
                    <spring:bind path="cnic">

                        <form:label path="cnic">CNIC</form:label>
                        <form:input type="text" path="cnic" onKeyup="insertHifensForCnic();" maxlength="15" placeholder="CNIC" id="cnic"></form:input>
                        <form:errors path="cnic"></form:errors>
                    </spring:bind>
                   </div>
                   </div>
                    <spring:bind path="apartmentId">
                        <form:input type="hidden" path="apartmentId" id="apartmentId"
                                    autofocus="true"></form:input>

                    </spring:bind>
                    <spring:bind path="apartmentHoldId">
                        <form:input type="hidden" path="apartmentHoldId" id="apartmentHoldId"
                                    autofocus="true"></form:input>

                    </spring:bind>
                </div>
                <div align="center" id="customerDiv3">
                        <spring:bind path="remarks">
                            <div align="center">
                                <form:label path="remarks">Remarks</form:label></div>
                            <form:textarea cssStyle="width: 700px" type="text" path="remarks"
                                           placeholder="Enter Remarks if any."
                                           autofocus="true"></form:textarea>
                            <form:errors path="remarks"></form:errors>

                        </spring:bind>
                </div>

                <div class="modal-footer" style=" background-color: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade modal-small modal-bg-o" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #1f2d5c" style=" color: white">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h3 class="modal-title" id="myModalLabel2">Hold Plot</h3>
            </div>
            <form:form method="POST" modelAttribute="apartmentHoldForm" action="${contextPath}/Apartment/holdApartment">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                    <spring:bind path="customerName">

                        <form:label path="customerName">Customer Name</form:label>
                        <form:input type="text" path="customerName" placeholder="Customer Name" required="true" id="customerName" ></form:input>
                        <form:errors path="customerName"></form:errors>
                    </spring:bind>
                    <spring:bind path="mobile">

                        <form:label path="mobile">Mobile</form:label>
                        <form:input type="text" path="mobile" maxlength="12" onKeyup="insertHifensForMobile();" required="true" placeholder="Mobile" id="mobile" ></form:input>
                        <form:errors path="mobile"></form:errors>
                    </spring:bind>
                    <spring:bind path="holdDate">

                        <form:label path="holdDate" style="display: none">Hold Date</form:label>
                            <form:input type="date"  path="holdDate" style="display: none" placeholder="Hold Date" id="holdDate" value="${currentDate}"></form:input>
                        <form:errors path="holdDate"></form:errors>
                    </spring:bind>
                   </div>
                   <div class="col-sm-6">
                    <spring:bind path="salesmanId">

                                <form:label path="salesmanId">Agent Name</form:label>
                                <form:select path="salesmanId">
                                    <c:forEach items="${salesmanList}" var="salesman">
                                        <form:option value="${salesman.distributorId}">${salesman.name}</form:option>
                                    </c:forEach>
                                </form:select>
                            </spring:bind>
                    <spring:bind path="onHoldDays">

                        <form:label path="onHoldDays">OnHold Days</form:label>
                        <form:input type="number" path="onHoldDays" placeholder="OnHold Days" required="true" id="onHoldDays" ></form:input>
                        <form:errors path="onHoldDays"></form:errors>

                    </spring:bind>
                    <spring:bind path="cnic">

                        <form:label path="cnic">CNIC</form:label>
                        <form:input type="text" path="cnic" onKeyup="insertHifensForCnic();" maxlength="15" required="true" placeholder="CNIC" id="cnic"></form:input>
                        <form:errors path="cnic"></form:errors>
                    </spring:bind>
                   </div>
                    <spring:bind path="apartmentHoldId">
                        <form:input type="hidden" path="apartmentHoldId" id="apartmentHoldId"
                                    autofocus="true"></form:input>

                    </spring:bind>
                    <spring:bind path="apartmentId">
                        <form:input type="hidden" path="apartmentId" id="apartmentId"
                                    autofocus="true"></form:input>

                    </spring:bind>
                </div>
                <div align="center">
                        <spring:bind path="remarks">
                            <div align="center">
                                <form:label path="remarks">Remarks</form:label></div>
                            <form:textarea cssStyle="width: 700px" type="text" path="remarks"
                                           placeholder="Enter Remarks if any."
                                           autofocus="true"></form:textarea>
                            <form:errors path="remarks"></form:errors>

                        </spring:bind>
                </div>
                <div class="modalfooter" style=" backgroundcolor: #1f2d5c">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
  </div>
</div>
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>
$('#myModal').on('hidden.bs.modal', function () {
    refreshPage();
})
      function refreshPage() {
          window.location.reload();
      }
    function goBack(){
        window.location = "${contextPath}/home#stock";
    }

    function remove(id){
        if(confirm("Are you sure you want to remove the Plot?")){
            $('#apartmentRemove'+id).submit();
        }
    }

   function holdApartment(apartmentId) {
        $.ajax({
            url: '${contextPath}/Apartment/holdApartment',
            contentType: 'application/json',
            data: {
                apartmentId: apartmentId

            },
            success: function (response) {
                holdForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
     function holdForm(response) {
            console.log(response);
            $("#myModal2").modal();
            $('#myModal2').find('form').trigger('reset');
            $('#myModal2').find('form').find('#apartmentHoldId').val(response.apartmentHoldId);
            $('#myModal2').find('form').find('#apartmentId').val(response.apartmentId);

        }
   function edit(apartmentId) {
        $.ajax({
            url: '${contextPath}/Apartment/editApartment',
            contentType: 'application/json',
            data: {
                apartmentId: apartmentId

            },
            success: function (response) {
                editForm(response);
            },
            error: function (e) {

                console.log("ERROR : ", e);

            }
        });
    }
     function editForm(response) {
            $("#myModal").modal();
            $('#myModal').find('form').trigger('reset');
            if(response.sold){
                $("#myModal").find('form').find('#sold').prop('checked', true);
            }
            if(response.hold){
             $("#myModal").find('form').find('#hold').prop('checked', true);
             $('#myModal').find('form').find('#remarks').val(response.remarks);
             $('#myModal').find('form').find('#onHoldDays').val(response.onHoldDays);
             $('#myModal').find('form').find('#customerName').val(response.customerName);
             $('#myModal').find('form').find('#mobile').val(response.mobile);
             $('#myModal').find('form').find('#cnic').val(response.cnic);
             $('#myModal').find('form').find('#holdDate').val(response.holdDate);
             $('#myModal').find('form').find('#salesmanId').val(response.salesmanId);
             $("#customerDiv").css("display", "block");
             $("#customerDiv2").css("display", "block");
             $("#customerDiv3").css("display", "block");
             $('#mobile').attr('required', 'true');
             $('#cnic').attr('required', 'true');
             $('#onHoldDays').attr('required', 'true');
             $('#customerName').attr('required', 'true');
             $('#salesmanId').attr('required', 'true');
            }
            else{
             $('#mobile').attr('required', false);
             $('#cnic').attr('required', false);
             $('#onHoldDays').attr('required', false);
             $('#customerName').attr('required', false);
             $('#salesmanId').attr('required', false);
             $("#customerDiv").css("display", "none");
             $("#customerDiv2").css("display", "none");
             $("#customerDiv3").css("display", "none");
            }
            $('#myModal').find('form').find('#apartmentHoldId').val(response.apartmentHoldId);
            $('#myModal').find('form').find('#apartmentNo').val(response.apartmentNo);
            $('#myModal').find('form').find('#description').val(response.description);
            $('#myModal').find('form').find('#rooms').val(response.rooms);
            $('#myModal').find('form').find('#floor').val(response.floor);
            $('#myModal').find('form').find('#sellingPrice').val(response.sellingPrice);
            $('#myModal').find('form').find('#stockId').val(response.stockId);
            $('#myModal').find('form').find('#apartmentId').val(response.apartmentId);
            $('#myModal').find('form').find('#isHold').val(response.hold);
        }
    $('#hold').change(function() {
        if($(this).is(":checked")) {
             $("#customerDiv").css("display", "block");
             $("#customerDiv2").css("display", "block");
             $("#customerDiv3").css("display", "block");
             $('#mobile').attr('required', 'true');
             $('#cnic').attr('required', 'true');
             $('#onHoldDays').attr('required', 'true');
             $('#customerName').attr('required', 'true');
             $('#salesmanId').attr('required', 'true');
        }
        else{
             $('#mobile').attr('required', false);
             $('#cnic').attr('required', false);
             $('#onHoldDays').attr('required', false);
             $('#customerName').attr('required', false);
             $('#salesmanId').attr('required', false);
             $("#customerDiv").css("display", "none");
             $("#customerDiv2").css("display", "none");
             $("#customerDiv3").css("display", "none");
        }

    });
   function insertHifensForMobile(){
     if($('#mobile').val().toString().length==4){
     var res = $('#mobile').val().concat('-');
     $('#mobile').val(res);
     }
   }
   function insertHifensForCnic(){
      console.log($('#cnic').val().toString().length);
     if($('#cnic').val().toString().length==5||$('#cnic').val().toString().length==13){
     var res = $('#cnic').val().concat('-');
     $('#cnic').val(res);
     }
   }
</script>
</body>
</html>
