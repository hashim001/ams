<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create GIN Request</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
option:checked {
    display: none;
}
</style>
</head>


<body>
<%@ include file="../header.jsp" %>
<div class="container">
${duplicateOrderNumber}
    <h2 class="form-signin-heading">GIN Request</h2>
    <form:form method="POST" modelAttribute="ginForm" id="formid" Class="form-inline formSaleVoucher formWidthDef">
        <div class="container">
            <div class="pull-left">
                <div class="form-Row">
                    <spring:bind path="ginNumber">
                        <form:label path="ginNumber">GIN#</form:label>
                        <form:input type="text" readonly="true" cssStyle="width: 100px" path="ginNumber"
                                    cssClass="form-control" value="${ginNumber}"
                                    autofocus="true"></form:input>
                    </spring:bind>
                </div>
              </div>
                    <spring:bind path="date">
                    <form:label path="date">Date </form:label>
                    <form:input cssStyle="width: 155px" type="date" path="date" value="${currentDate}" cssClass="form-control"
                                autofocus="true"></form:input>
                    </spring:bind>
                    <label>Department</label>
                    <select style="width: 200px"  id="deptId"
                                 cssClass="form-control">
                        <option value="0">NIL</option>
                         <option selected='selected' value="${currentDepartment.depId}">${currentDepartment.name}</option>
                        <c:forEach items="${departments}" var="employeeDepartment">
                            <option value="${employeeDepartment.depId}">${employeeDepartment.name}</option>
                        </c:forEach>
                    </select>
                    <label>Sub-Department</label>
                    <select style="width: 220px"  id="subDeptId"
                                 cssClass="form-control">
                      <option selected='selected' value="${currentSubDepartment.subDepId}">${currentSubDepartment.subDepName}</option>

                        <c:forEach items="${subDepartments}" var="subDepartment">

                            <option value="${subDepartment.subDepId}">${subDepartment.subDepName}</option>
                        </c:forEach>
                    </select>
                            <spring:bind path="subDeptId">
                                <form:input cssClass="form-control" id="departId" type="hidden"
                                            path="subDeptId"
                                            autofocus="true"></form:input>
                            </spring:bind>

        </div>

        <div class="tableScroll">
            <table class="table table-condensed saleVoucherTable">
                <thead>
                <tr>
                    <th>Item Account</th>
                     <th>Measure Unit</th>
                    <th>Issue Quantity</th>
                    <th>Remarks</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${ginForm.ginProductList}" var="product" varStatus="productStatus">
                    <tr>
                        <td>
                            <form:select path="ginProductList[${productStatus.index}].transitionCode"
                                         class="form-control" id ="stockAccountCode${productStatus.index}" onchange="getMeasureUnit(${productStatus.index});">
                                <form:option value="0">NIL</form:option>
                                <c:forEach items="${rawMaterials}" var="rm">
                                    <form:option value="${rm.accountCode}-${rm.rawMaterialId}">${rm.name}</form:option>
                                </c:forEach>
                            </form:select>
                        </td>
                        <td>
                            <form:input type="text" id="measureUnit${productStatus.index}"
                                        path="ginProductList[${productStatus.index}].measureUnit"
                                        class="form-control quantity" readonly="true"
                                        autofocus="true"></form:input>
                        </td>

                        <td>
                            <form:input type="number" id="innerInitialQuantity${productStatus.index}"
                                        path="ginProductList[${productStatus.index}].receivedQuantity"
                                        class="form-control quantity"
                                        autofocus="true" ></form:input>
                        </td>
                        <td>
                            <form:input type="text"
                                        path="ginProductList[${productStatus.index}].remarks"
                                        class="form-control"
                                        autofocus="true" value=""></form:input>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <input class="btn btn-primary pull-right btn-custom" id="btn_submit" type="submit" value="Submit">
    </form:form>


</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {
        $('#departId').val($('#subDeptId').val());
        $(".amountTotal").on('blur', function (e) {
            var netTotal = calculateAmountTotal();
            $("#totalBill").html(netTotal);
        });


        function calculateAmountTotal() {
            var netTotal = 0;
            $(".amountTotal").each(function (index, ele) {
                netTotal += parseFloat($(ele).val());
            });
            return netTotal.toFixed(2);
        }
        });



    function goBack(){
     window.location = "${contextPath}/Gin/ginListing";
    }

 $('#subDeptId').change(function(){
    $('#departId').val($('#subDeptId').val());
 });

       $('#deptId').change(function(){
           $.ajax({
                   url: '${contextPath}/SubDepartment/getSubDepartments',
                   contentType: 'application/json',
                   data: {
                       deptId: $('#deptId').val(),

                  },
                   success: function (response) {
                    var value;
                    $("#subDeptId").empty();
                    var subDept = document.getElementById('subDeptId');
                     $("#subDepart").html(response);
                   Object.keys(response).forEach(function(key) {
                        var subDeptOpt = document.createElement('option');
                         value = response[key]
                         subDeptOpt.innerHTML = value;
                         subDeptOpt.value = key;
                         subDept.appendChild(subDeptOpt);
                     });
                     $('#departId').val($('#subDeptId').val());
                   },
                   error: function (e) {

                       console.log("ERROR : ", e);

                   }
               });

    });
    function getMeasureUnit(id) {
        $.ajax({
            // Request to Stock Controller
            url: '${contextPath}/Stock/ajax/getMeasureUnit',
            contentType: 'application/json',
            data: {
                rawId: $('#stockAccountCode' + id).val()

            },

            success: function (response) {
                $('#measureUnit' + id).val(response);

            }
        });

    }

</script>
</body>
</html>
