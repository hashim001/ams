<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Day End Sheet</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<section class="main">
    <div class="container">
        <h2 class="heading-main">Daily Sales </h2>
        <div class="row">
            <div class="dayEndSec">
                <div class="col-sm-3">
                    <fieldset>
                        <legend>Cash Balance</legend>

                        <section class="">
                            <form class="form-horizontal">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Meter Sale(Liters)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </form>
                        </section>
                    </fieldset>
                    <fieldset>
                        <legend>Cash Balance</legend>

                        <section class="">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Opening cash</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cash sale(Fuel/oil)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">recepit/Collection</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Total Recepit</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Payment Deposit</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Withdrawl</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cash payments</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Expenses</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Closing cash</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </form>
                        </section>
                    </fieldset>
                </div>
                <div class="col-sm-9">
                    <form action="${contextPath}/Nozzle/dayEndFinalPost?${_csrf.parameterName}=${_csrf.token}" method="post">
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <input type="date" class="form-control"  name="reportDate" id="currentDate" onchange="this.form.submit()" value="${currentDate}">
                    </form>
                    <section class="dayEndTableSec">
                        <section class="dayEndTableScrollSec">
                            <div class="main-table">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Nozzel</th>
                                        <th>Item</th>
                                        <th>Meter Start</th>
                                        <th>Meter End</th>
                                        <th>Test</th>
                                        <th>Sale Qty</th>
                                        <th>Price</th>
                                        <th>Amount</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <form:form modelAttribute="dayEndList" method="POST">
                                    <c:forEach items="${dayEndList.dailyNozzleReadingList}" var="dailyReading" varStatus="status2">
                                        <tr>
                                            <td>${dailyReading.nozzle.nozzleName}</td>
                                            <td>${dailyReading.itemAccount.accountStock.name}</td>
                                            <td id ="openingReading${status2.index}" >${dailyReading.openingReading}</td>
                                            <td><spring:bind path="dailyNozzleReadingList[${status2.index}].closingReading">
                                                <form:input id ="closingReading${status2.index}" type="text" path="dailyNozzleReadingList[${status2.index}].closingReading"   onblur="updateValues(${status2.index})" autofocus="true" value = "0.0"></form:input>
                                            </spring:bind></td>
                                            <td><spring:bind path="dailyNozzleReadingList[${status2.index}].testQuantity">
                                                <form:input id="testQuantity${status2.index}" type="text" path="dailyNozzleReadingList[${status2.index}].testQuantity"    onblur="updateValues(${status2.index})" autofocus="true" value = "0.0"></form:input>
                                            </spring:bind></td>
                                            <form:input type="date"  path="dailyNozzleReadingList[${status2.index}].date"   cssStyle="display: none" autofocus="true" cssClass="theDate"></form:input>
                                            <td id="saleQuantity${status2.index}" > </td>
                                            <td id="sellingPrice${status2.index}">${dailyReading.itemAccount.accountStock.sellingPrice}</td>
                                            <td id="totalAmount${status2.index}" > </td>

                                        </tr>
                                    </c:forEach>


                                    </tbody>
                                </table>

                            </div>
                        </section>
                        <section class="dayEndTableScrollSec">
                            <div class="main-table">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Item Name</th>
                                        <th>Total Stock</th>
                                        <th>Left Behind</th>
                                        <th>sale Qty</th>
                                        <th>Price</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <c:forEach items="${dayEndList.dailyItemReadingList}" var="item" varStatus="status3">
                                        <tr>
                                            <td>${item.itemAccount.accountStock.name}</td>
                                            <td id ="openingReadingItem${status3.index}" >${item.openingReading}</td>
                                            <td><spring:bind path="dailyItemReadingList[${status3.index}].closingReading">
                                                <form:input  id ="closingReadingItem${status3.index}" type="text" path="dailyItemReadingList[${status3.index}].closingReading"   onblur="updateItemValues(${status3.index})" autofocus="true" value = "0.0"></form:input>
                                            </spring:bind></td>
                                            <form:input type="date"  path="dailyItemReadingList[${status3.index}].date"   cssStyle="display: none" autofocus="true" cssClass="theDate"></form:input>
                                            <td id="saleQuantityItem${status3.index}" >0.0</td>
                                            <td id="sellingPriceItem${status3.index}" >${item.itemAccount.accountStock.sellingPrice}</td>
                                            <td id="totalAmountItem${status3.index}" >0.0</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <button type="submit" >Submit</button>
                                </form:form>
                            </div>
                        </section>
                        <section class="row">
                            <section class="col-sm-7">
                                <section class="dayEndTableScrollSec">
                                    <div class="main-table">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Qauntity</th>
                                                <th>Test</th>
                                                <th>Sale Qty</th>
                                                <th>Sale Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Super</td>
                                                <td>0.0</td>
                                                <td> </td>
                                                <td>0.0</td>
                                                <td>0.0</td>
                                            </tr>
                                            <tr>
                                                <td>Super</td>
                                                <td>0.0</td>
                                                <td> </td>
                                                <td>0.0</td>
                                                <td>0.0</td>
                                            </tr>
                                            <tr>
                                                <td>Super</td>
                                                <td>0.0</td>
                                                <td> </td>
                                                <td>0.0</td>
                                                <td>0.0</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </section>
                            <section class="col-sm-5">
                                <section class="dayEndTableScrollSec">
                                    <div class="main-table">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Cash</th>
                                                <th>Credit</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${cashCreditList}" var="item">
                                                <tr>

                                                    <td>${item.item}</td>
                                                    <td>${item.cash}</td>
                                                    <td>${item.credit}</td>
                                                    <td>${item.total}</td>
                                                </tr>
                                            </c:forEach>

                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </section>
                        </section>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function() {
        $('.theDate').val($('#currentDate').val());
        $('#currentDate').change(function() {
            $('.theDate').val($('#currentDate').val());

        });

    });

    function updateValues(id) {

        $('#saleQuantity' + id).text($('#closingReading' + id).val() - $('#openingReading' + id).text() - $('#testQuantity' + id).val());
        $('#totalAmount' + id).text(($('#closingReading' + id).val() - $('#openingReading' + id).text() - $('#testQuantity' + id).val()) *$('#sellingPrice' + id).text() );

    }

    function updateItemValues(id) {

        $('#saleQuantityItem' + id).text($('#openingReadingItem' + id).text() - $('#closingReadingItem' + id).val());
        $('#totalAmountItem' + id).text(($('#openingReadingItem' + id).text() - $('#closingReadingItem' + id).val()) *$('#sellingPriceItem' + id).text() );

    }

</script>
</body>
</html>
