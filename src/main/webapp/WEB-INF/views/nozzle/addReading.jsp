<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Daily Reading</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "../header.jsp" %>

<div class="container">
    ${readingAddSuccess}

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form:form method="POST" modelAttribute="readingForm" >
                <h2 class="form-signin-heading">Add Reading</h2>
                <spring:bind path="accountCode">
                    <div class="form-group">
                        <form:label path="accountCode">Item Account</form:label>
                        <form:select  path="accountCode" class="form-control" >
                            <c:forEach items="${stockAccounts}" var="account" >
                                <form:option value="${account.accountCode}">${account.title}
                                    <c:choose>
                                        <c:when test="${account.parentAccount != null }"> - ${account.parentAccount.title}
                                        </c:when>
                                    </c:choose>
                                </form:option>
                            </c:forEach>
                        </form:select>

                    </div>
                </spring:bind>



                <spring:bind path="accountCode">
                    <div class="form-group">
                        <form:label path="accountCode">Item Account</form:label>
                        <form:select  path="nozzleCode" class="form-control" >
                            <c:forEach items="${nozzles}" var="nozzle" >
                                <form:option value="${nozzle.nozzleCode}">${nozzle.nozzleName}</form:option>
                            </c:forEach>
                        </form:select>

                    </div>
                </spring:bind>
                 <spring:bind path="openingReading">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="openingReading">Opening Reading</form:label>
                        <form:input type="text" path="openingReading" class="form-control" placeholder="Opening Reading"
                                    autofocus="true" value="0.0"></form:input>
                        <form:errors path="openingReading"></form:errors>
                    </div>
                </spring:bind>
                <spring:bind path="closingReading">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="closingReading">Closing Reading</form:label>
                        <form:input type="text" path="closingReading" class="form-control" placeholder="Closing Reading"
                                    autofocus="true" value="0.0"></form:input>
                        <form:errors path="closingReading"></form:errors>
                    </div>
                </spring:bind>
                <spring:bind path="testQuantity">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="testQuantity">Test Quantity</form:label>
                        <form:input type="text" path="testQuantity" class="form-control" placeholder="Test Quantity"
                                    autofocus="true" value="0.0"></form:input>
                        <form:errors path="testQuantity"></form:errors>
                    </div>
                </spring:bind>

                <spring:bind path="date">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="date">Date</form:label>
                        <form:input type="date" path="date" class="form-control"
                                    autofocus="true" value="${currentDate}"></form:input>
                        <form:errors path="date"></form:errors>
                    </div>
                </spring:bind>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
            </form:form>
        </div>
        <div class="col-md-2"></div>


    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function() {


    });

</script>
</body>
</html>
