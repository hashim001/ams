<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/dashboard.css">
    <script src="${contextPath}/resources/js/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
    <title>Dashboard</title>
</head>
<body>

<section class="dashboardHeaderSec">
    <section class="logoSec">
        <c:choose>
            <c:when test="${imageUrl==''}">
                <a href="${contextPath}/home"><img src="${contextPath}/resources/img/logo.png" alt="logo"></a>
            </c:when>
            <c:otherwise>
                <a href="${contextPath}/home"><img src="${imageUrl}" alt="logo"></a>
            </c:otherwise>
        </c:choose>
    </section>
    ${paymentResponse}
</section>


<section class="dashboradLeftMenuContent">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#home" aria-controls="profile" role="tab"
                                   data-toggle="tab">Home</a></li>
        <li role="presentation"><a href="#masterFile" aria-controls="profile" role="tab" data-toggle="tab">Accounts Master Files</a></li>
        <li role="presentation"><a href="#stock" aria-controls="profile" role="tab" data-toggle="tab">Plot Master Files</a></li>
        <li role="presentation"><a href="#transactionFile" aria-controls="profile" role="tab" data-toggle="tab">Accounts Transactions Files</a></li>
        <li role="presentation"><a href="#financialFile" aria-controls="profile" role="tab" data-toggle="tab">Financial Reporting</a></li>
        <li role="presentation"><a href="#reports" aria-controls="profile" role="tab" data-toggle="tab">Reports</a></li>
        <li role="presentation"><a href="#purshaseOrSale" aria-controls="profile" role="tab" data-toggle="tab">Sale</a>

        </li>

        <li role="presentation"><a href="#register" aria-controls="profile" role="tab"
                                   data-toggle="tab">SL/LP Register</a>

        </li>

        <li role="presentation"><a href="#settings" aria-controls="profile" role="tab" data-toggle="tab">Settings</a>

        </li>

        <li role="presentation"><a href="#accessControl" >Access Control</a>

        </li>
        <li role="presentation"><a href="#" onclick="logout()" aria-controls="profile" role="tab" data-toggle="tab">Logout</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <section class="tab-content">
        <section role="tabpanel" class="tab-pane active" id="home">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Home</h3>
            </section>
            <section class="dashboardGraphContent">
                <section class="row">

                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="graphSec">

                        </div>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="masterFile">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Accounts Master Files</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/Account/addAccount">Add Account</a></li>
                        <li><a href="${contextPath}/Account/AccountListing">Account List</a></li>
                        <li><a href="${contextPath}/Account/addCostCentre">Add Cost Centre</a></li>
                        <li><a href="${contextPath}/User/addBankDetails">Add Bank Details</a></li>
                        <li><a href="${contextPath}/Account/addExpense">Add Expense details</a></li>
                        <li><a href="${contextPath}/Account/addTaxDetails">Add Tax details</a></li>
                        <li><a href= "${contextPath}/Maker/addMaker">Add Make</a></li>
                        <li><a href= "${contextPath}/Salesman/addSalesman">Add Salesman</a></li>

                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
 <section role="tabpanel" class="tab-pane fade" id="financialFile">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Financial Reporting</h3>
            </section>
            <section class="dashboradNavBoxContent">

                <section class="dashboradNavBoxRow">

                    <ul>
                        <li><a href="${contextPath}/Account/addAccountNote">Add Note</a></li>
                        <li><a href="${contextPath}/User/generateStatement">BS/PL </a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane fade" id="transactionFile">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Accounts Transactions Files</h3>
            </section>
            <section class="dashboradNavBoxContent">

                <section class="dashboradNavBoxRow">

                    <ul>
                        <li>
                            <a href="${contextPath}/Account/createPaymentVoucher">Payment Voucher</a>
                        </li>
                        <li>
                            <a href="${contextPath}/Account/createReceiptVoucher">Receipt Voucher</a>
                        </li>
                        <li>
                            <a href="${contextPath}/Account/viewJournalVoucher">Journal Voucher</a>
                        </li>
                        <li>
                            <a href="${contextPath}/Account/bankReconciliation">Reconciliation</a>
                        </li>
                        <li>
                            <a href="${contextPath}/Account/createPdcJournalVoucher">PDC Voucher</a>
                        </li>
                        <li>
                            <a href="${contextPath}/Account/createBankChequeReturn">Cheque Return Voucher</a>
                        </li>
                        <li>
                            <a href="${contextPath}/Account/viewPettyCashVouchers">Payment Petty Cash</a>
                        </li>
                        <li>
                            <a href="${contextPath}/Account/viewPurchasePettyCashVouchers">Purchase Petty Cash</a>
                        </li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="reports">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Reports</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/Reports/chartOfAccount">Chart Of Account</a></li>
                        <li><a href="${contextPath}/Reports/costCentres">Cost Centre</a></li>
                        <li><a href="${contextPath}/Reports/paymentVoucher">Payment Voucher</a></li>
                        <li><a href="${contextPath}/Reports/receiptVoucher">Receipt Voucher</a></li>
                        <li><a href="${contextPath}/Reports/journalVoucher">Journal Voucher</a></li>
                        <li><a href="${contextPath}/Reports/ledgerReport">General Ledger</a></li>
                        <li><a href="${contextPath}/Reports/projectWiseReport">Project Wise Report</a></li>
                        <li><a href="${contextPath}/Reports/ledgerReportBank">Bank/Cash Book</a></li>
                        <li><a href="${contextPath}/Reports/trialBalanceBasic">Trial Balance (Basic)</a></li>
                        <li><a href="${contextPath}/Reports/trialBalanceDetail">Trial Balance (Detailed)</a></li>
                        <li><a href="${contextPath}/Reports/noteSummary">Note Summary</a></li>
                        <li><a href="${contextPath}/Reports/sheetReport">Balance Sheet</a></li>
                        <li><a href="${contextPath}/Reports/chassisLedger">Plot Ledger</a></li>
                        <li><a href="${contextPath}/Report/currentApartmentsReport">Plot Report</a></li>
                        <li><a href="${contextPath}/Reports/currentCustomerReport">Current Customer Report</a></li>
                        <li><a href="${contextPath}/Reports/currentBankReport">Current Bank Report</a></li>
                        <li><a href="${contextPath}/Salesman/salesmanReport">Agent Report</a></li>
                        <li><a href="${contextPath}/Report/holdApartmentReport">Hold Plot Report</a></li>
                        <li><a href="${contextPath}/Reports/detailedGeneralReport">Detailed General Report</a></li>


                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="stock">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Flat Master Files</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/Stock/addStockCategory">Add Site</a></li>
                        <li><a href="${contextPath}/Stock/addStock">Add Phase</a></li>
                        <li><a href="${contextPath}/Apartment/viewApartmentList">Add Plot</a></li>
                        <li><a href="${contextPath}/StockVendor/addVendor">Add Vendor</a></li>
                        <li><a href="${contextPath}/Customer/addCustomer">Add Customer</a></li>

                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="purshaseOrSale">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Sale</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <%--<li><a href="${contextPath}/Purchase/viewPurchaseVoucher">Purchase Vouchers</a></li>--%>
                        <li><a href="${contextPath}/Sale/createSaleVoucher">Add Sales Voucher</a></li>
                        <li><a href="${contextPath}/Sales/viewSalesVoucher">Sales Vouchers</a></li>
                        <li><a href="${contextPath}/SaleLead/addSaleLead">Sale Leads</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="register">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>SL/LP Register</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/Reports/saleRegister">Sale Register</a></li>
                        <li><a href="${contextPath}/Reports/purchaseRegister">Purchase Register</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="settings">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>Settings</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/staticinfo">Static Info</a></li>
                        <li><a href="${contextPath}/changePassword">Change Password</a></li>
                        <li><a href="${contextPath}/dbBackUp">DB Backup</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section role="tabpanel" class="tab-pane" id="accessControl">
            <section class="mainHeadingSec">
                <h2>Dashboard</h2>
                <h3>User Management</h3>
            </section>
            <section class="dashboradNavBoxContent">
                <section class="dashboradNavBoxRow">
                    <ul>
                        <li><a href="${contextPath}/AccessControl/users">Manage Access</a></li>
                    </ul>
                </section>
                <section class="dashboardGraphContent">
                    <section class="row">

                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">
                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="graphSec">

                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>
    <form action="${contextPath}/logout" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</section>
<script>
    $(document).ready(function () {
        $(function () {
            var hash = window.location.hash;
            console.log($('ul.nav a[href="' + hash + '"]'));
            console.log($('ul.nav a[href="' + hash + '"]'));
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            $('.nav-tabs a').click(function (e) {
                $(this).tab('show');
                var scrollmem = $('body').scrollTop();
                window.location.hash = this.hash;
                $('html,body').scrollTop(scrollmem);
            });
        });

    });

    function logout() {
        $('#logoutForm').submit();
    }
</script>


</body>
</html>

