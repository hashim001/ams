<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <title>Settings Voucher</title>
</head>
<body>
<%@ include file = "header2.jsp" %>

<div style="background-color: #ffcaca;height: 150px; margin-left: 10%  ;margin-top: 5%;width: 80%; text-align: center;padding-top: 5px;border: 2px solid #a50000;border-radius: 5px;font-size: x-large;">
    It is highly recommended NOT to use the functionality you are not aware of. Changes may occur which can only be reverted from the database.
</div>

<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>