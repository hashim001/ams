<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add SubHeading Account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file = "navigation.jsp" %>

<div class="container">
    ${subHeadAccountAddSuccess}
    ${subHeadAccountRemoveSuccess}

    <div class="row">
        <div class="col-md-4">
            <form method="post" action="${contextPath}/Account/addSubHeadAccount?${_csrf.parameterName}=${_csrf.token}" class="form-signin">

                <div class="form-group">
                    <label>Account</label>
                    <select name="accountCode" class="form-control" >
                        <c:forEach items="${levelAccounts}" var="account" >
                            <option value="${account.accountCode}">${account.title}</option>
                        </c:forEach>
                            </select>

                </div>
                <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                <input  type="hidden" name="subId" value="${subHeadingId}"/>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Add</button>
            </form>
        </div>
        <div class="col-md-2"></div>


    </div>
</div>
<div class="container">
    <h3>Subheading Accounts</h3>

    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Title</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${noteSubHeading.accounts}" var="account" varStatus="status">

            <tr>
                <td>${account.title}</td>
                <td><form method="post" action="${contextPath}/Account/removeSubHeadAccount?${_csrf.parameterName}=${_csrf.token}" class="form-inline pull-left">
                        <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
                        <input  type="hidden" name="subId" value="${subHeadingId}"/>
                        <input  type="hidden" name="accountCode" value="${account.accountCode}"/>
                        <input class="btn btn-xs btn-danger" type="submit" value="Remove"/>
                    </form></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>


<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>

    $(document).ready(function() {

    });



</script>
</body>
</html>
