<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create an account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>
<div class="container">
</div>

<div class="container">


    <h2>User Details</h2>
    <h4>Username: ${userModel.userAuth.userName}</h4>
    <div class="row">

        <div class="col-md-8">

            <h4>Email : ${userModel.userAuth.emailId}</h4>
            <h4>Status : ${userModel.userAuth.isActive}</h4>


        </div>

        <div class="col-md-4">
            <h4>Edit Employee</h4>

            <form:form method="POST" modelAttribute="userModel" action="${contextPath}/AccessControl/manageUserPost">

            <spring:bind path="userAuth.emailId">
                <div class="form-group">
                    <form:label path="userAuth.emailId">Email</form:label>
                    <form:input type="text" path="userAuth.emailId" class="form-control" placeholder="Enter amount"
                                autofocus="true"></form:input>

                </div>
            </spring:bind>

            <spring:bind path="userAuth.passwordConfirm">
                <div class="form-group">
                    <form:label path="userAuth.passwordConfirm">Enter new Password</form:label>
                    <form:input type="password" path="userAuth.passwordConfirm" class="form-control"
                                placeholder="Enter Password"
                                autofocus="true"></form:input>
                    <form:errors path="userAuth.passwordConfirm"></form:errors>
                    <p style="font-size: x-small">Keep Blank if no change</p>
                </div>
            </spring:bind>
        </div>
    </div>


</div>

<div class="container">

    <h3>Permissions</h3>

    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Role</th>
            <th>Description</th>
            <th>Access</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${userModel.roleList}" var="role" varStatus="status">

            <tr>
                <td>${role.name}</td>
                <td>${role.description}</td>
                <td><form:checkbox path="roleList[${status.index}].checked"/></td>

            </tr>
        </c:forEach>
        </tbody>
    </table>
    <button type="submit">Update</button>
    </form:form>
</div>

<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>

<script>
    function goBack(){
        window.location = "${contextPath}/AccessControl/users";
    }

</script>
</body>
</html>
