<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Management</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%@ include file="../header.jsp" %>


<section class="main">
    <div class="container">
        ${userStatusSuccess}
        ${userRemoveSuccess}
        ${userAccessChanged}
        <h2 class="heading-main">Users<span class="addIcon" data-toggle="modal"
                                            data-target="#myModal"><i><img
                src="${contextPath}/resources/img/add.png" alt=""></i>Add</span></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-table">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${users}" var="user" varStatus="status">

                            <tr>
                                <td>${user.userName}</td>
                                <td>${user.emailId}</td>
                                <td><c:choose>
                                    <c:when test="${user.isActive == 1}">
                                        ACTIVE
                                    </c:when>
                                    <c:otherwise>
                                        INACTIVE
                                    </c:otherwise>
                                </c:choose></td>

                                <td>
                                    <form method="post" id="userRemove${status.index}"
                                          action="${contextPath}/AccessControl/removeUser">
                                        <input type="hidden" name="authId" value="${user.auth_id}"/>
                                    </form>
                                    <form method="post" id="userStatus${status.index}"
                                          action="${contextPath}/AccessControl/toggleStatus">
                                        <input type="hidden" name="authId" value="${user.auth_id}"/>
                                    </form>
                                    <form method="post" id="userManage${status.index}"
                                          action="${contextPath}/AccessControl/manageUser">
                                        <input type="hidden" name="authId" value="${user.auth_id}"/>
                                    </form>
                                    <ul class="list-inline">
                                        <li><a title="Manage Access" href="#" onclick="manageUser(${status.index});"><img
                                                src="${contextPath}/resources/img/edit.png" alt=""></a></li>
                                        <li><a title="Toggle Status" href="#" onclick="toggleStatus(${status.index});"><img
                                                src="${contextPath}/resources/img/recycle.png" alt=""></a></li>
                                        <li><a title="Remove Permanently" href="#" onclick="remove(${status.index});"><img
                                                src="${contextPath}/resources/img/remove.png" alt=""></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-small" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span data-dismiss="modal" aria-label="Close" aria-hidden="true" class="closeFixed">&times;</span>
                <h3 class="modal-title" id="myModalLabel" style="color: black">Create User</h3>
            </div>

            <form:form method="POST" modelAttribute="userForm">
                <div class="modal-body">
                    <spring:bind path="userName">
                        <form:input type="text" path="userName" placeholder="Username"
                                    autofocus="true"></form:input>
                        <form:errors path="userName"></form:errors>
                    </spring:bind>
                    <spring:bind path="emailId">
                        <form:input type="text" path="emailId"  placeholder="Email"></form:input>
                        <form:errors path="emailId"></form:errors>
                    </spring:bind>
                    <spring:bind path="password">
                        <form:input type="password" path="password"  placeholder="Password"></form:input>
                        <form:errors path="password"></form:errors>
                    </spring:bind>
                    <spring:bind path="passwordConfirm">
                        <form:input type="password" path="passwordConfirm"
                                    placeholder="Confirm your password"></form:input>
                        <form:errors path="passwordConfirm"></form:errors>
                    </spring:bind>
                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit">Submit</button>
                        </div>
                        <div class="col-sm-6 text-center">
                            <button type="button" data-dismiss="modal">Exit</button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<!-- /container -->
<script src="${contextPath}/resources/js/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 7
        });
    });
    function goBack(){
        window.location = "${contextPath}/home#accessControl";
    }

    function remove(id){
        if(confirm("Are you sure you want to remove the User")){
            $("#userRemove" + id).submit();
        }
    }
    function toggleStatus(id){
        if(confirm("Are you sure you want to change User status")){
            $("#userStatus" + id).submit();
        }
    }
    function manageUser(id){
            $("#userManage" + id ).submit();

    }
</script>
</body>
</html>
