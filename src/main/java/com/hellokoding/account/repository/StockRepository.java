package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Maker;
import com.hellokoding.account.model.Stock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StockRepository extends PagingAndSortingRepository<Stock ,Long>, QueryByExampleExecutor<Stock> {

    @Modifying
    @Transactional
    @Query("DELETE FROM Stock s WHERE s.stockAccount = :account")
    void deleteByAccountCode(@Param("account") Account account);

    Stock findByStockId(Integer stockId);

    /**
     * Get Measure Unit through native Query
     * @param accountCode Stock Account Code
     * @return Measure Unit
     */
    @Query(value = "SELECT COALESCE (s.unit_measure,'- NIL -') from account_stock s where s.account_code =?1 ", nativeQuery = true)
    String getMeasureUnit(int accountCode);

    List<Stock> findByMaker(Maker maker);
}
