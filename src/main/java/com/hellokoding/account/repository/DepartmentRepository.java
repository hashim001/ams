package com.hellokoding.account.repository;

import com.hellokoding.account.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DepartmentRepository extends JpaRepository<Department, Long> {


    Department findByDepId(int deptId);

    List<Department> findByDeptGm(int deptGm);

    @Modifying
    @Transactional
    @Query("DELETE FROM Department d WHERE d.depId = :deptId")
    void removeDept(@Param("deptId") int deptId);

}
