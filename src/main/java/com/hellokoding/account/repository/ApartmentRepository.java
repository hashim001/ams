package com.hellokoding.account.repository;

import com.hellokoding.account.model.Apartment;
import com.hellokoding.account.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ApartmentRepository extends JpaRepository<Apartment, Long> {
    Apartment findByApartmentId(int apartmentId);

    @Query("select c from Apartment c where c.distributorId = :distributorId order by c.date ASC")
    List<Apartment> findByDistributorId(@Param("distributorId") int distributorId);

    @Modifying
    @Transactional
    @Query("DELETE FROM Apartment c WHERE c.apartmentId= :apartmentId")
    void removeApartment(@Param("apartmentId") int apartmentId);

    @Modifying
    @Transactional
    @Query("UPDATE Apartment a set a.commissionStatus =:status, a.commissionDue = current_date WHERE a.apartmentNo= :apartmentNumber")
    void changeCommissionStatus(@Param("apartmentNumber") String apartmentNumber,@Param("status") String commissionStatus);

    List<Apartment> findByIsSold(boolean isSold);

    Apartment findByApartmentNo(String chassisNumber);

    List<Apartment> findByCommissionStatus(String commissionStatus);

    List<Apartment> findByCustomerAccount(int customerAccount);

    List<Apartment> findByCustomerAccountAndDistributorId(int customerAccount, int distributorId);

    List<Apartment> findByCustomerAccountAndDistributorIdAndStock(int customerAccount, int distributorId, Stock stock);

    List<Apartment> findByDistributorIdNot(int distributorId);

    @Query("select c from Apartment c order by c.date ASC")
    List<Apartment> findAllByDate();

    @Query("select distinct a.floor from Apartment a order by a asc ")
    List<String> getFloorList();

    // for Floor Payment Plan Report
    @Query(value = "select a.apartment_number from apartment a where a.floor=?1 and is_sold = 0 GROUP BY stock_id", nativeQuery = true)
    List<String> findApartmentNoByFloor(String floor);

    @Query(value = "select a.apartment_number from apartment a where a.stock_id=?1 and a.floor=?2 and is_sold = 0 GROUP BY stock_id", nativeQuery = true)
    List<String> findApartmentNoByStockIdAndFloor(int stockId, String floor);

    @Query("select c from Apartment c where c.isHold = false and c.isSold = false")
    List<Apartment> findAllAvailableApartments();

    @Query("select c from Apartment c where c.isHold = false and c.isSold = false and c.stock = :stock")
    List<Apartment> findAllAvailableApartmentsByStock(@Param("stock") Stock stock);

}
