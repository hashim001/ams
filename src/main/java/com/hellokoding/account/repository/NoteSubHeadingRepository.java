package com.hellokoding.account.repository;

import com.hellokoding.account.model.NoteSubHeading;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteSubHeadingRepository extends JpaRepository<NoteSubHeading ,Long> {

    NoteSubHeading findBySubId(Integer subId);
}
