package com.hellokoding.account.repository;

import com.hellokoding.account.model.SubDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface SubDepartmentRepository extends JpaRepository<SubDepartment, Long> {
    SubDepartment findBySubDepId(int subDepId);

    @Modifying
    @Transactional
    @Query("DELETE FROM SubDepartment d WHERE d.subDepId = :subDepId")
    void removeSubDept(@Param("subDepId") int subDepId);
}
