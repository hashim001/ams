package com.hellokoding.account.repository;

import com.hellokoding.account.model.StockCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StockCategoryRepository  extends JpaRepository <StockCategory ,Long>{

    StockCategory findByCategoryId(Integer categoryId);


    List<StockCategory> findByAccountCodeGreaterThan(int accountCode);
}
