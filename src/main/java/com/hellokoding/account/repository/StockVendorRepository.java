package com.hellokoding.account.repository;

import com.hellokoding.account.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StockVendorRepository extends JpaRepository<StockVendor, Long> {

    @Modifying
    @Transactional
    @Query("DELETE FROM StockVendor s WHERE s.vendorAccount = :account")
    void deleteByAccountCode(@Param("account") Account account);

    StockVendor findByVendorId(int id);

    List<StockVendor> findByVendorDistributorIsNull();

/*    List<StockVendor> findByRiderIsNull();

    List<StockVendor> findByRider(Rider rider);*/

    List<StockVendor> findByVendorDistributor(Distributor distributor);

    @Query("SELECT v FROM StockVendor v join v.vendorAccount a WHERE a.title LIKE :vendorName")
    Page<StockVendor> findAllStockVendor(@Param("vendorName") String vendorName, Pageable pageable);
}


