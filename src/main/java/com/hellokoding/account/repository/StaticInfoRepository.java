package com.hellokoding.account.repository;


import com.hellokoding.account.model.StaticInfo;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StaticInfoRepository extends JpaRepository<StaticInfo, Long> {


    public StaticInfo findByInfoId(int infoId);
}

