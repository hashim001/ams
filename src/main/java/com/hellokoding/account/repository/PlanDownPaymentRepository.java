package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.PaymentPlanDown;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface PlanDownPaymentRepository extends JpaRepository<PaymentPlanDown, Long> {

    PaymentPlanDown findByDownId(int downId);

    @Modifying
    @Transactional
    @Query("DELETE FROM PaymentPlanDown ppd WHERE ppd.downId = :downId")
    void remove(@Param("downId") int downId);
}
