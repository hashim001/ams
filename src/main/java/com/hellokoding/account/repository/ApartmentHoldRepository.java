package com.hellokoding.account.repository;

import com.hellokoding.account.model.Apartment;
import com.hellokoding.account.model.ApartmentHold;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApartmentHoldRepository extends JpaRepository<ApartmentHold, Long> {
    ApartmentHold findByApartment(Apartment apartment);

    List<ApartmentHold> findBySalesmanId(int salesmanId);
}
