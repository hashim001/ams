package com.hellokoding.account.repository;

import com.hellokoding.account.model.UserAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserAuthRepository extends JpaRepository<UserAuth, Long> {

    UserAuth findByUserNameAndIsActive(String username, int isActive);

    UserAuth findByUserName(String username);

    @Query("select u from UserAuth u where u.auth_id =:Authid ")
    UserAuth findByAuthId(@Param("Authid") int authid);

    @Query("select u.userName from UserAuth u where u.auth_id =:Authid ")
    String findName(@Param("Authid") int authid);

    @Query("select u from UserAuth u where u.auth_id <>:Authid ")
    List<UserAuth> findAll(@Param("Authid") int authId);

    @Query("select u from UserAuth u join u.roles r where r.name = 'ROLE_GM'  ")
    List<UserAuth> findAllGM();


    @Modifying
    @Transactional
    @Query("DELETE FROM UserAuth uo WHERE uo.auth_id = :authId")
    void removeUser(@Param("authId") int auth);
}
