package com.hellokoding.account.repository;

import com.hellokoding.account.model.Distributor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface DistributorRepository extends JpaRepository<Distributor,Long > {


    @Modifying
    @Transactional
    @Query("DELETE FROM Distributor r WHERE r.distributorId = :disId")
    void deleteByDistributorId(@Param("disId") Integer id);

    Distributor findByDistributorId(int id);
}
