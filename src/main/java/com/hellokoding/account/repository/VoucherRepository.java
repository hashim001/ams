package com.hellokoding.account.repository;

import com.hellokoding.account.model.Voucher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

public interface VoucherRepository extends JpaRepository<Voucher, Long> {

    Voucher findByVoucherId(int voucherId);

    List<Voucher> findByVoucherNumber(String voucherNumber);

    List<Voucher> findByItemAccount(int itemAccount);

    @Query("Select v from Voucher v where v.itemAccount > 0")
    List<Voucher> findAllItemVouchers();

    List<Voucher> findByBankAccount(int bankAccount);

    List<Voucher> findByBankAccountOrderByVoucherDateAsc(int bankAccountCode);

    List<Voucher> findByReferenceVoucher(String referenceVoucher);

    List<Voucher> findByAccountCodeAndVoucherDateLessThanEqual(int bankAccountCode, Date date);

    @Query("select v from Voucher v where v.voucherDate >= :fromDate and v.voucherDate <= :toDate and v.bankAccount > 0 and v.status = 'PARTIAL'")
    List<Voucher> findBankAccountByDateOrder(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);


    List<Voucher> findByAccountCodeOrderByVoucherDateAsc(int accountCode);

    @Query("select max(v.voucherNumber) from Voucher v where v.voucherNumber like :prefixString")
    String findMaxVoucherNumber(@Param("prefixString") String prefixString);

    @Query("select v.voucherNumber from Voucher v where v.voucherId = (select max(v.voucherId) from v where v.referenceVoucher is not null)")
    String findMaxIdForChild();

    @Query("select v from Voucher v where v.voucherNumber like :prefixString")
    List<Voucher> findByVoucherType(@Param("prefixString") String prefixString);

    @Query("select v from Voucher v where v.bankAccount = :bank and v.voucherDate <= :date and v.clearingDate is null")
    List<Voucher> findPendingVouchers(@Param("bank") int bankAccount, @Param("date") Date date);

    @Query("select v from Voucher v where v.bankAccount = :bank and v.voucherDate <= :date and v.clearingDate is not null")
    List<Voucher> findClearedVouchers(@Param("bank") int bankAccount, @Param("date") Date date);

    @Query("select v from Voucher v where v.voucherDate >= :fromDate and v.voucherDate <= :toDate and v.voucherNumber like :prefixString")
    List<Voucher> findVouchersByPrefix(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("prefixString") String prefixString);

    @Query("select distinct v.voucherNumber from Voucher v where v.voucherDate >= :fromDate and v.voucherDate <= :toDate and v.voucherNumber like :prefixString")
    List<String> findJournalVouchersByPrefix(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("prefixString") String prefixString);

    @Query("select distinct v.voucherNumber from Voucher v where v.voucherNumber between :fromVoucher and :toVoucher and v.voucherNumber like :prefixString")
    List<String> findJournalVouchersByPrefixAndNumber(@Param("fromVoucher") String fromVoucher, @Param("toVoucher") String toVoucher, @Param("prefixString") String prefixString);

    @Query(value = "select v.voucher_number from voucher v where v.reference_voucher =?1 limit 1", nativeQuery = true)
    String getVoucherNumberByReference(String reference);

    @Query("select v from Voucher v where v.clearingDate >= :fromDate and v.clearingDate <= :toDate and v.bankAccount =:bankAccount")
    List<Voucher> findJournalVouchersByBankAccount(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("bankAccount") int bankAccount);

    @Query("select v from Voucher v where v.voucherNumber between :fromVoucher and :toVoucher and v.voucherNumber like :prefixString")
    List<Voucher> findVouchersByPrefixAndNumber(@Param("fromVoucher") String fromVoucher, @Param("toVoucher") String toVoucher, @Param("prefixString") String prefixString);

    @Query("select distinct v.voucherNumber from Voucher v where v.voucherNumber like :prefixString")
    List<String> findDistinctVoucherNumbers(@Param("prefixString") String prefixString);

    @Query("select v from Voucher v where v.voucherDate = :toDate and v.itemAccount =:itemAccount and v.voucherNumber like :prefixString")
    List<Voucher> findVouchersByItemAccountAndPrefix(@Param("toDate") Date toDate, @Param("itemAccount") Integer account, @Param("prefixString") String prefixString);


    @Query(value = "SELECT * FROM voucher WHERE voucher.account_code = ?1 AND voucher.voucher_id < ?2 AND voucher.voucher_date <= ?3 AND voucher.voucher_number LIKE 'LP%' ORDER BY voucher.voucher_id DESC LIMIT 1", nativeQuery = true)
    Voucher getVoucherForClosing(Integer accountCode, Integer voucherId, Date toDate);

    @Query("select v from Voucher v where v.voucherDate >= :fromDate and v.voucherDate <= :toDate and v.accountCode=:accountCode")
    List<Voucher> findVouchersByCurrentDate(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("accountCode") int accountCode);

    //////////////////////// Removal for  re insertion in daily reading /////////////////////////

    @Modifying
    @Transactional
    @Query("DELETE FROM Voucher v WHERE v.voucherNumber IN :numberList")
    void removeVouchers(@Param("numberList") List<String> voucherNumberList);

    @Query("SELECT distinct v.voucherNumber from Voucher v WHERE v.accountCode = :account and v.voucherDate = :voucherDate and v.voucherNumber like :prefixString")
    List<String> removalVoucher(@Param("account") Integer accountCode, @Param("voucherDate") Date voucherDate, @Param("prefixString") String prefixString);


    @Modifying
    @Transactional
    @Query("DELETE FROM Voucher v WHERE v.voucherNumber = :voucherNumber ")
    void removeVouchersByNumber(@Param("voucherNumber") String voucherNumber);

    @Modifying
    @Transactional
    @Query("DELETE FROM Voucher v WHERE v.referenceVoucher = :voucherNumber ")
    void removeVouchersByReference(@Param("voucherNumber") String voucherNumber);

    List<Voucher> findByVoucherNumberIn(List<String> voucherList);

    // For Purchase
    Page<Voucher> findByVoucherNumberLikeAndDebitGreaterThan(String prefix, double debit, Pageable pageable);

    // For Sale
    Page<Voucher> findByVoucherNumberLikeAndCreditGreaterThan(String prefix, double credit, Pageable pageable);

    // For Payment
    Page<Voucher> findByVoucherNumberLikeAndDebitGreaterThanAndBankAccountGreaterThan(String prefix, double debit, int bankAccount, Pageable pageable);

    // For Receipt
    Page<Voucher> findByVoucherNumberLikeAndCreditGreaterThanAndBankAccountGreaterThan(String prefix, double credit, int bankAccount, Pageable pageable);

    // For Journal
    Page<Voucher> findByVoucherNumberLike(String prefix, Pageable pageable);

    @Query(value = "SELECT * FROM voucher v JOIN account a ON v.`account_code` = a.`account_code` WHERE (a.`title` LIKE ?1 OR v.`voucher_number` LIKE ?1)  AND v.`voucher_number` LIKE ?2 ORDER BY ?#{#pageable}",
            countQuery = "SELECT COUNT(*) FROM voucher v JOIN account a ON v.`account_code` = a.`account_code` WHERE (a.`title` LIKE ?1 OR v.`voucher_number` LIKE ?1)  AND v.`voucher_number` LIKE ?2 ORDER BY ?#{#pageable}", nativeQuery = true)
    Page<Voucher> findJoucherVoucherList(String searchString, String prefix, Pageable pageable);


    // Get Secondary Account name

    @Query(value = "SELECT IFNULL ( (SELECT a.title FROM  account a JOIN voucher v ON v.account_code = a.account_code WHERE v.credit IS NULL AND v.voucher_number =?1 ) ,\"NOT FOUND\")", nativeQuery = true)
    String nameForCreditNull(String voucherNumber);

    @Query(value = "SELECT IFNULL ( (SELECT a.title FROM  account a JOIN voucher v ON v.account_code = a.account_code WHERE v.debit IS NULL AND v.voucher_number =?1 ) ,\"NOT FOUND\")", nativeQuery = true)
    String nameForDebitNull(String voucherNumber);

    List<Voucher> findByChassisIdIn(List<Integer> chassisIdList);

    List<Voucher> findByChassisId(int chassisId);

    List<Voucher> findByChassisNo(String chassisNumber);

    // Get Paid Amount for Due amount calculation
    @Query(value = "SELECT coalesce (sum(v.debit),0) FROM  voucher v where v.voucher_number like 'BR%' and v.chassis_no =?1 ", nativeQuery = true)
    double getPaidAmount(String apartmentNumber);

    @Query(value = "SELECT * FROM  voucher v where v.account_code =?2 and v.chassis_no =?1 and  v.voucher_number like 'BR%' ", nativeQuery = true)
    List<Voucher> getPaymentDueVoucher(String apartmentNumber, int accountCode);

    List<Voucher> findByProjectCodeAndAccountCodeInOrderByVoucherDateAsc(Integer projectCode, List<Integer> accountList);
}
