package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.BankDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.sql.Date;
import java.util.List;


public interface AccountRepository extends PagingAndSortingRepository<Account, Long>, QueryByExampleExecutor<Account> {

    @Query("select a from Account a where  a.parentAccount is null ")
    List<Account> findControlAccounts();

    @Query("select a.title from Account a where a.accountCode =:accountCode")
    String getAccountTitle(@Param("accountCode") int accountCode);

    @Query("select coalesce(max(a.accountCode), 0) from Account a where  a.parentAccount is null ")
    int findMaxControlAccount();

    @Query("select coalesce(max(a.accountCode), 0) from Account a where  a.parentAccount =:parent ")
    int findMaxInnerAccount(@Param("parent") Account parentCode);

    @Query("select coalesce(max(a.accountCode), 0) from Account a where  SUBSTRING(a.accountCode,1,6) = SUBSTRING(:parent,1,6) ")
    int findMaxForthInnerAccount(@Param("parent") Account parentCode);

    @Query(value = "select * from account a where a.parent_code = ?1",nativeQuery = true)
    List<Account> findAccountsByParentCode(int parentCode);

    List<Account> findByIsActive(int isActive);

   /* Account save(Account account);*/

    Account findByAccountCode(int accountCode);

    List<Account> findByBankDetailsListNotNull();

    List<Account> findByStockListNotNull();

    List<Account> findByStockCustomerListNotNull();

    List<Account> findByStockVendorListNotNull();

    List<Account> findDistinctByCostCentresNotNull();

    //List<Account> findByBankDetails(BankDetails bankDetails);

    @Query("select a from Account a order by a.title ASC")
    List<Account> findAllAccount();
    List<Account> findDistinctByTaxDetailListNotNull();

    @Query("select a from Account a where level = :level order by a.title ASC")
    List<Account> findAccountByLevelAlphabetically(@Param("level") int level);

    List<Account> findAccountByLevel(int level);

    List<Account> findAccountByLevelNot(int level);

    @Query("select a from Account a where a.accountCode in :accountCodeList ")
    List<Account> getAccountNames(@Param("accountCodeList") List<Integer> accountCodeList);




    @Modifying
    @Transactional
    @Query("DELETE FROM Account a where a.accountCode = :accountCode")
    void deleteAccount(@Param("accountCode") Integer accountCode);



    // ACCOUNT MAPS FOR OPTIMIZATION

    // Account Map normal Case
    @Query(value = "select a.account_code, a.title, COALESCE (p.title,'NA') from account a left OUTER join account p on a.parent_code = p.account_code order by a.title ASC", nativeQuery = true)
    List<Object[]> getAccountMap();

    // Account Map by Level
    @Query(value = "select a.account_code, a.title, COALESCE (p.title,'NA') from account a left OUTER join account p on a.parent_code = p.account_code where a.level=?1 order by a.title ASC", nativeQuery = true)
    List<Object[]> getAccountMapByLevel(int level);


    // Account Map For distributor Customers
    @Query(value = "select a.account_code, a.titlefrom account a join account_customer sc on sc.account_code = a.account_code where sc.distributor_id=?1", nativeQuery = true)
    List<Object[]> getDistributorCustomerMap(int distributorId);

    // Distributor Account Map
    @Query(value = "select a.account_code, a.title from account a join distributor d on d.account_code = a.account_code", nativeQuery = true)
    List<Object[]> getDistributorMap();

    // Vendor Account Map
    @Query(value = "select a.account_code, a.title from account a join account_vendor d on d.account_code = a.account_code", nativeQuery = true)
    List<Object[]> getVendorMap();

    // Stock Account Map
    @Query(value = "select a.account_code, a.title,s.selling_price from account a join account_stock s on s.account_code = a.account_code order by a.title ASC" , nativeQuery = true)
    List<Object[]> getStockMap();

    // Stock Account Map
    @Query(value = "select a.account_code, a.title, a.opening_debit, a.opening_credit from account a join account_bank b on b.account_code = a.account_code order by a.title ASC" , nativeQuery = true)
    List<Object[]> getBankMap();

    // Customer Account Map
    @Query(value = "select a.account_code, a.title from account a join account_customer sc on sc.account_code = a.account_code order by a.title ASC", nativeQuery = true)
    List<Object[]> getCustomerMap();

    // Account Title For MAP
    @Query(value = "select a.account_code, a.title from account a where a.account_code in ?1 ", nativeQuery = true)
    List<Object[]> getTitleList(List<Integer> accountCodeList);

    // Tax Account Map
    @Query(value = "select a.account_code, a.title from account a join account_tax t on t.account_code = a.account_code where t.effective_till >=?1  order by a.title ASC", nativeQuery = true)
    List<Object[]> getTaxMap(Date date);

    @Query(value = "select * from account a join voucher v on a.account_code = v.account_code where v.project_code = ?1 group by a.account_code", nativeQuery = true)
    List<Account> getProjectAccounts(int project_code);

}
