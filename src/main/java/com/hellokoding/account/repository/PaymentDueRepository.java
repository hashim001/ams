package com.hellokoding.account.repository;

import com.hellokoding.account.model.PaymentDue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

public interface PaymentDueRepository extends JpaRepository<PaymentDue, Long> {


    @Query("select max(pd.voucherNumber) from PaymentDue pd where pd.voucherNumber like :prefixString")
    String findMaxVoucherNumber(@Param("prefixString") String prefixString);

    @Query("select coalesce( sum(pd.debit),0) from PaymentDue pd where pd.apartmentNo =:apartmentNumber and pd.voucherDate <=:tillDate and pd.voucherNumber not like 'LPT%'")
    double getSumOfDueAmount(@Param("apartmentNumber") String apartmentNumber,@Param("tillDate") Date tillDate);

    @Query("select coalesce( sum(pd.debit),0) from PaymentDue pd where pd.apartmentNo =:apartmentNumber and pd.voucherNumber like 'DP%'")
    double getSumOfDownPayment(@Param("apartmentNumber") String apartmentNumber);

    @Query("select coalesce( sum(pd.debit),0) from PaymentDue pd where pd.apartmentNo =:apartmentNumber and pd.dueDate <:tillDate and pd.voucherNumber not like 'LPT%'")
    double getSumOfOutStandingAmount(@Param("apartmentNumber") String apartmentNumber,@Param("tillDate") Date tillDate);

    @Modifying
    @Transactional
    @Query("DELETE FROM PaymentDue pd WHERE pd.apartmentNo = :apartmentNumber ")
    void removePaymentDue(@Param("apartmentNumber") String apartmentNumber);

    @Query("select pd from PaymentDue pd where pd.voucherDate >= :fromDate and pd.voucherDate <= :toDate and pd.accountCode=:accountCode and pd.apartmentNo=:apartmentNumber")
    List<PaymentDue> findByApartmentNoAndAccountCode(@Param("apartmentNumber") String apartmentNumber,@Param("accountCode") int accountCode,@Param("fromDate") Date fromDate,@Param("toDate") Date toDate);

    @Query("select pd from PaymentDue pd where pd.voucherDate >= :fromDate and pd.voucherDate <= :toDate and pd.accountCode=:accountCode and pd.apartmentNo=:apartmentNumber and pd.voucherNumber like 'LPT%'")
    List<PaymentDue> findByApartmentNoAndAccountCodeForLate(@Param("apartmentNumber") String apartmentNumber,@Param("accountCode") int accountCode,@Param("fromDate") Date fromDate,@Param("toDate") Date toDate);

}
