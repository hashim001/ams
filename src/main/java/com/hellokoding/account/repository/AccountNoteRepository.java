package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.AccountNote;
import com.hellokoding.account.model.ReportStaging;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountNoteRepository extends JpaRepository<AccountNote ,Long> {


    AccountNote findByNoteId(int noteId);
    List<AccountNote> findByStatus(Integer status);
    List<AccountNote> findByStatusAndReportStaging(Integer status, ReportStaging reportStaging);
    List<AccountNote> findByReportTypeAndStatus(String reportType,Integer status);

}
