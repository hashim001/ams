package com.hellokoding.account.repository;

import com.hellokoding.account.model.Maker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MakerRepository extends JpaRepository<Maker, Long> {
    Maker findByMakerId(int makerId);
}
