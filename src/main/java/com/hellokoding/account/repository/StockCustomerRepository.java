package com.hellokoding.account.repository;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Distributor;
import com.hellokoding.account.model.StockCustomer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StockCustomerRepository extends JpaRepository<StockCustomer, Long> {

    @Modifying
    @Transactional
    @Query("DELETE FROM StockCustomer s WHERE s.customerAccount = :account")
    void deleteByAccountCode(@Param("account") Account account);


    StockCustomer findByCustomerId(int id);

    @Query("SELECT s FROM StockCustomer s join s.customerAccount a WHERE a.title LIKE :customerName")
    Page<StockCustomer> findAllStockCustomer(@Param("customerName") String customerName, Pageable pageable);


    StockCustomer findByCnic(String cnic);

    StockCustomer findByMobile(String mobile);

    // Account CNIC For MAP
    @Query(value = "select  a.account_code, a.cnic  from account_customer a where a.account_code in ?1 ", nativeQuery = true)
    List<Object[]> getCnicList(List<Integer> accountCodeList);
}
