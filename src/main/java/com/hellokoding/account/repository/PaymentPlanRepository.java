package com.hellokoding.account.repository;

import com.hellokoding.account.model.PaymentPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface PaymentPlanRepository extends JpaRepository<PaymentPlan,Long> {

    PaymentPlan findByPlanId(int planId);

    @Modifying
    @Transactional
    @Query("DELETE FROM PaymentPlan pp WHERE pp.planId = :planId")
    void remove(@Param("planId") int planId);
}
