package com.hellokoding.account.reportModel;

import com.hellokoding.account.model.NoteSubHeading;

import java.util.ArrayList;
import java.util.List;

public class SubHeadingSet {


    private Integer accountNoteId;
    private List<NoteSubHeading> subHeadingList;

    public Integer getAccountNoteId() {
        return accountNoteId;
    }

    public void setAccountNoteId(Integer accountNoteId) {
        this.accountNoteId = accountNoteId;
    }

    public List<NoteSubHeading> getSubHeadingList() {
        return subHeadingList;
    }

    public void setSubHeadingList(List<NoteSubHeading> subHeadingList) {
        this.subHeadingList = subHeadingList;
    }


    public SubHeadingSet() {
        this.subHeadingList = new ArrayList<NoteSubHeading>();
    }



    public void add(NoteSubHeading function) {
        this.subHeadingList.add(function);
    }
}
