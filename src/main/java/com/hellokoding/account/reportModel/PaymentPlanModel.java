package com.hellokoding.account.reportModel;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Apartment;
import com.hellokoding.account.model.Stock;

import java.sql.Date;
import java.util.List;

/**
 * Model to View Payment Plan for Apartment or Stock (Model)
 */

public class PaymentPlanModel implements Comparable<PaymentPlanModel>{

    private String model;// stock Name
    private Account customer;
    private Apartment apartment; // in case of Apartment
    private Stock stock; // in case of stock
    private String imageUrl;
    private String installmentMessage;
    private double installmentTotal;
    private Date installmentStartDate;


    private List<PlanInternal> downPaymentList;// Down Payment Rows
    private List<PlanInternal> installmentList; // Installment Rows

    public PaymentPlanModel(){

    }

    public Apartment getApartment() {
        return apartment;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Account getCustomer() {
        return customer;
    }

    public void setCustomer(Account customer) {
        this.customer = customer;
    }


    public List<PlanInternal> getDownPaymentList() {
        return downPaymentList;
    }

    public void setDownPaymentList(List<PlanInternal> downPaymentList) {
        this.downPaymentList = downPaymentList;
    }

    public List<PlanInternal> getInstallmentList() {
        return installmentList;
    }

    public void setInstallmentList(List<PlanInternal> installmentList) {
        this.installmentList = installmentList;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getInstallmentMessage() {
        return installmentMessage;
    }

    public void setInstallmentMessage(String installmentMessage) {
        this.installmentMessage = installmentMessage;
    }

    public double getInstallmentTotal() {
        return installmentTotal;
    }

    public void setInstallmentTotal(double installmentTotal) {
        this.installmentTotal = installmentTotal;
    }

    public Date getInstallmentStartDate() {
        return installmentStartDate;
    }

    public void setInstallmentStartDate(Date installmentStartDate) {
        this.installmentStartDate = installmentStartDate;
    }

    @Override
    public int compareTo(PaymentPlanModel paymentPlanModel) {
        double compareOrder = paymentPlanModel.getApartment().getSellingPrice();
        return (int)(this.getApartment().getSellingPrice() - compareOrder);

    }
}
