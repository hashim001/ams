package com.hellokoding.account.reportModel;

import java.sql.Date;

/**
 * Defines an internal Row of Payment Plan view Model (PaymentPlanModel)
 */

public class PlanInternal {

    private String title;
    private String message;
    private Date dueDate;
    private Double amount;

    public PlanInternal(String title, String message, Date dueDate, Double amount) {
        this.title = title;
        this.message = message;
        this.dueDate = dueDate;
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
