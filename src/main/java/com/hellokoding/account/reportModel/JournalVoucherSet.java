package com.hellokoding.account.reportModel;

import com.hellokoding.account.model.VoucherList;


import java.util.ArrayList;
import java.util.List;

public class JournalVoucherSet {

    public JournalVoucherSet(){

        this.voucherLists = new ArrayList<VoucherList>();
    }


    public List<VoucherList> getVoucherLists() {
        return voucherLists;
    }

    public void setVoucherLists(List<VoucherList> voucherLists) {
        this.voucherLists = voucherLists;
    }

    private List <VoucherList> voucherLists;


    public void add(VoucherList voucherList){
        this.voucherLists.add(voucherList);
    }

}
