package com.hellokoding.account.constants;



public interface Constants {


    String ACCOUNT_TYPE_DEBIT = "debit";
    String ACCOUNT_TYPE_CREDIT = "credit";
    String PREFIX_FOR_PAYMENT = "PV";
    String PREFIX_FOR_RECEIPT = "RV";
    String PREFIX_FOR_JOURNAL = "JV";
    String PREFIX_FOR_SALE = "SL";
    String PREFIX_FOR_PURCHASE = "LP";
    String ORDER_TYPE_PURCHASE = "Purchase";
    String ORDER_TYPE_SALE = "Sale";
    String PREFIX_FOR_BILL = "EVO";
    String PREFIX_FOR_DC = "EVO-DC";
    String PREFIX_FOR_GRN = "EVO-PO";
    String NOTE_OPTION_DEBIT = "Debit";
    String NOTE_OPTION_CREDIT = "Credit";
    String NOTE_OPTION_NET = "Net";
    String COMMISSION_STATUS_INACTIVE = "INACTIVE"; // status on apartment sale
    String COMMISSION_STATUS_UNPAID = "UNPAID"; // status when eligible to pay
    String COMMISSION_STATUS_PAID = "PAID"; // status when commission amount paid
    String SALARY_REFERENCE_PREFIX = "SAL-";
    Integer CASH_ACCOUNT = 1104010001;
    Integer SITE_PARENT_CODE = 1102000000;
    Integer MAX_FORM_ROWS = 20;

    // Pay Roll Constants
    int EOBI_DEDUCTION_VALUE = 130;
    int ALLOWED_LEAVES_PER_ANNUM = 18;
    int MONTHLY_LEAVES_BOUND = 4;
    int NUMBER_OF_SLABS = 12;
    Double LEAVES_PER_MONTH = 1.5;
    String SALARY_FOLDER_PATH = "SalaryScripts/";
    String SALARY_TEMPLATE_PATH = "ReportTemplates/MyTemplate.xlsx";
    String SALARY_LETTER_PATH = "ReportTemplates/TransferTemplate.xlsx";
    String TAX_LETTER_PATH = "ReportTemplates/TaxDeduction.xlsx";






}
