package com.hellokoding.account.web;


import com.hellokoding.account.model.Department;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.department.DepartmentService;
import com.hellokoding.account.service.location.LocationService;
import com.hellokoding.account.service.user.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private AccountService accountService;

    /**
     * GET for Listing of Departments and Form
     *
     * @param model Data Model
     * @return Response Page with listing and new Dept form.
     */
    @RequestMapping(value = "/Department/addDepartment", method = RequestMethod.GET)
    public String addDept(Model model) {
        model.addAttribute("deptForm", new Department());
        model.addAttribute("locations",locationService.findAll());
        model.addAttribute("departments", departmentService.findAll());
        model.addAttribute("gmList" , userAuthService.findAllGM());
        model.addAttribute("accounts", accountService.getAccountListByLevel(4));
        return "department/addDept";
    }


    /**
     * POST for Department Insertion
     * Update and New Insertion
     * @param department Form
     * @param attributes for message
     * @return Response Page
     */
    @RequestMapping(value = "/Department/addDepartment", method = RequestMethod.POST)
    public String addDept_post(@ModelAttribute("deptForm") Department department, RedirectAttributes attributes) {

        departmentService.save(department);
        attributes.addFlashAttribute("deptAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Department <strong>" + department.getName() + " </strong> added successfully.</div>");
        return "redirect:/Department/addDepartment";
    }


    /**
     * Remove Department by Id
     *
     * @param deptId     department Identity
     * @param attributes Redirect Message
     * @return Redirect to GET
     */
    @RequestMapping(value = "/Department/removeDept", method = RequestMethod.POST)
    public String removeDept_post(@RequestParam("deptId") int deptId, RedirectAttributes attributes) {

        departmentService.remove(deptId);
        attributes.addFlashAttribute("deptRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Department with id <strong>" + deptId + " </strong> removed successfully.</div>");
        return "redirect:/Department/addDepartment";
    }

    /**
     * Ajax Call to Edit Department by Id
     *
     * @param deptId target Department Identity
     * @return
     */

    @RequestMapping(value = "/Department/editDept", method = RequestMethod.GET)
    public @ResponseBody
    Department editDepart(@RequestParam("deptId") int deptId) {
        Department department = departmentService.findByDeptId(deptId);
        department.setSubDepartmentList(null);
        return department;
    }
}
