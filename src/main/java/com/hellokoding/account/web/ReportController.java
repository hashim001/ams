package com.hellokoding.account.web;

import com.hellokoding.account.model.*;
import com.hellokoding.account.reportModel.JournalVoucherSet;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.security.RoleSupport;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.service.voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.CostCentreService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.ledger.TrialBalanceService;
import com.hellokoding.account.service.note.NoteCalculationService;
import com.hellokoding.account.service.note.NoteService;
import com.hellokoding.account.service.statement.BalanceCalculationService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class ReportController {


    @Autowired
    private AccountService accountService;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private CostCentreService costCentreService;

    @Autowired
    private LedgerCalculationService ledgerCalculationService;

    @Autowired
    private TrialBalanceService trialBalanceService;

    @Autowired
    private NoteCalculationService noteCalculationService;

    @Autowired
    private BalanceCalculationService balanceCalculationService;

    @Autowired
    private NoteService noteService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private StockService stockService;

    @Autowired
    private DistributorService distributorService;

    @Autowired
    private ChassisService chassisService;

    //////////////////////// Cost Centre Report /////////////////////////////

    @RequestMapping(value = "/Reports/costCentres", method = RequestMethod.GET)
    public String costCentreReport(Model model) {

        model.addAttribute("accounts", accountService.findWithCostCentres());
        model.addAttribute("allAccounts", accountService.findWithCostCentres());

        return "reports/costCentreReport";
    }


    @RequestMapping(value = "/Reports/costCentres", method = RequestMethod.POST)
    public String costCentreReport_post(Model model, @RequestParam("account") Integer accountCode) {

        List<Account> accountList = new ArrayList<>();
        if (accountCode == null) {
            accountList = accountService.findWithCostCentres();

        } else {
            accountList.add(accountService.findAccount(accountCode));
        }
        model.addAttribute("accounts", accountList);
        model.addAttribute("allAccounts", accountService.findWithCostCentres());
        model.addAttribute("postAccount", accountCode);


        return "reports/costCentreReport";
    }

    //////////////////////// Payment voucher Report /////////////////////////////


    @RequestMapping(value = "/Reports/paymentVoucher", method = RequestMethod.GET)
    public String paymentVoucherReport(Model model) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("BP%"));
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("vouchers", voucherService.findByPrefix(date, date, "BP%"));


        return "reports/paymentVoucherReport";
    }

    @RequestMapping(value = "/Reports/paymentVoucher", method = RequestMethod.POST)
    public String paymentVoucherReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("voucherNumberFrom") String fromVoucher
            , @RequestParam("voucherNumberTo") String toVoucher) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("fromVoucher", fromVoucher);
        model.addAttribute("toVoucher", toVoucher);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("BP%"));
        if (fromVoucher.equals("none") || toVoucher.equals("none")) {
            model.addAttribute("vouchers", voucherService.findByPrefix(fromDate, toDate, "BP%"));
        } else {
            model.addAttribute("vouchers", voucherService.findByPrefixAndVoucher(fromVoucher, toVoucher, "BP%"));
        }


        return "reports/paymentVoucherReport";
    }

/////////////////////////////////// Receipt voucher Reports//////////////////////////////

    @RequestMapping(value = "/Reports/receiptVoucher", method = RequestMethod.GET)
    public String receiptVoucherReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("BR%"));
        model.addAttribute("vouchers", voucherService.findByPrefix(date, date, "BR%"));


        return "reports/receiptVoucherReport";
    }

    @RequestMapping(value = "/Reports/receiptVoucher", method = RequestMethod.POST)
    public String receiptVoucherReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("voucherNumberFrom") String fromVoucher
            , @RequestParam("voucherNumberTo") String toVoucher) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("fromVoucher", fromVoucher);
        model.addAttribute("toVoucher", toVoucher);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("BR%"));
        if (fromVoucher.equals("none") || toVoucher.equals("none")) {
            model.addAttribute("vouchers", voucherService.findByPrefix(fromDate, toDate, "BR%"));
        } else {
            model.addAttribute("vouchers", voucherService.findByPrefixAndVoucher(fromVoucher, toVoucher, "BR%"));
        }


        return "reports/receiptVoucherReport";
    }


    /////////////////////////////////// Journal voucher Reports//////////////////////////////

    @RequestMapping(value = "/Reports/journalVoucher", method = RequestMethod.GET)
    public String journalVoucherReport(Model model) {
        JournalVoucherSet journalVoucherSet = new JournalVoucherSet();
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("JV%"));
        for (String voucherNumber : voucherService.findJournalByPrefix(date, date, "JV%")) {
            VoucherList voucherList = new VoucherList();
            voucherList.setVoucherList(voucherService.findByVoucherNumber(voucherNumber));
            Map accountMap = new HashMap();
            for (Account account : accountService.findAll()) {
                accountMap.put(account.getAccountCode(), account.getTitle());
            }
            model.addAttribute("accountMap", accountMap);
            journalVoucherSet.add(voucherList);
        }

        model.addAttribute("journalVoucherSet", journalVoucherSet);
        return "reports/journalVoucherReport";
    }

    @RequestMapping(value = "/Reports/journalVoucher", method = RequestMethod.POST)
    public String journalVoucherReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("voucherNumberFrom") String fromVoucher
            , @RequestParam("voucherNumberTo") String toVoucher) {

        JournalVoucherSet journalVoucherSet = new JournalVoucherSet();
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("fromVoucher", fromVoucher);
        model.addAttribute("toVoucher", toVoucher);
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("JV%"));

        if (fromVoucher.equals("none") || toVoucher.equals("none")) {
            for (String voucherNumber : voucherService.findJournalByPrefix(fromDate, toDate, "JV%")) {

                VoucherList voucherList = new VoucherList();
                voucherList.setVoucherList(voucherService.findByVoucherNumber(voucherNumber));
                Map accountMap = new HashMap();
                for (Account account : accountService.findAll()) {
                    accountMap.put(account.getAccountCode(), account.getTitle());
                }
                model.addAttribute("accountMap", accountMap);
                journalVoucherSet.add(voucherList);
            }
        } else {
            for (String voucherNumber : voucherService.findJournalVouchersByPrefixAndNumber(fromVoucher, toVoucher, "JV%")) {

                VoucherList voucherList = new VoucherList();
                voucherList.setVoucherList(voucherService.findByVoucherNumber(voucherNumber));
                Map accountMap = new HashMap();
                for (Account account : accountService.findAll()) {
                    accountMap.put(account.getAccountCode(), account.getTitle());
                }
                model.addAttribute("accountMap", accountMap);
                journalVoucherSet.add(voucherList);
            }
        }

        model.addAttribute("journalVoucherSet", journalVoucherSet);
        return "reports/journalVoucherReport";
    }


    /////////////////////////////////// Chart Of Account Reports//////////////////////////////
    @RequestMapping(value = "/Reports/chartOfAccount", method = RequestMethod.GET)
    public String chartOfAccount(Model model) {

        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        model.addAttribute("baseAccounts", accountService.findAccountsByLevel(1));
        model.addAttribute("levels", levelList);
        model.addAttribute("currentLevel", 1);
        model.addAttribute("currentLevelOption", 0);


        return "reports/chartOfAccountReport";
    }

    @RequestMapping(value = "/Reports/chartOfAccount", method = RequestMethod.POST)
    public String chartOfAccount_post(Model model, @RequestParam("level") Integer level, @RequestParam("levelOption") Integer levelOption) {

        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        model.addAttribute("baseAccounts", accountService.findAccountsByLevel(level));
        model.addAttribute("levels", levelList);
        model.addAttribute("currentLevel", level);
        model.addAttribute("currentLevelOption", levelOption);

        if (levelOption == 0) {
            return "reports/chartOfAccountReport";
        }
        return "reports/chartOfAccountOnlyLevel";
    }


    /////////////////////////////////// ledger Reports//////////////////////////////


    @RequestMapping(value = "/Reports/ledgerReport", method = RequestMethod.GET)
    public String ledgerReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("costCentres", null);


        return "reports/ledgerReport";
    }


    @RequestMapping(value = "/Reports/ledgerReport", method = RequestMethod.POST)
    public String ledgerReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode
            , @RequestParam("costCentreCode") Long costCentreCode) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);
        model.addAttribute("currentCostCentre", costCentreCode);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAccountsByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        if (accountCode == null) {
            model.addAttribute("vouchers", null);
            model.addAttribute("costCentres", null);
        } else {
            model.addAttribute("vouchers", ledgerCalculationService.getSortedVouchers(accountCode, costCentreCode, fromDate, toDate, false, false, false, false, null));
            model.addAttribute("costCentres", costCentreService.getCostCentres(accountCode));
        }

        return "reports/ledgerReport";
    }

    /////////////////////////////////// Bank ledger Reports //////////////////////////////

    @RequestMapping(value = "/Reports/ledgerReportBank", method = RequestMethod.GET)
    public String ledgerReportBank(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.getDetailAccounts());


        return "reports/ledgerReportBank";
    }


    @RequestMapping(value = "/Reports/ledgerReportBank", method = RequestMethod.POST)
    public String ledgerReportBank_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode
    ) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("accounts", accountService.getDetailAccounts());
        if (accountCode == null) {
            model.addAttribute("vouchers", null);

        } else {
            model.addAttribute("vouchers", ledgerCalculationService.getSortedVouchers(accountCode, null, fromDate, toDate, true, false, false, false, null));

        }


        return "reports/ledgerReportBank";
    }


    /////////////////////////////////// trial Balance Basic Report//////////////////////////////

    @RequestMapping(value = "/Reports/trialBalanceBasic", method = RequestMethod.GET)
    public String TrailBalanceBasicReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("levels", levelList);
        model.addAttribute("trailModel", trialBalanceService.getTrailBalanceBasic(1, date, null, null));
        return "reports/trialBasic";
    }

    @RequestMapping(value = "/Reports/trialBalanceBasic", method = RequestMethod.POST)
    public String TrailBalanceBasicReport_post(Model model
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("level") int level) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("levels", levelList);
        model.addAttribute("trailModel", trialBalanceService.getTrailBalanceBasic(level, toDate, null, null));
        model.addAttribute("currentLevel", level);
        return "reports/trialBasic";
    }

    /////////////////////////////////// trail Balance Detailed Report//////////////////////////////

    @RequestMapping(value = "/Reports/trialBalanceDetail", method = RequestMethod.GET)
    public String TrailBalanceDetailReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("levels", levelList);
        model.addAttribute("trailModel", trialBalanceService.getTrailBalanceDetailed(1, date, date));
        return "reports/trialDetailed";
    }

    @RequestMapping(value = "/Reports/trialBalanceDetail", method = RequestMethod.POST)
    public String TrailBalanceDetailReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("level") int level) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<Integer> levelList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("levels", levelList);
        model.addAttribute("currentLevel", level);
        model.addAttribute("trailModel", trialBalanceService.getTrailBalanceDetailed(level, fromDate, toDate));
        return "reports/trialDetailed";
    }


    /////////////////////////////////// Note Summary Report//////////////////////////////


    @RequestMapping(value = "/Reports/noteSummary", method = RequestMethod.GET)
    public String noteSummary(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<AccountNote> accountNotes = noteCalculationService.getNoteSummary("ProfitAndLoss", date, noteService.findByNoteType("ProfitAndLoss", 1), false);
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("toDate", date);
        model.addAttribute("accountNotes", accountNotes);
        model.addAttribute("reportType", "ProfitAndLoss");
        return "reports/noteSummaryReport";
    }

    @RequestMapping(value = "/Reports/noteSummary", method = RequestMethod.POST)
    public String noteSummary_post(Model model
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("reportType") String reportType) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        List<AccountNote> accountNotes = noteCalculationService.getNoteSummary(reportType, toDate, noteService.findByNoteType(reportType, 1), false);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("accountNotes", accountNotes);
        model.addAttribute("reportType", reportType);
        return "reports/noteSummaryReport";
    }

    ////////////////////////////////////////////////////Balance / PL Sheet //////////////////////////////


    @RequestMapping(value = "/Reports/sheetReport", method = RequestMethod.GET)
    public String sheetReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("toDate", date);
        model.addAttribute("reportType", "ProfitAndLoss");
        model.addAttribute("reportSheet", balanceCalculationService.getReportSheet("ProfitAndLoss", date));

        return "reports/balanceSheetReport";
    }

    @RequestMapping(value = "/Reports/sheetReport", method = RequestMethod.POST)
    public String sheetReport_post(Model model
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("reportType") String reportType) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("reportType", reportType);
        model.addAttribute("reportSheet", balanceCalculationService.getReportSheet(reportType, toDate));

        return "reports/balanceSheetReport";
    }

    /**
     * Get request Product ledger of Stock
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/productLedger", method = RequestMethod.GET)
    public String productLedger(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.getStockAccounts());
        model.addAttribute("costCentres", null);

        return "reports/productLedger";
    }

    /**
     * Product ledger of Stock
     *
     * @param model
     * @param fromDate       from date
     * @param toDate         to date
     * @param accountCode    code of specific stock
     * @param costCentreCode cost centre code
     * @return
     */
    @RequestMapping(value = "/Reports/productLedger", method = RequestMethod.POST)
    public String productLedger_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode
            , @RequestParam("costCentreCode") Long costCentreCode) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);
        model.addAttribute("currentCostCentre", costCentreCode);
        Map accountMap = new HashMap();
        Map costCentreMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        for (CostCentre costCentre : costCentreService.findAll()) {
            costCentreMap.put(costCentre.getAccountCode(), costCentre.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("costCentreMap", costCentreMap);
        model.addAttribute("accounts", accountService.getStockAccounts());
        if (accountCode == null) {
            model.addAttribute("vouchers", null);
            model.addAttribute("costCentres", null);
        } else {
            model.addAttribute("vouchers", ledgerCalculationService.getSortedVouchers(accountCode, costCentreCode, fromDate, toDate, false, true, false, false, null));
            model.addAttribute("costCentres", costCentreService.getCostCentres(accountCode));
        }

        return "reports/productLedger";
    }

    /**
     * Report for sale trading
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/saleRegister", method = RequestMethod.GET)
    public String saleRegister(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.getCustomerAccounts());
        return "reports/saleRegister";
    }

    @RequestMapping(value = "/Reports/saleRegister", method = RequestMethod.POST)
    public String saleRegister_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);
        model.addAttribute("accounts", accountService.getCustomerAccounts());
        Map accountMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        //model.addAttribute("purchaseOrderModel", registerService.getSaleTradingListing(accountCode, fromDate, toDate, false));
        model.addAttribute("accountMap", accountMap);

        return "reports/saleRegister";

    }

    /**
     * Report for purchase trading
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/purchaseRegister", method = RequestMethod.GET)
    public String purchaseRegister(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.getVendorAccounts());
        return "reports/purchaseRegister";
    }

    @RequestMapping(value = "/Reports/purchaseRegister", method = RequestMethod.POST)
    public String purchaseRegister_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);
        model.addAttribute("accounts", accountService.getVendorAccounts());
        Map accountMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        //model.addAttribute("purchaseOrderModel", registerService.getSaleTradingListing(accountCode, fromDate, toDate, true));
        model.addAttribute("accountMap", accountMap);

        return "reports/purchaseRegister";

    }

    /**
     * Report for stock transfer
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Salesman/salesmanReport", method = RequestMethod.GET)
    public String salesmanReport(Model model) {
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        List<Distributor> salesmanList = new ArrayList<>();
        List<Distributor> distributorList = new ArrayList<>();
        if (RoleSupport.hasRole("ROLE_SALESMAN")) {
            distributorList.add(RoleSupport.getUserAuth().getDistributor());
        } else {
            distributorList = distributorService.findAll();
        }
        model.addAttribute("salesmanList", distributorList);
        List<Apartment> apartmentList;
        LocalDate localDate = LocalDate.now();
        for (Distributor distributor : distributorList) {
            apartmentList = chassisService.findByDistributorId(distributor.getDistributorId());
            for (Apartment apartment : apartmentList) {
                double amountPaid = 0;
                double commissionAmount = 0;
                for (Voucher voucher : voucherService.findByChassisId(apartment.getApartmentId())) {
                    if (voucher.getAccountCode() == apartment.getCustomerAccount() && voucher.getCredit() > 0) {
                        if (voucher.getVoucherNumber().contains("BR") || voucher.getVoucherNumber().contains("JV")) {
                            amountPaid += voucher.getCredit();
                        } else if ((voucher.getVoucherNumber().contains("BP") || voucher.getVoucherNumber().contains("JV")) && voucher.getDebit() > 0) {
                            amountPaid -= voucher.getDebit();
                        }
                    }
                    if(voucher.getVoucherNumber().contains("SL") && voucher.getSalesmanId() > 0){
                        commissionAmount = voucher.getCommissionAmount();
                    }
                }
                apartment.setCommissionAmount(commissionAmount);
                apartment.setAmountPaid(amountPaid);
                apartment.setDays(Duration.between(apartment.getDate().toLocalDate().atStartOfDay(), localDate.atStartOfDay()).toDays());
            }
            distributor.setApartmentList(apartmentList);
            salesmanList.add(distributor);
        }
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountList(0)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("distributors", salesmanList);
        return "reports/salesmanReport";
    }

    @RequestMapping(value = "/Salesman/salesmanReport", method = RequestMethod.POST)
    public String salesmanReport_post(Model model, @RequestParam("salesmanId") Integer salesmanId) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountList(0)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        List<Distributor> salesmanList = new ArrayList<>();
        List<Distributor> distributorList = new ArrayList<>();
        if (RoleSupport.hasRole("ROLE_SALESMAN")) {
            distributorList.add(RoleSupport.getUserAuth().getDistributor());
        } else {
            distributorList = distributorService.findAll();
        }
        model.addAttribute("currentSalesman", salesmanId);
        model.addAttribute("salesmanList", distributorList);
        List<Apartment> apartmentList;
        LocalDate localDate = LocalDate.now();
        if (salesmanId == null) {
            for (Distributor distributor : distributorList) {
                apartmentList = chassisService.findByDistributorId(distributor.getDistributorId());
                for (Apartment apartment : apartmentList) {
                    double amountPaid = 0;
                    for (Voucher voucher : voucherService.findByChassisNumber(apartment.getApartmentNo())) {
                        if (voucher.getAccountCode() == apartment.getCustomerAccount() && voucher.getCredit() > 0) {
                            if (voucher.getVoucherNumber().contains("BR")) {
                                amountPaid += voucher.getCredit();
                            } else if (voucher.getVoucherNumber().contains("BP") && voucher.getDebit() > 0) {
                                amountPaid -= voucher.getDebit();
                            }
                        }
                    }
                    apartment.setAmountPaid(amountPaid);
                    apartment.setDays(Duration.between(apartment.getDate().toLocalDate().atStartOfDay(), localDate.atStartOfDay()).toDays());
                }
                distributor.setApartmentList(apartmentList);
                salesmanList.add(distributor);
            }
        } else {
            Distributor distributor = distributorService.findByDistributorId(salesmanId);
            apartmentList = chassisService.findByDistributorId(salesmanId);
            for (Apartment apartment : apartmentList) {
                double amountPaid = 0;
                for (Voucher voucher : voucherService.findByChassisNumber(apartment.getApartmentNo())) {
                    if (voucher.getAccountCode() == apartment.getCustomerAccount() && voucher.getCredit() > 0) {
                        if (voucher.getVoucherNumber().contains("BR")) {
                            amountPaid += voucher.getCredit();
                        } else if (voucher.getVoucherNumber().contains("BP") && voucher.getDebit() > 0) {
                            amountPaid -= voucher.getDebit();
                        }
                    }
                }
                apartment.setAmountPaid(amountPaid);

                apartment.setDays(Duration.between(apartment.getDate().toLocalDate().atStartOfDay(), localDate.atStartOfDay()).toDays());
            }
            distributor.setApartmentList(apartmentList);
            salesmanList.add(distributor);
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("distributors", salesmanList);

        return "reports/salesmanReport";
    }

    /**
     * Get request for Current stock report
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/currentStockReport", method = RequestMethod.GET)
    public String currentStockReport(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("vouchers", ledgerCalculationService.currentAccountsReport(accountService.getStockAccounts(), true, false));
        return "reports/currentStockReport";
    }

    /**
     * Get request for Current customer report
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/currentCustomerReport", method = RequestMethod.GET)
    public String currentCustomerReport(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("vouchers", ledgerCalculationService.currentAccountsReport(accountService.getCustomerAccounts(), false, false));
        return "reports/currentCustomerReport";
    }

    /**
     * Get request for Current customer report
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/currentBankReport", method = RequestMethod.GET)
    public String currentBankReport(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("vouchers", ledgerCalculationService.currentAccountsReport(accountService.getBankAccountList(), false, true));
        return "reports/currentBankReport";
    }

    /**
     * Get request Apartment ledger of Stock
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/chassisLedger", method = RequestMethod.GET)
    public String chassisLedger(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("siteList", stockService.getStockCategories());
        model.addAttribute("chassisList", chassisService.findAll());

        return "reports/chassisLedger";
    }

    /**
     * Apartment ledger of Stock
     *
     * @param model
     * @param chassisId id of specific chassisNumber
     * @return
     */
    @RequestMapping(value = "/Reports/chassisLedger", method = RequestMethod.POST)
    public String chassisLedger_post(Model model, @RequestParam("chassisId") Integer chassisId,
                                     @RequestParam("siteId") Integer siteId) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("siteList", stockService.getStockCategories());
        model.addAttribute("currentDate", date);
        model.addAttribute("currentChassis", chassisId);
        model.addAttribute("currentSite", siteId);
        Map accountMap = new HashMap();
        List<Integer> plotList = new ArrayList<>();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("chassisList", chassisService.findAll());
        if(siteId == 0 && chassisId == 0){
            List<Stock> stockList = stockService.findAllStock();
            stockList.forEach(s->s.getApartmentList().forEach(apartment -> {
                plotList.add(apartment.getApartmentId());
            }));
            model.addAttribute("vouchers", ledgerCalculationService.getSortedVouchers(chassisId, null, null, null, false, false, false, true, plotList));
        } else if (siteId > 0 && chassisId == 0) {
            List<Stock> stockList = stockService.findByCategoryId(siteId).getStockList();
            stockList.forEach(s->s.getApartmentList().forEach(apartment -> {
                plotList.add(apartment.getApartmentId());
            }));
            model.addAttribute("vouchers", ledgerCalculationService.getSortedVouchers(chassisId, null, null, null, false, false, false, true, plotList));
        } else if(chassisId > 0) {
            plotList.add(chassisId);
            model.addAttribute("vouchers", ledgerCalculationService.getSortedVouchers(chassisId, null, null, null, false, false, false, true, plotList));
        }
        else{
            model.addAttribute("vouchers", null);
        }
        return "reports/chassisLedger";
    }

    /**
     * Get request for payment pending report for suzuki account
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/paymentPendingReport", method = RequestMethod.GET)
    public String paymentPendingReport(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        Map accountMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("chassisList", ledgerCalculationService.setPaymentPendingReport(chassisService.findAllByChassisDate(), date));
        return "reports/paymentPendingReport";
    }

    /**
     * current stock report
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Report/currentApartmentsReport", method = RequestMethod.GET)
    public String currentApartmentsReport(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        VoucherList voucherList = new VoucherList();
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountList(0)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        List<String> statusList = new ArrayList<>();
        statusList.add("Available");
        if (RoleSupport.hasRole("ROLE_MASTER_ADMIN")) {
            model.addAttribute("apartments", chassisService.findAll());
            statusList.add("Hold");
            statusList.add("Sold");
        } else {
            model.addAttribute("apartments", chassisService.findAllAvailableApartments());
        }
        model.addAttribute("statusList", statusList);
        model.addAttribute("stocks", stockService.findAllStock());
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("vouchers", voucherList);
        return "reports/currentApartmentsReport";
    }

    @RequestMapping(value = "/Report/currentApartmentsReport", method = RequestMethod.POST)
    public String currentApartmentsReport_post(Model model
            , @RequestParam("status") String status
            , @RequestParam("stockId") Integer stockId
    ) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountList(0)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        List<Stock> stockList = stockService.findAllStock();
        List<String> statusList = new ArrayList<>();
        statusList.add("Available");
        if (RoleSupport.hasRole("ROLE_MASTER_ADMIN")) {
            statusList.add("Hold");
            statusList.add("Sold");
        }
        model.addAttribute("statusList", statusList);
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("currentDate", date);
        model.addAttribute("stocks", stockList);
        model.addAttribute("currentStatus", status);
        model.addAttribute("currentStock", stockId);

        List<Apartment> apartmentFinalList = new ArrayList<>();
        List<Apartment> apartmentList;
        if (stockId == 0) {
            if (RoleSupport.hasRole("ROLE_MASTER_ADMIN")) {
                apartmentList = chassisService.findAll();
            } else {
                apartmentList = chassisService.findAllAvailableApartments();
            }
        } else {
            Stock stock = stockService.findByStockId(stockId);
            if (RoleSupport.hasRole("ROLE_MASTER_ADMIN")) {
                apartmentList = stock.getApartmentList();
            } else {
                apartmentList = chassisService.findAllAvailableApartmentsByStock(stock);
            }
        }
            for (Apartment apartment : apartmentList) {
                if (status.equals("Available")) {
                    apartmentFinalList = apartmentList;
                } else if (status.equals("Hold")) {
                    if (apartment.isHold()) {
                        apartmentFinalList.add(apartment);
                    }
                } else if (status.equals("Sold")) {
                    if (apartment.isSold()) {
                        apartmentFinalList.add(apartment);
                    }
                }
            }

        model.addAttribute("apartments", apartmentFinalList);

        return "reports/currentApartmentsReport";
    }

    /**
     * Get request for hold apartment report
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Report/holdApartmentReport", method = RequestMethod.GET)
    public String holdApartmentReport(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        if (RoleSupport.hasRole("ROLE_MASTER_ADMIN")) {
            model.addAttribute("holdApartmentList", ledgerCalculationService.setholdapartmentreport(chassisService.findAllHoldApartments(), date));
        } else {
            model.addAttribute("holdApartmentList", ledgerCalculationService.setholdapartmentreport(chassisService.findHoldApartmentBySalesmanId(RoleSupport.getUserAuth().getDistributor().getDistributorId()), date));

        }
        return "reports/holdApartmentReport";
    }

    /**
     * Get request Detailed General Report
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/detailedGeneralReport", method = RequestMethod.GET)
    public String detailedGeneralReport(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("accounts", accountService.findAccountsByLevelNot(4));
        return "reports/detailedGeneralReport";
    }

    @RequestMapping(value = "/Reports/detailedGeneralReport", method = RequestMethod.POST)
    public String detailedGeneralReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);
        Map accountMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("accounts", accountService.findAccountsByLevelNot(4));
        if (accountCode == null) {
            model.addAttribute("vouchers", null);
        } else {
            List<Voucher> voucherList = new ArrayList<>();
            for(Account account : accountService.findAccountsByParentCode(accountCode)){
               voucherList.addAll(ledgerCalculationService.getSortedVouchers(account.getAccountCode(), null, fromDate, toDate, false, false, false, false, null));
            }
            voucherList.sort(Comparator.comparing(v -> v.getVoucherDate()));
            model.addAttribute("vouchers", voucherList);
        }

        return "reports/detailedGeneralReport";
    }

    /**
     * Get request Detailed General Report
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Reports/projectWiseReport", method = RequestMethod.GET)
    public String projectWiseReport(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("projectList", stockService.getStockCategories());
        model.addAttribute("accountList", new ArrayList<>());
        return "reports/projectWiseReport";
    }

    @RequestMapping(value = "/Reports/projectWiseReport", method = RequestMethod.POST)
    public String projectWiseReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("accountCode") Integer accountCode
            , @RequestParam("projectCode") Integer projectCode) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentAccount", accountCode);
        model.addAttribute("currentProject", projectCode);
        Map accountMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        List<Account> accountList = new ArrayList<>();
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("projectList", stockService.getStockCategories());
        if (projectCode == 0) {
            model.addAttribute("vouchers", null);
        } else {
            accountList = accountService.findAccountsByProjectCode(projectCode);
            List<Integer> accountCodeList = accountList.stream().map(Account::getAccountCode).collect(Collectors.toList());
            List<Voucher> voucherList = new ArrayList<>();
            if(accountCode > 0){
                voucherList.addAll(ledgerCalculationService.getSortedVouchers(projectCode, Arrays.asList(accountCode),fromDate,toDate));
            }
            else {
                voucherList.addAll(ledgerCalculationService.getSortedVouchers(projectCode, accountCodeList,fromDate,toDate));

            }
            model.addAttribute("vouchers", voucherList);
        }
        model.addAttribute("accounts", accountList);

        return "reports/projectWiseReport";
    }
}
