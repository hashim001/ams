package com.hellokoding.account.web;

import com.hellokoding.account.model.Department;
import com.hellokoding.account.model.SubDepartment;
import com.hellokoding.account.service.department.DepartmentService;
import com.hellokoding.account.service.subDepartment.SubDepartmentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class SubDepartmentController {
    @Autowired
    private SubDepartmentService subDepartmentService;
    @Autowired
    private DepartmentService departmentService;

    /**
     * GET for Listing of Departments and Form
     *
     * @param model Data Model
     * @return Response Page with listing and new Dept form.
     */
    @RequestMapping(value = "/SubDepartment/addSubDepartment", method = RequestMethod.GET)
    public String addDept(Model model) {
        model.addAttribute("deptForm", new SubDepartment());
        model.addAttribute("subDepartments", subDepartmentService.findAll());
        model.addAttribute("departments", departmentService.findAll());
        return "department/addSubDept";
    }


    /**
     * POST for Sub Department Insertion
     *
     * @param subDepartment Form
     * @param attributes    for message
     * @return Response Page
     */
    @RequestMapping(value = "/SubDepartment/addSubDepartment", method = RequestMethod.POST)
    public String addDept_post(@ModelAttribute("deptForm") SubDepartment subDepartment, RedirectAttributes attributes) {

        Department department = departmentService.findByDeptId(subDepartment.getDeptId());
        subDepartment.setSubDepartment(department);
        subDepartmentService.save(subDepartment);
        attributes.addFlashAttribute("deptAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">SubDepartment <strong>" + subDepartment.getSubDepName() + " </strong> added successfully.</div>");
        return "redirect:/SubDepartment/addSubDepartment";
    }


    /**
     * Remove SubDepartment by Id
     *
     * @param deptId     sub department Identity
     * @param attributes Redirect Message
     * @return Redirect to GET
     */
    @RequestMapping(value = "/SubDepartment/removeSubDept", method = RequestMethod.POST)
    public String removeDept_post(@RequestParam("deptId") int deptId, RedirectAttributes attributes) {

        subDepartmentService.remove(deptId);
        attributes.addFlashAttribute("deptRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">SubDepartment with id <strong>" + deptId + " </strong> removed successfully.</div>");
        return "redirect:/SubDepartment/addSubDepartment";
    }

    /**
     * Ajax Call to Edit SubDepartment by Id
     *
     * @param deptId
     * @return
     */

    @RequestMapping(value = "/SubDepartment/editSubDept", method = RequestMethod.GET)
    public @ResponseBody
    SubDepartment editSubDepart(@RequestParam("subDeptId") int deptId) {
        SubDepartment subDepartment = subDepartmentService.findBySubDeptId(deptId);
        subDepartment.setDeptId(subDepartment.getSubDepartment().getDepId());
        subDepartment.setSubDepartment(null);
        return subDepartment;
    }

    /**
     * To get the list of sub department of given deptId
     *
     * @param deptId
     * @return
     */
    @RequestMapping(value = "/SubDepartment/getSubDepartments", method = RequestMethod.GET)
    public @ResponseBody
    Map<Integer, String> getSubDepartments(@RequestParam("deptId") int deptId) {
        Map<Integer, String> subDepartmentResponse = new HashMap<>();

        if (deptId == 0) {
            for (SubDepartment subDepartment : subDepartmentService.findAll()) {
                subDepartmentResponse.put(subDepartment.getSubDepId(), subDepartment.getSubDepName());
            }
            return subDepartmentResponse;
        }
        for (SubDepartment subDepartment : departmentService.findByDeptId(deptId).getSubDepartmentList()) {
            subDepartmentResponse.put(subDepartment.getSubDepId(), subDepartment.getSubDepName());
        }
        return subDepartmentResponse;
    }

}
