package com.hellokoding.account.web;

import com.hellokoding.account.model.*;

import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.BankDetailService;
import com.hellokoding.account.service.account.ExpenseDetailService;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.maker.MakerService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.validator.AccountValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;

@Controller
public class AccountSupportController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountValidator accountValidator;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private StockService stockService;

    @Autowired
    private ExpenseDetailService expenseDetailService;

    @Autowired
    private BankDetailService bankDetailService;

    @Autowired
    private MakerService makerService;

    /**
     * Create Account Customer (account_customer)
     * GET
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/AccountSupport/addCustomerAccount", method = RequestMethod.GET)
    public String addCustomerAccount(Model model) {
        Account account = new Account();
        account.setStockCustomer(new StockCustomer());
        model.addAttribute("accountForm", account);
        model.addAttribute("accounts", accountService.findAllFiltered());
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getCustomerHead() > 0) {
            model.addAttribute("customerHead", staticInfo.getCustomerHead());
            return "stock/addCustomerAccount";
        }
        return "redirect:/staticinfo";
    }

    /**
     * Create Account Customer
     * POST
     *
     * @param accountForm
     * @param model
     * @param bindingResult
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/Customer/AccountSupport/addCustomerAccount", method = RequestMethod.POST)
    public String addCustomerAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAllFiltered());
            return "stock/addCustomer";
        }
        if (accountForm.getAccountCodeForEdit() > 0) {
            accountForm.setAccountCode(accountForm.getAccountCodeForEdit());
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getCustomerHead() > 0) {
            StockCustomer stockCustomer = accountForm.getStockCustomer();
            if (accountForm.getAccountCode() > 0) {
                StockCustomer customerEdit = accountService.findAccount(accountForm.getAccountCode()).getStockCustomer();
                stockCustomer.setCustomerAccount(customerEdit.getCustomerAccount());
                accountForm.setParentAccount(accountService.findAccount(accountForm.getParentCode()));
                accountForm.setStockCustomer(null);
                accountForm.setIsActive(1);
                accountService.update(accountForm);
                stockCustomer.setCustomerId(customerEdit.getCustomerId());
                stockService.saveCustomer(stockCustomer);
                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Customer Account with Code: <strong>" + accountForm.getAccountCode() + " </strong>updated successfully.</div>");

            } else {

                accountForm.setStockCustomer(null);
                Account account = accountService.save(accountForm);
                stockCustomer.setCustomerAccount(account);
                stockService.saveCustomer(stockCustomer);
                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Customer Account with Code: <strong>" + account.getAccountCode() + " </strong>added successfully.</div>");

            }
        } else {
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Customer Account.</div>");
        }

        return "redirect:/Customer/addCustomer";
    }

    /**
     * Create Account Vendor
     * GET
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/AccountSupport/addVendorAccount", method = RequestMethod.GET)
    public String addvendorAccount(Model model) {
        Account account = new Account();
        account.setStockVendor(new StockVendor());
        model.addAttribute("accountForm", account);
        model.addAttribute("accounts", accountService.findAllFiltered());
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getVendorHead() > 0) {
            model.addAttribute("vendorHead", staticInfo.getVendorHead());
            return "stock/addVendorAccount";
        }
        return "redirect:/staticinfo";
    }

    /**
     * Create Account Vendor
     * POST
     *
     * @param accountForm
     * @param model
     * @param bindingResult
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/StockVendor/addVendorAccount", method = RequestMethod.POST)
    public String addVendorAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAllFiltered());
            return "stock/addVendor";
        }
        if (accountForm.getAccountCodeForEdit() > 0) {
            accountForm.setAccountCode(accountForm.getAccountCodeForEdit());
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getVendorHead() > 0) {
            StockVendor stockVendor = accountForm.getStockVendor();
            if (accountForm.getAccountCode() > 0) {
                StockVendor vendorEdit = accountService.findAccount(accountForm.getAccountCode()).getStockVendor();
                stockVendor.setVendorAccount(vendorEdit.getVendorAccount());
                accountForm.setParentAccount(accountService.findAccount(accountForm.getParentCode()));
                accountForm.setStockVendor(null);
                accountForm.setIsActive(1);
                accountService.update(accountForm);
                stockVendor.setVendorId(vendorEdit.getVendorId());
                stockVendor.setVendorDistributor(vendorEdit.getVendorDistributor());

                stockService.saveVendor(stockVendor);
                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Vendor Account with Code: <strong>" + accountForm.getAccountCode() + " </strong>updated successfully.</div>");

            } else {
                accountForm.setStockVendor(null);
                Account account = accountService.save(accountForm);
                stockVendor.setVendorAccount(account);
                stockService.saveVendor(stockVendor);

                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Vendor Account with Code: <strong>" + account.getAccountCode() + " </strong>added successfully.</div>");
            }
        } else {
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Vendor Account.</div>");
        }

        return "redirect:/StockVendor/addVendor";
    }


    /**
     * Create Account Stock
     * GET
     * CURRENTLY NOT USED
     * @param model
     * @return
     */
    @RequestMapping(value = "/AccountSupport/addStockAccount", method = RequestMethod.GET)
    public String addStockAccount(Model model) {
        Account account = new Account();
        account.setAccountStock(new Stock());
        model.addAttribute("accountForm", account);
        model.addAttribute("accounts", accountService.findAllFiltered());
        model.addAttribute("categories", stockService.getStockCategories());
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getProductHead() > 0) {
            model.addAttribute("stockHead", staticInfo.getProductHead());
            return "stock/addStockAccount";
        }
        return "redirect:/staticinfo";
    }

    /**
     * Create Account Stock
     * POST
     *
     * @param accountForm
     * @param model
     * @param bindingResult
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/AccountSupport/addStockAccount", method = RequestMethod.POST)
    public String addStockAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAllFiltered());
            return "stock/addStock";
        }
        if (accountForm.getAccountCodeForEdit() > 0) {
            accountForm.setAccountCode(accountForm.getAccountCodeForEdit());
        }
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getProductHead() > 0) {

            Stock stock = accountForm.getAccountStock();
            StockCategory stockCategory = stockService.findByCategoryId(stock.getCategoryId());
            if (accountForm.getAccountCode() > 0) {
                Stock stockEdit = accountService.findAccount(accountForm.getAccountCode()).getAccountStock();
                stock.setStockAccount(stockEdit.getStockAccount());
                accountForm.setParentAccount(accountService.findAccount(stockCategory.getAccountCode()));
                accountForm.setAccountStock(null);
                accountForm.setIsActive(1);
                accountService.update(accountForm);
                stock.setName(accountForm.getTitle());
                stock.setStockId(stockEdit.getStockId());
                stock.setStockCategory(stockCategory);
                if (stock.getMakerId() > 0) {
                    stock.setMaker(makerService.findByMakerId(stock.getMakerId()));
                }
                if (stock.getUnitMeasure().isEmpty()) {
                    stock.setUnitMeasure("NA");
                }
                stockService.saveStock(stock);
                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock Account with Code: <strong>" + accountForm.getAccountCode() + " </strong>updated successfully.</div>");

            } else {
                accountForm.setAccountStock(null);
                accountForm.setParentCode(stockCategory.getAccountCode());
                Account account = accountService.save(accountForm);
                stock.setStockAccount(account);
                stock.setName(account.getTitle());
                stock.setStockCategory(stockCategory);
                if (stock.getMakerId() > 0) {
                    stock.setMaker(makerService.findByMakerId(stock.getMakerId()));
                }
                if (stock.getUnitMeasure().isEmpty()) {
                    stock.setUnitMeasure("NA");
                }
                stockService.saveStock(stock);
                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock Account with Code: <strong>" + account.getAccountCode() + " </strong>added successfully.</div>");

            }
        } else {
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Product Account.</div>");
        }

        return "redirect:/Stock/addStock";
    }

    /**
     * Create Account Expense
     * POST
     * GET in Account Controller
     *
     * @param accountForm
     * @param model
     * @param bindingResult
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/AccountSupport/addExpenseAccount", method = RequestMethod.POST)
    public String addExpenseAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAllFiltered());
            return "master/addExpenseDetails";
        }
        if (accountForm.getAccountCodeForEdit() > 0) {
            accountForm.setAccountCode(accountForm.getAccountCodeForEdit());
        }
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getExpenseHead() > 0) {
            ExpenseDetail expenseDetail = accountForm.getExpenseDetail();
            if (accountForm.getAccountCode() > 0) {
                ExpenseDetail expenseDetailEdit = accountService.findAccount(accountForm.getAccountCode()).getExpenseDetail();
                expenseDetail.setExpenseAccount(expenseDetailEdit.getExpenseAccount());
                accountForm.setParentAccount(accountService.findAccount(accountForm.getParentCode()));
                accountForm.setExpenseDetail(null);
                accountForm.setIsActive(1);
                accountService.update(accountForm);
                expenseDetail.setExpenseId(expenseDetailEdit.getExpenseId());
                expenseDetailService.save(expenseDetail);
                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Expense Account with Code: <strong>" + accountForm.getAccountCode() + " </strong>updated successfully.</div>");

            } else {
                accountForm.setExpenseDetail(null);
                Account account = accountService.save(accountForm);
                expenseDetail.setExpenseAccount(account);
                expenseDetailService.save(expenseDetail);

                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Expense Account with Code: <strong>" + account.getAccountCode() + " </strong>added successfully.</div>");
            }
        } else {
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Expense Account.</div>");
        }

        return "redirect:/Account/addExpense";
    }


    /**
     * Create Bank Account
     * POST
     * GET in Account Controller
     *
     * @param accountForm
     * @param model
     * @param bindingResult
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/AccountSupport/addBankAccount", method = RequestMethod.POST)
    public String addBankAccount_post(@ModelAttribute("accountForm") Account accountForm, Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        accountValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts", accountService.findAllFiltered());
            return "master/addBankDetails";
        }
        if (accountForm.getAccountCodeForEdit() > 0) {
            accountForm.setAccountCode(accountForm.getAccountCodeForEdit());
        }
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getBankHead() > 0) {

            BankDetails bankDetails = accountForm.getBankDetails();
            if (accountForm.getAccountCode() > 0) {
                BankDetails bankDetailEdit = accountService.findAccount(accountForm.getAccountCode()).getBankDetails();
                bankDetails.setAccount(bankDetailEdit.getAccount());
                accountForm.setParentAccount(accountService.findAccount(accountForm.getParentCode()));
                accountForm.setBankDetails(null);
                accountForm.setIsActive(1);
                accountService.update(accountForm);
                bankDetails.setDetailId(bankDetailEdit.getDetailId());
                bankDetailService.save(bankDetails);
                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Bank Account with Code: <strong>" + accountForm.getAccountCode() + " </strong>updated successfully.</div>");

            } else {
                accountForm.setBankDetails(null);
                Account account = accountService.save(accountForm);
                bankDetails.setAccount(account);
                bankDetailService.save(bankDetails);
                attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Bank Account with Code: <strong>" + accountForm.getAccountCode() + " </strong>added successfully.</div>");
            }

        } else {
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Failed to add Account.Check Static info for Head Bank Account.</div>");
        }

        return "redirect:/User/addBankDetails";
    }

}


