package com.hellokoding.account.web;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Location;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.location.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class LocationController {
    @Autowired
    private LocationService locationService;

    /**
     * GET for Listing of Location and Form
     *
     * @param model Data Model
     * @return Response Page with listing and new Location form.
     */
    @RequestMapping(value = "/Location/addLocation", method = RequestMethod.GET)
    public String addLocation(Model model) {
        model.addAttribute("locationForm", new Location());
        model.addAttribute("locations", locationService.findAll());
        return "location/addLocation";
    }

    /**
     * POST for Location Insertion
     *
     * @param location   Form
     * @param attributes for message
     * @return Response Page
     */
    @RequestMapping(value = "/Location/addLocation", method = RequestMethod.POST)
    public String addLocation_post(@ModelAttribute("locationForm") Location location, RedirectAttributes attributes) {

        locationService.save(location);
        attributes.addFlashAttribute("locationAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Location <strong>" + location.getLocationName() + " </strong> added successfully.</div>");
        return "redirect:/Location/addLocation";
    }

    /**
     * Remove Location by Id
     *
     * @param locationId     location Identity
     * @param attributes Redirect Message
     * @return Redirect to GET
     */
    @RequestMapping(value = "/Location/removeLocation", method = RequestMethod.POST)
    public String removeLocation_post(@RequestParam("locationId") int locationId, RedirectAttributes attributes) {

        locationService.remove(locationId);
        attributes.addFlashAttribute("locationRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Location with id <strong>" + locationId+ " </strong> removed successfully.</div>");
        return "redirect:/Location/addLocation";
    }

    /**
     * Ajax Call to Edit Location by locationId
     *
     * @param locationId
     * @return Location
     */

    @RequestMapping(value = "/Location/editLocation", method = RequestMethod.GET)
    public @ResponseBody
    Location editLocation(@RequestParam("locationId") int locationId) {
        return locationService.findByLocationId(locationId);
    }


    /// testing
    @Autowired
    private AccountService accountService;


    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @ResponseBody
    public String testAccount(@RequestParam(value = "code",defaultValue = "0") int accountCode) {


        List<Account> accountList = accountService.findAll();
        String returnString = "<pre>";
        for(Account account : accountList){
            returnString += account.getTitle() + "</br>";
        }

        //Account account = accountService.findAccount(accountCode);
        return returnString + "</pre>";//account.getAccountStock().getName();
    }
}
