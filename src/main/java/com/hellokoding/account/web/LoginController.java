package com.hellokoding.account.web;


import com.hellokoding.account.model.StaticInfo;
import com.hellokoding.account.model.UserAuth;
import com.hellokoding.account.service.automation.UpdateBalanceService;
import com.hellokoding.account.service.security.RoleSupport;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.user.UserAuthService;
import com.hellokoding.account.validator.CredentialValidator;
import com.hellokoding.account.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;


@Controller
@SessionAttributes({"userForm"})
public class LoginController {


    @Autowired
    private CredentialValidator credentialValidator;

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private UpdateBalanceService updateBalanceService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    /**
     * Normal Login Case
     * GET
     *
     * @param model
     * @param error
     * @param logout
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username or password is invalid or Account has been expired .");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        model.addAttribute("loginActive", "active");
        model.addAttribute("credintialForm", new UserAuth());
        return "login/login";
    }

    /**
     * Logout mapping
     * POST for session Destruction
     *
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String showLoggedout() {
        return "login/logout";
    }


    /**
     * Create New User POST
     * DEPRECIATED
     *
     * @param userForm
     * @param bindingResult
     * @param model
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registration(@ModelAttribute("credintialForm") UserAuth userForm, BindingResult bindingResult, Model model, RedirectAttributes attributes) {
        credentialValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("signupActive", "active");
            return "login/login";
        }

        userForm.setPassword(bCryptPasswordEncoder.encode(userForm.getPassword()));
        userForm.setIsActive(0);
        userAuthService.save(userForm);

        //   securityService.autologin(userForm.getUserName(), userForm.getPasswordConfirm());

        attributes.addFlashAttribute("userAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">User :<strong>" + userForm.getUserName() + " </strong> added successfully.</div>");
        return "redirect:/AccessControl/users";
    }


    /**
     * GET Home Index Page
     * Request Mapping TO BE REMOVED IN FUTURE AND MAPPED ON /
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model) {
        //java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        StaticInfo staticInfo = staticInfoService.findStaticInfo();

        if (staticInfo != null) {
/*            if(staticInfo.getUpdatedDate()!=null) {
                if (!(staticInfo.getUpdatedDate().toString()).equals(date.toString())) {
                    staticInfo.setUpdatedDate(date);
                    staticInfoService.save(staticInfo);
                    updateBalanceService.calculateDailyBalance();
                }
            }
            else{
                staticInfo.setUpdatedDate(date);
                staticInfoService.save(staticInfo);
                updateBalanceService.calculateDailyBalance();
            }*/

            if (staticInfo.getImageUrl() == null || staticInfo.getImageUrl().isEmpty()) {
                model.addAttribute("imageUrl", "");
            } else {
                model.addAttribute("imageUrl", staticInfo.getImageUrl());
            }
        } else {
            model.addAttribute("imageUrl", "");
        }
        if (RoleSupport.hasRole("ROLE_SALESMAN")) {
            model.addAttribute("riderName", RoleSupport.getUserName());
            return "dashboardSalesman";
        }

        return "dashboard";
    }


    /**
     * Get Project Settings
     * Not Used in Normal Cases
     *
     * @return
     */
    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String settings() {
        return "settings";
    }


    /**
     * Change User Password
     * GET
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public String change_password(Model model) {
        SecurityContext securityContext = SecurityContextHolder.getContext();

        String username = "";
        if (null != securityContext.getAuthentication()) {
            username = securityContext.getAuthentication().getName();
        }
        UserAuth userAuth = userAuthService.findByUserName(username);
        userAuth.setPassword("");
        model.addAttribute("userForm", userAuth);

        return "changepassword";
    }

    /**
     * Change User Password
     * POST
     *
     * @param userForm
     * @param bindingResult
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public String chnage_password_post(@ModelAttribute("userForm") UserAuth userForm, BindingResult bindingResult, RedirectAttributes attributes) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "changepassword";
        }
        userAuthService.save(userForm);
        attributes.addFlashAttribute("passwordChangeSuccess",
                "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">User credentials changed successfully.</div>");
        return "redirect:/home";
    }


}
