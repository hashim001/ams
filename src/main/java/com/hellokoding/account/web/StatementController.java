package com.hellokoding.account.web;


import com.hellokoding.account.model.AccountNote;
import com.hellokoding.account.model.NoteSubHeading;
import com.hellokoding.account.model.ReportStaging;
import com.hellokoding.account.reportModel.SubHeadingSet;
import com.hellokoding.account.service.note.NoteService;
import com.hellokoding.account.service.statement.ReportStageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;

@Controller
@SessionAttributes({"AccountNoteForm","subHeadList","AccountReportForm"})
public class StatementController {

    @Autowired
    private ReportStageService reportStageService;

    @Autowired
    private NoteService noteService;

////////////////////////////////////////Stage One > Title generation /////////////////////////////////////
    @RequestMapping(value = "/User/generateStatement", method = RequestMethod.GET)
    public String stagingOne(Model model) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        ReportStaging reportStaging = new ReportStaging();
        model.addAttribute("reportForm", reportStaging);
        model.addAttribute("currentDate" , date);
        model.addAttribute("reports" , reportStageService.findAll());

        return "master/statementOne";
    }

    @RequestMapping(value = "/User/generateStatement", method = RequestMethod.POST)
    public String stagingOne_post(Model model, @ModelAttribute("reportForm") ReportStaging reportStaging, RedirectAttributes attributes) {


        reportStageService.saveTitle(reportStaging);
        attributes.addFlashAttribute("titleSaveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Report Title Added Successfully.</div>");
        return "redirect:/User/generateStatement";
    }



    ///////////////////////////////////////// Stage Three >> Add Notes Subheadings ///////////////////////////////////



    @RequestMapping(value = "/User/removeReport", method = RequestMethod.POST)
    public String removeReport_post(@RequestParam("reportId") Integer reportId,RedirectAttributes attributes) {

        ReportStaging reportStaging = reportStageService.findByReportId(reportId);
        reportStaging.getAccountNoteList().forEach(e->e.getNoteSubHeadings().clear());
        reportStageService.removeReport(reportStaging);
        attributes.addFlashAttribute("reportRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Report Removed Successfully.</div>");
        return "redirect:/User/generateStatement";
    }


    ///////////////////////////////////// Testing Account Report //////////////////////////////


    @RequestMapping(value = "/Account/accountReport", method = RequestMethod.GET)
    public String accountReport(Model model,@RequestParam(name = "reportId" ,defaultValue = "0") Integer reportId) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        ReportStaging reportStaging = reportStageService.findByReportId(reportId);
        model.addAttribute("AccountReportForm", reportStaging);
        model.addAttribute("currentDate" , date);
        model.addAttribute("accountNotes" , noteService.findAll());
        model.addAttribute("subHeadList" , noteService.findAllSubHead());

        return "master/accountReport";
    }

    @RequestMapping(value = "/Account/accountReport", method = RequestMethod.POST)
    public String accountReport_post(Model model, @ModelAttribute("AccountReportForm") ReportStaging reportStaging, RedirectAttributes attributes) {


        reportStageService.saveTitle(reportStaging);
        attributes.addFlashAttribute("titleSaveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Report Title Added Successfully.</div>");
        return "redirect:/Account/accountReport?reportId="+reportStaging.getStageId();
    }


    @RequestMapping(value = "/User/addReportNote", method = RequestMethod.POST)
    public String removeReportNoteSubHead_post(@RequestParam("reportId") Integer reportId,@RequestParam("heading") String heading) {

        ReportStaging reportStaging = reportStageService.findByReportId(reportId);
        AccountNote accountNote = new AccountNote();
        accountNote.setHeading(heading);
        accountNote.setStatus(0);
        accountNote.setReportType(reportStaging.getReportType());
        accountNote.setReportStaging(reportStaging);
        if(!reportStaging.getAccountNoteList().contains(accountNote)){
            reportStaging.getAccountNoteList().add(accountNote);
        }


        reportStageService.saveTitle(reportStaging);


        return "redirect:/Account/accountReport?reportId=" + reportId;
    }


    @RequestMapping(value = "/User/addReportNoteSubHead", method = RequestMethod.POST)
    public String addReportNoteSubHead_post(@RequestParam(name = "subId" ,defaultValue = "0") Integer subId
            ,@RequestParam(name = "noteId" ,defaultValue = "0") Integer noteId
            ,@RequestParam(name = "reportId" ,defaultValue = "0") Integer reportId) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        AccountNote accountNote = noteService.findByNoteId(noteId);
        NoteSubHeading noteSubHeading = noteService.findBySubId(subId);
        if(!accountNote.getNoteSubHeadings().contains(noteSubHeading)) {
                    accountNote.getNoteSubHeadings().add(noteSubHeading);
                }

        noteService.saveNote(accountNote);

        return "redirect:/Account/accountReport?reportId="+reportId;
    }


    @RequestMapping(value = "/User/removeReportNoteSubHead", method = RequestMethod.POST)
    public String removeReportNoteSubHead_post(@RequestParam("subId") Integer subId,@RequestParam("noteId") Integer noteId,RedirectAttributes attributes) {

        AccountNote accountNote = noteService.findByNoteId(noteId);
        NoteSubHeading noteSubHeading = noteService.findBySubId(subId);
        if(accountNote.getNoteSubHeadings().contains(noteSubHeading)){
            accountNote.getNoteSubHeadings().remove(noteSubHeading);
        }

        noteService.saveNote(accountNote);

        return "redirect:/Account/accountReport?reportId="+accountNote.getReportStaging().getStageId();
    }

    @RequestMapping(value = "/User/removeReportNote", method = RequestMethod.POST)
    public String removeReportNote_post(@RequestParam("reportId") Integer reportId,@RequestParam("noteId") Integer noteId) {

        ReportStaging reportStaging = reportStageService.findByReportId(reportId);
        AccountNote accountNote = noteService.findByNoteId(noteId);

        if(reportStaging.getAccountNoteList().contains(accountNote)){
            reportStaging.getAccountNoteList().remove(accountNote);
        }
        reportStageService.saveTitle(reportStaging);

        return "redirect:/Account/accountReport?reportId="+accountNote.getReportStaging().getStageId();
    }



}
