package com.hellokoding.account.web;


import com.hellokoding.account.model.*;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import java.util.*;


@Controller
@SessionAttributes({"voucherListOrder", "orderUpdateForm"})
public class PurchaseController {

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private JSONPopulateService jsonPopulateService;


    @Autowired
    private ChassisService chassisService;

    @RequestMapping(value = "/Purchase/viewPurchaseVoucher", method = RequestMethod.GET)
    public String viewPurchaseVoucher(Model model) {

        return "purchase/viewPurchaseVoucher";
    }


    /**
     * Does not create voucher Used for
     * Apartment Insertion
     * GET
     * @param model
     * @return
     */
    @RequestMapping(value = "/Purchase/createPurchaseVoucher", method = RequestMethod.GET)
    public String addPurchaseVoucher(Model model) {

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        VoucherList voucherList = new VoucherList();
        for (int i = 0; i < staticInfo.getMaxFormRows(); i++) {
            voucherList.add(new Voucher());
        }


        model.addAttribute("voucherList", voucherList);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("stockAccounts", accountService.getStockAccounts());

        return "purchase/addPurchaseVoucher";
    }


    /**
     * Inserts in Apartment
     * No Voucher Created
     * @param voucherList
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/Purchase/createPurchaseVoucher", method = RequestMethod.POST)
    public String addPurchaseVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        Apartment apartment;
        for (Voucher voucher : voucherList.getVoucherList()) {

            if (voucher.getDebit() > 0 && !voucher.getApartmentNo().isEmpty()) {
/*                if (chassisService.findByChassisNumber(voucher.getApartmentNo()) != null) {
                    attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: red;height: 40px;text-align: center;color: white; padding-top: 5px;border: 2px solid red;border-radius: 5px;font-size: initial;\">Apartment Number <strong>" + voucher.getApartmentNo() + " </strong> already exist.</div>");
                    return "redirect:/Purchase/createPurchaseVoucher";
                }*/
                apartment = new Apartment();
                apartment.setApartmentNo(voucher.getApartmentNo());
                apartment.setFloor(voucher.getFloor());
                apartment.setSellingPrice(voucher.getDebit());
                apartment.setDescription(voucher.getRemarks());
                apartment.setRooms(voucher.getRooms());
                apartment.setStock(accountService.findAccount(voucher.getItemAccount()).getAccountStock());
                chassisService.save(apartment);

            }

        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Apartment(s) added successfully.</div>");
        return "redirect:/Purchase/createPurchaseVoucher";
    }

    /////////////////////////////////////////// Edit Purchase Vouchers //////////////////////////////////

    @RequestMapping(value = "/Purchase/updatePurchaseVoucher", method = RequestMethod.GET)
    public String updatePurchaseVoucher(Model model, @RequestParam(value = "voucherNumber") String voucherNumber) {
        List<Voucher> voucherItemList = voucherService.findByVoucherNumber(voucherNumber);
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        VoucherList voucherList = new VoucherList();
        int currentVendorAccountCode = 0;
        java.sql.Date currentDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        Apartment apartment;
        for (Voucher voucher : voucherItemList) {
            if (voucher.getItemAccount() > 0) {
                apartment = chassisService.findByChassisId(voucher.getChassisId());
                voucher.setApartmentNo(apartment.getApartmentNo());
                voucher.setFloor(apartment.getFloor());
                voucher.setItemQuantity(voucher.getItemQuantity());
                voucher.setItemRate(voucher.getItemRate());
                voucher.setBillAmount(voucher.getBillAmount());
                voucher.setRemarks(apartment.getDescription());
                voucher.setTransitionCode(Integer.toString(voucher.getAccountCode()) + "-" + Integer.toString(voucher.getItemAccount()));
                voucherList.add(voucher);
            } else {
                currentVendorAccountCode = voucher.getAccountCode();
                currentDate = voucher.getVoucherDate();
            }
        }
        int size = voucherItemList.size();
        for (int i = 0; i < staticInfoService.findStaticInfo().getMaxFormRows() - size; i++) {
            Voucher voucher = new Voucher();
            voucher.setItemQuantity(0.0);
            voucher.setBillAmount(0.0);
            voucher.setItemRate(0.0);
            voucherList.add(voucher);
        }
        List<Integer> yearList = new ArrayList<>();
        for (int i = 2010; i < 2050; i++) {
            yearList.add(i);
        }
        model.addAttribute("yearList", yearList);
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("currentDate", currentDate);
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("currentVendorAccountCode", currentVendorAccountCode);
        model.addAttribute("stockAccounts", accountService.getStockAccounts());
        model.addAttribute("accounts", accountService.getVendorAccounts());
        model.addAttribute("cashInHand", staticInfo.getCashInHand());

        return "purchase/updatePurchaseVoucher";
    }


    @RequestMapping(value = "/Purchase/updatePurchaseVoucher", method = RequestMethod.POST)
    public String updatePurchaseVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, @RequestParam("voucherNumber") String voucherNumber, RedirectAttributes attributes) {
        Double supplierCreditAmount = 0.0;
        Voucher supplierVoucher = new Voucher();
        List<Voucher> voucherNumberList = voucherService.findByVoucherNumber(voucherNumber);
        List<Integer> chassisList = new ArrayList<>();
        String jtVoucherNumber = "";
        if (voucherNumberList.size() > 0) {
            for (Voucher jtVoucher : voucherService.findByReferenceVoucher(voucherNumber)) {
                voucherNumberList.add(jtVoucher);
                jtVoucherNumber = jtVoucher.getVoucherNumber();
            }
            for (Voucher voucher : voucherNumberList) {
                if (voucher.getItemAccount() > 0) {
                    chassisService.remove(voucher.getChassisId());
                }
            }
            voucherService.remove(voucherNumberList);
        }
        Apartment apartment;
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getItemAccount() > 0 && voucher.getDebit() > 0 && !voucher.getChassisNo().isEmpty()) {
/*                if (chassisService.findByChassisNumber(voucher.getChassisNo()) != null) {
                    attributes.addFlashAttribute("voucherUpdateSuccess", "<div style=\"background-color: red;height: 40px;text-align: center;color: white; padding-top: 5px;border: 2px solid red;border-radius: 5px;font-size: initial;\">Apartment Number <strong>" + voucher.getChassisNo() + " </strong>already exist.</div>");
                    return "redirect:/Purchase/updatePurchaseVoucher?voucherNumber=" + voucherNumber;
                }*/
                apartment = new Apartment();
                apartment.setApartmentNo(voucher.getApartmentNo());
                apartment.setFloor(voucher.getFloor());
                apartment.setDate(voucher.getVoucherDate());
                apartment.setSellingPrice(voucher.getDebit());
                apartment.setDescription(voucher.getRemarks());
                apartment.setStock(accountService.findAccount(voucher.getItemAccount()).getAccountStock());
                chassisService.save(apartment);

            }

        }

        attributes.addFlashAttribute("voucherUpdateSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Purchase Vouchers with number <strong>" + voucherNumber + " </strong>updated successfully.</div>");
        return "redirect:/Purchase/viewPurchaseVoucher";
    }


    /**
     * Listing For Delivery For Gate
     * First Phase
     *
     * @return Listing
     */
    @RequestMapping(value = "/Trading/viewGrnOrders", method = RequestMethod.GET)
    public String viewGrnOrders() {

        return "purchasetrade/grnListing";
    }


    /////////////////////////// JSON Generator On DataTable Ajax Call //////////////////

    @RequestMapping(value = "/Purchase/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> saleVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populatePurchaseVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherNumber", false, searchString, "LP"), draw), HttpStatus.OK);


    }


}
