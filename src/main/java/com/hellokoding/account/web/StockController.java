package com.hellokoding.account.web;


import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.*;
import com.hellokoding.account.model.jsonentity.AccountDto;
import com.hellokoding.account.model.jsonentity.CategoryDto;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.maker.MakerService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.CustomerService;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.service.stock.VendorService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.*;

@Controller
@SessionAttributes({"stockFormUpdate"})
public class StockController {

    @Autowired
    private StockService stockService;

    @Autowired
    private CustomerService customerService;
    @Autowired
    private VendorService vendorService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private JSONPopulateService jsonPopulateService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private LedgerCalculationService ledgerCalculationService;

    @Autowired
    private MakerService makerService;

    //////////////////////////////////Stock ///////////////////////////////////////
    @RequestMapping(value = "/Stock/addStock", method = RequestMethod.GET)
    public String addStock(Model model, RedirectAttributes attributes) {
        model.addAttribute("stockForm", new Stock());
        model.addAttribute("categories", stockService.getStockCategories());

        Account account = new Account();
        account.setAccountStock(new Stock());
        model.addAttribute("accountForm", account);
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        model.addAttribute("makers", makerService.findAllMakers());
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getProductHead() > 0) {
            model.addAttribute("stockHead", staticInfo.getProductHead());
            return "stock/addStock";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid stock Head Account before entering a stock.</div>");
        return "redirect:/staticinfo";


    }


    @RequestMapping(value = "/Stock/removeStock", method = RequestMethod.POST)
    public String removeStock(RedirectAttributes attributes, @RequestParam("accountCode") Integer accountCode) {

        Account account = accountService.findAccount(accountCode);
        if(account.getAccountStock().getApartmentList().size() > 0){
            attributes.addFlashAttribute("stockRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">First Remove Plots of this model !.</div>");
            return "redirect:/Stock/addStock";
        }
        stockService.removeStock(account);
        accountService.delete(accountCode);
        attributes.addFlashAttribute("stockRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock removed successfully.</div>");
        return "redirect:/Stock/addStock";
    }


    //////////////////////////////////Vendor ///////////////////////////////////////
    @RequestMapping(value = "/StockVendor/addVendor", method = RequestMethod.GET)
    public String addVendor(Model model, RedirectAttributes attributes) {
        model.addAttribute("vendorForm", new StockVendor());
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("vendors", vendorService.findAll());

        Account account = new Account();
        account.setStockVendor(new StockVendor());
        model.addAttribute("accountForm", account);
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getVendorHead() > 0) {
            model.addAttribute("vendorHead", staticInfo.getVendorHead());
            return "stock/addVendor";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid Vendor Head Account before entering a Vendor.</div>");
        return "redirect:/staticinfo";
    }

    @RequestMapping(value = "/StockVendor/removeVendor", method = RequestMethod.POST)
    public String removeVendor(RedirectAttributes attributes, @RequestParam("accountCode") Integer accountCode) {

        stockService.removeVendor(accountService.findAccount(accountCode));
        accountService.delete(accountCode);
        attributes.addFlashAttribute("vendorRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Vendor removed successfully.</div>");
        return "redirect:/StockVendor/addVendor";
    }

    ////////////////////////////////// Customer ///////////////////////////////////////
    @RequestMapping(value = "/Customer/addCustomer", method = RequestMethod.GET)
    public String addCustomer(Model model, RedirectAttributes attributes) {
        model.addAttribute("customerForm", new StockCustomer());
        model.addAttribute("accounts", accountService.findAccountsByLevel(4));
        model.addAttribute("customers", customerService.findAll());

        Account account = new Account();
        account.setStockCustomer(new StockCustomer());
        model.addAttribute("accountForm", account);
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getCustomerHead() > 0) {
            model.addAttribute("customerHead", staticInfo.getCustomerHead());
            return "stock/addCustomer";
        }
        attributes.addFlashAttribute("emptyCaseMessage", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Please enter a valid Customer Head Account before entering a Customer.</div>");
        return "redirect:/staticinfo";
    }


    @RequestMapping(value = "/Customer/removeCustomer", method = RequestMethod.POST)
    public String removeCustomer(RedirectAttributes attributes, @RequestParam("accountCode") Integer accountCode) {

        stockService.removeCustomer(accountService.findAccount(accountCode));
        accountService.delete(accountCode);
        attributes.addFlashAttribute("customerRemoveSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Customer removed successfully.</div>");
        return "redirect:/Customer/addCustomer";
    }

    ////////////////////////////////////////// Stock Category /////////////////////////////////////

    @RequestMapping(value = "/Stock/addStockCategory", method = RequestMethod.GET)
    public String addCategory(Model model) {
        model.addAttribute("categoryForm", new StockCategory());
        model.addAttribute("categories", stockService.getStockCategories());
        model.addAttribute("accounts", accountService.findAccountsByParentCode(Constants.SITE_PARENT_CODE));
        return "stock/addCategory";
    }

    @RequestMapping(value = "/Stock/addStockCategory", method = RequestMethod.POST)
    public String addCategory_post(@ModelAttribute("categoryForm") StockCategory stockCategory, RedirectAttributes attributes) {
        if (stockCategory.getCategory() > 0) {
            stockCategory.setCategoryId(stockCategory.getCategory());
        }
        if(stockCategory.getAccountCode() > 0){
            Account account = accountService.findAccount(stockCategory.getAccountCode());
            account.setTitle(stockCategory.getTitle());
            accountService.update(account);
        }
        else{
            Account accountForm = new Account();
            accountForm.setParentCode(Constants.SITE_PARENT_CODE);
            accountForm.setTitle(stockCategory.getTitle());
            accountForm.setOpeningDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            Account account = accountService.save(accountForm);
            stockCategory.setAccountCode(account.getAccountCode());
        }
        stockService.saveCategory(stockCategory);
        attributes.addFlashAttribute("stockCategoryAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Category <strong>" + stockCategory.getTitle() + " </strong>added successfully.</div>");
        return "redirect:/Stock/addStockCategory";
    }

    /**
     * Delete stock category
     *
     * @param attributes
     * @param categoryId
     * @return
     */
    @RequestMapping(value = "/Stock/removeCategory", method = RequestMethod.POST)
    public String removeCategory(RedirectAttributes attributes, @RequestParam("categoryId") Integer categoryId) {
        StockCategory stockCategory = stockService.findByCategoryId(categoryId);
        if(stockCategory.getStockList().size() > 0){
            attributes.addFlashAttribute("categoryRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">First Remove Models of this site !.</div>");
            return "redirect:/Stock/addStockCategory";
        }
        stockService.removeCategory(stockService.findByCategoryId(categoryId));

        attributes.addFlashAttribute("categoryRemoveSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Stock Category removed successfully.</div>");
        return "redirect:/Stock/addStockCategory";
    }


    /////////////////////////// JSON Generator On DataTable Ajax Call For Stock Listing//////////////////

    @RequestMapping(value = "/Stock/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Stock>> stockListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populateStockList(stockService.findAllPaged(pageIndex, length, "", false, searchString), draw), HttpStatus.OK);

    }

    /**
     * Edit stock
     *
     * @param accountCode
     * @return
     */
    @RequestMapping(value = "/Stock/ajax/editStock", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Stock> editStock(@RequestParam("accountCode") int accountCode) {
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        Stock stock = accountService.findAccount(accountCode).getAccountStock();
        ModelMapper modelMapper = new ModelMapper();
        AccountDto accountDto = modelMapper.map(stock.getStockAccount(), AccountDto.class);
        CategoryDto categoryDto = modelMapper.map(stock.getStockCategory(), CategoryDto.class);
        accountDto.setParentCode(staticInfo.getProductHead());
        stock.setCategoryDto(categoryDto);
        stock.setAccountDto(accountDto);
        stock.setMakerId(stock.getMaker().getMakerId());
        stock.setMaker(null);
        stock.setStockAccount(null);
        stock.setStockCategory(null);
        stock.setApartmentList(null);
        stock.setPaymentPlan(null);
        return new ResponseEntity(stock, HttpStatus.OK);
    }


    /**
     * Edit Customer
     *
     * @param accountCode
     * @return
     */
    @RequestMapping(value = "/Stock/ajax/editCustomer", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<StockCustomer> editCustomer(@RequestParam("accountCode") int accountCode) {
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        StockCustomer stockCustomer = accountService.findAccount(accountCode).getStockCustomer();
        ModelMapper modelMapper = new ModelMapper();
        AccountDto accountDto = modelMapper.map(stockCustomer.getCustomerAccount(), AccountDto.class);
        accountDto.setParentCode(staticInfo.getCustomerHead());
        stockCustomer.setAccountDto(accountDto);
        stockCustomer.setCustomerAccount(null);
        return new ResponseEntity(stockCustomer, HttpStatus.OK);
    }

    /**
     * Edit Vendor
     *
     * @param accountCode
     * @return
     */
    @RequestMapping(value = "/StockVendor/ajax/editVendor", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<StockVendor> editVendor(@RequestParam("accountCode") int accountCode) {
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        StockVendor stockVendor = accountService.findAccount(accountCode).getStockVendor();
        ModelMapper modelMapper = new ModelMapper();
        AccountDto accountDto = modelMapper.map(stockVendor.getVendorAccount(), AccountDto.class);
        accountDto.setParentCode(staticInfo.getVendorHead());
        stockVendor.setAccountDto(accountDto);
        stockVendor.setVendorAccount(null);
        stockVendor.setVendorDistributor(null);
        return new ResponseEntity(stockVendor, HttpStatus.OK);
    }

    /**
     * Edit Category
     *
     * @param categoryId
     * @return
     */
    @RequestMapping(value = "/Stock/ajax/editCategory", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<StockCategory> editCategory(@RequestParam("categoryId") int categoryId) {
        StockCategory stockCategory = stockService.findByCategoryId(categoryId);
        stockCategory.setStockList(null);
        return new ResponseEntity(stockCategory, HttpStatus.OK);
    }

    /**
     * JSON Generator On DataTable Ajax Call For Stock Customer Listing
     *
     * @param start
     * @param length
     * @param draw
     * @param searchString
     * @return
     */

    @RequestMapping(value = "/Customer/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<StockCustomer>> customerListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populateStockCustomerList(stockService.findAllCustomerPaged(pageIndex, length, "mobile", false, searchString), draw), HttpStatus.OK);

    }

    /**
     * JSON Generator On DataTable Ajax Call For Stock Vendor Listing
     *
     * @param start
     * @param length
     * @param draw
     * @param searchString
     * @return
     */

    @RequestMapping(value = "/StockVendor/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<StockVendor>> vendorListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populateStockVendorList(stockService.findAllVendorPaged(pageIndex, length, "mobile", false, searchString), draw), HttpStatus.OK);

    }

    /**
     * Get Measure Unit of Stock
     * Currently Used in Purchase Request
     *
     * @param rawId Stock Account Code
     * @return String
     */
    @RequestMapping(value = "/Stock/ajax/getMeasureUnit", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<String> getMeasureUnit(@RequestParam("rawId") String rawId) {

        String responseString = "";
        if (!rawId.equals("0")) {
            rawId = rawId.split("-")[1];
            responseString = stockService.getMeasureUnit(Integer.parseInt(rawId), true);
        }
        return new ResponseEntity(responseString, HttpStatus.OK);
    }

    /**
     * Print Stock Categories
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/Stock/printStockCategory", method = RequestMethod.GET)
    public String printStockCategory(Model model) {
        model.addAttribute("categories", stockService.getStockCategories());
        return "stock/printStockCategory";
    }

    /**
     * Print Stock Categories
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/Stock/printCategoryWithHead", method = RequestMethod.GET)
    public String printCategoryWithHead(Model model) {
        model.addAttribute("categories", stockService.getStockCategories());
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountListByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accounts", accountMap);
        return "stock/printCategoryWithHead";
    }

    /**
     * Get Measure Unit of Stock And Average Rate of Raw Material
     * Currently Used in  Add Recipe
     *
     * @param rawId Stock Account Code
     * @return String
     */
    @RequestMapping(value = "/Stock/ajax/getProductInfo", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<List<String>> getProductInfo(@RequestParam("rawId") String rawId) {

        List<String> responseStringList = new ArrayList<>();
        if (!rawId.equals("0")) {
            rawId = rawId.split("-")[1];
            responseStringList.add("0");
        }
        return new ResponseEntity(responseStringList, HttpStatus.OK);
    }
}
