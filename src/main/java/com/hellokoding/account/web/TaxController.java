package com.hellokoding.account.web;


import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.TaxDetail;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.CostCentreService;
import com.hellokoding.account.service.taxation.TaxDetailService;
import com.hellokoding.account.validator.TaxDetailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class TaxController {

    @Autowired
    private TaxDetailService taxDetailService;

    @Autowired
    private AccountService accountService;
    @Autowired
    private TaxDetailValidator taxDetailValidator;





    @RequestMapping(value = "/Account/addTaxDetails", method = RequestMethod.GET)
    public String addAccount(Model model) {
        model.addAttribute("taxDetailForm",new TaxDetail());
        model.addAttribute("accounts",accountService.findAll());
        model.addAttribute("taxDetails", taxDetailService.findAll());


        return "master/addtaxdetails";
    }

    @RequestMapping(value = "/Account/addTaxDetails", method = RequestMethod.POST)
    public String addAccount_post(@ModelAttribute("taxDetailForm") TaxDetail taxDetail,Model model, BindingResult bindingResult, RedirectAttributes attributes) {
        taxDetailValidator.validate(taxDetail, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("accounts",accountService.findAll());
            return "master/addtaxdetails";
        }
        taxDetailService.save(taxDetail);
        attributes.addFlashAttribute("taxDetailAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Taxation Details for Account with Code: <strong>"+taxDetail.getAccountCode()+" </strong>added successfully.</div>");
        return "redirect:/Account/addTaxDetails";
    }

    @RequestMapping(value = "/Account/removeDetail", method = RequestMethod.POST)
    public String removeTaxDetail(RedirectAttributes attributes,@RequestParam("accountCode") Integer accountCode) {

        taxDetailService.remove(accountService.findAccount(accountCode));

        attributes.addFlashAttribute("taxDetailRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Taxation Detail removed successfully.</div>");
        return "redirect:/Account/addTaxDetails";
    }


}
