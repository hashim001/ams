package com.hellokoding.account.web;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.CostCentre;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.plan.PaymentDueService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller for manipulation of Payment Dues
 */

@Controller
public class PaymentDueController {


    @Autowired
    private AccountService accountService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private LedgerCalculationService ledgerCalculationService;

    @Autowired
    private ChassisService chassisService;

    @Autowired
    private PaymentDueService paymentDueService;


    /**
     * Payment Due Ledger
     * GET
     * @param model
     * @return
     */
    @RequestMapping(value = "/Dues/paymentDueReport", method = RequestMethod.GET)
    public String ledgerReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("soldApartments", chassisService.findSoldApartment());
        return "plan/paymentDueLedger";
    }


    /**
     * Payment Due Ledger
     * POST
     * @param model
     * @param fromDate
     * @param toDate
     * @return
     */
    @RequestMapping(value = "/Dues/paymentDueReport", method = RequestMethod.POST)
    public String ledgerReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("apartmentNo") String apartmentNo) {


        if(apartmentNo.isEmpty()){
            return "error";
        }
        if(apartmentNo.equals("0")){
            return "error";
        }
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentApartment", apartmentNo);
        Map accountMap = new HashMap();
        List<Account> accountList = accountService.findAccountsByLevel(4);
        for (Account account : accountList) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("soldApartments", chassisService.findSoldApartment());
        model.addAttribute("vouchers", ledgerCalculationService.getSortedDueVouchers(apartmentNo,  fromDate, toDate,false));


        return "plan/paymentDueLedger";
    }


    /**
     * Payment Due Lates Ledger
     * GET
     * @param model
     * @return
     */
    @RequestMapping(value = "/Dues/lateDueReport", method = RequestMethod.GET)
    public String lateLedgerReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("soldApartments", chassisService.findSoldApartment());
        return "plan/lateDueLedger";
    }


    /**
     * Payment Due Late Ledger
     * POST
     * @param model
     * @param fromDate
     * @param toDate
     * @return
     */
    @RequestMapping(value = "/Dues/lateDueReport", method = RequestMethod.POST)
    public String lateLedgerReport_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("apartmentNo") String apartmentNo) {


        if(apartmentNo.isEmpty()){
            return "error";
        }
        if(apartmentNo.equals("0")){
            return "error";
        }
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentApartment", apartmentNo);
        Map accountMap = new HashMap();
        List<Account> accountList = accountService.findAccountsByLevel(4);
        for (Account account : accountList) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("soldApartments", chassisService.findSoldApartment());
        model.addAttribute("vouchers", ledgerCalculationService.getSortedDueVouchers(apartmentNo,  fromDate, toDate,true));


        return "plan/lateDueLedger";
    }


    /**
     * Generate Payment Due entries for late Payments
     * Hit by Cron
     * @return Message
     */
    @RequestMapping(value = "/Cron/generateLateEntries",method = RequestMethod.GET)
    @ResponseBody
    public String generateLateEntries(){
         return paymentDueService.generateLateEntries();
    }
}
