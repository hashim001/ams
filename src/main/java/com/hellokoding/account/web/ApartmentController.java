package com.hellokoding.account.web;

import com.hellokoding.account.model.*;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.security.RoleSupport;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.CustomerService;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.service.voucher.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@Controller
@SessionAttributes({"apartmentHoldForm", "apartmentForm"})
public class ApartmentController {
    /**
     * GET for Listing of Chassis
     *
     * @param model Data Model
     * @return Response Page with listing.
     */
    @Autowired
    private ChassisService chassisService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private StockService stockService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private DistributorService distributorService;
    @Autowired
    private StaticInfoService staticInfoService;
    @Autowired
    private VoucherService voucherService;

    @RequestMapping(value = "/Apartment/viewApartmentList", method = RequestMethod.GET)
    public String viewApartmentList(Model model) {
        Map accountMap = new HashMap();

        for (Account account : accountService.getCustomerAccountList()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }


        model.addAttribute("apartmentForm", new Apartment());
        model.addAttribute("apartmentHoldForm", new ApartmentHold());
        model.addAttribute("stockList", stockService.findAllStock());
        model.addAttribute("accountMap", accountMap);
        if (RoleSupport.hasRole("ROLE_MASTER_ADMIN")) {
            model.addAttribute("apartmentList", chassisService.findAll());
        } else {
            model.addAttribute("apartmentList", chassisService.findAllAvailableApartments());
        }
        model.addAttribute("customerList", customerService.findAll());
        model.addAttribute("bankAccounts", accountService.getBankAccountList());
        List<Distributor> distributorList = new ArrayList<>();
        if (RoleSupport.hasRole("ROLE_MASTER_ADMIN")) {
            distributorList = distributorService.findAll();
        } else {
            distributorList.add(RoleSupport.getUserAuth().getDistributor());
        }
        model.addAttribute("salesmanList", distributorList);
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));

        return "apartment/viewApartmentList";
    }

    /**
     * Ajax Call to Edit Apartment by Apartment
     *
     * @param apartmentId
     * @return Apartment
     */

    @RequestMapping(value = "/Apartment/editApartment", method = RequestMethod.GET)
    public @ResponseBody
    Apartment editApartment(@RequestParam("apartmentId") int apartmentId) {
        Apartment apartment = chassisService.findByChassisId(apartmentId);
        ApartmentHold apartmentHold = chassisService.findByApartment(apartment);
        if (apartmentHold != null) {
            apartment.setApartmentHoldId(apartmentHold.getApartmentHoldId());
            apartment.setOnHoldDays(apartmentHold.getOnHoldDays());
            apartment.setRemarks(apartmentHold.getRemarks());
            apartment.setCustomerName(apartmentHold.getCustomerName());
            apartment.setCnic(apartmentHold.getCnic());
            apartment.setMobile(apartmentHold.getMobile());
            apartment.setHoldDate(apartmentHold.getHoldDate());
            apartment.setSalesmanId(apartmentHold.getSalesmanId());
            apartment.setApartmentHold(null);
        }
        apartment.setStockId(apartment.getStock().getStockId());
        apartment.setPaymentPlan(null);
        apartment.setStock(null);
        return apartment;
    }

    /**
     * Inserts in Apartment in Edit case
     * No Voucher Created
     *
     * @return
     */
    @RequestMapping(value = "/Apartment/editApartment", method = RequestMethod.POST)
    public String editApartment_post(@ModelAttribute("apartmentForm") Apartment apartment, RedirectAttributes attributes) {
        apartment.setStock(stockService.findByStockId(apartment.getStockId()));
        chassisService.save(apartment);
        ApartmentHold apartmentHold = new ApartmentHold();
        //   StaticInfo staticInfo = staticInfoService.findStaticInfo();
        //   if (staticInfo.getHoldAccount() > 0 && apartment.isHold()) {
        if (apartment.isHold()) {
            if (apartment.getApartmentHoldId() > 0) {
                apartmentHold.setApartmentHoldId(apartment.getApartmentHoldId());
            }
            apartmentHold.setOnHoldDays(apartment.getOnHoldDays());
            apartmentHold.setRemarks(apartment.getRemarks());
            apartmentHold.setCustomerName(apartment.getCustomerName());
            apartmentHold.setCnic(apartment.getCnic());
            apartmentHold.setMobile(apartment.getMobile());
            apartmentHold.setHoldDate(apartment.getHoldDate());
            apartmentHold.setSalesmanId(apartment.getSalesmanId());
            apartmentHold.setApartment(apartment);
            chassisService.saveApartmentHold(apartmentHold);
        } else {
            if (apartment.getApartmentHoldId() > 0) {
                chassisService.removeHoldApartment(chassisService.findByApartment(apartment));
            }
        }

        // br voucher for hold amount
    /*        List<Voucher> voucherList = voucherService.findByChassisNumber(apartment.getApartmentNo());
            for (Voucher voucher : voucherList) {
                voucherService.deleteVouchers(voucher.getVoucherNumber());
            }
            Voucher voucher = new Voucher();
            voucher.setVoucherNumber(voucherService.generateVoucherNumber("BR"));
            voucher.setVoucherDate(apartmentHold.getHoldDate());
            voucher.setAccountCode(staticInfo.getHoldAccount());
            voucher.setBankAccount(apartmentHold.getBankAccount());
            voucher.setCredit(apartmentHold.getOnholdAmount());
            voucher.setChassisNo(apartment.getApartmentNo());
            voucher.setRemarks(apartmentHold.getRemarks());
            voucherService.save(voucher);
            Voucher bankVoucher = new Voucher();
            bankVoucher.setVoucherNumber(voucher.getVoucherNumber());
            bankVoucher.setVoucherDate(apartmentHold.getHoldDate());
            bankVoucher.setAccountCode(voucher.getBankAccount());
            bankVoucher.setDebit(apartmentHold.getOnholdAmount());
            bankVoucher.setChassisNo(apartment.getApartmentNo());
            bankVoucher.setRemarks(apartmentHold.getRemarks());
            voucherService.save(bankVoucher);*/
        //   }

        attributes.addFlashAttribute("ApartmentAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Apartment with number " + apartment.getApartmentDetail() + " updated successfully.</div>");
        return "redirect:/Apartment/viewApartmentList";
    }

    /**
     * Ajax Call to Hold Apartment by Apartment
     *
     * @param apartmentId
     * @return Apartmnet hold
     */

    @RequestMapping(value = "/Apartment/holdApartment", method = RequestMethod.GET)
    public @ResponseBody
    ApartmentHold holdApartment(@RequestParam("apartmentId") int apartmentId) {
        Apartment apartment = chassisService.findByChassisId(apartmentId);
        ApartmentHold apartmentHold = chassisService.findByApartment(apartment);
        if (apartmentHold != null) {
            apartmentHold.setApartmentId(apartmentId);
            apartmentHold.setApartment(null);
        } else {
            apartmentHold = new ApartmentHold();
            apartmentHold.setApartmentId(apartmentId);
        }
        return apartmentHold;
    }

    /**
     * Inserts in Apartment hold
     *
     * @return
     */
    @RequestMapping(value = "/Apartment/holdApartment", method = RequestMethod.POST)
    public String holdApartment_post(@ModelAttribute("apartmentHoldForm") ApartmentHold apartmentHold) {
        Apartment apartment = chassisService.findByChassisId(apartmentHold.getApartmentId());
        apartment.setHold(true);
        chassisService.save(apartment);
        apartmentHold.setApartment(apartment);
        chassisService.saveApartmentHold(apartmentHold);
        return "redirect:/Apartment/viewApartmentList";
    }

    /**
     * Delete Apartment
     *
     * @param attributes
     * @param apartmentId
     * @return
     */
    @RequestMapping(value = "/Apartment/removeApartment", method = RequestMethod.POST)
    public String removeApartment(RedirectAttributes attributes, @RequestParam("apartmentId") Integer apartmentId) {

        chassisService.remove(apartmentId);

        attributes.addFlashAttribute("apartmentRemoveSuccess", " <div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Plot removed successfully.</div>");
        return "redirect:/Apartment/viewApartmentList";
    }

    /**
     * Get apartment list for selected category id
     *
     * @param categoryId customer accountCode
     * @return
     */
    @RequestMapping(value = "/Apartment/getApartmentList", method = RequestMethod.GET)
    public @ResponseBody
    Map<Integer,String> getApartmentMap(@ModelAttribute("categoryId") int categoryId) {

        Map<Integer,String> apartmentMap = new HashMap<>();
        List<Stock> stockList;
        if(categoryId == 0){
            stockList = stockService.findAllStock();
        } else {
            stockList = stockService.findByCategoryId(categoryId).getStockList();
        }
        stockList.forEach(s->s.getApartmentList().forEach(apartment -> {
            apartmentMap.put(apartment.getApartmentId(), apartment.getApartmentDetail());
        }));
        return apartmentMap;
    }

    @RequestMapping(value = "/Apartment/getPhaseList", method = RequestMethod.GET)
    public @ResponseBody
    Map<Integer, String> getPhaseList(@ModelAttribute("categoryId") int categoryId) {
        Map<Integer, String> phaseMap = new HashMap<>();
        List<Stock> stockList;
        if(categoryId == 0){
            stockList = stockService.findAllStock();
        } else {
            stockList = stockService.findByCategoryId(categoryId).getStockList();
        }
        for (Stock stock : stockList) {
            if (!phaseMap.containsKey(stock.getStockId())) {
                phaseMap.put(stock.getStockId(), stock.getName());
            }
        }
        return phaseMap;
    }

    @RequestMapping(value = "/Apartment/getPlotList", method = RequestMethod.GET)
    public @ResponseBody
    Map<Integer, String> getApartmentList(@ModelAttribute("phaseId") int phaseId) {
        Map<Integer, String> apartmentMap = new HashMap<>();
        Stock stock = stockService.findByStockId(phaseId);
        List<Apartment> apartmentList;
        if(stock != null){
            apartmentList = stock.getApartmentList();
        }
        else {
            apartmentList = new ArrayList<>();
        }
        for (Apartment apartment : apartmentList) {
            if (!apartmentMap.containsKey(apartment.getApartmentId())) {
                apartmentMap.put(apartment.getApartmentId(), apartment.getApartmentDetail());
            }
        }
        return apartmentMap;
    }
}
