package com.hellokoding.account.web;

import com.hellokoding.account.model.*;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.saleleads.SaleLeadService;
import com.hellokoding.account.service.security.RoleSupport;
import com.hellokoding.account.service.stock.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class LeadController {


    @Autowired
    private SaleLeadService saleLeadService;

    @Autowired
    private StockService stockService;

    @Autowired
    private DistributorService distributorService;

    /* Listing for Sale Leads and form
     * GET
     *
     * @param model Data Model
     * @return Response Page with listing.
     */
    @RequestMapping(value = "/SaleLead/addSaleLead", method = RequestMethod.GET)
    public String addSaleLead(Model model) {

        SaleLead saleLead = new SaleLead();
        model.addAttribute("leadForm", saleLead);
        if(RoleSupport.hasRole("ROLE_SALESMAN")){
            model.addAttribute("saleLeadList", saleLeadService.findBySalesmanId(RoleSupport.getUserAuth().getDistributor().getDistributorId()));
        }
        else if(RoleSupport.hasRole("ROLE_MASTER_ADMIN")){
            model.addAttribute("saleLeadList", saleLeadService.findAll());
            List<Distributor> distributorList = distributorService.findAll();
            model.addAttribute("salesmanMap", distributorList.stream().collect(Collectors.toMap(Distributor::getDistributorId, Distributor::getName)));
            model.addAttribute("salesmanList", distributorList);
        }
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        model.addAttribute("modelList", stockService.findAllStock());

        return "salelead/addSaleLead";
    }

    /**
     * Sale Lead Insertion
     * POST
     *
     * @param saleLead  Form
     * @param attributes for message
     * @return Response Page
     */
    @RequestMapping(value = "/SaleLead/addSaleLead", method = RequestMethod.POST)
    public String addSaleLead_post(@ModelAttribute("leadForm") SaleLead saleLead, RedirectAttributes attributes) {


            attributes.addFlashAttribute("saleLeadMessage", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Lead Added successfully.</div>");
            if(RoleSupport.hasRole("ROLE_SALESMAN")){
                saleLead.setSalesmanId(RoleSupport.getUserAuth().getDistributor().getDistributorId());
            }
            saleLeadService.save(saleLead);
            return "redirect:/SaleLead/addSaleLead";

    }

    /**
     * Remove Sale lead by Id
     *
     * @param leadId Exemption Identity
     * @param attributes  Redirect Message
     * @return Redirect to GET
     */
    @RequestMapping(value = "/SaleLead/remove", method = RequestMethod.POST)
    public String removeExemption(@RequestParam("leadId") int leadId,RedirectAttributes attributes) {

            saleLeadService.removeLead(leadId);
            attributes.addFlashAttribute("saleLeadMessage", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Sale lead deleted successfully.</div>");
            return "redirect:/SaleLead/addSaleLead";

    }

    /**
     * Ajax Call to Edit SaleLead by leadId
     *
     * @param leadId identity
     * @return SaleLead
     */

    @RequestMapping(value = "/SaleLead/edit", method = RequestMethod.GET)
    public @ResponseBody
    SaleLead editExemption(@RequestParam("leadId") int leadId) {
        return saleLeadService.findByLeadId(leadId);
    }

    /**
     * Get List of Model internals (Apartments or Shops)
     * @param stockId
     * @return
     */
    @RequestMapping(value = "/SaleLead/getInternalList", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> getInternalList(@RequestParam("stockId") int stockId) {
        Stock stock = stockService.findByStockId(stockId);
        if(stock ==  null){
            return null;
        }
        return stock.getApartmentList()
                .stream()
                .collect(Collectors.toMap(Apartment::getApartmentNo , Apartment::getApartmentNo));
    }


}
