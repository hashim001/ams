package com.hellokoding.account.web;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Calendar;

@Controller
public class InventoryReportController {


    @RequestMapping(value = "/Inventory/purchaseReport", method = RequestMethod.GET)
    public String inventoryPurchaseReport(Model model) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("fromDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("currentDate", date);
        //model.addAttribute("reportModel", new ArrayList<Challan>());
        model.addAttribute("reportType","GATE");

        return "inventoryReport/purchaseReport";
    }

    @RequestMapping(value = "/Inventory/purchaseReport", method = RequestMethod.POST)
    public String inventoryPurchaseReport_post(Model model, @RequestParam("fromDate") java.sql.Date fromDate
            , @RequestParam("toDate") java.sql.Date toDate
            , @RequestParam("reportType") String reportType) {


        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        //model.addAttribute("reportModel", challanService.findReportChallan(fromDate,toDate,reportType));
        model.addAttribute("reportType", reportType);

        return "inventoryReport/purchaseReport";
    }

}
