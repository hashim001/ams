package com.hellokoding.account.web;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.AccountNote;
import com.hellokoding.account.model.NoteSubHeading;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.note.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@Controller
@SessionAttributes({"AccountNoteForm","noteSubForm"})
public class NoteController {


    @Autowired
    private NoteService noteService;

    @Autowired
    private AccountService accountService;


    /////////////////////////////////////////Account Note ///////////////////////////////////
    @RequestMapping(value = "/Account/addAccountNote", method = RequestMethod.GET)
    public String addAccountNote(Model model) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        AccountNote accountNote = new AccountNote();
        model.addAttribute("AccountNoteForm", accountNote);
        model.addAttribute("currentDate" , date);
        model.addAttribute("accountNotes" , noteService.findAll());

        return "master/addAccountNote";
    }

    @RequestMapping(value = "/Account/addAccountNote", method = RequestMethod.POST)
    public String addAccountNote_post(@ModelAttribute("AccountNoteForm") AccountNote accountNote,RedirectAttributes attributes) {

        accountNote.setStatus(1);
        noteService.saveNote(accountNote);
        attributes.addFlashAttribute("accountNoteRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Account Note Added Successfully.</div>");
        return "redirect:/Account/addAccountNote";
    }


    ////////////////////////////////////Testing Account Note /////////////////////////////////

    @RequestMapping(value = "/Account/accountNote", method = RequestMethod.GET)
    public String accountNote(Model model,@RequestParam(name = "noteId" ,defaultValue = "0") Integer noteId) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        AccountNote accountNote = noteService.findByNoteId(noteId);
        Map<Integer,List> levelAccounts= new HashMap<>();

        for (NoteSubHeading noteSubHeading : accountNote.getNoteSubHeadings()) {
            levelAccounts.put(noteSubHeading.getSubId(), accountService.findAccountsByLevel(noteSubHeading.getLevel()));
        }

        model.addAttribute("AccountNoteForm", accountNote);
        model.addAttribute("levelAccounts",levelAccounts);
        model.addAttribute("currentDate" , date);
        model.addAttribute("noteId", noteId);
        model.addAttribute("accountNotes" , noteService.findAll());

        return "master/accountNote";
    }

    @RequestMapping(value = "/Account/accountNote", method = RequestMethod.POST)
    public String accountNote_post(@ModelAttribute("AccountNoteForm") AccountNote accountNote,RedirectAttributes attributes) {

        accountNote.setStatus(1);
        noteService.saveNote(accountNote);
        attributes.addFlashAttribute("accountNoteRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Account Note Added Successfully.</div>");
        return "redirect:/Account/accountNote?noteId="+accountNote.getNoteId();
    }


    ////////////////////////////////////// Testing Add Sub Heading - Account Note ////////////////////////////////////////
    @RequestMapping(value = "/Account/addSubheading", method = RequestMethod.POST)
    public String addSubHeading_post(@RequestParam(name = "heading" ,defaultValue = "0") String heading
            ,@RequestParam(name = "level" ,defaultValue = "0") Integer level
            ,@RequestParam(name = "noteId" ,defaultValue = "0") Integer noteId
            ,@RequestParam(name = "operation" ,defaultValue = "Net") String operation) {

        AccountNote accountNote = noteService.findByNoteId(noteId);
        NoteSubHeading noteSubHeading = new NoteSubHeading();
        noteSubHeading.setHeading(heading);
        noteSubHeading.setLevel(level);
        noteSubHeading.setOperation(operation);
        if(!accountNote.getNoteSubHeadings().contains(noteSubHeading)) {
            accountNote.getNoteSubHeadings().add(noteSubHeading);
        }
        noteService.saveNote(accountNote);

        return "redirect:/Account/accountNote?noteId="+noteId;
    }


    @RequestMapping(value = "/Account/removeNote", method = RequestMethod.POST)
    public String removeNote_post(@RequestParam("noteId") Integer noteId,RedirectAttributes attributes) {

        noteService.removeNote(noteService.findByNoteId(noteId));
        attributes.addFlashAttribute("accountNoteRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Account Note Removed Successfully.</div>");
        return "redirect:/Account/addAccountNote";
    }


    /////////////////////////////////////////Account Note Subheading///////////////////////////////////





    @RequestMapping(value = "/Account/removeNoteSubHead", method = RequestMethod.POST)
    public String removeSub_post(Model model,@RequestParam("subId") Integer subId,@RequestParam("noteId") Integer noteId,RedirectAttributes attributes) {

        NoteSubHeading noteSubHeading = noteService.findBySubId(subId);
        noteService.removeNoteSubHead(noteSubHeading);
        attributes.addFlashAttribute("subHeadRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Account Note Subheading Removed Successfully.</div>");
        return "redirect:/Account/accountNote?noteId="+noteId;
    }


    /////////////////////////////////////////Subheading Accounts///////////////////////////////////



    @RequestMapping(value = "/Account/addSubHeadAccount", method = RequestMethod.POST)
    public String addSubHeadingAccount_post(Model model,@RequestParam(name = "subId" ,defaultValue = "0") Integer subId
    ,@RequestParam(name = "accountCode" ,defaultValue = "0") Integer accountCode
            ,@RequestParam(name = "noteId" ,defaultValue = "0") Integer noteId
            ,RedirectAttributes attributes) {

        NoteSubHeading noteSubHeading = noteService.findBySubId(subId);
        Account account = accountService.findAccount(accountCode);
        if(!noteSubHeading.getAccounts().contains(account)) {
            noteSubHeading.getAccounts().add(account);
            noteService.saveNoteSubHead(noteSubHeading);
            attributes.addFlashAttribute("subHeadAccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Subheading Account added Successfully.</div>");
        }else{
            attributes.addFlashAttribute("subHeadAccountAddSuccess", "<div style=\"background-color: #e8c8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Account already exists in subheading.</div>");
        }


        return "redirect:/Account/accountNote?noteId="+noteId;
    }


    @RequestMapping(value = "/Account/removeSubHeadAccount", method = RequestMethod.POST)
    public String addSubHeadingAccount_post(@RequestParam(name = "subId" ,defaultValue = "0") Integer subId
            ,@RequestParam(name = "accountCode" ,defaultValue = "0") Integer accountCode
            ,@RequestParam(name = "noteId" ,defaultValue = "0") Integer noteId
            ,RedirectAttributes attributes) {

        NoteSubHeading noteSubHeading = noteService.findBySubId(subId);
        noteSubHeading.getAccounts().remove(accountService.findAccount(accountCode));
        noteService.saveNoteSubHead(noteSubHeading);

        attributes.addFlashAttribute("subHeadAccountRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Subheading Account removed Successfully.</div>");
        return "redirect:/Account/accountNote?noteId="+noteId;
    }





}
