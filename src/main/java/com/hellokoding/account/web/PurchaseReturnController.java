package com.hellokoding.account.web;


import com.hellokoding.account.model.*;
import com.hellokoding.account.purchase.ReturnProduct;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.voucher.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@Controller
@SessionAttributes({"voucherList"})
public class PurchaseReturnController {

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private AccountService accountService;


    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private JSONPopulateService jsonPopulateService;



    /**
     * Listing For Purchase Return Vouchers
     * List By Ajax Call
     *
     * @return Response Page
     */
    @RequestMapping(value = "/PurchaseReturn/viewReturnVoucher", method = RequestMethod.GET)
    public String viewReturnVoucher() {

        return "purchasetrade/returnListing";
    }


    /**
     * Create Purchase Voucher
     * GET
     *
     * @param model data Model
     * @return Response Page with Form
     */
    @RequestMapping(value = "/PurchaseReturn/createReturnVoucher", method = RequestMethod.GET)
    public String addPurchaseVoucher(Model model) {

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        VoucherList voucherList = new VoucherList();
        for (int i = 0; i < staticInfo.getMaxFormRows(); i++) {
            voucherList.add(new Voucher());
        }

        model.addAttribute("voucherList", voucherList);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("RT"));
        //model.addAttribute("rawMaterialList", rawMaterialService.findAll());
        model.addAttribute("accounts", accountService.getVendorAccountList());
        // CHANGE THIS TO VALID PO
        //model.addAttribute("orders", purchaseOrderService.findAll("Purchase"));
        model.addAttribute("cashInHand", staticInfo.getCashInHand());

        return "purchasetrade/addReturn";
    }


    /**
     * POST for Purchase Return Vouchers
     *
     * @param voucherList Voucher Model
     * @param attributes
     * @return Redirect to Listing
     */
    @RequestMapping(value = "/PurchaseReturn/createReturnVoucher", method = RequestMethod.POST)
    public String addPurchaseVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        Double supplierDebitAmount = 0.0;
        Voucher supplierVoucher = new Voucher();
        for (Voucher voucher : voucherList.getVoucherList()) {

            if (!voucher.getTransitionCode().equals("0") && voucher.getCredit() > 0 && voucher.isCheck()) {
                supplierDebitAmount += voucher.getCredit();
                supplierVoucher.setAccountCode(voucher.getAccountCode());
                supplierVoucher.setVoucherNumber(voucher.getVoucherNumber());
                supplierVoucher.setBillDate(voucher.getBillDate());
                supplierVoucher.setBillNumber(voucher.getBillNumber());
                supplierVoucher.setVoucherDate(voucher.getVoucherDate());

                String[] transitionList = voucher.getTransitionCode().split("-");

                voucher.setAccountCode(Integer.parseInt(transitionList[0]));
                voucher.setItemAccount(Integer.parseInt(transitionList[1]));
                voucher.setReferenceVoucher(voucherList.getReferenceNumber());
                voucherService.saveJournal(voucher);

                //orderProductService.updateReturnQuantity(voucher.getProductId(), voucher.getItemQuantity());

            }

        }

        if (supplierVoucher.getVoucherNumber() != null) {
            voucherNumber = supplierVoucher.getVoucherNumber();
            supplierVoucher.setDebit(supplierDebitAmount);
            supplierVoucher.setReferenceVoucher(voucherList.getReferenceNumber());
            voucherService.saveJournal(supplierVoucher);
        }
        if (voucherNumber.equals("")) {
            attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">No Purchase voucher added.</div>");
            return "redirect:/PurchaseReturn/createReturnVoucher";
        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Purchase Vouchers with number <strong>" + voucherNumber + " </strong>added successfully.</div>");
        return "redirect:/PurchaseReturn/viewReturnVoucher";
    }


    /**
     * GET for Update
     *
     * @param model         data Model
     * @param voucherNumber Voucher Number for Update
     * @return
     */
    @RequestMapping(value = "/PurchaseReturn/updateReturn", method = RequestMethod.GET)
    public String updatePurchaseVoucher(Model model, @RequestParam(value = "voucherNumber") String voucherNumber) {

        return "purchasetrade/updateReturn";
    }


    /**
     * POST Call for Purchase Return
     *
     * @param voucherList   Data Model
     * @param voucherNumber
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/PurchaseReturn/updateReturn", method = RequestMethod.POST)
    public String updatePurchaseVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, @RequestParam("voucherNumber") String voucherNumber, RedirectAttributes attributes) {

        return "redirect:/PurchaseReturn/viewReturnVoucher";
    }



    /**
     * Ajax Call For Return Vouchers
     *
     * @param start
     * @param length
     * @param draw
     * @param searchString
     * @return
     */
    @RequestMapping(value = "/PurchaseReturn/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> returnVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populateReturnVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherNumber", false, searchString, "RT"), draw), HttpStatus.OK);


    }



}
