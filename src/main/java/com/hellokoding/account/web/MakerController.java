package com.hellokoding.account.web;

import com.hellokoding.account.model.Maker;
import com.hellokoding.account.service.maker.MakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MakerController {
    @Autowired
    private MakerService makerService;

    /**
     * GET for Listing of Makers and Form
     *
     * @param model Data Model
     * @return Response Page with listing and new Maker form.
     */
    @RequestMapping(value = "/Maker/addMaker", method = RequestMethod.GET)
    public String addMaker(Model model) {
        model.addAttribute("makerForm", new Maker());
        model.addAttribute("makers", makerService.findAllMakers());
        return "maker/addMaker";
    }

    /**
     * POST for Maker Insertion
     *
     * @param maker      Form
     * @param attributes for message
     * @return Response Page
     */
    @RequestMapping(value = "/Maker/addMaker", method = RequestMethod.POST)
    public String addMaker_post(@ModelAttribute("makerForm") Maker maker, RedirectAttributes attributes) {

        makerService.save(maker);
        attributes.addFlashAttribute("locationAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Make <strong>" + maker.getMakerName() + " </strong> added successfully.</div>");
        return "redirect:/Maker/addMaker";
    }


    /**
     * Ajax Call to Edit Maker by makerId
     *
     * @param makerId
     * @return Maker
     */

    @RequestMapping(value = "/Maker/editMaker", method = RequestMethod.GET)
    public @ResponseBody
    Maker editMaker(@RequestParam("makerId") int makerId) {
        Maker maker = makerService.findByMakerId(makerId);
        maker.setStockList(null);
        return maker;
    }

}
