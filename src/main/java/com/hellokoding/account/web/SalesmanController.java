package com.hellokoding.account.web;


/*
 NOTE :DISTRIBUTOR ACTING AS SALES MAN
 */


import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.*;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.ledger.TrialBalanceService;
import com.hellokoding.account.service.security.RoleSupport;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.CustomerService;
import com.hellokoding.account.service.stock.VendorService;
import com.hellokoding.account.service.user.RegistrationService;
import com.hellokoding.account.service.user.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import java.util.*;

@Controller
public class SalesmanController {

    @Autowired
    private DistributorService distributorService;


    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TrialBalanceService trialBalanceService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private VendorService vendorService;

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private ChassisService chassisService;

    /**
     * Distributor (Salesman) creation
     * GET
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/Salesman/addSalesman", method = RequestMethod.GET)
    public String addDistributor(Model model) {

        model.addAttribute("salesmanForm", new Distributor());
        model.addAttribute("currentDate", new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        model.addAttribute("distributors", distributorService.findAll());
        return "distributor/addDistributor";

    }


    /**
     * Distributor (Salesman Add)
     * POST
     *
     * @param attributes
     * @return Response and Rider Listing Page
     */
    @RequestMapping(value = "/Salesman/addSalesman", method = RequestMethod.POST)
    public String addDistributorAccount_post(@ModelAttribute("salesmanForm") Distributor distributor, RedirectAttributes attributes) {
        // UserAuth insertion
        if (distributor.getDistributorId() == null) {
            UserAuth userAuth = new UserAuth();
            List<Role> roles = new ArrayList<>();
            roles.add(registrationService.findByName("ROLE_SALESMAN"));
            userAuth.setRoles(roles);
            userAuth.setPassword("sale1234");
            userAuth.setEmailId(distributor.getName().replace(" ", "_") + "@test.com");
            userAuth.setUserName(distributor.getName().replace(" ", "_"));
            userAuth = userAuthService.save(userAuth);
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Salesman with title: <strong>" + distributor.getName() + " </strong>, Username :<strong>" + userAuth.getUserName() + "</strong> and temporary Password: <strong>'sale1234'</strong> added successfully.</div>");
            distributor.setUserAuth(userAuth);
        }
        else{
            attributes.addFlashAttribute("AccountAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Salesman with title: <strong>" + distributor.getName() + " </strong> updated successfully.</div>");
        }
        distributorService.save(distributor);
        return "redirect:/Salesman/addSalesman";

    }


    /**
     * Distributor Remove
     *
     * @param attributes    Redirect Messages
     * @param distributorId To fetch Distributor
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/removeDistributor", method = RequestMethod.POST)
    public String removeDistributor(RedirectAttributes attributes, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId) {

        distributorService.remove(distributorId);
        attributes.addFlashAttribute("distributorRemoveSuccess", "<div style=\"background-color: #d89d8b;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #800000;border-radius: 5px;font-size: initial;\">Salesman Removed Successfully.</div>");
        return "redirect:/Salesman/addSalesman";

    }

    /**
     * Ajax Call to Edit Distributor (Salesman) by locationId
     *
     * @param distributorId
     * @return Location
     */

    @RequestMapping(value = "/Distributor/editDistributor", method = RequestMethod.GET)
    public @ResponseBody
    Distributor editDistributor(@RequestParam("distributorId") int distributorId) {
        return distributorService.findByDistributorId(distributorId);
    }


    /**
     * Listing Of Distributor's Registered Customers
     *
     * @param model
     * @param distributorId Get request Id
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/addCustomer", method = RequestMethod.GET)
    public String addDistributorCustomer(Model model, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId) {

        Distributor distributor = distributorService.findByDistributorId(distributorId);
        if (distributor == null) {
            return "error";
        }

        model.addAttribute("distributor", distributor);
        return "distributor/distributorCustomers";

    }

    /**
     * Distributor Customer Add POST
     *
     * @param attributes    For Success Message
     * @param distributorId Distributor Identification
     * @param customerId    Customer Identification
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/addCustomer", method = RequestMethod.POST)
    public String addDistributorCustomer_post(RedirectAttributes attributes, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId
            , @RequestParam(name = "customerId", defaultValue = "0") Integer customerId) {

        Distributor distributor = distributorService.findByDistributorId(distributorId);
        StockCustomer stockCustomer = customerService.findByCustomerId(customerId);
        if (distributor == null || stockCustomer == null) {
            return "error";
        }
        customerService.save(stockCustomer);
        attributes.addFlashAttribute("distributorAddSuccess", "<div style=\"background-color: #d6ffbc;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #bbb5b5;border-radius: 5px;font-size: initial;\">Customer For Distributor " + distributor.getName() + " added Successfully.</div>");
        return "redirect:/Distributor/addCustomer?distributorId=" + distributorId;

    }


    /**
     * Remove Customer from Distributor List
     *
     * @param attributes
     * @param distributorId Distributor ID For Redirection
     * @param customerId    CustomerID for Removal
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/removeCustomer", method = RequestMethod.POST)
    public String removeCustomer_post(RedirectAttributes attributes, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId
            , @RequestParam(name = "customerId", defaultValue = "0") Integer customerId) {

        StockCustomer stockCustomer = customerService.findByCustomerId(customerId);
        if (stockCustomer == null) {
            return "error";
        }
        customerService.save(stockCustomer);
        attributes.addFlashAttribute("customerRemoveSuccess", "<div style=\"background-color: #d6ffbc;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #bbb5b5;border-radius: 5px;font-size: initial;\">Customer Removed Successfully.</div>");
        return "redirect:/Distributor/addCustomer?distributorId=" + distributorId;

    }


    /**
     * Listing Of Distributor's Registered Vendors
     *
     * @param model         To send data to view
     * @param distributorId Get request Id
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/addVendor", method = RequestMethod.GET)
    public String addDistributorVendor(Model model, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId) {

        Distributor distributor = distributorService.findByDistributorId(distributorId);
        if (distributor == null) {
            return "error";
        }
        model.addAttribute("distributorVendors", vendorService.findCurrentVendors(distributor));
        model.addAttribute("availableVendors", vendorService.findAvailableVendors(false));
        model.addAttribute("distributor", distributor);
        return "distributor/distributorVendors";

    }

    /**
     * Distributor Vendor Add POST
     *
     * @param attributes    For Success Message
     * @param distributorId Distributor Identification
     * @param vendorId      Vendor Identification
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/addVendor", method = RequestMethod.POST)
    public String addDistributorVendor_post(RedirectAttributes attributes, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId
            , @RequestParam(name = "vendorId", defaultValue = "0") Integer vendorId) {

        Distributor distributor = distributorService.findByDistributorId(distributorId);
        StockVendor stockVendor = vendorService.findByVendorId(vendorId);
        if (distributor == null || stockVendor == null) {
            return "error";
        }
        stockVendor.setVendorDistributor(distributor);
        vendorService.save(stockVendor);
        attributes.addFlashAttribute("vendorAddSuccess", "<div style=\"background-color: #d6ffbc;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #bbb5b5;border-radius: 5px;font-size: initial;\">Vendor For Distributor " + distributor.getName() + " added Successfully.</div>");
        return "redirect:/Distributor/addVendor?distributorId=" + distributorId;

    }

    /**
     * Remove Vendor from Distributor List
     *
     * @param attributes
     * @param distributorId Distributor ID For Redirection
     * @param vendorId      VendorID for Removal
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/removeVendor", method = RequestMethod.POST)
    public String removeVendor_post(RedirectAttributes attributes, @RequestParam(name = "distributorId", defaultValue = "0") Integer distributorId
            , @RequestParam(name = "vendorId", defaultValue = "0") Integer vendorId) {

        StockVendor stockVendor = vendorService.findByVendorId(vendorId);
        if (stockVendor == null) {
            return "error";
        }
        stockVendor.setVendorDistributor(null);
        vendorService.save(stockVendor);
        attributes.addFlashAttribute("vendorRemoveSuccess", "<div style=\"background-color: #d6ffbc;height: 40px;text-align: center;padding-top: 5px;border: 2px solid #bbb5b5;border-radius: 5px;font-size: initial;\">Vendor Removed Successfully.</div>");
        return "redirect:/Distributor/addVendor?distributorId=" + distributorId;

    }

    /**
     * To check Distributor's Customer Balances . GET Request
     *
     * @param model
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/customerBalance", method = RequestMethod.GET)
    public String customerBalance(Model model) {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", date);
        model.addAttribute("distributors", distributorService.findAll());
        model.addAttribute("currentDistributor", 0);
        model.addAttribute("trialModel", null);
        return "distributor/customerBalance";
    }

    /**
     * POST call for Distributor's Customer Balance
     *
     * @param model
     * @param toDate        Balance Till date
     * @param distributorId Rider Identification
     * @return Response Page
     */
    @RequestMapping(value = "/Distributor/customerBalance", method = RequestMethod.POST)
    public String customerBalance_post(Model model
            , @RequestParam("dateTo") java.sql.Date toDate
            , @RequestParam("distributorId") int distributorId) {


        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }
        model.addAttribute("currentDate", date);
        model.addAttribute("toDate", toDate);
        model.addAttribute("distributors", distributorService.findAll());
        model.addAttribute("currentDistributor", distributorId);
        model.addAttribute("trialModel", trialBalanceService.getTrailBalanceBasic(1, toDate, null, accountService.findAccountFor(distributorId, false)));
        return "distributor/customerBalance";
    }


    /**
     * To check Distributor's Commission Balances .
     * GET Request
     * @param model
     * @return Response Page
     */
    @RequestMapping(value = "/Salesman/commission", method = RequestMethod.GET)
    public String distributorCommission(Model model) {

        if (staticInfoService.findStaticInfo() != null) {
            model.addAttribute("imageUrl", staticInfoService.findStaticInfo().getImageUrl());
        }

        Map<Integer ,String> distributorMap = new HashMap<>();
        List<Distributor> distributorList = distributorService.findAll();
        for(Distributor distributor :distributorList){
            distributorMap.put(distributor.getDistributorId(), distributor.getName());
        }

        model.addAttribute("distributorMap", distributorMap);
        model.addAttribute("apartmentList", chassisService.findByCommissionStatus(Constants.COMMISSION_STATUS_UNPAID));
        return "distributor/commission";
    }

}
