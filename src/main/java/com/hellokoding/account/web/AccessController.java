package com.hellokoding.account.web;


import com.hellokoding.account.model.UserAccessModel;
import com.hellokoding.account.model.UserAuth;
import com.hellokoding.account.service.accessControl.AccessControlService;
import com.hellokoding.account.service.department.DepartmentService;
import com.hellokoding.account.service.user.UserAuthService;
import com.hellokoding.account.validator.CredentialValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@SessionAttributes({"userModel"})
public class AccessController {

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private AccessControlService accessControlService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private CredentialValidator credentialValidator;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    /**
     * Get Listing Of All Users
     * New User Generation Form
     * GET
     * @param model Data Model
     * @return
     */
    @RequestMapping(value = "/AccessControl/users", method = RequestMethod.GET)
    public String userListing(Model model) {

        model.addAttribute("userForm",new UserAuth());
        model.addAttribute("users", userAuthService.findAll());
        return "access/userListing";
    }

    /**
     * Create New User
     * POST
     * @param userForm detail Of User
     * @param model
     * @param bindingResult
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/AccessControl/users", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") UserAuth userForm,Model model, BindingResult bindingResult,RedirectAttributes attributes) {
        credentialValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("userForm",new UserAuth());
            model.addAttribute("users", userAuthService.findAll());
            return "access/userListing";
        }
        userForm.getUserName().replace(' ', '_');
        userForm.setPassword(bCryptPasswordEncoder.encode(userForm.getPassword()));
        userAuthService.save(userForm);
        attributes.addFlashAttribute("userRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">User with Username : <strong>"+ userForm.getUserName() +" </strong> added successfully.</div>");
        return "redirect:/AccessControl/users";
    }

    /**
     * Remove User from DB
     * @param authId user identity
     * @param attributes for message
     * @return
     */
    @RequestMapping(value = "/AccessControl/removeUser", method = RequestMethod.POST)
    public String removeUser(@RequestParam("authId") int authId, RedirectAttributes attributes) {

        userAuthService.remove(authId);
        attributes.addFlashAttribute("userRemoveSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">User with id <strong>"+ authId +" </strong> removed successfully.</div>");
        return "redirect:/AccessControl/users";
    }


    /**
     * Toggle User status
     * Active, Inactve
     * @param authId target User Id
     * @param attributes For Message
     * @return redirect to user listing
     */
    @RequestMapping(value = "/AccessControl/toggleStatus", method = RequestMethod.POST)
    public String toggleUserStatus(@RequestParam("authId") int authId, RedirectAttributes attributes) {

        UserAuth userAuth = userAuthService.findByAuthId(authId);
        if(userAuth.getIsActive() == 0){
            userAuth.setIsActive(1);
        }else if(userAuth.getIsActive() == 1){
            userAuth.setIsActive(0);
        }
        userAuthService.save(userAuth);
        attributes.addFlashAttribute("userStatusSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Status for user : <strong>"+ userAuth.getUserName() +" </strong> changed.</div>");
        return "redirect:/AccessControl/users";
    }


    /**
     * Manage Specific User
     * Assign Password, Dept , Roles
     * @param authId Target User Id
     * @param model Data Model
     * @return
     */
    @RequestMapping(value = "/AccessControl/manageUser", method = RequestMethod.POST)
    public String manageUser(@RequestParam("authId") int authId, Model model) {


        model.addAttribute("userModel", accessControlService.getUserModel(authId));
        model.addAttribute("departments", departmentService.findAll());
        return "access/userAccess";
    }


    /**
     * Update User Data
     * POST for MANAGE USER
     * @param userAccessModel Data Model
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/AccessControl/manageUserPost", method = RequestMethod.POST)
    public String manageUser_post(@ModelAttribute("userModel") UserAccessModel userAccessModel,RedirectAttributes attributes) {


        accessControlService.updateUser(userAccessModel);
        attributes.addFlashAttribute("userAccessChanged", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">User : <strong>"+ userAccessModel.getUserAuth().getUserName() +" </strong> Access Updated.</div>");
        return "redirect:/AccessControl/users";
    }



}
