package com.hellokoding.account.web;


import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.*;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.maker.MakerService;
import com.hellokoding.account.service.plan.PaymentPlanService;
import com.hellokoding.account.service.security.RoleSupport;
import com.hellokoding.account.service.stock.CustomerService;
import com.hellokoding.account.service.voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.taxation.TaxDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@SessionAttributes({"voucherListOrder", "orderUpdateForm"})
public class SaleController {

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private JSONPopulateService jsonPopulateService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ChassisService chassisService;

    @Autowired
    private DistributorService distributorService;

    @Autowired
    private PaymentPlanService paymentPlanService;

    /**
     * Listing for Sale Vouchers
     * Data form AJAX CALL
     *
     * @return Response Page
     */
    @RequestMapping(value = "/Sales/viewSalesVoucher", method = RequestMethod.GET)
    public String viewSalesVoucher() {

        return "sales/viewSaleVoucher";
    }

    /**
     * Create Sale
     * GET
     * makes entry for Apartment Field for sale
     *
     * @param model data Model
     * @return
     */
    @RequestMapping(value = "/Sale/createSaleVoucher", method = RequestMethod.GET)
    public String addSaleVoucher(Model model) {
        VoucherList voucherList = new VoucherList();
        voucherList.add(new Voucher()); // single voucher for Apartment Transfer

        List<Distributor> distributorList = new ArrayList<>();
        if (RoleSupport.hasRole("ROLE_SALESMAN")) {
            distributorList.add(RoleSupport.getUserAuth().getDistributor());
        } else {
            distributorList = distributorService.findAll();
        }
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("SL"));
        model.addAttribute("stockAccounts", accountService.getStockAccounts());
        model.addAttribute("accounts", accountService.getCustomerAccountList());
        model.addAttribute("salesmanList", distributorList);
        model.addAttribute("customerAccounts", accountService.getCustomerAccountList());
        return "sales/addSaleVoucher";
    }

    /**
     * Create Sale
     * POST
     * makes entry for Apartment Field for sale
     * Attach Payment Plan with Apartment
     * VoucherList.doPrint for
     * redirection to Print Voucher
     *
     * @param voucherList Voucher Data Model
     * @param attributes  For messages
     * @return redirect to Create Sale
     */
    @RequestMapping(value = "/Sale/createSaleVoucher", method = RequestMethod.POST)
    public String addSaleVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        Double customerDebitAmount = 0.0;
        Voucher customerVoucher = new Voucher();
        Integer userAuth = null;
        if (RoleSupport.hasRole("ROLE_SALESMAN")) {
            userAuth = RoleSupport.getUserAuth().getAuth_id();
        }

        if (voucherList.getCustomerAccountCode() == 0) {
            attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">No Sale voucher added.</div>");
            return "redirect:/Sale/createSaleVoucher";
        }

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        String newVoucherNumber = voucherService.generateVoucherNumber("SL");// to avoid Sale Number duplication on multiple users
        for (Voucher voucher : voucherList.getVoucherList()) {

            if (voucher.getItemAccount() > 0 && voucher.getCredit() > 0 && !voucher.getApartmentNo().isEmpty()) {

                voucher.setVoucherNumber(newVoucherNumber);
                Apartment apartment = chassisService.findByChassisId(Integer.parseInt(voucher.getApartmentNo()));
                apartment.setCustomerAccount(voucherList.getCustomerAccountCode());
                apartment.setSold(true);
                apartment.setSellingPrice(voucher.getCredit());
                apartment.setDistributorId(voucher.getSalesmanId());
                apartment.setDate(voucher.getVoucherDate());
                apartment.setCommissionAmount(voucher.getCommissionAmount());
                apartment.setCommissionStatus(Constants.COMMISSION_STATUS_INACTIVE);
/*                                *//*
                Validation for Payment Plan
                 *//*
                if (apartment.getStock().getPaymentPlan() == null) {
                    attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">No Sale voucher added. Please Set Payment Plan for Model : " + apartment.getStock().getStockAccount().getTitle() + ".</div>");
                    return "redirect:/Sale/createSaleVoucher";
                }*/

                chassisService.save(apartment);
                //paymentPlanService.attachPaymentPlan(apartment);// also Generate Plan Due entries
                customerDebitAmount += voucher.getCredit();
                customerVoucher.setVoucherNumber(voucher.getVoucherNumber());
                customerVoucher.setBillDate(voucher.getBillDate());
                customerVoucher.setBillNumber(voucher.getBillNumber());
                customerVoucher.setVoucherDate(voucher.getVoucherDate());

                voucher.setChassisNo(apartment.getApartmentNo());
                voucher.setChassisId(apartment.getApartmentId());
                voucher.setItemQuantity(1.0);
                voucher.setTaxAmount(voucher.getCredit() - voucher.getItemRate());
                voucher.setAccountCode(voucher.getItemAccount());
                if (voucher.getAdvanceTax() > 0 && staticInfo.getAdvanceTaxAccount() > 0) {
                    Voucher advanceTaxVoucher = new Voucher();
                    advanceTaxVoucher.setVoucherNumber(voucherService.generateVoucherNumber("AV"));
                    advanceTaxVoucher.setVoucherDate(voucher.getVoucherDate());
                    advanceTaxVoucher.setReferenceVoucher(voucher.getVoucherNumber());
                    advanceTaxVoucher.setAccountCode(staticInfo.getAdvanceTaxAccount());
                    advanceTaxVoucher.setCredit(voucher.getAdvanceTax());
                    advanceTaxVoucher.setChassisNo(voucher.getChassisNo());
                    advanceTaxVoucher.setChassisId(voucher.getChassisId());
                    advanceTaxVoucher.setAuthId(userAuth);
                    voucherService.saveJournal(advanceTaxVoucher);
                    // Customer Advance Tax Voucher Account
                    Voucher advanceTaxVoucherCustomer = new Voucher();
                    advanceTaxVoucherCustomer.setVoucherNumber(advanceTaxVoucher.getVoucherNumber());
                    advanceTaxVoucherCustomer.setVoucherDate(voucher.getVoucherDate());
                    advanceTaxVoucherCustomer.setReferenceVoucher(voucher.getVoucherNumber());
                    advanceTaxVoucherCustomer.setAccountCode(voucher.getAccountCode());
                    advanceTaxVoucherCustomer.setDebit(voucher.getAdvanceTax());
                    advanceTaxVoucherCustomer.setChassisNo(voucher.getChassisNo());
                    advanceTaxVoucherCustomer.setChassisId(voucher.getChassisId());
                    advanceTaxVoucherCustomer.setAuthId(userAuth);
                    voucherService.saveJournal(advanceTaxVoucherCustomer);
                }
                if (voucher.getCommissionAmount() > 0 && staticInfo.getCommissionAccount() > 0) {
                    Voucher commissionVoucher = new Voucher();
                    commissionVoucher.setVoucherNumber(voucherService.generateVoucherNumber("CV"));
                    commissionVoucher.setVoucherDate(voucher.getVoucherDate());
                    commissionVoucher.setReferenceVoucher(voucher.getVoucherNumber());
                    commissionVoucher.setAccountCode(staticInfo.getCommissionAccount());
                    commissionVoucher.setCredit(voucher.getCommissionAmount());
                    commissionVoucher.setChassisNo(voucher.getChassisNo());
                    commissionVoucher.setChassisId(voucher.getChassisId());
                    commissionVoucher.setAuthId(userAuth);
                    //voucherService.saveJournal(commissionVoucher); Commission to be triggered after down Payments
                    // Customer commission Account
                    Voucher commissionVoucherCustomer = new Voucher();
                    commissionVoucherCustomer.setVoucherNumber(commissionVoucher.getVoucherNumber());
                    commissionVoucherCustomer.setVoucherDate(voucher.getVoucherDate());
                    commissionVoucherCustomer.setReferenceVoucher(voucher.getVoucherNumber());
                    commissionVoucherCustomer.setAccountCode(staticInfo.getCompanyPaymentAccount());
                    commissionVoucherCustomer.setDebit(voucher.getCommissionAmount());
                    commissionVoucherCustomer.setAuthId(userAuth);
                    commissionVoucherCustomer.setChassisNo(voucher.getChassisNo());
                    commissionVoucherCustomer.setChassisId(voucher.getChassisId());
                    //voucherService.saveJournal(commissionVoucherCustomer); Commission to be triggered after down Payments
                }
                voucher.setAuthId(userAuth);
                voucherService.saveJournal(voucher);

            }

        }

        if (customerVoucher.getVoucherNumber() != null) {
            voucherNumber = customerVoucher.getVoucherNumber();
            if (voucherList.getDiscountAmount() > 0.0) {
                Voucher discountVoucher = new Voucher();
                discountVoucher.setVoucherNumber(customerVoucher.getVoucherNumber());
                discountVoucher.setVoucherDate(customerVoucher.getVoucherDate());
                discountVoucher.setAccountCode(staticInfoService.findStaticInfo().getDiscountAccount());
                discountVoucher.setDebit(voucherList.getDiscountAmount());
                discountVoucher.setAuthId(userAuth);
                voucherService.saveJournal(discountVoucher);
                customerDebitAmount -= voucherList.getDiscountAmount();
            }
            customerVoucher.setAuthId(userAuth);
            customerVoucher.setAccountCode(voucherList.getCustomerAccountCode());
            customerVoucher.setDebit(customerDebitAmount);
            voucherService.saveJournal(customerVoucher);
        }

        if (voucherList.isDoPrint() && !voucherNumber.equals("")) {
            return "redirect:/Sales/printSaleVoucher?voucherNumber=" + voucherNumber;
        }
        if (voucherNumber.equals("")) {
            attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">No Sale voucher added.</div>");
            return "redirect:/Sale/createSaleVoucher";
        }
        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Sale voucher with number <strong>" + voucherNumber + " </strong>added successfully.</div>");
        return "redirect:/Sale/createSaleVoucher";
    }

    /**
     * Update Sale
     * GET
     *
     * @param model         Data Model
     * @param voucherNumber Target Voucher Number
     * @return
     */
    @RequestMapping(value = "/Sales/updateSaleVoucher", method = RequestMethod.GET)
    public String updateSaleVoucher(Model model, @RequestParam(value = "voucherNumber") String voucherNumber) {
        List<Voucher> voucherItemList = voucherService.findByVoucherNumber(voucherNumber);

        VoucherList voucherList = new VoucherList();
        Account currentCustomerAccount = new Account();
        int salesmanId = 0;
        Apartment apartment;
        for (Voucher voucher : voucherItemList) {
            if (voucher.getItemAccount() > 0) {
                apartment = chassisService.findByChassisId(voucher.getChassisId());
                voucher.setRooms(apartment.getRooms());
                voucher.setFloor(apartment.getFloor());
                voucher.setItemQuantity(voucher.getItemQuantity());
                voucher.setItemRate(voucher.getItemRate());
                voucher.setBillAmount(voucher.getBillAmount());
                voucher.setApartmentNo(voucher.getChassisNo());
                voucherList.add(voucher);
                salesmanId = voucher.getSalesmanId();
            } else {

                currentCustomerAccount = accountService.findAccount(voucher.getAccountCode());
            }
        }
        // setting current Account attributes to voucherList
        voucherList.setCustomerAccountCode(currentCustomerAccount.getAccountCode());
        voucherList.setTitle(currentCustomerAccount.getTitle());
        voucherList.setTaxStatus(currentCustomerAccount.getStockCustomer().getTaxStatus());
        voucherList.setCnic(currentCustomerAccount.getStockCustomer().getCnic());
        voucherList.setAddress(currentCustomerAccount.getStockCustomer().getAddress());
        voucherList.setCreateDate(voucherItemList.get(1).getVoucherDate());
        voucherList.setMobile(currentCustomerAccount.getStockCustomer().getMobile());
        voucherList.setEmail(currentCustomerAccount.getStockCustomer().getEmailAddress());
        voucherList.setLandline(currentCustomerAccount.getStockCustomer().getLandline());
        voucherList.setDeliveryNumber(voucherNumber); // used as Voucher Number
        List<Distributor> distributorList = new ArrayList<>();
        if (RoleSupport.hasRole("ROLE_SALESMAN")) {
            distributorList.add(RoleSupport.getUserAuth().getDistributor());
        } else {
            distributorList = distributorService.findAll();
        }
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("currentDate", voucherItemList.get(1).getVoucherDate());
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("stockAccounts", accountService.getStockAccounts());
        model.addAttribute("chassisList", chassisService.findAll());
        model.addAttribute("accounts", accountService.getCustomerAccounts());
        model.addAttribute("salesmanList", distributorList);
        model.addAttribute("currentSalesman", salesmanId);
        model.addAttribute("customerAccounts", accountService.getCustomerAccountList());


        return "sales/updateSaleVoucher";

    }

    /**
     * Update Sale
     * POST
     *
     * @param voucherList Data Model
     * @param attributes  for Message
     * @return
     */
    @RequestMapping(value = "/Sales/updateSaleVoucher", method = RequestMethod.POST)
    public String updateSaleVoucher_Post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {


        String voucherNumber = "";
        Double customerDebitAmount = 0.0;
        Voucher customerVoucher = new Voucher();
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        Integer userAuth = null;
        if (RoleSupport.hasRole("ROLE_SALESMAN")) {
            userAuth = RoleSupport.getUserAuth().getAuth_id();
        }
        if (voucherList.getCustomerAccountCode() == 0) {
            attributes.addFlashAttribute("voucherUpdateSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">No Sale voucher updated.</div>");
            return "redirect:/Sales/viewSalesVoucher";
        }
        List<Voucher> voucherNumberList = voucherService.findByVoucherNumber(voucherList.getDeliveryNumber());
        if (voucherNumberList.size() > 0) {
            Voucher voucher = voucherNumberList.stream().filter(e -> e.getItemAccount() > 0).findFirst().get();
            Apartment apartment = chassisService.findByChassisId(voucher.getChassisId());
            if (apartment != null) {
                //paymentPlanService.removePaymentPlan(apartment.getPaymentPlan().getPlanId());
                apartment.setCustomerAccount(0);
                apartment.setSold(false);
                apartment.setDistributorId(0);
                apartment.setDate(null);
            }
            voucherNumberList.addAll(voucherService.findByReferenceVoucher(voucherList.getDeliveryNumber()));
            voucherService.remove(voucherNumberList);
        }
        String newVoucherNumber = voucherService.generateVoucherNumber("SL");// to avoid Sale Number duplication on multiple users
        for (Voucher voucher : voucherList.getVoucherList()) {

            if (voucher.getItemAccount() > 0 && voucher.getCredit() > 0 && !voucher.getApartmentNo().isEmpty()) {

                voucher.setVoucherNumber(newVoucherNumber);
                Apartment apartment = chassisService.findByChassisId(Integer.parseInt(voucher.getApartmentNo()));
                apartment.setCustomerAccount(voucherList.getCustomerAccountCode());
                apartment.setSold(true);
                apartment.setSellingPrice(voucher.getCredit());
                apartment.setDistributorId(voucher.getSalesmanId());
                apartment.setDate(voucher.getVoucherDate());
                apartment.setCommissionAmount(voucher.getCommissionAmount());
                apartment.setCommissionStatus(Constants.COMMISSION_STATUS_INACTIVE);
/*                                *//*
                Validation for Payment Plan
                 *//*
                if (apartment.getStock().getPaymentPlan() == null) {
                    attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">No Sale voucher added. Please Set Payment Plan for Model : " + apartment.getStock().getStockAccount().getTitle() + ".</div>");
                    return "redirect:/Sale/createSaleVoucher";
                }*/

                chassisService.save(apartment);
               // paymentPlanService.attachPaymentPlan(apartment);// also Generate Plan Due entries
                customerDebitAmount += voucher.getCredit();
                customerVoucher.setVoucherNumber(voucher.getVoucherNumber());
                customerVoucher.setBillDate(voucher.getBillDate());
                customerVoucher.setBillNumber(voucher.getBillNumber());
                customerVoucher.setVoucherDate(voucher.getVoucherDate());

                voucher.setChassisNo(apartment.getApartmentNo());
                voucher.setChassisId(apartment.getApartmentId());
                voucher.setItemQuantity(1.0);
                voucher.setTaxAmount(voucher.getCredit() - voucher.getItemRate());
                voucher.setAccountCode(voucher.getItemAccount());
                if (voucher.getAdvanceTax() > 0 && staticInfo.getAdvanceTaxAccount() > 0) {
                    Voucher advanceTaxVoucher = new Voucher();
                    advanceTaxVoucher.setVoucherNumber(voucherService.generateVoucherNumber("AV"));
                    advanceTaxVoucher.setVoucherDate(voucher.getVoucherDate());
                    advanceTaxVoucher.setReferenceVoucher(voucher.getVoucherNumber());
                    advanceTaxVoucher.setAccountCode(staticInfo.getAdvanceTaxAccount());
                    advanceTaxVoucher.setCredit(voucher.getAdvanceTax());
                    advanceTaxVoucher.setChassisNo(voucher.getChassisNo());
                    advanceTaxVoucher.setChassisId(voucher.getChassisId());
                    advanceTaxVoucher.setAuthId(userAuth);
                    voucherService.saveJournal(advanceTaxVoucher);
                    // Customer Advance Tax Voucher Account
                    Voucher advanceTaxVoucherCustomer = new Voucher();
                    advanceTaxVoucherCustomer.setVoucherNumber(advanceTaxVoucher.getVoucherNumber());
                    advanceTaxVoucherCustomer.setVoucherDate(voucher.getVoucherDate());
                    advanceTaxVoucherCustomer.setReferenceVoucher(voucher.getVoucherNumber());
                    advanceTaxVoucherCustomer.setAccountCode(voucher.getAccountCode());
                    advanceTaxVoucherCustomer.setDebit(voucher.getAdvanceTax());
                    advanceTaxVoucherCustomer.setChassisNo(voucher.getChassisNo());
                    advanceTaxVoucherCustomer.setChassisId(voucher.getChassisId());
                    advanceTaxVoucherCustomer.setAuthId(userAuth);
                    voucherService.saveJournal(advanceTaxVoucherCustomer);
                }
                if (voucher.getCommissionAmount() > 0 && staticInfo.getCommissionAccount() > 0) {
                    Voucher commissionVoucher = new Voucher();
                    commissionVoucher.setVoucherNumber(voucherService.generateVoucherNumber("CV"));
                    commissionVoucher.setVoucherDate(voucher.getVoucherDate());
                    commissionVoucher.setReferenceVoucher(voucher.getVoucherNumber());
                    commissionVoucher.setAccountCode(staticInfo.getCommissionAccount());
                    commissionVoucher.setCredit(voucher.getCommissionAmount());
                    commissionVoucher.setChassisNo(voucher.getChassisNo());
                    commissionVoucher.setChassisId(voucher.getChassisId());
                    commissionVoucher.setAuthId(userAuth);
                   // voucherService.saveJournal(commissionVoucher);
                    // Customer commission Account
                    Voucher commissionVoucherCustomer = new Voucher();
                    commissionVoucherCustomer.setVoucherNumber(commissionVoucher.getVoucherNumber());
                    commissionVoucherCustomer.setVoucherDate(voucher.getVoucherDate());
                    commissionVoucherCustomer.setReferenceVoucher(voucher.getVoucherNumber());
                    commissionVoucherCustomer.setAccountCode(staticInfo.getCompanyPaymentAccount());
                    commissionVoucherCustomer.setDebit(voucher.getCommissionAmount());
                    commissionVoucherCustomer.setAuthId(userAuth);
                    commissionVoucherCustomer.setChassisNo(voucher.getChassisNo());
                    commissionVoucherCustomer.setChassisId(voucher.getChassisId());
                   // voucherService.saveJournal(commissionVoucherCustomer);
                }
                voucher.setAuthId(userAuth);
                voucherService.saveJournal(voucher);

            }

        }

        if (customerVoucher.getVoucherNumber() != null) {
            voucherNumber = customerVoucher.getVoucherNumber();
            if (voucherList.getDiscountAmount() > 0.0) {
                Voucher discountVoucher = new Voucher();
                discountVoucher.setVoucherNumber(customerVoucher.getVoucherNumber());
                discountVoucher.setVoucherDate(customerVoucher.getVoucherDate());
                discountVoucher.setAccountCode(staticInfoService.findStaticInfo().getDiscountAccount());
                discountVoucher.setDebit(voucherList.getDiscountAmount());
                discountVoucher.setAuthId(userAuth);
                voucherService.saveJournal(discountVoucher);
                customerDebitAmount -= voucherList.getDiscountAmount();
            }
            customerVoucher.setAccountCode(voucherList.getCustomerAccountCode());
            customerVoucher.setDebit(customerDebitAmount);
            customerVoucher.setAuthId(userAuth);
            voucherService.saveJournal(customerVoucher);
        }

        attributes.addFlashAttribute("voucherUpdateSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Sale voucher with number <strong>" + voucherNumber + " </strong>updated successfully.</div>");
        return "redirect:/Sales/viewSalesVoucher";
    }


    /**
     * Print Sale Voucher
     *
     * @param model         Data Model with Sale Details
     * @param voucherNumber Target Voucher Number
     * @return Response Page with Details
     */
    @RequestMapping(value = "/Sales/printSaleVoucher", method = RequestMethod.GET)
    public String printSaleVoucher(Model model, @RequestParam("voucherNumber") String voucherNumber) {
        List<Voucher> voucherList = new ArrayList<>();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        for (Voucher voucher : voucherService.findByVoucherNumber(voucherNumber)) {
            if (voucher.getItemAccount() > 0) {
                voucherList.add(voucher);
            } else {
                model.addAttribute("accountCode", voucher.getAccountCode());
                model.addAttribute("voucherDate", dateFormat.format(voucher.getVoucherDate()));
                model.addAttribute("voucherNumber", voucherNumber);
            }
        }

        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountList(0)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("companyName", staticInfo.getCompanyName());
        return "sales/printSaleVoucher";
    }


    /////////////////////////// JSON Generator On DataTable Ajax Call //////////////////


    /**
     * Get Sale Voucher List with Pagination
     * AJAX (SALES)
     *
     * @param start
     * @param length
     * @param draw
     * @param searchString
     * @return
     */
    @RequestMapping(value = "/Sales/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> saleVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }

        return new ResponseEntity(jsonPopulateService.populateSaleVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherNumber", false, searchString, "SL"), draw), HttpStatus.OK);


    }

    /**
     * Get Apartment List on Selected stockId
     *
     * @param accountCode
     * @return
     */
    @RequestMapping(value = "Account/ajax/getApartmentList", method = RequestMethod.GET)

    public @ResponseBody
    Map<Integer, String> getChassisList(@RequestParam("accountCode") int accountCode, @RequestParam("isUpdate") boolean isUpdate) {
        Map<Integer, String> stockMap = new HashMap<>();
        if (accountCode == 0) {
            return stockMap;
        }
        Stock stock = accountService.findAccount(accountCode).getAccountStock();
        for (Apartment apartment : stock.getApartmentList()) {
            if (!apartment.isSold() || isUpdate) {
                stockMap.put(apartment.getApartmentId(), apartment.getApartmentNo());
            }
        }
        return stockMap;

    }

    @RequestMapping(value = "Account/ajax/getSelectedApartment", method = RequestMethod.GET)
    public @ResponseBody
    Apartment getSelectedChassis(@RequestParam("apartmentId") int apartmentId) {
        Apartment apartment = chassisService.findByChassisId(apartmentId);
        //apartment.setCommissionAmount(apartment.getStock().getCommissionAmount());
        apartment.setStock(null);
        apartment.setPaymentPlan(null);
        apartment.setApartmentHold(null);
        return apartment;
    }

    /**
     * Get Customer info if exists
     *
     * @param mobile mobile number
     * @param cnic   number
     * @return StockCustomer
     */
    @RequestMapping(value = "Account/getCustomerDetails", method = RequestMethod.GET)
    public @ResponseBody
    StockCustomer getCustomerDetails(@RequestParam("mobile") String mobile, @RequestParam("cnic") String cnic, @RequestParam("customerAccountCode") int customerAccountCode) {

        StockCustomer stockCustomer;
        if (!mobile.isEmpty()) {
            stockCustomer = customerService.getCustomerDetail(mobile, null);
        } else if (!cnic.isEmpty()) {
            stockCustomer = customerService.getCustomerDetail(null, cnic);
        } else {
            stockCustomer = accountService.findAccount(customerAccountCode).getStockCustomer();
        }
        if (stockCustomer == null) {
            return null;
        }
        stockCustomer.setAccountCode(stockCustomer.getCustomerAccount().getAccountCode());
        stockCustomer.setAccountTitle(stockCustomer.getCustomerAccount().getTitle());
        stockCustomer.setCustomerAccount(null);
        return stockCustomer;
    }

}
