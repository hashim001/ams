package com.hellokoding.account.web;


/**
 * Payment Plan Controller
 */

import com.hellokoding.account.model.PaymentPlan;
import com.hellokoding.account.model.PaymentPlanDown;
import com.hellokoding.account.model.Stock;
import com.hellokoding.account.reportModel.PaymentPlanModel;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.plan.PaymentPlanService;
import com.hellokoding.account.service.stock.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@Controller
@SessionAttributes({"paymentPlan"})
public class PlanController {


    @Autowired
    private PaymentPlanService paymentPlanService;

    @Autowired
    private StockService stockService;

    @Autowired
    private ChassisService chassisService;

    /**
     * Create / Update Payment Plan for Stock (Model)
     * GET
     * @param model
     * @param stockId target Model
     * @return
     */
    @RequestMapping(value = "/Plan/paymentPlan", method = RequestMethod.GET)
    public String paymentPlan(Model model, @RequestParam(name = "stockId", defaultValue = "0") Integer stockId) {

        Stock stock = stockService.findByStockId(stockId);
        PaymentPlan paymentPlan = stock.getPaymentPlan();
        if (paymentPlan == null) {
            paymentPlan = new PaymentPlan();
            paymentPlan.setPlanStock(stock);
            List<PaymentPlanDown> paymentPlanDownList = new ArrayList<>();
            for (int i = 0; i < 7; i++) {
                paymentPlanDownList.add(new PaymentPlanDown());
            }
            paymentPlan.setPaymentPlanDownList(paymentPlanDownList);

        } else {
            List<PaymentPlanDown> paymentPlanDownList = new ArrayList<>();
            paymentPlanDownList.addAll(paymentPlan.getPaymentPlanDownList());
            for (PaymentPlanDown paymentPlanDown : paymentPlanDownList) {
                paymentPlanDown.setCheck(true);
            }
            int size = paymentPlanDownList.size();
            for (int i = 0; i < 7 - size; i++) {
                paymentPlanDownList.add(new PaymentPlanDown());
            }
            paymentPlan.setPaymentPlanDownList(paymentPlanDownList);
        }
        model.addAttribute("paymentPlan", paymentPlan);
        return "plan/stockPlan";
    }


    /**
     * Create / Update Payment Plan
     * @param paymentPlan Plan Data
     * @return
     */
    @RequestMapping(value = "/Plan/paymentPlan", method = RequestMethod.POST)
    public String paymentPlanPost(@ModelAttribute("paymentPlan") PaymentPlan paymentPlan) {

        List<PaymentPlanDown> paymentPlanDownList = new ArrayList<>();
        for (PaymentPlanDown paymentPlanDown : paymentPlan.getPaymentPlanDownList()) {
            if (paymentPlanDown.isCheck() && paymentPlanDown.getRate() > 0) {
                paymentPlanDown.setPaymentPlan(paymentPlan);
                paymentPlanDownList.add(paymentPlanDown);
            }
        }
        paymentPlan.setPaymentPlanDownList(paymentPlanDownList);
        paymentPlan.setUpdated(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        paymentPlanService.savePlan(paymentPlan);
        return "redirect:/Stock/addStock";
    }

    /**
     * View Payment Plan
     * Can be used for Stock Plan or Apartment Fixed Plan
     * @param model
     * @param stockId in case of stock (Model)
     * @param apartmentNo in case of Apartment
     * @return
     */
    @RequestMapping(value = "/Plan/viewPaymentPlan", method = RequestMethod.GET)
    public String viewPaymentPlan(Model model,@RequestParam(name = "stockId", defaultValue = "0") Integer stockId
                              ,@RequestParam(name = "apartmentNo", defaultValue = "0") String apartmentNo) {

        PaymentPlanModel paymentPlanModel = paymentPlanService.getPlanModel(stockId,apartmentNo);
        if(paymentPlanModel == null){
            return "error";
        }

        model.addAttribute("paymentPlan",paymentPlanModel);
        if(stockId != 0){
            return "plan/viewPaymentPlan-s";
        }
        return "plan/viewPaymentPlan-a";
    }

    /**
     * Get Payment Plan for Floor
     * Set of All Lying Apartments
     * @param model
     * @param stockId
     * @param floor
     * @return
     */
    @RequestMapping(value = "/Plan/viewFloorPlan", method = RequestMethod.GET)
    public String viewFloorPlan(Model model,@RequestParam(name = "stockId", defaultValue = "0") Integer stockId
            ,@RequestParam(name = "floor", defaultValue = "3 - 8") String floor) {

        model.addAttribute("modelList",stockService.findAllStock());
        model.addAttribute("floorList",getFloorList());
        model.addAttribute("currentFloor", floor);
        model.addAttribute("currentStockId",stockId);
        model.addAttribute("PaymentPlanModelList",paymentPlanService.getFloorPlan(stockId,floor.substring(0,2)));
        return "plan/viewFloorPaymentPlan";
    }

    /**
     * Floor Message
     * HARD CODING FOR GALAXY
     * @param floor Request Floor
     * @return
     */
    private String  getFloorMessage(String floor){
        if(floor.equals("3") ||floor.equals("4") ||floor.equals("5") ||floor.equals("6") ||floor.equals("7")||floor.equals("8") ){
            return "3 to 8";
        }else if (floor.equals("9") ||floor.equals("10") ||floor.equals("11") ||floor.equals("12") ||floor.equals("13")||floor.equals("14")){
            return "9 to 14";
        }else if (floor.equals("15") ||floor.equals("16") ||floor.equals("17") ||floor.equals("18") ||floor.equals("19")||floor.equals("20")){
            return "15 to 20";
        }else if (floor.equals("21") ||floor.equals("22") ||floor.equals("23") ||floor.equals("24") ||floor.equals("25")){
            return "21 to 25";
        }else if(floor.equals("26") ||floor.equals("27") ||floor.equals("28") ||floor.equals("29") ||floor.equals("30")) {
            return "26 to 30";
        }
        return "";

    }

    /**
     * A hardcoded List for floors
     * @return
     */
    private List<String> getFloorList(){

        List<String> floorList = new ArrayList<>();
        floorList.add("3 to 8");
        floorList.add("9 to 14");
        floorList.add("15 to 20");
        floorList.add("21 to 25");
        floorList.add("26 to 30");
        return floorList;
    }

}
