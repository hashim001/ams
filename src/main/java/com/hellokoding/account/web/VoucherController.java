package com.hellokoding.account.web;

import com.hellokoding.account.constants.Constants;
import com.hellokoding.account.model.*;

import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.plan.PaymentDueService;
import com.hellokoding.account.service.security.RoleSupport;
import com.hellokoding.account.service.stock.CustomerService;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.service.voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.account.BankDetailService;
import com.hellokoding.account.service.account.CostCentreService;
import com.hellokoding.account.service.automation.UpdateBalanceService;
import com.hellokoding.account.service.datalisting.JSONPopulateService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.taxation.TaxDetailService;
import com.hellokoding.account.validator.VoucherValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class VoucherController {


    @Autowired
    private VoucherService voucherService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CostCentreService costCentreService;

    @Autowired
    private TaxDetailService taxDetailService;

    @Autowired
    private VoucherValidator voucherValidator;

    @Autowired
    private StaticInfoService staticInfoService;

    @Autowired
    private UpdateBalanceService updateBalanceService;

    @Autowired
    private JSONPopulateService jsonPopulateService;

    @Autowired
    private BankDetailService bankDetailService;
    @Autowired
    private LedgerCalculationService ledgerCalculationService;
    @Autowired
    private DistributorService distributorService;
    @Autowired
    private ChassisService chassisService;
    @Autowired
    private StockService stockService;
    @Autowired
    private CustomerService customerService;

    @Autowired
    private PaymentDueService paymentDueService;
    /////////////////////////////////////// Payment Voucher ///////////////////////////////////


    @RequestMapping(value = "/Account/createPaymentVoucher", method = RequestMethod.GET)
    public String addPaymentVoucher(Model model) {

        model.addAttribute("voucherForm", new Voucher());
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        model.addAttribute("accounts", accountService.getAccountListByLevel(4));
        model.addAttribute("bankDetailAccounts", accountService.getBankAccountList());
        model.addAttribute("taxationAccounts", accountService.getTaxAccounts());
        model.addAttribute("costCentres", costCentreService.getCostCentreMap());
        model.addAttribute("suzukiAccount", staticInfo.getCompanyPaymentAccount());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("BP"));
        // STILL CAUSING EAGER ISSUES
        model.addAttribute("taxDetail", taxDetailService.findAll());
         model.addAttribute("chassisList", chassisService.findAllByDistributorNotNull());

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/addPaymentVoucher";
    }

    @RequestMapping(value = "/Account/createPaymentVoucher", method = RequestMethod.POST)
    public String addPaymentVoucher_post(@ModelAttribute("voucherForm") Voucher voucher, BindingResult bindingResult, RedirectAttributes attributes) {
        voucherValidator.validate(voucher, bindingResult);

        if (bindingResult.hasErrors()) {
            return "redirect:/Account/createPaymentVoucher";
        }

        String voucherNumber = voucherService.savePayment(voucher).getVoucherNumber();
        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">voucher <strong>" + voucherNumber + " </strong>for Account with code: <strong>" + voucher.getAccountCode() + " </strong>added successfully.</div>");
        return "redirect:/Account/createPaymentVoucher";
    }


    /////////////////////////////////////// Receipt voucher ///////////////////////////////////

    @RequestMapping(value = "/Account/createReceiptVoucher", method = RequestMethod.GET)
    public String addReceiptVoucher(Model model) {
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        model.addAttribute("voucherForm", new Voucher());
        model.addAttribute("accounts", accountService.getAccountListByLevel(4));
        model.addAttribute("bankDetailAccounts", accountService.getBankAccountList());
        model.addAttribute("staticInfo", staticInfoService.findStaticInfo());
        // STILL CAUSING EAGER ISSUES
        model.addAttribute("taxDetail", taxDetailService.findAll());
        model.addAttribute("taxationAccounts", accountService.getTaxAccounts());
        model.addAttribute("costCentres", costCentreService.getCostCentreMap());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("BR"));
        model.addAttribute("paymentAccount", staticInfo.getCompanyPaymentAccount());
        model.addAttribute("holdAccount", staticInfo.getHoldAccount());
        model.addAttribute("apartmentList", chassisService.findAllByDistributorNotNull());


        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/addReceiptVoucher";
    }


    /**
     * CURRENTLY UNUSED
     * POST CALL TO AJAX
     *
     * @param voucher
     * @param bindingResult
     * @param attributes
     * @return
     */
    @RequestMapping(value = "/Account/createReceiptVoucher", method = RequestMethod.POST)
    public String addReceiptVoucher_post(@ModelAttribute("voucherForm") Voucher voucher, BindingResult bindingResult, RedirectAttributes attributes) {
        voucherValidator.validate(voucher, bindingResult);

        if (bindingResult.hasErrors()) {
            return "redirect:/Account/createReceiptVoucher";
        }
        String voucherNumber = voucherService.saveReceipt(voucher).getVoucherNumber();
        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">voucher <strong>" + voucherNumber + " </strong>for Account with code: <strong>" + voucher.getAccountCode() + " </strong>added successfully.</div>");
        return "redirect:/Account/createReceiptVoucher";
    }


/////////////////////////////////////// Tax Values ///////////////////////////////////

    @RequestMapping(value = "/Account/ajax", method = RequestMethod.GET)

    public @ResponseBody
    List<Double> ajaxAccount(@RequestParam(value = "taxAccountId", defaultValue = "0") int taxAccountId) {

        if (taxAccountId > 0) {

            List<Double> taxResponseList = new ArrayList<>();
            TaxDetail taxDetail = taxDetailService.findDetail(accountService.findAccount(taxAccountId));
            taxResponseList.add(taxDetail.getFilerAmount());
            taxResponseList.add(taxDetail.getNonFilerAmount());
            return taxResponseList;
        } else {
            List<Double> taxResponseList = new ArrayList<>();
            return taxResponseList;
        }
    }


    /////////////////////////////////////// Journal voucher ///////////////////////////////////

    @RequestMapping(value = "/Account/createJournalVoucher", method = RequestMethod.GET)
    public String addJournalVoucher(Model model) {
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        VoucherList voucherList = new VoucherList();
        for (int i = 0; i < staticInfo.getMaxFormRows(); i++) {
            voucherList.add(new Voucher());
        }

        List<Account> accountList = accountService.getAccountListByLevel(4);
        model.addAttribute("projectList", stockService.getStockCategories());
        model.addAttribute("accounts", accountList);
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("costcentres", costCentreService.getCostCentreMap());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("JV"));
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/addJournalVoucher";
    }

    @RequestMapping(value = "/Account/createJournalVoucher", method = RequestMethod.POST)
    public String addJournalVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getChassisId() > 0) {
                Apartment apartment = chassisService.findByChassisId(voucher.getChassisId());
                apartment.setAmountPaid(apartment.getAmountPaid() - voucher.getDebit() + voucher.getCredit() );
                voucher.setChassisNo(apartment.getApartmentNo());
                chassisService.save(apartment);
            }
            if (voucher.getAccountCode() > 0) {
                voucherService.saveJournal(voucher);
                voucherNumber = voucher.getInvoiceNumber();
            }

        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Journal Vouchers with number <strong>" + voucherNumber + " </strong>added successfully.</div>");
        return "redirect:/Account/createJournalVoucher";
    }


    /////////////////////////////////////// Bank Reconciliation ///////////////////////////////////

    @RequestMapping(value = "/Account/bankReconciliation", method = RequestMethod.GET)
    public String bankReconciliation(Model model) {

        model.addAttribute("accounts", accountService.getDetailAccounts());

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/bankReconciliation";
    }

    @RequestMapping(value = "/Account/reconciliation_post", method = RequestMethod.POST)
    public String bankReconciliation_post(Model model, @RequestParam("account") int accountCode
            , @RequestParam("voucherStatus") String status
            , @RequestParam("dateTill") java.sql.Date date) {

        Map oMap = new HashMap();

        if (status.equals("Pending")) {
            List<Voucher> voucherList = voucherService.getVouchersByBankAccount(accountCode, date, true);
            model.addAttribute("vouchers", voucherList);
            List<Integer> accountCodes = new ArrayList<>();
            for (Voucher voucher : voucherList) {
                accountCodes.add(voucher.getAccountCode());
            }
            if (accountCodes.size() > 0) {
                for (Account account : accountService.getAccountNames(accountCodes)) {
                    oMap.put(account.getAccountCode(), account.getTitle());
                }
            }

        } else {
            List<Voucher> voucherList = voucherService.getVouchersByBankAccount(accountCode, date, false);
            model.addAttribute("vouchers", voucherList);
            List<Integer> accountCodes = new ArrayList<>();
            for (Voucher voucher : voucherList) {
                accountCodes.add(voucher.getAccountCode());
            }
            if (accountCodes.size() > 0) {
                for (Account account : accountService.getAccountNames(accountCodes)) {
                    oMap.put(account.getAccountCode(), account.getTitle());
                }
            }
        }
        model.addAttribute("accounts", accountService.getDetailAccounts());
        model.addAttribute("currentDate", date);
        model.addAttribute("postAccount", accountCode);
        model.addAttribute("status", status);
        model.addAttribute("oMap", oMap);

        return "transaction/bankReconciliationView";
    }

    @RequestMapping(value = "/Account/voucherAction", method = RequestMethod.POST)
    public String voucherAction(Model model, @RequestParam("account") int accountCode
            , @RequestParam("voucherStatus") String status
            , @RequestParam("voucherId") int voucherId
            , @RequestParam("clearDate") java.sql.Date date) {

        Map oMap = new HashMap();
        if (status.equals("Pending")) {
            Voucher voucher = voucherService.getVoucher(voucherId);
            voucher.setClearingDate(date);
            voucherService.save(voucher);
            List<Voucher> voucherList = voucherService.getVouchersByBankAccount(accountCode, date, true);
            model.addAttribute("vouchers", voucherList);
            List<Integer> accountCodes = new ArrayList<>();
            for (Voucher voucher2 : voucherList) {
                accountCodes.add(voucher2.getAccountCode());
            }
            if (accountCodes.size() > 0) {
                for (Account account : accountService.getAccountNames(accountCodes)) {
                    oMap.put(account.getAccountCode(), account.getTitle());
                }
            }
        } else {
            Voucher voucher = voucherService.getVoucher(voucherId);
            voucher.setClearingDate(null);
            voucherService.save(voucher);
            List<Voucher> voucherList = voucherService.getVouchersByBankAccount(accountCode, date, false);
            model.addAttribute("vouchers", voucherList);
            List<Integer> accountCodes = new ArrayList<>();
            for (Voucher voucher2 : voucherList) {
                accountCodes.add(voucher2.getAccountCode());
            }
            if (accountCodes.size() > 0) {
                for (Account account : accountService.getAccountNames(accountCodes)) {
                    oMap.put(account.getAccountCode(), account.getTitle());
                }
            }
        }
        model.addAttribute("accounts", accountService.getDetailAccounts());
        model.addAttribute("currentDate", date);
        model.addAttribute("postAccount", accountCode);
        model.addAttribute("status", status);
        model.addAttribute("oMap", oMap);

        return "transaction/bankReconciliationView";
    }


    /////////////////////////////////////////// voucher Removal ///////////////////

    @RequestMapping(value = "/Account/deleteVoucher", method = RequestMethod.POST)
    public String removeVoucher(@RequestParam("voucherNumber") String voucherNumber, @RequestParam("redirectUrl") String redirectUrl) {

        if (voucherNumber.contains("SL")) {
            Voucher voucher = voucherService.findByVoucherNumber(voucherNumber).stream().filter(e -> e.getChassisId() > 0).findAny().get();
            Apartment apartment = chassisService.findByChassisId(voucher.getChassisId());
            apartment.setSold(false);
            apartment.setSellingPrice(0.0);
            apartment.setCustomerAccount(0);
            apartment.setDistributorId(0);
            apartment.setAmountPaid(0);
            chassisService.save(apartment);
        }
        else if(voucherNumber.contains("JV")){
            List<Voucher> voucherList = voucherService.findByVoucherNumber(voucherNumber).stream().filter(e -> e.getChassisId() > 0).collect(Collectors.toList());
            for(Voucher voucher : voucherList){
                Apartment apartment = chassisService.findByChassisId(voucher.getChassisId());
                apartment.setAmountPaid(apartment.getAmountPaid() + voucher.getDebit() - voucher.getCredit());
                chassisService.save(apartment);
            }
        }
        else if(voucherNumber.contains("BR")){
            Optional<Voucher> voucher = voucherService.findByVoucherNumber(voucherNumber).stream().filter(e -> e.getChassisId() > 0).findAny();
            if(voucher.isPresent()){
                Apartment apartment = chassisService.findByChassisId(voucher.get().getChassisId());
                apartment.setAmountPaid(apartment.getAmountPaid() - voucher.get().getDebit());
                chassisService.save(apartment);
            }
        }
        voucherService.deleteVouchers(voucherNumber);
        return "redirect:/" + redirectUrl;
    }

    /////////////////////////////////////////// Get customer info /////////////////////////////////

    @RequestMapping(value = "Account/ajax/getCustomerInfo", method = RequestMethod.GET)
    public @ResponseBody
    List<String> getCustomerInfo(@RequestParam(value = "accountCode", defaultValue = "0") int accountCode, @RequestParam("isPurchase") boolean isPurchase) {

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (accountCode == staticInfo.getCashInHand()) {
            List<String> responseList = new ArrayList<>();
            responseList.add("CASH IN HAND");
            responseList.add(staticInfo.getCompanyAddress() + staticInfo.getAddressSecondary());
            responseList.add(staticInfo.getMobile());
            responseList.add(staticInfo.getSalesTaxNumber());
            responseList.add(staticInfo.getPhone());

            return responseList;
        }

        Account account = updateBalanceService.calculateLedgerBalance(accountService.findAccount(accountCode));


        return accountService.getInfo(account, isPurchase);
    }


    //////////////////////////////// Add voucher ///////////////////////////////////////////////

    @RequestMapping(value = "Account/ajax/addVoucher", method = RequestMethod.POST)
    public ResponseEntity<?> addVoucher(@ModelAttribute("voucherData") Voucher voucher, @RequestParam("prefix") String prefix) {

        if (voucher.getBillAmount() == 0) {
            return ResponseEntity.ok(voucherService.generateVoucherNumber(prefix));
        }
        if (prefix.equals("BR")) {
            if (voucher.getBankAccount() == staticInfoService.findStaticInfo().getPostDateCheckAccount()) {
                voucher.setStatus("PDC-PENDING");
            }
            voucherService.saveReceipt(voucher);
            // change commission status if all down payments are paid
            if((voucherService.getPaidAmount(voucher.getApartmentNo()) - paymentDueService.getSumOfDownPayment(voucher.getApartmentNo()) >= 0)){
                chassisService.updateCommissionStatus(voucher.getApartmentNo(),Constants.COMMISSION_STATUS_UNPAID);
            }
        } else if (prefix.equals("BP")) {
            voucherService.savePayment(voucher);
        }
        return ResponseEntity.ok(voucherService.generateVoucherNumber(prefix));
    }


    ///////////////////////////// Edit voucher /////////////////////////////////////////

    @RequestMapping(value = "Account/ajax/editVoucher", method = RequestMethod.GET)
    public @ResponseBody
    Voucher editVoucher(@ModelAttribute("voucherId") int voucherId) {
        Voucher voucherDetail = voucherService.getVoucher(voucherId);
        voucherDetail.setApartmentNo(voucherDetail.getChassisNo());
        return voucherDetail;
    }

    ///////////////////////// View Journal voucher ////////////////////////////////////////


    /**
     * Data Form Ajax Call JournalVoucher/json
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/Account/viewJournalVoucher", method = RequestMethod.GET)
    public String viewJournalVoucher(Model model) {

        return "transaction/viewJournalVoucher";
    }

    ///////////////////////////////// Edit Journal voucher //////////////////////////////////

    @RequestMapping(value = "/Account/editJournalVoucher", method = RequestMethod.GET)
    public String editJournalVoucher(Model model, @RequestParam(value = "voucherNumber") String voucherNumber) {
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        List<Voucher> voucherItemList = voucherService.findByVoucherNumber(voucherNumber);
        VoucherList voucherList = new VoucherList();
        String invoiceNumber = "";
        for (Voucher voucher : voucherItemList) {
            if(voucher.getChassisId() > 0){
                Apartment apartment = chassisService.findByChassisId(voucher.getChassisId());
            }
            invoiceNumber = voucher.getInvoiceNumber();
            voucherList.add(voucher);
        }
        int size = voucherItemList.size();
        for (int i = 0; i < staticInfo.getMaxFormRows() - size; i++) {
            voucherList.add(new Voucher());
        }

        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accounts", accountService.getAccountListByLevel(4));
        model.addAttribute("costcentres", costCentreService.getCostCentreMap());
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("invoiceNumber", invoiceNumber);
        model.addAttribute("journalVouchers", voucherService.findByVoucherType("JV%"));
        model.addAttribute("projectList", stockService.getStockCategories());


        if (voucherItemList.size() > 1) {
            model.addAttribute("currentDate", voucherItemList.get(1).getVoucherDate());
        }

        return "transaction/updateJournalVoucher";
    }

    @RequestMapping(value = "/Account/editJournalVoucher", method = RequestMethod.POST)
    public String updateJournalVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList,
                                            @RequestParam("voucherNumber") String voucherNumber,
                                            RedirectAttributes attributes) {
        List<Voucher> voucherNumberList = voucherService.findByVoucherNumber(voucherNumber);
        if (voucherNumberList.size() > 0) {
            List<Voucher> apartmentVoucherList = voucherNumberList.stream().filter(e -> e.getChassisId() > 0).collect(Collectors.toList());
            for(Voucher voucher : apartmentVoucherList){
                Apartment apartment = chassisService.findByChassisId(voucher.getChassisId());
                apartment.setAmountPaid(apartment.getAmountPaid() + voucher.getDebit() - voucher.getCredit());
                chassisService.save(apartment);
            }
            voucherService.remove(voucherNumberList);
        }
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getAccountCode() > 0) {
                if (voucher.getChassisId() > 0) {
                    Apartment apartment = chassisService.findByChassisId(voucher.getChassisId());
                    apartment.setAmountPaid(apartment.getAmountPaid() - voucher.getDebit() + voucher.getCredit());
                    voucher.setChassisNo(apartment.getApartmentNo());
                    chassisService.save(apartment);
                }
                voucherService.saveJournal(voucher);
                voucherNumber = voucher.getInvoiceNumber();
            }

        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Journal Vouchers with number <strong>" + voucherNumber + " </strong>updated successfully.</div>");
        return "redirect:/Account/viewJournalVoucher";
    }

    /////////////////////////////Get ledger balance for customer and venders ////////////////////////////

    @RequestMapping(value = "Account/ajax/getAmountBalance", method = RequestMethod.GET)
    public @ResponseBody
    List<Double> getAmountBalance(@ModelAttribute("accountCode") int accountCode, @RequestParam("prefix") String prefix) {
        List<Double> amountInfoList = new ArrayList<>();
        Account account = accountService.findAccount(accountCode);
        if (prefix.equals("BR")) {
            if (account.getStockCustomer() != null) {
                account = updateBalanceService.calculateLedgerBalance(accountService.findAccount(accountCode));
                amountInfoList.add(account.getTotalBalanceAmount());
                amountInfoList.add(account.getAmountLimit());

            }
        } else if (prefix.equals("BP")) {
            if (account.getStockVendor() != null) {
                account = updateBalanceService.calculateLedgerBalance(accountService.findAccount(accountCode));
                amountInfoList.add(account.getTotalBalanceAmount());
                amountInfoList.add(account.getAmountLimit());

            }
        }

        return amountInfoList;
    }

    /////////////////////////// JSON Generator On DataTable Ajax Call For Payment Vouchers//////////////////

    @RequestMapping(value = "/Payments/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> paymentVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populatePaymentVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherNumber", false, searchString, "BP"), draw), HttpStatus.OK);


    }

    /////////////////////////// JSON Generator On DataTable Ajax Call For Receipt Vouchers//////////////////

    @RequestMapping(value = "/Receipt/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> receiptVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }
        return new ResponseEntity(jsonPopulateService.populateReceiptVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherNumber", false, searchString, "BR"), draw), HttpStatus.OK);


    }

    /////////////////////////////////////////////// pdc journal vouchers ////////////////////////////////////////
    @RequestMapping(value = "/Account/createPdcJournalVoucher", method = RequestMethod.GET)
    public String addPdcJournalVoucher(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("dateFrom", date);
        model.addAttribute("dateTo", date);
        model.addAttribute("bankAccounts", bankDetailService.findAll());
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("PD%"));
        VoucherList voucherList = new VoucherList();
        for (Voucher voucher : voucherService.findJournalVouchersByPdcBankAccount(date, date, staticInfoService.findStaticInfo().getPostDateCheckAccount())) {
            if (voucher.getStatus().equals("PDC-PENDING")) {
                voucherList.add(voucher);
            }
        }
        model.addAttribute("accounts", accountService.getBankAccountList());
        model.addAttribute("costcentres", costCentreService.findAll());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("PD"));
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        model.addAttribute("accountMap", accountMap);
        model.addAttribute("currentDate", date);
        return "transaction/pdcJournalVoucher";
    }

    @RequestMapping(value = "/Account/createPdcJournalVoucher", method = RequestMethod.POST)
    public String addPdcJournalVoucher_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("dateFrom", fromDate);
        model.addAttribute("dateTo", toDate);
        model.addAttribute("bankAccounts", bankDetailService.findAll());
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("PD%"));
        VoucherList voucherList = new VoucherList();
        for (Voucher voucher : voucherService.findJournalVouchersByPdcBankAccount(fromDate, toDate, staticInfoService.findStaticInfo().getPostDateCheckAccount())) {
            if (voucher.getStatus().equals("PDC-PENDING")) {
                voucherList.add(voucher);
            }
        }
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accounts", accountService.getBankAccountList());
        model.addAttribute("costcentres", costCentreService.findAll());
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("PD"));

        return "transaction/pdcJournalVoucher";
    }

    @RequestMapping(value = "/Account/savePdcJournalVoucher", method = RequestMethod.POST)
    public String savePdcJournalVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        for (Voucher voucher : voucherList.getVoucherList()) {

            if (voucher.getAccountCode() > 0 && voucher.isCheck()) {
                for (Voucher receiptVoucher : voucherService.findByVoucherNumber(voucher.getVoucherNumberForPdc())) {
                    receiptVoucher.setStatus("PDC-CREATED");
                    voucherService.save(receiptVoucher);
                }
                voucher.setDebit(voucher.getCredit());
                voucher.setAccountCode(staticInfoService.findStaticInfo().getPostDateCheckAccount());
                voucher.setStatus("PARTIAL");
                voucher.setCredit(null);
                voucherService.saveJournal(voucher);
                Voucher bankVoucher = new Voucher();
                bankVoucher.setVoucherNumber(voucher.getVoucherNumber());
                bankVoucher.setAccountCode(voucher.getBankAccount());
                bankVoucher.setBankAccount(0);
                bankVoucher.setVoucherDate(voucher.getVoucherDate());
                bankVoucher.setStatus("PARTIAL");
                bankVoucher.setCredit(voucher.getDebit());
                bankVoucher.setDebit(null);
                voucherService.saveJournal(bankVoucher);
                voucherNumber = bankVoucher.getVoucherNumber();
            }

        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Post Dated Vouchers added successfully.</div>");
        return "redirect:/Account/createPdcJournalVoucher";
    }
    /////////////////////////////////////////////////// Bank Check Return Voucher //////////////////////////////////////////////////////////

    @RequestMapping(value = "/Account/createBankChequeReturn", method = RequestMethod.GET)
    public String addBankCheckReturn(Model model) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("dateFrom", date);
        model.addAttribute("dateTo", date);
        model.addAttribute("bankAccounts", bankDetailService.findAll());
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("CR%"));
        VoucherList voucherList = new VoucherList();
        for (Voucher voucher : voucherService.findBankAccountByDate(date, date)) {
            voucherList.add(voucher);
        }
        model.addAttribute("pdcAccount", staticInfoService.findStaticInfo().getPostDateCheckAccount());
        model.addAttribute("accounts", accountService.getBankAccountList());
        model.addAttribute("costcentres", costCentreService.findAll());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("CR"));
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        model.addAttribute("accountMap", accountMap);
        model.addAttribute("currentDate", date);
        return "transaction/chequeReturnVoucher";
    }

    @RequestMapping(value = "/Account/createBankChequeReturn", method = RequestMethod.POST)
    public String addBankCheckReturn_post(Model model, @RequestParam("dateFrom") java.sql.Date fromDate
            , @RequestParam("dateTo") java.sql.Date toDate) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", date);
        model.addAttribute("dateFrom", fromDate);
        model.addAttribute("dateTo", toDate);
        model.addAttribute("bankAccounts", bankDetailService.findAll());
        model.addAttribute("pdcAccount", staticInfoService.findStaticInfo().getPostDateCheckAccount());
        model.addAttribute("voucherNumbers", voucherService.getVoucherNumbers("CR%"));
        VoucherList voucherList = new VoucherList();
        for (Voucher voucher : voucherService.findBankAccountByDate(fromDate, toDate)) {
            voucherList.add(voucher);
        }
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accounts", accountService.getBankAccountList());
        model.addAttribute("costcentres", costCentreService.findAll());
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("CR"));
        return "transaction/chequeReturnVoucher";
    }

    @RequestMapping(value = "/Account/saveBankChequeReturn", method = RequestMethod.POST)
    public String saveBankChequeReturn_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        for (Voucher voucher : voucherList.getVoucherList()) {

            if (voucher.getAccountCode() > 0 && voucher.isCheck()) {
                for (Voucher receiptVoucher : voucherService.findByVoucherNumber(voucher.getVoucherNumberForPdc())) {
                    receiptVoucher.setStatus("RETURN");
                    voucherService.save(receiptVoucher);
                }
                voucher.setAccountCode(staticInfoService.findStaticInfo().getPostDateCheckAccount());
                voucher.setStatus("RETURN");
                voucherService.saveJournal(voucher);
                Voucher bankVoucher = new Voucher();
                bankVoucher.setVoucherNumber(voucher.getVoucherNumber());
                bankVoucher.setAccountCode(voucher.getBankAccount());
                bankVoucher.setBankAccount(0);
                bankVoucher.setVoucherDate(voucher.getVoucherDate());
                bankVoucher.setStatus("RETURN");
                bankVoucher.setDebit(voucher.getCredit());
                bankVoucher.setCredit(null);
                voucherService.saveJournal(bankVoucher);
                voucherNumber = bankVoucher.getVoucherNumber();
            }

        }

        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Bank Check Return Vouchers added successfully.</div>");
        return "redirect:/Account/createBankChequeReturn";
    }

    /**
     * For print payment voucher
     *
     * @param model
     * @param voucherNumber - To get Voucher list of this voucherNumber
     * @return - Print payment voucher page
     */

    @RequestMapping(value = "/Account/printPaymentVoucher", method = RequestMethod.GET)
    public String printPaymentVoucher(Model model, @RequestParam("voucherNumber") String voucherNumber) {
        List<Voucher> voucherList = voucherService.findByVoucherNumber(voucherNumber);
        for (Voucher voucher : voucherService.findByReferenceVoucher(voucherNumber)) {
            voucherList.add(voucher);
        }
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        Voucher voucher = voucherList.stream().findFirst().get();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        model.addAttribute("chequeNo", voucher.getChequeNumber());
        model.addAttribute("voucherDate", df.format(voucher.getVoucherDate()));
        model.addAttribute("remarks", voucher.getRemarks());
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("user", RoleSupport.getUserName());
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountListByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("imageUrl",staticInfo.getImageUrl());
        model.addAttribute("companyName", staticInfo.getCompanyName());
        model.addAttribute("accountMap", accountMap);
        return "transaction/printPaymentVoucher";
    }

    /**
     * For print receipt voucher
     *
     * @param model
     * @param voucherNumber - To get Voucher list of this voucherNumber
     * @return - Print receipt voucher page
     */

    @RequestMapping(value = "/Account/printReceiptVoucher", method = RequestMethod.GET)
    public String printReceiptVoucher(Model model, @RequestParam("voucherNumber") String voucherNumber) {
        List<Voucher> voucherList = voucherService.findByVoucherNumber(voucherNumber);
        for (Voucher voucher : voucherService.findByReferenceVoucher(voucherNumber)) {
            voucherList.add(voucher);
        }
        Voucher voucher = voucherList.stream().findFirst().get();
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        model.addAttribute("chequeNo", voucher.getChequeNumber());
        model.addAttribute("voucherDate", df.format(voucher.getVoucherDate()));
        model.addAttribute("remarks", voucher.getRemarks());
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("user", RoleSupport.getUserName());
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountListByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("imageUrl",staticInfo.getImageUrl());
        model.addAttribute("companyName",staticInfo.getCompanyName());
        model.addAttribute("accountMap", accountMap);
        return "transaction/printReceiptVoucher";
    }

    /**
     * Petty cash payment for Cash in hand account
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Account/addPettyCash", method = RequestMethod.GET)
    public String addPettyCash(Model model) {
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        VoucherList voucherList = new VoucherList();
        for (int i = 0; i < staticInfo.getMaxFormRows(); i++) {
            voucherList.add(new Voucher());
        }

        //  model.addAttribute("totalAmount", ledgerCalculationService.getCashInHandAmount(staticInfo.getCashInHand()));
        model.addAttribute("bankAccounts", accountService.findAccount(staticInfo.getBankHead()).getChildList());
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accounts", accountService.findAccountByLevelAlphabetically(4));
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("CP"));
        // model.addAttribute("journalVouchers", voucherService.findByVoucherType("JV%"));
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        model.addAttribute("accountMap", accountMap);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/addPettyCash";
    }

    @RequestMapping(value = "/Account/addPettyCash", method = RequestMethod.POST)
    public String addPettyCash_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        Double credit = 0.0;
        Voucher editBankVoucher = voucherList.getVoucherList().stream().findFirst().get();
        java.sql.Date date = editBankVoucher.getVoucherDate();
        int bankAccount = editBankVoucher.getBankVoucher();
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getAccountCode() > 0) {
                voucher.setBankAccount(bankAccount);
                voucher.setBillAmount(voucher.getDebit());
                voucher.setTaxAmount(0.0);
                voucherService.saveJournal(voucher);
                voucherNumber = voucher.getVoucherNumber();
                credit += voucher.getDebit();

            }

        }

        Voucher bankVoucher = new Voucher();
        bankVoucher.setVoucherDate(date);
        bankVoucher.setVoucherNumber(voucherNumber);
        bankVoucher.setAccountCode(bankAccount);
        bankVoucher.setCredit(credit);
        voucherService.saveJournal(bankVoucher);
        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Payment Petty Cash with number <strong>" + voucherNumber + " </strong>added successfully.</div>");
        return "redirect:/Account/viewPettyCashVouchers";
    }

    /**
     * Change balance amount of specific bank account
     *
     * @param accountCode
     * @return
     */
    @RequestMapping(value = "Account/ajax/changeBalance", method = RequestMethod.GET)
    public @ResponseBody
    Double getBankBalance(@RequestParam(value = "bankAccount", defaultValue = "0") int accountCode) {
        return ledgerCalculationService.getCashInHandAmount(accountCode);
    }

    /**
     * Petty cash vouchers listing
     *
     * @return
     */
    @RequestMapping(value = "/Account/viewPettyCashVouchers", method = RequestMethod.GET)
    public String viewPettyCashVoucher() {

        return "transaction/viewPettyCashVoucher";
    }


    /**
     * JSON Generator On DataTable Ajax Call For Petty Cash Vouchers (Payment)
     *
     * @param start
     * @param length
     * @param draw
     * @param searchString
     * @return
     */
    @RequestMapping(value = "/PettyCash/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> pettyCashVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }

        return new ResponseEntity(jsonPopulateService.populatePettyCashVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherNumber", false, searchString, "CP"), draw), HttpStatus.OK);


    }

    /**
     * Update payment petty cash voucher
     *
     * @param model
     * @param voucherNumber
     * @return
     */
    @RequestMapping(value = "/Account/updatePettyCashVoucher", method = RequestMethod.GET)
    public String updatePettyCashVoucher(Model model, @RequestParam(value = "voucherNumber") String voucherNumber) {
        List<Voucher> voucherItemList = voucherService.findByVoucherNumber(voucherNumber);
        double total = 0;
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        VoucherList voucherList = new VoucherList();
        int currentBankAccountCode = 0;
        for (Voucher voucher : voucherItemList) {
            if (voucher.getBankAccount() > 0) {
                total += voucher.getDebit();
                voucherList.add(voucher);
            } else {
                currentBankAccountCode = voucher.getAccountCode();
            }
        }
        int size = voucherItemList.size();
        for (int i = 0; i < staticInfoService.findStaticInfo().getMaxFormRows() - size; i++) {
            Voucher voucher = new Voucher();
            voucherList.add(voucher);
        }
        Map accountMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("voucherList", voucherList);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("total", total);
        model.addAttribute("currentDate", voucherItemList.get(1).getVoucherDate());
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("currentBankAccountCode", currentBankAccountCode);
        model.addAttribute("accounts", accountService.findAccountByLevelAlphabetically(4));
        model.addAttribute("bankAccounts", accountService.findAccount(staticInfo.getBankHead()).getChildList());
        return "transaction/updatePettyCashVoucher";

    }

    @RequestMapping(value = "/Account/updatePettyCashVoucher", method = RequestMethod.POST)
    public String updatePettyCashVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        Double credit = 0.0;
        String voucherNumber = "";
        Voucher editBankVoucher = voucherList.getVoucherList().stream().findFirst().get();
        List<Voucher> voucherNumberList = voucherService.findByVoucherNumber(editBankVoucher.getVoucherNumber());
        if (voucherNumberList.size() > 0) {
            voucherService.remove(voucherNumberList);
        }

        java.sql.Date date = editBankVoucher.getVoucherDate();
        int bankAccount = editBankVoucher.getBankVoucher();
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getAccountCode() > 0) {
                voucher.setBankAccount(bankAccount);
                voucher.setBillAmount(voucher.getDebit());
                voucher.setTaxAmount(0.0);
                voucherService.saveJournal(voucher);
                voucherNumber = voucher.getVoucherNumber();
                credit += voucher.getDebit();

            }

        }

        Voucher bankVoucher = new Voucher();
        bankVoucher.setVoucherDate(date);
        bankVoucher.setVoucherNumber(voucherNumber);
        bankVoucher.setAccountCode(bankAccount);
        bankVoucher.setCredit(credit);
        voucherService.saveJournal(bankVoucher);
        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Payment Petty Cash with number <strong>" + voucherNumber + " </strong>updated successfully.</div>");
        return "redirect:/Account/viewPettyCashVouchers";
    }

    /**
     * Petty cash Purchase for Cash in hand account
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/Account/addPurchasePettyCash", method = RequestMethod.GET)
    public String addPurchasePettyCash(Model model) {
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        VoucherList voucherList = new VoucherList();
        for (int i = 0; i < staticInfo.getMaxFormRows(); i++) {
            voucherList.add(new Voucher());
        }

        model.addAttribute("bankAccounts", accountService.findAccount(staticInfo.getBankHead()).getChildList());
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("accounts", accountService.getStockAccounts());
        model.addAttribute("voucherNumber", voucherService.generateVoucherNumber("PP"));
        Map accountMap = new HashMap();

        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }

        model.addAttribute("accountMap", accountMap);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("currentDate", formatter.format(date));

        return "transaction/addPurchasePettyCash";
    }

    @RequestMapping(value = "/Account/addPurchasePettyCash", method = RequestMethod.POST)
    public String addPurchasePettyCash_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        String voucherNumber = "";
        Double credit = 0.0;
        Voucher editBankVoucher = voucherList.getVoucherList().stream().findFirst().get();
        java.sql.Date date = editBankVoucher.getVoucherDate();
        int bankAccount = editBankVoucher.getBankVoucher();
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getAccountCode() > 0) {
                voucher.setBankAccount(bankAccount);
                voucher.setBillAmount(voucher.getDebit());
                voucher.setTaxAmount(0.0);
                voucherService.saveJournal(voucher);
                voucherNumber = voucher.getVoucherNumber();
                credit += voucher.getDebit();

            }

        }

        Voucher bankVoucher = new Voucher();
        bankVoucher.setVoucherDate(date);
        bankVoucher.setVoucherNumber(voucherNumber);
        bankVoucher.setAccountCode(bankAccount);
        bankVoucher.setCredit(credit);
        voucherService.saveJournal(bankVoucher);
        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Purchase Petty Cash with number <strong>" + voucherNumber + " </strong>added successfully.</div>");
        return "redirect:/Account/viewPurchasePettyCashVouchers";
    }


    /**
     * Petty cash vouchers listing
     *
     * @return
     */
    @RequestMapping(value = "/Account/viewPurchasePettyCashVouchers", method = RequestMethod.GET)
    public String viewPurchasePettyCashVoucher() {

        return "transaction/viewPurchasePettyCashVoucher";
    }


    /**
     * JSON Generator On DataTable Ajax Call For Petty Cash Vouchers (Payment)
     *
     * @param start
     * @param length
     * @param draw
     * @param searchString
     * @return
     */
    @RequestMapping(value = "/PurchasePettyCash/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> purchasePettyCashVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }

        return new ResponseEntity(jsonPopulateService.populatePurchasePettyCashVoucherList(voucherService.findAllPaged(pageIndex, length, "voucherNumber", false, searchString, "PP"), draw), HttpStatus.OK);


    }


    /**
     * JSON Generator On DataTable Ajax Call For Journal Vouchers (Payment)
     *
     * @param start
     * @param length
     * @param draw
     * @param searchString
     * @return
     */
    @RequestMapping(value = "/JournalVoucher/json", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<Page<Voucher>> journalVoucherListingJSON(@RequestParam(name = "start", defaultValue = "0") int start
            , @RequestParam(name = "length", defaultValue = "10") int length
            , @RequestParam(name = "draw") int draw
            , @RequestParam(name = "search[value]", defaultValue = "") String searchString) {


        int pageIndex = 0;
        if (start > 0) {
            pageIndex = start / length;
        }

        return new ResponseEntity(jsonPopulateService.populateJournalVoucherList(voucherService.findAllPaged(pageIndex, length, "voucher_number", false, searchString, Constants.PREFIX_FOR_JOURNAL), draw), HttpStatus.OK);


    }

    /**
     * Update payment petty cash voucher
     *
     * @param model
     * @param voucherNumber
     * @return
     */
    @RequestMapping(value = "/Account/updatePurchasePettyCashVoucher", method = RequestMethod.GET)
    public String updatePurchasePettyCashVoucher(Model model, @RequestParam(value = "voucherNumber") String voucherNumber) {
        List<Voucher> voucherItemList = voucherService.findByVoucherNumber(voucherNumber);
        double total = 0;
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        VoucherList voucherList = new VoucherList();
        int currentBankAccountCode = 0;
        for (Voucher voucher : voucherItemList) {
            if (voucher.getBankAccount() > 0) {
                total += voucher.getDebit();
                voucherList.add(voucher);
            } else {
                currentBankAccountCode = voucher.getAccountCode();
            }
        }
        int size = voucherItemList.size();
        for (int i = 0; i < staticInfoService.findStaticInfo().getMaxFormRows() - size; i++) {
            Voucher voucher = new Voucher();
            voucherList.add(voucher);
        }
        Map accountMap = new HashMap();
        for (Account account : accountService.findAll()) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("totalAmount", ledgerCalculationService.getCashInHandAmount(staticInfo.getCashInHand()));
        model.addAttribute("accountMap", accountMap);
        model.addAttribute("total", total);
        model.addAttribute("voucherList", voucherList);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("currentDate", voucherItemList.get(1).getVoucherDate());
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("currentBankAccountCode", currentBankAccountCode);
        model.addAttribute("accounts", accountService.getStockAccounts());
        model.addAttribute("bankAccounts", accountService.findAccount(staticInfo.getBankHead()).getChildList());
        return "transaction/updatePurchasePettyCashVoucher";

    }

    @RequestMapping(value = "/Account/updatePurchasePettyCashVoucher", method = RequestMethod.POST)
    public String updatePurchasePettyCashVoucher_post(@ModelAttribute("voucherList") VoucherList voucherList, RedirectAttributes attributes) {

        Double credit = 0.0;
        String voucherNumber = "";
        Voucher editBankVoucher = voucherList.getVoucherList().stream().findFirst().get();
        List<Voucher> voucherNumberList = voucherService.findByVoucherNumber(editBankVoucher.getVoucherNumber());
        if (voucherNumberList.size() > 0) {
            voucherService.remove(voucherNumberList);
        }

        java.sql.Date date = editBankVoucher.getVoucherDate();
        int bankAccount = editBankVoucher.getBankVoucher();
        for (Voucher voucher : voucherList.getVoucherList()) {
            if (voucher.getAccountCode() > 0) {
                voucher.setBankAccount(bankAccount);
                voucher.setBillAmount(voucher.getDebit());
                voucher.setTaxAmount(0.0);
                voucherService.saveJournal(voucher);
                voucherNumber = voucher.getVoucherNumber();
                credit += voucher.getDebit();

            }

        }

        Voucher bankVoucher = new Voucher();
        bankVoucher.setVoucherDate(date);
        bankVoucher.setVoucherNumber(voucherNumber);
        bankVoucher.setAccountCode(bankAccount);
        bankVoucher.setCredit(credit);
        voucherService.saveJournal(bankVoucher);
        attributes.addFlashAttribute("voucherAddSuccess", "<div style=\"background-color: #d4e8b5;height: 40px;text-align: center;padding-top: 5px;border: 2px solid green;border-radius: 5px;font-size: initial;\">Payment Voucher with number <strong>" + voucherNumber + " </strong>updated successfully.</div>");
        return "redirect:/Account/viewPurchasePettyCashVouchers";
    }

    /**
     * For print Journal voucher
     *
     * @param model
     * @param voucherNumber - To get Voucher list of this voucherNumber
     * @return - Print Journal voucher page
     */

    @RequestMapping(value = "/Account/printJournalVoucher", method = RequestMethod.GET)
    public String printJournalVoucher(Model model, @RequestParam("voucherNumber") String voucherNumber) {
        List<Voucher> voucherList = voucherService.findByVoucherNumber(voucherNumber);
        Voucher voucher = voucherList.stream().findFirst().get();
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        model.addAttribute("voucherDate", df.format(voucher.getVoucherDate()));
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("user", RoleSupport.getUserName());
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountListByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("imageUrl",staticInfo.getImageUrl());
        model.addAttribute("companyName",staticInfo.getCompanyName());
        model.addAttribute("accountMap", accountMap);
        return "transaction/printJournalVoucher";
    }

    /**
     * For print payment petty cash voucher
     *
     * @param model
     * @param voucherNumber - To get Voucher list of this voucherNumber
     * @return - Print payment petty cash voucher page
     */

    @RequestMapping(value = "/Account/printPaymentPettyCash", method = RequestMethod.GET)
    public String printPaymentPettyCash(Model model, @RequestParam("voucherNumber") String voucherNumber) {
        List<Voucher> voucherList = voucherService.findByVoucherNumber(voucherNumber);
        double total;
        total = voucherList.stream().filter(e -> e.getCredit() > 0).findFirst().get().getCredit();
        Voucher voucher = voucherList.stream().filter(e -> e.getBankAccount() > 0).findFirst().get();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        model.addAttribute("total", total);
        model.addAttribute("openingBalance", ledgerCalculationService.getCashInHandAmount(voucher.getBankAccount()));
        model.addAttribute("voucherDate", df.format(voucher.getVoucherDate()));
        model.addAttribute("bankAccount", voucher.getBankAccount());
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("user", RoleSupport.getUserName());
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountListByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        return "transaction/printPaymentPettyCash";
    }

    /**
     * For print payment petty cash voucher
     *
     * @param model
     * @param voucherNumber - To get Voucher list of this voucherNumber
     * @return - Print payment petty cash voucher page
     */

    @RequestMapping(value = "/Account/printPurchasePettyCash", method = RequestMethod.GET)
    public String printPurchasePettyCash(Model model, @RequestParam("voucherNumber") String voucherNumber) {
        List<Voucher> voucherList = voucherService.findByVoucherNumber(voucherNumber);
        double total;
        total = voucherList.stream().filter(e -> e.getCredit() > 0).findFirst().get().getCredit();
        Voucher voucher = voucherList.stream().filter(e -> e.getBankAccount() > 0).findFirst().get();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        model.addAttribute("total", total);
        model.addAttribute("openingBalance", ledgerCalculationService.getCashInHandAmount(voucher.getBankAccount()));
        model.addAttribute("voucherDate", df.format(voucher.getVoucherDate()));
        model.addAttribute("bankAccount", voucher.getBankAccount());
        model.addAttribute("voucherList", voucherList);
        model.addAttribute("voucherNumber", voucherNumber);
        model.addAttribute("user", RoleSupport.getUserName());
        Map accountMap = new HashMap();
        for (Account account : accountService.getAccountListByLevel(4)) {
            accountMap.put(account.getAccountCode(), account.getTitle());
        }
        model.addAttribute("accountMap", accountMap);
        return "transaction/printPurchasePettyCash";
    }

    /**
     * Get salesman list for selected customer accountCode
     *
     * @param accountCode customer accountCode
     * @return
     */
    @RequestMapping(value = "Account/ajax/getSalesman", method = RequestMethod.GET)
    public @ResponseBody
    List<Distributor> getSalesman(@ModelAttribute("accountCode") int accountCode) {

        List<Distributor> distributorList = new ArrayList<>();

        Account account = accountService.findAccount(accountCode);
        if (account.getStockCustomer() != null) {
            for (Apartment apartment : chassisService.findByCustomerAccount(accountCode)) {
                Distributor distributor = distributorService.findByDistributorId(apartment.getDistributorId());
                if (distributor != null) {
                    distributor.setUserAuth(null);
                    if (!distributorList.contains(distributor))
                        distributorList.add(distributor);
                }
            }
        }

        return distributorList;
    }

    /**
     * Get stock map for selected salesman
     *
     * @param salesmanId salesman id
     * @return
     */
    @RequestMapping(value = "Account/ajax/getStockListForSalesman", method = RequestMethod.GET)
    public @ResponseBody
    Map<Integer, String> getStockListForSalesman(@ModelAttribute("salesman") int salesmanId, @ModelAttribute("accountCode") int accountCode) {
        Map<Integer, String> stockMap = new HashMap<>();
        for (Apartment apartment : chassisService.findByCustomerAccountAndDistributorId(accountCode, salesmanId)) {
            if (!stockMap.containsKey(apartment.getStock().getStockId())) {
                stockMap.put(apartment.getStock().getStockId(), apartment.getStock().getName());
            }
        }
        return stockMap;
    }

    /**
     * Get chassis map for selected stockId
     *
     * @param stockId stockId
     * @return
     */
    @RequestMapping(value = "Account/ajax/getChassisListForSalesman", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> getChassisListForSalesman(@ModelAttribute("stockId") int stockId,
                                                  @ModelAttribute("accountCode") int accountCode,
                                                  @ModelAttribute("salesman") int salesmanId) {
        Map<String, String> chassisMap = new HashMap<>();

        for (Apartment apartment : chassisService.getChassisForSpecificCustomer(accountCode, salesmanId, stockService.findByStockId(stockId))) {
            if (!chassisMap.containsKey(apartment.getApartmentNo())) {
                chassisMap.put(apartment.getApartmentNo(), apartment.getApartmentNo());
            }
        }

        return chassisMap;
    }

    /**
     * Get salesman, stock and customer for selected chassis number
     *
     * @param apartmentId
     * @return
     */
    @RequestMapping(value = "Account/ajax/getChassisDetail", method = RequestMethod.GET)
    public @ResponseBody
    Apartment getChassisDetail(@ModelAttribute("apartmentId") int apartmentId) {
        if (apartmentId == 0) {
            Apartment apartment = new Apartment();
            apartment.setApartmentId(0);
            return apartment;
        }
        Apartment apartment = chassisService.findByChassisId(apartmentId);
        apartment.setStockId(apartment.getStock().getStockId());
        apartment.setItemName(apartment.getStock().getName());
        Distributor distributor = distributorService.findByDistributorId(apartment.getDistributorId());
        if (distributor != null) {
            apartment.setSalesmanName(distributor.getName());
        }
        apartment.setCategoryId(apartment.getStock().getStockCategory().getCategoryId());
        apartment.setSellingPrice(apartment.getSellingPrice() - apartment.getAmountPaid());
        apartment.setStock(null);
        apartment.setApartmentHold(null);
        apartment.setPaymentPlan(null);
       // apartment.setDueAmount(paymentDueService.getDueAmount(apartmentNumber, new java.sql.Date(Calendar.getInstance().getTime().getTime())));

        return apartment;
    }

    /**
     * Get all chassis map in reset case
     *
     * @return
     */
    @RequestMapping(value = "Account/ajax/getAllChassisList", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> getAllChassisList(@ModelAttribute("bankAccount") int bankAccount) {
        Map<String, String> chassisMap = new HashMap<>();
        List<Apartment> apartmentList;
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getHoldAccount() == bankAccount) {
            apartmentList = chassisService.findAll();
        } else {
            apartmentList = chassisService.findAllByDistributorNotNull();
        }

        for (Apartment apartment : apartmentList) {
            if (!chassisMap.containsKey(apartment.getApartmentNo())) {
                chassisMap.put(apartment.getApartmentNo(), apartment.getApartmentNo());
            }
        }
        return chassisMap;
    }


}
