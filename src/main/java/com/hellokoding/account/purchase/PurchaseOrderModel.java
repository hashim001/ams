package com.hellokoding.account.purchase;



import java.sql.Date;
import java.util.ArrayList;

public class PurchaseOrderModel {

    private String orderNumber;
    private Integer supplierCode;
    private Integer storeOrderId;
    private Date deliveryDate;
    private Date orderDate;
    private String remarks;
    private String billNumber;
    private boolean isOrderFinal;
    private String orderStatus;
    private String paymentStatus;
    private String poType;

    private Integer requestId;

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(Integer supplierCode) {
        this.supplierCode = supplierCode;
    }

    public Integer getStoreOrderId() {
        return storeOrderId;
    }

    public void setStoreOrderId(Integer storeOrderId) {
        this.storeOrderId = storeOrderId;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isOrderFinal() {
        return isOrderFinal;
    }

    public void setOrderFinal(boolean orderFinal) {
        isOrderFinal = orderFinal;
    }


    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPoType() {
        return poType;
    }

    public void setPoType(String poType) {
        if (!poType.equals("IMO") && !poType.equals("LPO")) {
            this.poType = "LPO";
        } else {
            this.poType = poType;
        }
    }

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }
}
