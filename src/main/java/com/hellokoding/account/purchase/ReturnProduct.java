package com.hellokoding.account.purchase;

public class ReturnProduct {

    private String transitionCode;
    private double rate;
    private String name;
    private double maxQuantity;
    private String measureUnit;
    private int orderProductId;


    public ReturnProduct(String transitionCode, double rate, String name, double maxQuantity, String measureUnit, int orderProductId) {
        this.transitionCode = transitionCode;
        this.rate = rate;
        this.name = name;
        this.maxQuantity = maxQuantity;
        this.measureUnit = measureUnit;
        this.orderProductId = orderProductId;
    }

    public String getTransitionCode() {
        return transitionCode;
    }

    public void setTransitionCode(String transitionCode) {
        this.transitionCode = transitionCode;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(double maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public int getOrderProductId() {
        return orderProductId;
    }

    public void setOrderProductId(int orderProductId) {
        this.orderProductId = orderProductId;
    }
}
