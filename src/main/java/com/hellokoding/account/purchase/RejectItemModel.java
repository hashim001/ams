package com.hellokoding.account.purchase;

public class RejectItemModel {

    private String itemName;
    private double rejectedQuantity;

    public RejectItemModel(String itemName, double rejectedQuantity) {
        this.itemName = itemName;
        this.rejectedQuantity = rejectedQuantity;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getRejectedQuantity() {
        return rejectedQuantity;
    }

    public void setRejectedQuantity(double rejectedQuantity) {
        this.rejectedQuantity = rejectedQuantity;
    }
}
