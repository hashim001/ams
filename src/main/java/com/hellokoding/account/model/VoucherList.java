package com.hellokoding.account.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class  VoucherList {

    private List <Voucher> voucherList;
    private String deliveryNumber;
    private String referenceNumber;
    private Double discountAmount;
    private boolean doPrint;
    private int supplierId;
    // Customer Attributes
    private Integer customerAccountCode;
    private String title;
    private String mobile;
    private String landline;
    private String address;
    private String email;
    private String cnic;
    private String taxStatus;
    private Date createDate;


    public VoucherList(){

        this.voucherList = new ArrayList<>();
    }

    public String getDeliveryNumber() {
        return deliveryNumber;
    }

    public void setDeliveryNumber(String deliveryNumber) {
        this.deliveryNumber = deliveryNumber;
    }

    public List<Voucher> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<Voucher> voucherList) {
        this.voucherList = voucherList;
    }

    public void add(Voucher voucher){
        this.voucherList.add(voucher);
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Double getDiscountAmount() {

        if(discountAmount == null){
            return 0.0;
        }
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public boolean isDoPrint() {
        return doPrint;
    }

    public void setDoPrint(boolean doPrint) {
        this.doPrint = doPrint;
    }

    public Integer getCustomerAccountCode() {
        return customerAccountCode;
    }

    public void setCustomerAccountCode(Integer customerAccountCode) {
        this.customerAccountCode = customerAccountCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }
}
