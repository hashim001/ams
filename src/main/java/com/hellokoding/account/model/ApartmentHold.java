package com.hellokoding.account.model;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "apartment_hold")
public class ApartmentHold {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "apartment_hold_id")
    private int apartmentHoldId;
    @Column(name = "onhold_days")
    private int onHoldDays;
    @Column(name = "remarks")
    private String remarks;
    @Column(name = "customer_name")
    private String customerName;
    @Column(name = "cnic")
    private String cnic;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "hold_date")
    private Date holdDate;
    @Column(name = "salesman_id")
    private Integer salesmanId;
    @Column(name = "hold_amount")
    private Double holdAmount;
    @Transient
    private int apartmentId;
    @Transient
    private long days;
    @Transient
    private String salesmanName;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "apartment_id", unique = true, nullable = true, insertable = true, updatable = true)
    private Apartment apartment;

    public int getApartmentHoldId() {
        return apartmentHoldId;
    }

    public void setApartmentHoldId(int apartmentHoldId) {
        this.apartmentHoldId = apartmentHoldId;
    }

    public int getOnHoldDays() {
        return onHoldDays;
    }

    public void setOnHoldDays(int onHoldDays) {
        this.onHoldDays = onHoldDays;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getHoldDate() {
        return holdDate;
    }

    public void setHoldDate(Date holdDate) {
        this.holdDate = holdDate;
    }


    public Integer getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Integer salesmanId) {
        this.salesmanId = salesmanId;
    }

    public long getDays() {
        return days;
    }

    public void setDays(long days) {
        this.days = days;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public Double getHoldAmount() {
        return holdAmount;
    }

    public void setHoldAmount(Double holdAmount) {
        this.holdAmount = holdAmount;
    }
}
