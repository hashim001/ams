package com.hellokoding.account.model;


import com.hellokoding.account.model.jsonentity.AccountDto;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;


@Entity
@Table(name = "account_bank")
public class BankDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "detail_id")
    private int detailId;


    @Column(name = "bank_account")
    private String bankAccount;
    @Column(name = "bank_branch")
    private String bankBranch;
    @Column(name = "bank_contact_person")
    private String bankContactPerson;
    @Column(name = "bank_ntn")
    private String bankNtn;
    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "bank_landline")
    private String bankLandline;
    @Column(name = "email_address")
    private String emailAddress;


    @Column(name = "scan_form")
    private String scanForm;
    @Column(name = "opening_date")
    private Date accountOpeningDate;

    @Column(name = "branch_code")
    private String branchCode;
    @Column(name = "url")
    private String url;

    @Transient
    private AccountDto accountDto;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_code", unique = true, nullable = true, insertable = true, updatable = true)
    private Account account;


    @Transient
    private int accountCode;

    public Date getAccountOpeningDate() {
        return accountOpeningDate;
    }

    public void setAccountOpeningDate(Date accountOpeningDate) {
        this.accountOpeningDate = accountOpeningDate;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public int getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(int accountCode) {
        this.accountCode = accountCode;
    }


    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }


    public int getDetailId() {
        return detailId;
    }

    public void setDetailId(int detailId) {
        this.detailId = detailId;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankContactPerson() {
        return bankContactPerson;
    }

    public void setBankContactPerson(String bankContactPerson) {
        this.bankContactPerson = bankContactPerson;
    }

    public String getBankNtn() {
        return bankNtn;
    }

    public void setBankNtn(String bankNtn) {
        this.bankNtn = bankNtn;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getBankLandline() {
        return bankLandline;
    }

    public void setBankLandline(String bankLandline) {
        this.bankLandline = bankLandline;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getScanForm() {
        return scanForm;
    }

    public void setScanForm(String scanForm) {
        this.scanForm = scanForm;
    }

    public AccountDto getAccountDto() {
        return accountDto;
    }

    public void setAccountDto(AccountDto accountDto) {
        this.accountDto = accountDto;
    }
}
