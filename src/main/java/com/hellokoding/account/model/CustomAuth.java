/**
 * Object For handling SpringSecurity User and holding UserAuth
 *  throughout the Session.
 */


package com.hellokoding.account.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CustomAuth extends User {

    private UserAuth userAuth;


    public CustomAuth(UserAuth userAuth,String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.userAuth =userAuth;
    }

    public CustomAuth(UserAuth userAuth,String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.userAuth =userAuth;
    }

    public UserAuth getUserAuth() {
        return userAuth;
    }

    public void setUserAuth(UserAuth userAuth) {
        this.userAuth = userAuth;
    }
}
