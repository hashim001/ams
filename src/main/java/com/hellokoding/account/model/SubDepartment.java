package com.hellokoding.account.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "department_sub")
public class SubDepartment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "subdep_id")
    private int subDepId;
    @Column(name = "subdep_name")
    private String subDepName;

    @Transient
    private Integer deptId;

    @ManyToOne()
    @JoinColumn(name = "dep_id", nullable = true)
    private Department subDepartment;

    public int getSubDepId() {
        return subDepId;
    }

    public void setSubDepId(int subDepId) {
        this.subDepId = subDepId;
    }

    public String getSubDepName() {
        return subDepName;
    }

    public void setSubDepName(String subDepName) {
        this.subDepName = subDepName;
    }

    public Department getSubDepartment() {
        return subDepartment;
    }

    public void setSubDepartment(Department subDepartment) {
        this.subDepartment = subDepartment;
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }
}
