package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "payment_plan")
public class    PaymentPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "plan_id")
    private Integer planId;
    @Column(name = "title")
    private String title;
    @Column(name = "updated")
    private Date updated;
    @Column(name = "installment_rate")
    private double installmentRate;

    @Transient
    private int stockId;
    @Transient
    private int apartmentId;


    @ManyToOne()
    @JoinColumn(name = "stock_id", nullable = true)
    private Stock planStock;

    @ManyToOne()
    @JoinColumn(name = "apartment_id", nullable = true)
    private Apartment planApartment;

    @OneToMany(mappedBy = "paymentPlan" ,cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PaymentPlanDown> paymentPlanDownList;

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public int getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }

    public Apartment getPlanApartment() {
        return planApartment;
    }

    public void setPlanApartment(Apartment planApartment) {
        this.planApartment = planApartment;
    }

    public List<PaymentPlanDown> getPaymentPlanDownList() {
        return paymentPlanDownList;
    }

    public void setPaymentPlanDownList(List<PaymentPlanDown> paymentPlanDownList) {
        this.paymentPlanDownList = paymentPlanDownList;
    }

    public Stock getPlanStock() {
        return planStock;
    }

    public void setPlanStock(Stock planStock) {
        this.planStock = planStock;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public double getInstallmentRate() {
        return installmentRate;
    }

    public void setInstallmentRate(double installmentRate) {
        this.installmentRate = installmentRate;
    }


}
