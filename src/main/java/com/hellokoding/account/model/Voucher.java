package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;


@Entity
@Table(name = "voucher")
public class Voucher implements Comparable<Voucher>{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "voucher_id")
    private int voucherId;
    @Column(name = "voucher_number")
    private String voucherNumber;
    @Column(name = "voucher_date")
    private Date voucherDate;
    @Column(name = "cheque_number")
    private String chequeNumber;
    @Column(name = "bank_account")
    private int bankAccount;
    @Column(name = "account_code")
    private Integer accountCode;
    @Column(name = "cost_centre")
    private Long costCentre;
    @Column(name = "tax_account")
    private Integer taxAccount;
    @Column(name = "bill_amount")
    private Double billAmount;
    @Column(name = "tax_amount")
    private Double taxAmount;
    @Column(name = "clearing_date")
    private Date clearingDate;
    private String remarks;
    private Integer isActive;
    @Column(name = "reference_voucher")
    private String referenceVoucher;
    private Double credit;
    private Double debit;
    @Column(name = "item_account")
    private Integer itemAccount;
    @Column(name = "bill_number")
    private String billNumber;
    @Column(name = "bill_date")
    private Date billDate;
    @Column(name = "item_quantity")
    private Double itemQuantity;
    @Column(name = "item_rate")
    private Double itemRate;
    @Column(name = "tax_rate")
    private Double taxRate;
    @Column(name = "status")
    private String status;
    @Column(name = "chassis_id")
    private int chassisId;
    @Column(name = "own_amount")
    private double ownAmount;
    @Column(name = "chassis_no")
    private String chassisNo;
    @Column(name = "advance_tax")
    private double advanceTax;
    @Column(name = "salesman_id")
    private int salesmanId;
    //for commission amount of car maker
    @Column(name = "commission_amount")
    private Double commissionAmount;
    @Column(name = "auth_id")
    private Integer authId;
    @Column(name = "invoice_number")
    private String invoiceNumber;
    @Column(name = "project_code")
    private Integer projectCode;
    @Transient
    private boolean check;

    @Transient
    private double balance;

    @Transient
    private String voucherType;

    @Transient
    private String itemName;

    /// for purchase order voucher
    @Transient
    private double requestQuantity;

    @Transient
    private Double stageQuantity;

    @Transient
    private Double finalQuantity;

    @Transient
    private String measureUnit;

    @Transient
    private String taxString;

    // For delivery
    @Transient
    private Integer productId;

    // For bill payment
    @Transient
    private Integer billId;

    // for journal voucher
    @Transient
    private Integer creditAccount;

    @Transient
    private String voucherNumberForPdc;

    // for petty cash
    @Transient
    private Integer bankVoucher;

    // For RawMaterial Purchase Order
    @Transient
    private String transitionCode;

    @Transient
    private List<Voucher> internalVoucherList;


    // For RawMaterial Report
    @Transient
    private double issuedQuantity;

    @Transient
    private double receivedQuantity;

    @Transient
    private double totalBalance;

    @Transient
    private double total;

    // for new Apartment number

    @Transient
    private String floor;
    @Transient
    private int rooms;
    @Transient
    private String apartmentNo;

    //for current stock report
    @Transient
    private List<String> chassisList;

    @Transient
    private int siteId;

    @Transient
    private int phaseId;

    public List<Voucher> getInternalVoucherList() {
        return internalVoucherList;
    }

    public void setInternalVoucherList(List<Voucher> internalVoucherList) {
        this.internalVoucherList = internalVoucherList;
    }

    ///
    public Integer getItemAccount() {
        if (itemAccount == null)
            return 0;

        return itemAccount;
    }


    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public double getRequestQuantity() {
        return requestQuantity;
    }

    public void setRequestQuantity(double requestQuantity) {
        this.requestQuantity = requestQuantity;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public void setItemAccount(Integer itemAccount) {
        this.itemAccount = itemAccount;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public Double getItemQuantity() {
        if (itemQuantity == null) {
            return 0.0;
        }

        return itemQuantity;
    }

    public void setItemQuantity(Double itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public Double getItemRate() {
        return itemRate;
    }

    public void setItemRate(Double itemRate) {
        this.itemRate = itemRate;
    }


    public Date getClearingDate() {
        return clearingDate;
    }

    public void setClearingDate(Date clearingDate) {
        this.clearingDate = clearingDate;
    }


    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getReferenceVoucher() {
        return referenceVoucher;
    }

    public void setReferenceVoucher(String referenceVoucher) {
        this.referenceVoucher = referenceVoucher;
    }

    public int getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(int voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public Date getVoucherDate() {
        return voucherDate;
    }

    public void setVoucherDate(Date voucherDate) {
        this.voucherDate = voucherDate;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public int getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(int bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Integer getAccountCode() {
        if (accountCode == null)
            return 0;

        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }

    public Long getCostCentre() {
        return costCentre;
    }

    public void setCostCentre(Long costCentre) {
        this.costCentre = costCentre;
    }

    public Integer getTaxAccount() {
        return taxAccount;
    }

    public void setTaxAccount(Integer taxAccount) {
        this.taxAccount = taxAccount;
    }

    public Double getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(Double billAmount) {
        this.billAmount = billAmount;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Double getCredit() {
        if (credit == null)
            return 0.0;

        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getDebit() {
        if (debit == null)
            return 0.0;

        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Integer getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(Integer creditAccount) {
        this.creditAccount = creditAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getVoucherNumberForPdc() {
        return voucherNumberForPdc;
    }

    public void setVoucherNumberForPdc(String voucherNumberForPdc) {
        this.voucherNumberForPdc = voucherNumberForPdc;
    }

    public Integer getBankVoucher() {
        return bankVoucher;
    }

    public void setBankVoucher(Integer bankVoucher) {
        this.bankVoucher = bankVoucher;
    }

    public Double getStageQuantity() {
        if (stageQuantity == null) {
            return 0.0;
        }
        return stageQuantity;
    }

    public void setStageQuantity(Double stageQuantity) {
        this.stageQuantity = stageQuantity;
    }

    public Double getFinalQuantity() {
        return finalQuantity;
    }

    public void setFinalQuantity(Double finalQuantity) {
        this.finalQuantity = finalQuantity;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public String getTransitionCode() {
        return transitionCode;
    }

    public void setTransitionCode(String transitionCode) {
        this.transitionCode = transitionCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getIssuedQuantity() {
        return issuedQuantity;
    }

    public void setIssuedQuantity(double issuedQuantity) {
        this.issuedQuantity = issuedQuantity;
    }

    public double getReceivedQuantity() {
        return receivedQuantity;
    }

    public void setReceivedQuantity(double receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(double totalBalance) {
        this.totalBalance = totalBalance;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartmentNo() {
        return apartmentNo;
    }

    public void setApartmentNo(String apartmentNo) {
        this.apartmentNo = apartmentNo;
    }

    public int getChassisId() {
        return chassisId;
    }

    public void setChassisId(int chassisId) {
        this.chassisId = chassisId;
    }

    public double getOwnAmount() {
        return ownAmount;
    }

    public void setOwnAmount(double ownAmount) {
        this.ownAmount = ownAmount;
    }

    public String getTaxString() {
        return taxString;
    }

    public void setTaxString(String taxString) {
        this.taxString = taxString;
    }

    public double getAdvanceTax() {
        return advanceTax;
    }

    public void setAdvanceTax(double advanceTax) {
        this.advanceTax = advanceTax;
    }

    public int getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(int salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Double getCommissionAmount() {
        if (commissionAmount == null) {
            return 0.0;
        }
        return commissionAmount;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public void setCommissionAmount(Double commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public Integer getAuthId() {
        return authId;
    }

    public void setAuthId(Integer authId) {
        this.authId = authId;
    }

    public List<String> getChassisList() {
        return chassisList;
    }

    public void setChassisList(List<String> chassisList) {
        this.chassisList = chassisList;
    }


    @Override
    public int compareTo(Voucher voucher) {

        return this.voucherDate.compareTo(voucher.getVoucherDate());

    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(int phaseId) {
        this.phaseId = phaseId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Integer getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(Integer projectCode) {
        this.projectCode = projectCode;
    }
}
