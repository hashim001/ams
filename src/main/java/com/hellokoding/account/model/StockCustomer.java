package com.hellokoding.account.model;

import com.hellokoding.account.model.jsonentity.AccountDto;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "account_customer")
public class StockCustomer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "customer_id")
    private int customerId;
    @Column(name = "address")
    private String address;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "landline")
    private String landline;
    @Column(name = "cnic")
    private String cnic;

    @Column(name = "saletax_number")
    private String saletaxNumber;
    @Column(name = "ntn")
    private String nationalTaxNumber;
    @Column(name = "tax_status")
    private String taxStatus;
    @Column(name = "email_address")
    private String emailAddress;
    @Transient
    private Integer accountCode;

    @Transient
    private String contactPersons;

    @Transient
    private AccountDto accountDto;

    // For Sales view
    @Transient
    private String accountTitle;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_code", unique = true, nullable = true, insertable = true, updatable = true)
    private Account customerAccount;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getSaletaxNumber() {
        return saletaxNumber;
    }

    public void setSaletaxNumber(String saletaxNumber) {
        this.saletaxNumber = saletaxNumber;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    public Integer getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }

    public String getContactPersons() {
        return contactPersons;
    }

    public void setContactPersons(String contactPersons) {
        this.contactPersons = contactPersons;
    }

    public Account getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(Account customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getNationalTaxNumber() {
        return nationalTaxNumber;
    }

    public void setNationalTaxNumber(String nationalTaxNumber) {
        this.nationalTaxNumber = nationalTaxNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }


    public AccountDto getAccountDto() {
        return accountDto;
    }

    public void setAccountDto(AccountDto accountDto) {
        this.accountDto = accountDto;
    }

    public String getAccountTitle() {
        return accountTitle;
    }

    public void setAccountTitle(String accountTitle) {
        this.accountTitle = accountTitle;
    }

}
