package com.hellokoding.account.model.jsonentity;

import java.sql.Date;

public class AccountDto {

    private String title;
    private String description;
    private int accountCode;
    private Date openingDate;
    private double openingCredit;
    private double openingDebit;
    private double lastOpeningCredit;
    private double lastOpeningDebit;
    private Double ledgerBalance;
    private Double amountLimit;
    private double openingBalanceLast;
    private double openingBalance;
    private Integer parentCode;
    private String parentName;
    private Double totalBalanceAmount;
    private int isActive;
    private int level;


    public int getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(int accountCode) {
        this.accountCode = accountCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public double getOpeningCredit() {
        return openingCredit;
    }

    public void setOpeningCredit(double openingCredit) {
        this.openingCredit = openingCredit;
    }

    public double getOpeningDebit() {
        return openingDebit;
    }

    public void setOpeningDebit(double openingDebit) {
        this.openingDebit = openingDebit;
    }

    public double getLastOpeningCredit() {
        return lastOpeningCredit;
    }

    public void setLastOpeningCredit(double lastOpeningCredit) {
        this.lastOpeningCredit = lastOpeningCredit;
    }

    public double getLastOpeningDebit() {
        return lastOpeningDebit;
    }

    public void setLastOpeningDebit(double lastOpeningDebit) {
        this.lastOpeningDebit = lastOpeningDebit;
    }

    public Double getLedgerBalance() {
        return ledgerBalance;
    }

    public void setLedgerBalance(Double ledgerBalance) {
        this.ledgerBalance = ledgerBalance;
    }

    public Double getAmountLimit() {
        return amountLimit;
    }

    public void setAmountLimit(Double amountLimit) {
        this.amountLimit = amountLimit;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public double getOpeningBalanceLast() {
        return openingBalanceLast;
    }

    public void setOpeningBalanceLast(double openingBalanceLast) {
        this.openingBalanceLast = openingBalanceLast;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public Integer getParentCode() {
        return parentCode;
    }

    public void setParentCode(Integer parentCode) {
        this.parentCode = parentCode;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Double getTotalBalanceAmount() {
        return totalBalanceAmount;
    }

    public void setTotalBalanceAmount(Double totalBalanceAmount) {
        this.totalBalanceAmount = totalBalanceAmount;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
