package com.hellokoding.account.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "apartment")
public class Apartment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "apartment_id")
    private int apartmentId;
    private String floor;
    private int rooms;
    @Column(name = "apartment_number")
    private String apartmentNo;
    @Column(name = "description")
    private String description;
    @Column(name = "selling_price")
    private Double sellingPrice;
    @Column(name = "is_sold")
    private boolean isSold;
    @Column(name = "distributor_id")
    private int distributorId;
    @Column(name = "amount_paid")
    private double amountPaid;
    @Column(name = "customer_account")
    private int customerAccount;
    @Column(name = "date")
    private Date date;
    @Column(name = "is_hold")
    private boolean isHold;
    @Column(name = "commission_amount")
    private double commissionAmount;
    @Column(name = "commission_status")
    private String commissionStatus;
    @Column(name = "commission_due")
    private Date commissionDue;

    @Transient
    private String itemName;
    // for apartment search option in receipt vouchers
    @Transient
    private int stockId;
    @Transient
    private String salesmanName;

    @Transient
    private long days;
    @Transient
    private double dueAmount; // to show on payment

    // transient fields for apartment hold

    @Transient
    private int apartmentHoldId;
    @Transient
    private int onHoldDays;
    @Transient
    private String remarks;
    @Transient
    private String customerName;
    @Transient
    private String cnic;
    @Transient
    private String mobile;
    @Transient
    private Date holdDate;
    @Transient
    private Integer salesmanId;
    @Transient
    private int categoryId;

    @ManyToOne()
    @JoinColumn(name = "stock_id", nullable = true)
    private Stock stock;

    @OneToOne(mappedBy = "planApartment", cascade = CascadeType.ALL)
    private PaymentPlan paymentPlan;

    @OneToOne(mappedBy = "apartment", cascade = CascadeType.ALL)
    private ApartmentHold apartmentHold;

    public int getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }


    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }


    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartmentNo() {
        return apartmentNo;
    }

    public String getApartmentDetail() {
        if(stock != null){
            return apartmentNo + " - " + stock.getName();
        }
        return apartmentNo;
    }

    public void setApartmentNo(String apartmentNo) {
        this.apartmentNo = apartmentNo;
    }

    public Double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    public int getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public int getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(int customerAccount) {
        this.customerAccount = customerAccount;
    }

    public double getCommissionAmount() {
        return commissionAmount;
    }

    public String getCommissionStatus() {
        return commissionStatus;
    }

    public Date getCommissionDue() {
        return commissionDue;
    }

    public void setCommissionDue(Date commissionDue) {
        this.commissionDue = commissionDue;
    }

    public void setCommissionStatus(String commissionStatus) {
        this.commissionStatus = commissionStatus;
    }

    public void setCommissionAmount(double commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getDays() {
        return days;
    }

    public void setDays(long days) {
        this.days = days;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public PaymentPlan getPaymentPlan() {
        return paymentPlan;
    }

    public void setPaymentPlan(PaymentPlan paymentPlan) {
        this.paymentPlan = paymentPlan;
    }

    public ApartmentHold getApartmentHold() {
        return apartmentHold;
    }

    public void setApartmentHold(ApartmentHold apartmentHold) {
        this.apartmentHold = apartmentHold;
    }

    public boolean isHold() {
        return isHold;
    }

    public void setHold(boolean hold) {
        isHold = hold;
    }

    public double getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(double dueAmount) {
        this.dueAmount = dueAmount;
    }

    public int getApartmentHoldId() {
        return apartmentHoldId;
    }

    public void setApartmentHoldId(int apartmentHoldId) {
        this.apartmentHoldId = apartmentHoldId;
    }

    public int getOnHoldDays() {
        return onHoldDays;
    }

    public void setOnHoldDays(int onHoldDays) {
        this.onHoldDays = onHoldDays;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getHoldDate() {
        return holdDate;
    }

    public void setHoldDate(Date holdDate) {
        this.holdDate = holdDate;
    }

    public Integer getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Integer salesmanId) {
        this.salesmanId = salesmanId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
