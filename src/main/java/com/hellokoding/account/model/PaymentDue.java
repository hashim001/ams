package com.hellokoding.account.model;

/**
 * Model for All Payment Dues on an Apartment
 * Mimics Voucher
 */

import javax.persistence.*;
import java.sql.Date;


@Entity
@Table(name = "payment_due")
public class PaymentDue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "voucher_id")
    private int voucherId;
    @Column(name = "voucher_number")
    private String voucherNumber;
    @Column(name = "voucher_date")
    private Date voucherDate; // Applicable date for payment (trigger date)
    @Column(name = "cheque_number")
    private String chequeNumber;
    @Column(name = "bank_account")
    private int bankAccount;
    @Column(name = "account_code")
    private Integer accountCode; // customer code
    @Column(name = "tax_account")
    private Integer taxAccount;
    @Column(name = "bill_amount")
    private Double billAmount;
    @Column(name = "tax_amount")
    private Double taxAmount;
    private String remarks;
    private Integer isActive;
    @Column(name = "reference_voucher")
    private String referenceVoucher;
    private Double credit;
    private Double debit;
    @Column(name = "item_account")
    private Integer itemAccount;
    @Column(name = "bill_number")
    private String billNumber;
    @Column(name = "due_date")
    private Date dueDate; // last Date to pay before late trigger
    @Column(name = "item_quantity")
    private Double itemQuantity;
    @Column(name = "item_rate")
    private Double itemRate;
    @Column(name = "tax_rate")
    private Double taxRate;
    @Column(name = "status")
    private String status;
    @Column(name = "apartment_id")
    private int apartmentId;
    @Column(name = "own_amount")
    private double ownAmount;
    @Column(name = "apartment_number")
    private String apartmentNo;
    @Column(name = "advance_tax")
    private double advanceTax;
    @Column(name = "commission_amount")
    private Double commissionAmount;

    public PaymentDue(){

    }

    public PaymentDue(String voucherNumber, Date voucherDate, Integer accountCode, String remarks, Double debit, Integer itemAccount, Date dueDate, Double itemQuantity, Double itemRate, int apartmentId, String apartmentNo) {
        this.voucherNumber = voucherNumber;
        this.voucherDate = voucherDate;
        this.accountCode = accountCode;
        this.remarks = remarks;
        this.debit = debit;
        this.itemAccount = itemAccount;
        this.dueDate = dueDate;
        this.itemQuantity = itemQuantity;
        this.itemRate = itemRate;
        this.apartmentId = apartmentId;
        this.apartmentNo = apartmentNo;
    }

    public int getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(int voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public Date getVoucherDate() {
        return voucherDate;
    }

    public void setVoucherDate(Date voucherDate) {
        this.voucherDate = voucherDate;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public int getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(int bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Integer getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }

    public Integer getTaxAccount() {
        return taxAccount;
    }

    public void setTaxAccount(Integer taxAccount) {
        this.taxAccount = taxAccount;
    }

    public Double getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(Double billAmount) {
        this.billAmount = billAmount;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getReferenceVoucher() {
        return referenceVoucher;
    }

    public void setReferenceVoucher(String referenceVoucher) {
        this.referenceVoucher = referenceVoucher;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Integer getItemAccount() {
        return itemAccount;
    }

    public void setItemAccount(Integer itemAccount) {
        this.itemAccount = itemAccount;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Double getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(Double itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public Double getItemRate() {
        return itemRate;
    }

    public void setItemRate(Double itemRate) {
        this.itemRate = itemRate;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }

    public double getOwnAmount() {
        return ownAmount;
    }

    public void setOwnAmount(double ownAmount) {
        this.ownAmount = ownAmount;
    }

    public String getApartmentNo() {
        return apartmentNo;
    }

    public void setApartmentNo(String apartmentNo) {
        this.apartmentNo = apartmentNo;
    }

    public double getAdvanceTax() {
        return advanceTax;
    }

    public void setAdvanceTax(double advanceTax) {
        this.advanceTax = advanceTax;
    }

    public Double getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(Double commissionAmount) {
        this.commissionAmount = commissionAmount;
    }
}
