package com.hellokoding.account.model;

/**
 * NOTE : TO AVOID EAGER LOADING OF ONE TO ONE RELATIONS
 *        THEY ARE CONVERTED TO ONE TO MANY AND MAPPED TO
 *        RESPECTIVE TRANSIENT FIELD
 */


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "account")
public class Account implements Serializable {

    @Id
    @Column(name = "account_code")
    private int accountCode;

    private int level;
    private String title;
    @Column(name = "opening_date")
    private Date openingDate;
    @Column(name = "opening_credit")
    private double openingCredit;
    @Column(name = "opening_debit")
    private double openingDebit;
    @Column(name = "last_opening_credit")
    private double lastOpeningCredit;
    @Column(name = "last_opening_debit")
    private double lastOpeningDebit;
    @Column(name = "ledger_balance")
    private Double ledgerBalance;
    @Column(name = "amount_limit")
    private Double amountLimit;
    @Column(name = "isActive")
    private int isActive;

    @OneToMany(mappedBy = "parentAccount", cascade = CascadeType.ALL)
    private List<CostCentre> costCentres;

    @Transient
    private double openingBalance;

    @Transient
    private double openingBalanceLast;

    @Transient
    private Integer parentCode;

    @Transient
    private Integer accountCodeForEdit;

    @Transient
    private String parentName;

    //amount for ledger balance
    @Transient
    private Double totalBalanceAmount;

    @ManyToOne(cascade = {CascadeType.ALL} )
    @JoinColumn(name = "parent_code")
    private Account parentAccount;

    @OneToMany(mappedBy = "parentAccount")
    private List<Account> childList;

    @ManyToMany(mappedBy = "accounts", fetch = FetchType.LAZY)
    private List<NoteSubHeading> noteSubHeadings;



    public List<NoteSubHeading> getNoteSubHeadings() {
        return noteSubHeadings;
    }

    public void setNoteSubHeadings(List<NoteSubHeading> noteSubHeadings) {
        this.noteSubHeadings = noteSubHeadings;
    }


    public List<TaxDetail> getTaxDetailList() {
        return taxDetailList;
    }

    public void setTaxDetailList(List<TaxDetail> taxDetailList) {
        this.taxDetailList = taxDetailList;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "baseAccount", cascade = CascadeType.ALL)
    private List<TaxDetail> taxDetailList;


    @Transient
    /*@OneToOne(mappedBy = "distributorAccount", cascade = CascadeType.ALL)*/
    private Distributor distributor;


    // EAGER ISSUE RECOVERY
    // ONE TO ONE CONVERTED TO ONE TO MANY
    // Oth ELEMENT WILL BE MAPPED TO TRANSIENT FIELD

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = CascadeType.ALL)
    private List<BankDetails> bankDetailsList;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "stockAccount", cascade = CascadeType.ALL)
    private List<Stock> stockList;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "vendorAccount", cascade = CascadeType.ALL)
    private List<StockVendor> stockVendorList;


    @OneToMany(fetch = FetchType.LAZY,mappedBy = "customerAccount", cascade = CascadeType.ALL)
    private List<StockCustomer> stockCustomerList;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "expenseAccount", cascade = CascadeType.ALL)
    private List<ExpenseDetail> expenseDetailList;


    // ONE TO ONE MAPPINGS CREATING EAGER ISSUES
    // CONVERTED IN ONE TO MANY

    @Transient
    private BankDetails bankDetails;

    @Transient
    private Stock accountStock;

    @Transient
    private StockVendor stockVendor;


    @Transient
    private StockCustomer stockCustomer;

    @Transient
    private ExpenseDetail expenseDetail;



    public Stock getAccountStock() {
        if(this.getStockList()!= null){
            if(this.getStockList().size() > 0){
                accountStock = this.getStockList().get(0);
            }
        }
        return accountStock;
    }

    public void setAccountStock(Stock accountStock) {

        List<Stock> tempStockList = new ArrayList<>();
        tempStockList.add(accountStock);
        stockList = tempStockList;

    }

    public StockVendor getStockVendor() {
        if(this.getStockVendorList()!= null){
            if(this.getStockVendorList().size() > 0){
                stockVendor = this.getStockVendorList().get(0);
            }
        }
        return stockVendor;
    }

    public void setStockVendor(StockVendor stockVendor) {

        List<StockVendor> tempList = new ArrayList<>();
        tempList.add(stockVendor);
        stockVendorList = tempList;
    }

    public ExpenseDetail getExpenseDetail() {
        if(this.getExpenseDetailList()!= null){
            if(this.getExpenseDetailList().size() > 0){
                expenseDetail = this.getExpenseDetailList().get(0);
            }
        }
        return expenseDetail;
    }

    public void setExpenseDetail(ExpenseDetail expenseDetail) {

        List<ExpenseDetail> tempList = new ArrayList<>();
        tempList.add(expenseDetail);
        expenseDetailList = tempList;
    }

    public StockCustomer getStockCustomer() {
        if(this.getStockCustomerList()!= null){
            if(this.getStockCustomerList().size() > 0){
                stockCustomer = this.getStockCustomerList().get(0);
            }
        }
        return stockCustomer;
    }

    public void setStockCustomer(StockCustomer stockCustomer) {

        List<StockCustomer> tempList = new ArrayList<>();
        tempList.add(stockCustomer);
        stockCustomerList = tempList;
    }

    public BankDetails getBankDetails() {
        if(this.getBankDetailsList()!= null){
            if(this.getBankDetailsList().size() > 0){
                bankDetails = this.getBankDetailsList().get(0);
            }
        }
        return bankDetails;

    }

    public void setBankDetails(BankDetails bankDetails) {

        List<BankDetails> tempList = new ArrayList<>();
        tempList.add(bankDetails);
        bankDetailsList = tempList;
    }



    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        if (level > 0 && level < 5)
            this.level = level;
    }

    public double getLastOpeningCredit() {
        return lastOpeningCredit;
    }

    public void setLastOpeningCredit(double lastOpeningCredit) {
        this.lastOpeningCredit = lastOpeningCredit;
    }

    public double getLastOpeningDebit() {
        return lastOpeningDebit;
    }

    public void setLastOpeningDebit(double lastOpeningDebit) {
        this.lastOpeningDebit = lastOpeningDebit;
    }

    public double getOpeningBalanceLast() {
        return openingBalanceLast;
    }

    public void setOpeningBalanceLast(double openingBalanceLast) {
        this.openingBalanceLast = openingBalanceLast;
    }

    public double getOpeningCredit() {
        return openingCredit;
    }

    public void setOpeningCredit(double openingCredit) {
        this.openingCredit = openingCredit;
    }

    public double getOpeningDebit() {
        return openingDebit;
    }

    public void setOpeningDebit(double openingDebit) {
        this.openingDebit = openingDebit;
    }

    public Account getParentAccount() {
        return parentAccount;
    }

    public void setParentAccount(Account parentAccount) {
        this.parentAccount = parentAccount;
    }

    public List<Account> getChildList() {
        return childList;
    }

    public void setChildList(List<Account> childList) {
        this.childList = childList;
    }

    public List<CostCentre> getCostCentres() {
        return costCentres;
    }

    public void setCostCentres(List<CostCentre> costCentres) {
        this.costCentres = costCentres;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }


    public Integer getParentCode() {
        return parentCode;
    }

    public void setParentCode(Integer parentCode) {
        this.parentCode = parentCode;
    }


    public int getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(int AccountCode) {
        accountCode = AccountCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public Double getLedgerBalance() {
        return ledgerBalance;
    }

    public void setLedgerBalance(Double ledgerBalance) {
        this.ledgerBalance = ledgerBalance;
    }

    public Double getAmountLimit() {
        return amountLimit;
    }

    public void setAmountLimit(Double amountLimit) {
        this.amountLimit = amountLimit;
    }

    public Double getTotalBalanceAmount() {
        return totalBalanceAmount;
    }

    public void setTotalBalanceAmount(Double totalBalanceAmount) {
        this.totalBalanceAmount = totalBalanceAmount;
    }

    public Distributor getDistributor() {
        return distributor;
    }

    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    public Integer getAccountCodeForEdit() {
        return accountCodeForEdit;
    }

    public void setAccountCodeForEdit(Integer accountCodeForEdit) {
        this.accountCodeForEdit = accountCodeForEdit;
    }


    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }


    // getters for lists Eager recovery setters not required


    public List<BankDetails> getBankDetailsList() {
        return bankDetailsList;
    }

    public List<Stock> getStockList() {
        return stockList;
    }

    public List<StockVendor> getStockVendorList() {
        return stockVendorList;
    }

    public List<StockCustomer> getStockCustomerList() {
        return stockCustomerList;
    }

    public List<ExpenseDetail> getExpenseDetailList() {
        return expenseDetailList;
    }

}
