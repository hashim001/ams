package com.hellokoding.account.model;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "static_info")
public class StaticInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "info_id")
    private int infoId;
    @Column(name = "company_name")
    private String companyName;
    @Column(name = "company_address")
    private String companyAddress;
    @Column(name = "address_secondary")
    private String addressSecondary;
    @Column(name = "tagline")
    private String tagLine;
    @Column(name = "phone")
    private String phone;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "logo_image")
    private String imageUrl;
    @Column(name = "footer_image")
    private String footerImageUrl;
    @Column(name = "expense_head")
    private Integer expenseHead;
    @Column(name = "customer_head")
    private Integer customerHead;
    @Column(name = "bank_head")
    private Integer bankHead;
    @Column(name = "employee_head")
    private Integer employeeHead;
    @Column(name = "vendor_head")
    private Integer vendorHead;
    @Column(name = "salary_account")
    private Integer salaryAccount;
    @Column(name = "product_head")
    private Integer productHead;
    @Column(name = "cash_in_hand")
    private Integer cashInHand;
    @Column(name = "discount_account")
    private Integer discountAccount;
    @Column(name = "prefix_dc")
    private String prefixDc;
    @Column(name = "prefix_grn")
    private String prefixGRN;
    @Column(name = "prefix_grn_gate")
    private String prefixGrnGate;
    @Column(name = "prefix_grn_store")
    private String prefixGrnStore;
    @Column(name = "prefix_gin")
    private String prefixGin;
    @Column(name = "prefix_grn_quality")
    private String prefixGrnQuality;
    @Column(name = "prefix_bill")
    private String prefixBill;
    @Column(name = "prefix_store_request")
    private String prefixStoreRequest;
    @Column(name = "min_dc_number")
    private Integer minDcNumber;
    @Column(name = "min_bill_number")
    private Integer minBillNumber;
    @Column(name = "ntn")
    private String nationalTaxNumber;
    @Column(name = "strn")
    private String salesTaxNumber;
    @Column(name = "max_form_rows")
    private Integer maxFormRows;
    @Column(name = "updated_date")
    private Date updatedDate;
    @Column(name = "project_name")
    private String projectName;
    @Column(name = "conditions")
    private String conditions;
    @Column(name = "email_address")
    private String emailAddress;
    @Column(name = "email_password")
    private String emailPassword;
    @Column(name = "email_body")
    private String emailBody;
    @Column(name = "email_subject")
    private String emailSubject;
    @Column(name = "smtp_server")
    private String smtpServer;
    @Column(name = "port_number")
    private int portNumber;
    @Column(name = "post_dated_check_account")
    private int postDateCheckAccount;

    @Column(name = "distributor_head")
    private Integer distributorHead;
    @Column(name = "rider_head")
    private Integer riderHead;
    @Column(name = "raw_material_head")
    private Integer rawMaterialHead;
    @Column(name = "goods_account")
    private Integer goodsAccount;
    @Column(name = "own_amount_account")
    private Integer ownAmountAccount;
    @Column(name = "advance_tax_account")
    private Integer advanceTaxAccount;
    @Column(name = "company_payment_account")
    private Integer companyPaymentAccount;
    @Column(name = "commission_account")
    private Integer commissionAccount;
    @Column(name = "hold_account")
    private Integer holdAccount;

    @Transient
    private MultipartFile logoFile;
    @Transient
    private MultipartFile footerFile;
    @Transient
    private String imagePath;

    public int getInfoId() {
        return infoId;
    }

    public void setInfoId(int infoId) {
        this.infoId = infoId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getTagLine() {
        return tagLine;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFooterImageUrl() {
        return footerImageUrl;
    }

    public void setFooterImageUrl(String footerImageUrl) {
        this.footerImageUrl = footerImageUrl;
    }

    public MultipartFile getLogoFile() {
        return logoFile;
    }

    public void setLogoFile(MultipartFile logoFile) {
        this.logoFile = logoFile;
    }

    public MultipartFile getFooterFile() {
        return footerFile;
    }

    public void setFooterFile(MultipartFile footerFile) {
        this.footerFile = footerFile;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getExpenseHead() {
        return expenseHead;
    }

    public void setExpenseHead(Integer expenseHead) {
        this.expenseHead = expenseHead;
    }

    public Integer getCustomerHead() {
        return customerHead;
    }

    public void setCustomerHead(Integer customerHead) {
        this.customerHead = customerHead;
    }

    public Integer getEmployeeHead() {
        return employeeHead;
    }

    public void setEmployeeHead(Integer employeeHead) {
        this.employeeHead = employeeHead;
    }

    public Integer getSalaryAccount() {
        return salaryAccount;
    }

    public void setSalaryAccount(Integer salaryAccount) {
        this.salaryAccount = salaryAccount;
    }

    public Integer getVendorHead() {
        return vendorHead;
    }

    public void setVendorHead(Integer vendorHead) {
        this.vendorHead = vendorHead;
    }

    public Integer getProductHead() {
        return productHead;
    }

    public void setProductHead(Integer productHead) {
        this.productHead = productHead;
    }

    public Integer getCashInHand() {
        return cashInHand;
    }

    public void setCashInHand(Integer cashInHand) {
        this.cashInHand = cashInHand;
    }

    public Integer getDiscountAccount() {
        return discountAccount;
    }

    public void setDiscountAccount(Integer discountAccount) {
        this.discountAccount = discountAccount;
    }

    public String getPrefixDc() {
        return prefixDc;
    }

    public void setPrefixDc(String prefixDc) {
        this.prefixDc = prefixDc;
    }

    public String getPrefixGRN() {
        return prefixGRN;
    }

    public void setPrefixGRN(String prefixGRN) {
        this.prefixGRN = prefixGRN;
    }

    public String getPrefixGrnGate() {
        return prefixGrnGate;
    }

    public void setPrefixGrnGate(String prefixGrnGate) {
        this.prefixGrnGate = prefixGrnGate;
    }

    public String getPrefixGrnStore() {
        return prefixGrnStore;
    }

    public void setPrefixGrnStore(String prefixGrnStore) {
        this.prefixGrnStore = prefixGrnStore;
    }

    public String getPrefixGrnQuality() {
        return prefixGrnQuality;
    }

    public void setPrefixGrnQuality(String prefixGrnQuality) {
        this.prefixGrnQuality = prefixGrnQuality;
    }

    public String getPrefixBill() {
        return prefixBill;
    }

    public void setPrefixBill(String prefixBill) {
        this.prefixBill = prefixBill;
    }

    public Integer getMinDcNumber() {
        return minDcNumber;
    }

    public void setMinDcNumber(Integer minDcNumber) {
        this.minDcNumber = minDcNumber;
    }

    public Integer getMinBillNumber() {
        return minBillNumber;
    }

    public void setMinBillNumber(Integer minBillNumber) {
        this.minBillNumber = minBillNumber;
    }

    public String getNationalTaxNumber() {
        return nationalTaxNumber;
    }

    public void setNationalTaxNumber(String nationalTaxNumber) {
        this.nationalTaxNumber = nationalTaxNumber;
    }

    public String getSalesTaxNumber() {
        return salesTaxNumber;
    }

    public void setSalesTaxNumber(String salesTaxNumber) {
        this.salesTaxNumber = salesTaxNumber;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddressSecondary() {
        return addressSecondary;
    }

    public void setAddressSecondary(String addressSecondary) {
        this.addressSecondary = addressSecondary;
    }

    public Integer getMaxFormRows() {
        return maxFormRows;
    }

    public void setMaxFormRows(Integer maxFormRows) {
        this.maxFormRows = maxFormRows;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public Integer getBankHead() {
        return bankHead;
    }

    public void setBankHead(Integer bankHead) {
        this.bankHead = bankHead;
    }

    public int getPostDateCheckAccount() {
        return postDateCheckAccount;
    }

    public void setPostDateCheckAccount(int postDateCheckAccount) {
        this.postDateCheckAccount = postDateCheckAccount;
    }

    public Integer getDistributorHead() {
        return distributorHead;
    }

    public void setDistributorHead(Integer distributorHead) {
        this.distributorHead = distributorHead;
    }

    public Integer getRiderHead() {
        return riderHead;
    }

    public void setRiderHead(Integer riderHead) {
        this.riderHead = riderHead;
    }

    public Integer getRawMaterialHead() {
        return rawMaterialHead;
    }

    public void setRawMaterialHead(Integer rawMaterialHead) {
        this.rawMaterialHead = rawMaterialHead;
    }

    public String getPrefixStoreRequest() {
        return prefixStoreRequest;
    }

    public void setPrefixStoreRequest(String prefixStoreRequest) {
        this.prefixStoreRequest = prefixStoreRequest;
    }

    public String getPrefixGin() {
        return prefixGin;
    }

    public void setPrefixGin(String prefixGin) {
        this.prefixGin = prefixGin;
    }

    public Integer getGoodsAccount() {
        return goodsAccount;
    }

    public void setGoodsAccount(Integer goodsAccount) {
        this.goodsAccount = goodsAccount;
    }

    public Integer getOwnAmountAccount() {
        return ownAmountAccount;
    }

    public void setOwnAmountAccount(Integer ownAmountAccount) {
        this.ownAmountAccount = ownAmountAccount;
    }

    public Integer getAdvanceTaxAccount() {
        return advanceTaxAccount;
    }

    public void setAdvanceTaxAccount(Integer advanceTaxAccount) {
        this.advanceTaxAccount = advanceTaxAccount;
    }

    public Integer getCompanyPaymentAccount() {
        return companyPaymentAccount;
    }

    public void setCompanyPaymentAccount(Integer companyPaymentAccount) {
        this.companyPaymentAccount = companyPaymentAccount;
    }

    public Integer getCommissionAccount() {
        return commissionAccount;
    }

    public void setCommissionAccount(Integer commissionAccount) {
        this.commissionAccount = commissionAccount;
    }

    public Integer getHoldAccount() {
        return holdAccount;
    }

    public void setHoldAccount(Integer holdAccount) {
        this.holdAccount = holdAccount;
    }
}
