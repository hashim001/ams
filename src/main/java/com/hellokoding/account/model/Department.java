package com.hellokoding.account.model;



import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "department")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "dep_id")
    private int depId;
    @Column(name = "name")
    private String name;
    @Column(name = "dep_code")
    private String depCode;
    @Column(name = "location")
    private String location;

    @Column(name = "dept_gm")
    private int deptGm;

    @Column(name = "account_code")
    private int deptAccount;

    @OneToMany(mappedBy = "subDepartment", cascade = CascadeType.ALL)
    private List<SubDepartment> subDepartmentList;

    public int getDepId() {
        return depId;
    }

    public void setDepId(int depId) {
        this.depId = depId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepCode() {
        return depCode;
    }

    public void setDepCode(String depCode) {
        this.depCode = depCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getDeptGm() {
        return deptGm;
    }

    public void setDeptGm(int deptGm) {
        this.deptGm = deptGm;
    }

    public int getDeptAccount() {
        return deptAccount;
    }

    public void setDeptAccount(int deptAccount) {
        this.deptAccount = deptAccount;
    }

    public List<SubDepartment> getSubDepartmentList() {
        return subDepartmentList;
    }

    public void setSubDepartmentList(List<SubDepartment> subDepartmentList) {
        this.subDepartmentList = subDepartmentList;
    }
}
