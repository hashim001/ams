package com.hellokoding.account.model;

import javax.persistence.*;



@Entity
@Table(name = "payment_plan_down")
public class PaymentPlanDown implements Comparable<PaymentPlanDown>{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "down_id")
    private Integer downId;
    @Column(name = "rate")
    private double rate;
    @Column(name = "sort_order")
    private int order;
    @Column(name = "title")
    private String title;

    @Transient
    private boolean check;

    @ManyToOne()
    @JoinColumn(name = "plan_id", nullable = true)
    private PaymentPlan paymentPlan;

    public PaymentPlan getPaymentPlan() {
        return paymentPlan;
    }

    public void setPaymentPlan(PaymentPlan paymentPlan) {
        this.paymentPlan = paymentPlan;
    }

    public Integer getDownId() {
        return downId;
    }

    public void setDownId(Integer downId) {
        this.downId = downId;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int compareTo(PaymentPlanDown paymentPlanDown) {
        int compareOrder = paymentPlanDown.getOrder();
        return this.order - compareOrder;

    }
}
