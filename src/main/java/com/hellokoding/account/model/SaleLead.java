package com.hellokoding.account.model;
/**
 * Model for simple data Record for sales lead
 * and customer interest in purchase
 * NOTE : NO LITERAL MAPPING WITH OTHER TABLES
 */

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "sale_lead")
public class SaleLead {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "lead_id")
    private Integer leadId;
    @Column(name = "remarks")
    private String remarks;
    @Column(name = "salesman_id")
    private Integer salesmanId;
    @Column(name = "customer")
    private String customer;
    @Column(name = "apartment_number")
    private String apartmentNumber;
    @Column(name = "created")
    private Date created;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "email")
    private String email;

    public Integer getLeadId() {
        return leadId;
    }

    public void setLeadId(Integer leadId) {
        this.leadId = leadId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Integer salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
