package com.hellokoding.account.model;


import com.hellokoding.account.model.jsonentity.AccountDto;
import com.hellokoding.account.model.jsonentity.CategoryDto;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;


@Entity
@Table(name = "account_stock")
public class Stock {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stock_id")
    private Integer stockId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "quantity")
    private Double quantity;
    @Column(name = "opening_quantity")
    private Double openingQuantity;
    @Column(name = "cost")
    private Double cost;
    @Column(name = "selling_price")
    private Double sellingPrice;
    @Column(name = "unit_measure")
    private String unitMeasure;
    @Column(name = "item_date")
    private Date itemDate;
    @Column(name = "commission_amount")
    private Double commissionAmount;
    @Column(name = "engine_prefix")
    private String enginePrefix;
    @Column(name = "chassis_prefix")
    private String chassisPrefix;
    @Column(name = "image_url")
    private String imageUrl;
    @Transient
    private Integer categoryId;
    @Transient
    private List<String> chassisNumberList;
    @Transient
    private Integer makerId;

    @Transient
    private Integer accountCode;

    @Transient
    private AccountDto accountDto;

    @Transient
    private CategoryDto categoryDto;

    public Integer getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Integer accountCode) {
        this.accountCode = accountCode;
    }


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_code", unique = true, nullable = true, insertable = true, updatable = true)
    private Account stockAccount;


    @ManyToOne()
    @JoinColumn(name = "category_id", nullable = true)
    private StockCategory stockCategory;

    @ManyToOne()
    @JoinColumn(name = "maker_id", nullable = true)
    private Maker maker;

    @OneToMany(mappedBy = "stock")
    private List<Apartment> apartmentList;

    @OneToOne(mappedBy = "planStock", cascade = CascadeType.ALL)
    private PaymentPlan paymentPlan;

    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    public Date getItemDate() {
        return itemDate;
    }

    public void setItemDate(Date itemDate) {
        this.itemDate = itemDate;
    }

    public Account getStockAccount() {
        return stockAccount;
    }

    public void setStockAccount(Account stockAccount) {
        this.stockAccount = stockAccount;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }


    public StockCategory getStockCategory() {
        return stockCategory;
    }

    public void setStockCategory(StockCategory stockCategory) {
        this.stockCategory = stockCategory;
    }

    public String getName() {
        if(stockCategory != null){
            return name + " - " + stockCategory.getTitle();
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getOpeningQuantity() {
        return openingQuantity;
    }

    public void setOpeningQuantity(Double openingQuantity) {
        this.openingQuantity = openingQuantity;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public AccountDto getAccountDto() {
        return accountDto;
    }

    public void setAccountDto(AccountDto accountDto) {
        this.accountDto = accountDto;
    }

    public CategoryDto getCategoryDto() {
        return categoryDto;
    }

    public void setCategoryDto(CategoryDto categoryDto) {
        this.categoryDto = categoryDto;
    }

    public Integer getMakerId() {
        return makerId;
    }

    public void setMakerId(Integer makerId) {
        this.makerId = makerId;
    }

    public Maker getMaker() {
        return maker;
    }

    public void setMaker(Maker maker) {
        this.maker = maker;
    }

    public List<Apartment> getApartmentList() {
        return apartmentList;
    }

    public void setApartmentList(List<Apartment> apartmentList) {
        this.apartmentList = apartmentList;
    }

    public List<String> getChassisNumberList() {
        return chassisNumberList;
    }

    public void setChassisNumberList(List<String> chassisNumberList) {
        this.chassisNumberList = chassisNumberList;
    }

    public Double getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(Double commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public String getEnginePrefix() {
        return enginePrefix;
    }

    public void setEnginePrefix(String enginePrefix) {
        this.enginePrefix = enginePrefix;
    }

    public String getChassisPrefix() {
        return chassisPrefix;
    }

    public void setChassisPrefix(String chassisPrefix) {
        this.chassisPrefix = chassisPrefix;
    }

    public PaymentPlan getPaymentPlan() {
        return paymentPlan;
    }

    public void setPaymentPlan(PaymentPlan paymentPlan) {
        this.paymentPlan = paymentPlan;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
