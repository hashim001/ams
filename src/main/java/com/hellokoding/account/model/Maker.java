package com.hellokoding.account.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "maker")
public class Maker {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "maker_id")
    private int makerId;
    @Column(name = "maker_name")
    private String makerName;
    @OneToMany(mappedBy = "maker", cascade = CascadeType.ALL)
    private List<Stock> stockList;

    public int getMakerId() {
        return makerId;
    }

    public void setMakerId(int makerId) {
        this.makerId = makerId;
    }

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public List<Stock> getStockList() {
        return stockList;
    }

    public void setStockList(List<Stock> stockList) {
        this.stockList = stockList;
    }
}
