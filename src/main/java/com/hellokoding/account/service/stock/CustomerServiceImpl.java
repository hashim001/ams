package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.*;
import com.hellokoding.account.repository.AccountRepository;
import com.hellokoding.account.repository.StockCustomerRepository;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private StockCustomerRepository stockCustomerRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private StaticInfoService staticInfoService;


    public void removeCustomer(Account account){
        stockCustomerRepository.deleteByAccountCode(account);
    }

    public List<StockCustomer> findAll(){
        return stockCustomerRepository.findAll();
    }
    public void save(StockCustomer stockCustomer){
        stockCustomerRepository.save(stockCustomer);
    }

    @Override
    public StockCustomer findByCustomerId(int id) {
        return stockCustomerRepository.findByCustomerId(id);
    }

    @Override
    public Integer addCustomerAccount(VoucherList voucherList) {

        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        if (staticInfo.getCustomerHead() > 0) {


            Account account = new Account();
            account.setParentCode(staticInfo.getCustomerHead());
            account.setTitle(voucherList.getTitle());
            account.setOpeningCredit(0.0);
            account.setOpeningDebit(0.0);
            account.setLastOpeningCredit(0.0);
            account.setLastOpeningDebit(0.0);
            account.setOpeningDate(voucherList.getCreateDate());
            account.setIsActive(1);
            // Persisted Account
            account = accountService.save(account);
            StockCustomer stockCustomer = new StockCustomer();
            stockCustomer.setCustomerAccount(account);
            stockCustomer.setMobile(voucherList.getMobile());
            stockCustomer.setAddress(voucherList.getAddress());
            stockCustomer.setCnic(voucherList.getCnic());
            stockCustomer.setEmailAddress(voucherList.getEmail());
            stockCustomer.setLandline(voucherList.getLandline());
            stockCustomer.setTaxStatus(voucherList.getTaxStatus());
            save(stockCustomer);

            return account.getAccountCode();

        }

        return null;
    }


    @Override
    public StockCustomer getCustomerDetail(String mobile, String cnic) {
        if(cnic != null){
            return stockCustomerRepository.findByCnic(cnic);
        }
        if(mobile != null){
        return stockCustomerRepository.findByMobile(mobile);
        }
        return null;
    }

    @Override
    public Map<Integer, String> getCnicMap(List<Voucher> voucherList) {
        List<Integer> accountCodeList = new ArrayList<>();
        for(Voucher voucher : voucherList){
            // CASE ACCOUNT CODE
            if (voucher.getAccountCode() != null) {
                if (voucher.getAccountCode() != 0) {
                    if (!accountCodeList.contains(voucher.getAccountCode()))
                        accountCodeList.add(voucher.getAccountCode());
                }
            }
        }
        // To avoid query Format Exception
        if (accountCodeList.size() < 1) {
            accountCodeList.add(0);
        }
        List<Object[]> objectList = stockCustomerRepository.getCnicList(accountCodeList);
        Map<Integer, String> cnicMap = new HashMap<>();
        for (Object[] objectArray : objectList) {
            cnicMap.put((int) objectArray[0], (String) objectArray[1]);
        }
        return cnicMap;
    }
}
