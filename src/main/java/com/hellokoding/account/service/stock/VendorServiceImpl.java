package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.*;
import com.hellokoding.account.repository.StockVendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VendorServiceImpl implements VendorService {


    @Autowired
    private StockVendorRepository stockVendorRepository;

     public  void removeVendor(Account account){
        stockVendorRepository.deleteByAccountCode(account);
    }

     public List<StockVendor> findAll(){
         return stockVendorRepository.findAll();
    }

    public void save(StockVendor stockVendor){
         stockVendorRepository.save(stockVendor);
    }

    @Override
    public StockVendor findByVendorId(int vendorId) {
        return stockVendorRepository.findByVendorId(vendorId);
    }

    @Override
    public List<StockVendor> findAvailableVendors(boolean isRider) {

        if(isRider){
            return null;
        }
        return stockVendorRepository.findByVendorDistributorIsNull();
    }

    @Override
    public List<StockVendor> findCurrentVendors(Distributor distributor) {

        return stockVendorRepository.findByVendorDistributor(distributor);
    }

}
