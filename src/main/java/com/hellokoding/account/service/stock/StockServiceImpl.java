package com.hellokoding.account.service.stock;


import com.hellokoding.account.model.*;
import com.hellokoding.account.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private StockCategoryRepository stockCategoryRepository;

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private StockVendorRepository stockVendorRepository;

    @Autowired
    private StockCustomerRepository stockCustomerRepository;

    public List<StockCategory> getStockCategories() {

        return stockCategoryRepository.findAll();
    }

    public List<StockCategory> getRawCategories() {

        return stockCategoryRepository.findByAccountCodeGreaterThan(0);
    }

    public List<Stock> findAllStock() {
        return (List<Stock>) stockRepository.findAll();
    }

    public Stock findByStockId(Integer stockId) {
        return stockRepository.findByStockId(stockId);
    }

    public StockCategory findByCategoryId(Integer categoryId) {
        return stockCategoryRepository.findByCategoryId(categoryId);
    }

    public void saveStock(Stock stock) {
        stockRepository.save(stock);
    }


    public void removeStock(Account account) {
        stockRepository.deleteByAccountCode(account);
    }

    public void removeVendor(Account account) {
        stockVendorRepository.deleteByAccountCode(account);
    }

    public void removeCustomer(Account account) {
        stockCustomerRepository.deleteByAccountCode(account);
    }

    public void saveVendor(StockVendor stockVendor) {
        stockVendorRepository.save(stockVendor);
    }

    public void saveCustomer(StockCustomer stockCustomer) {
        stockCustomerRepository.save(stockCustomer);
    }

    public void saveCategory(StockCategory stockCategory) {
        stockCategoryRepository.save(stockCategory);
    }

    public void removeCategory(StockCategory stockCategory) {
        stockCategoryRepository.delete(stockCategory);
    }

    public List<Stock> getStockByMaker(Maker maker) {
        return stockRepository.findByMaker(maker);
    }


    @Override
    public String getMeasureUnit(int accountCode, boolean isRaw) {
        return stockRepository.getMeasureUnit(accountCode);
    }

    public Page<Stock> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString) {

        Stock exampleStock = new Stock();
        StockCategory stockCategory = new StockCategory();
        stockCategory.setTitle(searchString);
        exampleStock.setName(searchString);


        //exampleAccount.setAccountCode(Integer.parseInt(searchString));
        ExampleMatcher columnMappingMatcher = ExampleMatcher.matchingAny()
                .withIgnorePaths("sellingPrice", "openingQuantity", "cost", "StockCategory.categoryId")
                .withIgnoreNullValues()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase());


        Example<Stock> stockExample = Example.of(exampleStock, columnMappingMatcher);

        return stockRepository.findAll(stockExample, new PageRequest(pageIndex, size));
    }

    public Page<StockCustomer> findAllCustomerPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString) {
        Sort sort;
        if (isAsc) {
            sort = new Sort(Sort.Direction.ASC, orderColumn);
        } else {
            sort = new Sort(Sort.Direction.DESC, orderColumn);
        }
        return stockCustomerRepository.findAllStockCustomer("%" + searchString + "%", new PageRequest(pageIndex, size, sort));
    }

    public Page<StockVendor> findAllVendorPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString) {
        Sort sort;
        if (isAsc) {
            sort = new Sort(Sort.Direction.ASC, orderColumn);
        } else {
            sort = new Sort(Sort.Direction.DESC, orderColumn);
        }
        return stockVendorRepository.findAllStockVendor("%" + searchString + "%", new PageRequest(pageIndex, size, sort));
    }

    @Override
    public Map<Integer, String> categoryMap() {
        Map<Integer, String> categoryMap = new HashMap<>();
        for(StockCategory stockCategory : stockCategoryRepository.findAll()){
            categoryMap.put(stockCategory.getAccountCode(), stockCategory.getTitle());
        }
        return categoryMap;
    }
}
