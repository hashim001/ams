package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.*;

import java.util.List;
import java.util.Map;

public interface CustomerService {

    void removeCustomer(Account account);
    List<StockCustomer> findAll();
    void save(StockCustomer stockCustomer);

    /**
     * Find Customer By CustomerID
     * @param id Customer Primary Key
     * @return Customer
     */
    StockCustomer findByCustomerId(int id);

    /**
     * Add Customer Account
     * USE : Sale if customer not registered
     * @param voucherList Sale Voucher Model
     * @return accountCode of added account
     */
    Integer addCustomerAccount(VoucherList voucherList);

    /**
     * get Customer Details
     * @param mobile cell number
     * @param cnic national id
     * @return StockCustomer
     */
    StockCustomer getCustomerDetail(String mobile, String cnic);

    Map<Integer, String> getCnicMap(List<Voucher> voucherList);


}
