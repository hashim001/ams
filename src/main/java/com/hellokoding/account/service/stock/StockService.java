package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.*;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface StockService {

    List<StockCategory> getStockCategories();

    List<StockCategory> getRawCategories();

    List<Stock> findAllStock();

    Stock findByStockId(Integer stockId);

    StockCategory findByCategoryId(Integer categoryId);

    void saveStock(Stock stock);

    void removeStock(Account account);

    void removeVendor(Account account);

    void removeCustomer(Account account);

    void saveVendor(StockVendor stockVendor);

    void saveCustomer(StockCustomer stockCustomer);

    void saveCategory(StockCategory stockCategory);

    void removeCategory(StockCategory stockCategory);

    List<Stock> getStockByMaker(Maker maker);

    /**
     * Get Measure Unit for
     * Raw Material , Stock
     *
     * @param accountCode
     * @param isRaw
     * @return
     */
    String getMeasureUnit(int accountCode, boolean isRaw);

    Page<Stock> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString);

    Page<StockCustomer> findAllCustomerPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString);

    Page<StockVendor> findAllVendorPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString);

    Map<Integer, String> categoryMap();

}
