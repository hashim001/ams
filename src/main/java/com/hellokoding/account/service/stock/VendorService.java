package com.hellokoding.account.service.stock;

import com.hellokoding.account.model.*;

import java.util.List;

public interface VendorService {

    void removeVendor(Account account);
    List<StockVendor> findAll();
    void save(StockVendor stockVendor);
    StockVendor findByVendorId(int vendorId);

    /**
     * Find Vendors  available for assignment to Distributor or Rider
     * @param isRider Whether Rider or Customer
     * @return List Of Available Vendors
     */
    List<StockVendor> findAvailableVendors(boolean isRider);

    /**
     * Find Current Vendor Assignments to Distributor
     * @param distributor
     * @return List Of Assignments
     */
    List<StockVendor> findCurrentVendors(Distributor distributor);
}
