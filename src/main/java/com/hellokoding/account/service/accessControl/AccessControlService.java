package com.hellokoding.account.service.accessControl;


import com.hellokoding.account.model.Role;
import com.hellokoding.account.model.UserAccessModel;
import com.hellokoding.account.model.UserAuth;
import com.hellokoding.account.repository.RoleRepository;
import com.hellokoding.account.repository.UserAuthRepository;
import com.hellokoding.account.service.department.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccessControlService {

    @Autowired
    private UserAuthRepository userAuthRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Autowired
    private DepartmentService departmentService;

    public UserAccessModel getUserModel(int authId){

        UserAccessModel userAccessModel = new UserAccessModel();
        UserAuth userAuth  = userAuthRepository.findByAuthId(authId);
        userAccessModel.setUserAuth(userAuth);
        List<Role> roleList = roleRepository.findAll();
        for(Role role : roleList){
            if(userAuth.getRoles().contains(role)){
                role.setChecked(true);
            }
        }


        userAccessModel.setRoleList(roleList);
        return userAccessModel;
    }


    public void updateUser(UserAccessModel userAccessModel){

        List<Role> roleList = userAccessModel.getRoleList()
                .stream()
                .filter(e -> e.isChecked())
                .collect(Collectors.toList());

        UserAuth userAuth = userAccessModel.getUserAuth();
        if(!userAuth.getPasswordConfirm().isEmpty()){
            userAuth.setPassword(bCryptPasswordEncoder.encode(userAuth.getPasswordConfirm()));
        }
        userAuth.setRoles(roleList);
        userAuthRepository.save(userAuth);
    }
    
}
