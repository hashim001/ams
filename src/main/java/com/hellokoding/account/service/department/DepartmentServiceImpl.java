package com.hellokoding.account.service.department;


import com.hellokoding.account.model.Department;
import com.hellokoding.account.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department findByDeptId(int id) {
        return departmentRepository.findByDepId(id);
    }

    @Override
    public List<Department> findAll() {
        return departmentRepository.findAll();
    }

    @Override
    public Department save(Department department) {
        return departmentRepository.save(department);
    }

    @Override
    public void remove(int deptId) {
        departmentRepository.removeDept(deptId);
    }
}

