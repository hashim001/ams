package com.hellokoding.account.service.department;

import com.hellokoding.account.model.Department;
import java.util.List;

public interface DepartmentService {


    /**
     * Find Department by departmentId
     * @param id dept Identity
     * @return Department
     */
    Department findByDeptId(int id);


    /**
     * Get all Departments
     * @return List of Departments
     */
    List<Department> findAll();

    /**
     * Save Department
     * @param department To save
     * @return Persisted Department
     */
    Department save(Department department);

    /**
     * Remove Dept with DeptId
     * @param deptId department Identity
     */
    void remove(int deptId);


}
