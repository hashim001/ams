package com.hellokoding.account.service.maker;

import com.hellokoding.account.model.Maker;

import java.util.List;

public interface MakerService {
    List<Maker> findAllMakers();
    void save(Maker maker);
    Maker findByMakerId(int makerId);
}
