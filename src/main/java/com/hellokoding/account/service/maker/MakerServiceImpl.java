package com.hellokoding.account.service.maker;

import com.hellokoding.account.model.Maker;
import com.hellokoding.account.repository.MakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MakerServiceImpl implements MakerService {
    @Autowired
    private MakerRepository makerRepository;

    @Override
    public List<Maker> findAllMakers() {
        return makerRepository.findAll();
    }

    @Override
    public void save(Maker maker) {
        makerRepository.save(maker);
    }

    @Override
    public Maker findByMakerId(int makerId) {
        return makerRepository.findByMakerId(makerId);
    }

}
