package com.hellokoding.account.service.user;

import com.hellokoding.account.model.UserAuth;

import java.util.List;

public interface UserAuthService{

    UserAuth save(UserAuth user);

    UserAuth findByUserName(String username);

    UserAuth findByAuthId(int authId);

    /**
     * Find All GM UserAuth
     * Current Usage in Dept scenario
     * @return List
     */
    List<UserAuth> findAllGM();

    List<UserAuth> findAll();

    /**
     * Remove User Form DB
     * @param authId User Identity
     */
    void remove(int authId);

}
