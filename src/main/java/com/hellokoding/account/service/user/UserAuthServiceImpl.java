package com.hellokoding.account.service.user;


import com.hellokoding.account.model.UserAuth;
import com.hellokoding.account.repository.UserAuthRepository;
import com.hellokoding.account.service.security.RoleSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserAuthServiceImpl implements UserAuthService{


    @Autowired
    private UserAuthRepository userAuthRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserAuth save(UserAuth user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setIsActive(1);
        return userAuthRepository.save(user);
    }


    @Override
    public UserAuth findByUserName(String username) {
        return userAuthRepository.findByUserName(username);
    }

    @Override
    public UserAuth findByAuthId(int authId) {
        return userAuthRepository.findByAuthId(authId);
    }

    @Override
    public List<UserAuth> findAll() {
        // For Security set Password to blank
        List<UserAuth> userList = new ArrayList<>();
        for(UserAuth userAuth : userAuthRepository.findAll()){
            // Restrict Admin From the List
            if(!userAuth.getUserName().equals(RoleSupport.getUserName())) {
                userAuth.setPassword("");
                userList.add(userAuth);
            }
        }
        return userList;
    }

    @Override
    public List<UserAuth> findAllGM() {
        return userAuthRepository.findAllGM();
    }

    @Override
    public void remove(int authId) {
        userAuthRepository.removeUser(authId);
    }
}
