package com.hellokoding.account.service.user;


import com.hellokoding.account.model.Role;

import java.util.List;

public interface RegistrationService {

    Role findByName(String name);

}
