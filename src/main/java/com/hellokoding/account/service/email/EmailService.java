package com.hellokoding.account.service.email;

import com.hellokoding.account.model.EmailModel;
import com.hellokoding.account.purchase.RejectItemModel;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.Map;
import java.util.Properties;


@Service
public class EmailService {

    public void doSendEmail(String filePath, EmailModel emailModel) throws javax.mail.MessagingException {
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", emailModel.getSmtpServer());
        properties.put("mail.smtp.port", emailModel.getPortNumber());
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.user", "databasebk001@gmail.com");
        properties.put("mail.password", "Databasebk_12345");
        properties.put("mail.smtp.ssl.trust", emailModel.getSmtpServer());

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("databasebk001@gmail.com", "Databasebk_12345");
            }
        };
        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress("databasebk001@gmail.com"));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailModel.getRecipient()));
        msg.setSubject("DB Backup - "+ LocalDate.now());
        msg.setSentDate(new Date());

        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent("This is DB backup", "text/html");

        // creates multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        MimeBodyPart attachPart = new MimeBodyPart();
        try {
            attachPart.attachFile(filePath);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        multipart.addBodyPart(attachPart);

        // sets the multi-part as e-mail's content
        msg.setContent(multipart);

        // sends the e-mail
        Transport.send(msg);
    }
}
