package com.hellokoding.account.service.statement;

import com.hellokoding.account.model.ReportStaging;
import com.hellokoding.account.repository.ReportStagingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportStageServiceImpl implements ReportStageService {

    @Autowired
    private ReportStagingRepository reportStagingRepository;

    public List<ReportStaging> findAll(){
        return reportStagingRepository.findAll();
    }

    public void saveTitle(ReportStaging reportStaging){
        reportStagingRepository.save(reportStaging);
    }

    public ReportStaging findByReportId(Integer reportId){
        return reportStagingRepository.findByStageId(reportId);
    }

    public void removeReport(ReportStaging reportStaging){
        reportStagingRepository.delete(reportStaging);
    }
}
