package com.hellokoding.account.service.plan;

import com.hellokoding.account.model.PaymentPlan;
import com.hellokoding.account.model.Voucher;

import java.sql.Date;
import java.util.List;

public interface PaymentDueService {


    String generateVoucherNumber(String prefix);

    boolean generatePaymentDues(PaymentPlan paymentPlan);

    /**
     * Get due amount on an Apartment
     * Amount due till date - amount paid till date
     * @param apartmentNo
     * @param tillDate
     * @return
     */
    double getDueAmount(String apartmentNo, Date tillDate);

    /**
     * Map Payment Due to Voucher
     * @param apartmentNo Payment Dues of Apartment by apartmentNo
     * @param accountCode Customer Account code
     * @param isLate if used for late ledger
     * @return List of Vouchers mapped
     */
    List<Voucher> mapToVoucher(String apartmentNo, Integer accountCode,Date fromDate ,Date toDate, boolean isLate);

    /**
     * Generate Late entries for sold apartments
     * Entries generated on basis of due date in Payment Due table
     * @return message
     */
    String generateLateEntries();

    /**
     * Sum of all down Payments of installment
     * @param apartmentNumber
     * @return
     */
    double getSumOfDownPayment(String apartmentNumber);
}
