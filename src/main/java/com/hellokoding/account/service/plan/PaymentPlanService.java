package com.hellokoding.account.service.plan;

import com.hellokoding.account.model.Apartment;
import com.hellokoding.account.model.PaymentPlan;
import com.hellokoding.account.model.PaymentPlanDown;
import com.hellokoding.account.reportModel.PaymentPlanModel;

import java.util.List;

/**
 * Service for Payment Plan
 */

public interface PaymentPlanService {

    /**
     * find Payment Plan
     *
     * @param planId
     * @return
     */
    PaymentPlan findByPlanId(int planId);

    /**
     * Remove Payment Plan by id
     *
     * @param planId
     * @return
     */
    boolean removePaymentPlan(int planId);

    /**
     * Save PaymentPlan
     *
     * @param paymentPlan
     * @return
     */
    PaymentPlan savePlan(PaymentPlan paymentPlan);

    /**
     * find payment plan downPayment
     *
     * @param downId
     * @return
     */
    PaymentPlanDown findByDownId(int downId);

    /**
     * Remove downPayment by Id
     *
     * @param downId
     * @return
     */
    boolean removeDownPayment(int downId);

    void removePaymentPlan(PaymentPlan paymentPlan);

    /**
     * save Payment DownPayment
     *
     * @param paymentPlanDown
     * @return
     */
    PaymentPlanDown saveDownPayment(PaymentPlanDown paymentPlanDown);

    /**
     * Attach Payment Plan to Apartment
     * Generate Entries for Payment Dues
     * Current Plan in Stock (Model) will be attached
     *
     * @param apartment Target Apartment
     * @return check
     */
    boolean attachPaymentPlan(Apartment apartment);

    /**
     * Get Payment Plan Model for View
     *
     * @param stockId     if for stock (Model) else null
     * @param apartmentNo apartment Number if for apartment else null
     * @return PaymentPlanModel
     */
    PaymentPlanModel getPlanModel(Integer stockId, String apartmentNo);

    /**
     * Get List of Plan Model for floor
     *
     * @param stockId in case or specific Stock (Model)
     * @param floor   floor title
     * @return
     */
    List<PaymentPlanModel> getFloorPlan(Integer stockId, String floor);

}
