package com.hellokoding.account.service.plan;


import com.hellokoding.account.model.*;
import com.hellokoding.account.repository.ApartmentRepository;
import com.hellokoding.account.repository.PaymentDueRepository;
import com.hellokoding.account.repository.VoucherRepository;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@Service
public class PaymentDueServiceImpl implements PaymentDueService {

    @Autowired
    private PaymentDueRepository paymentDueRepository;

    @Autowired
    private VoucherRepository voucherRepository;

    @Autowired
    private ApartmentRepository apartmentRepository;


    public String generateVoucherNumber(String prefix) {
        String voucherNumber = "";
        int maxVoucherID = 0;
        String responseVoucherNumber = paymentDueRepository.findMaxVoucherNumber(prefix + "%");
        if (responseVoucherNumber == null) {
            maxVoucherID++;
            voucherNumber = Integer.toString(maxVoucherID);
        } else {
            maxVoucherID = Integer.parseInt(responseVoucherNumber.replaceAll("[^0-9]+", ""));
            maxVoucherID++;
            voucherNumber = Integer.toString(maxVoucherID);
        }
        return prefix + voucherNumber;
    }


    @Override
    public boolean generatePaymentDues(PaymentPlan paymentPlan) {

        paymentDueRepository.removePaymentDue(paymentPlan.getPlanApartment().getApartmentNo()); // Removal in case already exist to avoid duplicate entries
        Date saleDate = paymentPlan.getPlanApartment().getDate();
        LocalDate dueDate;
        LocalDate triggerDate; // date on which Payment is triggered (Applicable)
        Double saleAmount = paymentPlan.getPlanApartment().getSellingPrice();
        Double downPaymentAmount = 0.0;
        int paymentIndex = 1;

        List<PaymentPlanDown> paymentPlanDownList = paymentPlan.getPaymentPlanDownList();
        Collections.sort(paymentPlanDownList); // Sort By Sort Order

        if (paymentPlanDownList.size() > 0) {
            dueDate = saleDate.toLocalDate().plusDays(15);
        } else {
            dueDate = saleDate.toLocalDate();
        }
        triggerDate = saleDate.toLocalDate();
        String downPaymentVoucherNumber = generateVoucherNumber("DP");
        for (PaymentPlanDown paymentPlanDown : paymentPlanDownList) {

            double paymentAmount = saleAmount * (paymentPlanDown.getRate() / 100);

            paymentDueRepository.save(new PaymentDue(downPaymentVoucherNumber
                    , java.sql.Date.valueOf(triggerDate)
                    , paymentPlan.getPlanApartment().getCustomerAccount()
                    , "DOWN PAYMENT " + Integer.toString(paymentIndex)
                    , paymentAmount
                    , paymentPlan.getPlanApartment().getStock().getStockAccount().getAccountCode()
                    , java.sql.Date.valueOf(dueDate)
                    , 1.0
                    , paymentPlan.getPlanApartment().getSellingPrice()
                    , paymentPlan.getPlanApartment().getApartmentId()
                    , paymentPlan.getPlanApartment().getApartmentNo())
            );
            dueDate = dueDate.plusMonths(1);
            triggerDate = triggerDate.plusMonths(1);
            downPaymentAmount += paymentAmount;
            paymentIndex++;
        }

        paymentIndex = 1;
        double installmentAmount = saleAmount * (paymentPlan.getInstallmentRate() / 100);

        String installmentVoucherNumber = generateVoucherNumber("IS");
        triggerDate = PaymentPlanSupport.getNextQuarterDate(dueDate.minusMonths(1), 1);
        dueDate = PaymentPlanSupport.getNextQuarterDate(dueDate.minusMonths(1), 10);// subtract because loop adds one after the last Down payment due
        while (saleAmount - downPaymentAmount > 0) {

            paymentDueRepository.save(new PaymentDue(installmentVoucherNumber
                    , java.sql.Date.valueOf(triggerDate)
                    , paymentPlan.getPlanApartment().getCustomerAccount()
                    , "INSTALLMENT " + Integer.toString(paymentIndex)
                    , installmentAmount
                    , paymentPlan.getPlanApartment().getStock().getStockAccount().getAccountCode()
                    , java.sql.Date.valueOf(dueDate)
                    , 1.0
                    , paymentPlan.getPlanApartment().getSellingPrice()
                    , paymentPlan.getPlanApartment().getApartmentId()
                    , paymentPlan.getPlanApartment().getApartmentNo())
            );
            triggerDate = triggerDate.plusMonths(3);
            dueDate = dueDate.plusMonths(3);
            saleAmount -= installmentAmount;
            paymentIndex++;
        }
        return true;
    }

    @Override
    public double getDueAmount(String apartmentNo, Date tillDate) {
        double paidAmount = voucherRepository.getPaidAmount(apartmentNo); // Amount Paid From Vouchers (BR)
        double amountTriggered = paymentDueRepository.getSumOfDueAmount(apartmentNo, tillDate);// Amount Valid to Pay from Payment Due
        return amountTriggered - paidAmount;
    }


    @Override
    public List<Voucher> mapToVoucher(String apartmentNo, Integer accountCode, Date fromDate, Date toDate, boolean isLate) {

        List<PaymentDue> paymentDueList;
        if (isLate) {
            paymentDueList = paymentDueRepository.findByApartmentNoAndAccountCodeForLate(apartmentNo, accountCode, fromDate, toDate);
        } else {
            paymentDueList = paymentDueRepository.findByApartmentNoAndAccountCode(apartmentNo, accountCode, fromDate, toDate);
        }
        List<Voucher> voucherList = new ArrayList<>();
        List<Voucher> lateVoucherList = new ArrayList<>();
        for (PaymentDue paymentDue : paymentDueList) {
            Voucher voucher = new Voucher();
            voucher.setVoucherNumber(paymentDue.getVoucherNumber());
            voucher.setVoucherDate(paymentDue.getVoucherDate());
            voucher.setDebit(paymentDue.getDebit());
            if (!paymentDue.getVoucherNumber().startsWith("LPT")) {
                voucher.setRemarks(paymentDue.getRemarks() + " @due on : " + paymentDue.getDueDate().toString());
                voucherList.add(voucher); // add to normal List in case not late entry
            } else {
                voucher.setRemarks(paymentDue.getRemarks());
                lateVoucherList.add(voucher); // add to late list to create single voucher in proceeding
            }
        }
        if(isLate){
            return lateVoucherList;
        }
        // Add single Voucher for late in case of Payment Ledger
        Voucher mergerVoucher = mergeVoucher(lateVoucherList);
        if(mergerVoucher != null){
            voucherList.add(mergerVoucher);
        }
        return voucherList;
    }

    /**
     * Merges Voucher to create single entry
     * @param voucherList List of vouchers to merge
     * @return Single Voucher will sum in the debit
     */
    private Voucher mergeVoucher(List<Voucher> voucherList){
        if(voucherList.size() < 1){
            return null;
        }
        Voucher mergeVoucher = new Voucher();
        mergeVoucher.setVoucherNumber("LATE");
        mergeVoucher.setRemarks("Outstanding Charges till date");
        Date lastLateTriggerDate = java.sql.Date.valueOf("2000-11-01");
        double totalLateDues = 0.0;
        for(Voucher voucher: voucherList){
            if(voucher.getVoucherDate().after(lastLateTriggerDate)) {
                lastLateTriggerDate = voucher.getVoucherDate();
            }
            totalLateDues += voucher.getDebit();
        }
        mergeVoucher.setVoucherDate(lastLateTriggerDate);
        mergeVoucher.setDebit(totalLateDues);
        return mergeVoucher;

    }


    @Override
    public String generateLateEntries() {
        List<Apartment> soldApartmentList = apartmentRepository.findByIsSold(true);
        int entriesGenerated = 0;
        String voucherNumber = generateVoucherNumber("LPT");
        Date currentDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        for (Apartment apartment : soldApartmentList) {
            double paidAmount = voucherRepository.getPaidAmount(apartment.getApartmentNo()); // Amount Paid From Vouchers (BR)
            double amountTriggered = paymentDueRepository.getSumOfOutStandingAmount(apartment.getApartmentNo(), currentDate);// Amount After due dates
            double outStandingAmount = amountTriggered - paidAmount;
            if (outStandingAmount > 0) {
                generateLateEntry(apartment, outStandingAmount, voucherNumber, currentDate);
                entriesGenerated++;
            }
        }
        return entriesGenerated + " Late Entries Generated.";
    }

    /**
     * Generate Single Entry in Payment Due for late
     *
     * @param apartment         Target Apartment
     * @param outStandingAmount late Amount
     * @param voucherNumber     number to be given to entry
     */
    private void generateLateEntry(Apartment apartment, double outStandingAmount, String voucherNumber, Date currentDate) {

        paymentDueRepository.save(new PaymentDue(voucherNumber
                , currentDate
                , apartment.getCustomerAccount()
                , "LATE AMOUNT ON DUES SUMMING Rs. " + outStandingAmount + " triggered on " + currentDate.toString()
                , LedgerCalculationService.round(outStandingAmount * 0.0023, 2)
                , apartment.getStock().getStockAccount().getAccountCode()
                , currentDate
                , 1.0
                , apartment.getSellingPrice()
                , apartment.getApartmentId()
                , apartment.getApartmentNo())
        );
    }

    @Override
    public double getSumOfDownPayment(String apartmentNumber) {
        return paymentDueRepository.getSumOfDownPayment(apartmentNumber);
    }
}
