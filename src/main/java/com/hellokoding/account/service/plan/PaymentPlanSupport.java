package com.hellokoding.account.service.plan;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Apartment;
import com.hellokoding.account.model.PaymentPlanDown;
import com.hellokoding.account.model.Stock;
import com.hellokoding.account.reportModel.PaymentPlanModel;
import com.hellokoding.account.reportModel.PlanInternal;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.stock.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@Service
public class PaymentPlanSupport {

    @Autowired
    private ChassisService chassisService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private StockService stockService;


    /**
     * Get Payment Plan for Stock(MODEL)
     * @param stockId
     * @return
     */
    PaymentPlanModel getPlanForStock(int stockId) {
        Stock stock = stockService.findByStockId(stockId);
        if (stock == null) {
            return null;
        }
        if(stock.getPaymentPlan() == null){
            return null;
        }
        PaymentPlanModel paymentPlanModel = new PaymentPlanModel();
        paymentPlanModel.setStock(stock);
        return setPaymentInternalListModel(paymentPlanModel);
    }

    /**
     * Get Payment Plan for Apartment
     * Specific to apartment
     * @param apartmentNumber
     * @return
     */
    PaymentPlanModel getPlanForApartment(String apartmentNumber) {
        Apartment apartment = chassisService.findByChassisNumber(apartmentNumber);
        if (apartment == null) {
            return null;
        }
        PaymentPlanModel paymentPlanModel = new PaymentPlanModel();
        paymentPlanModel.setApartment(apartment);
        Account customerAccount = accountService.findAccount(apartment.getCustomerAccount());
        paymentPlanModel.setCustomer(customerAccount);

        if(customerAccount == null){
            return setPaymentInternalListApartment(paymentPlanModel,false);
        }
        return setPaymentInternalListApartment(paymentPlanModel,true);
    }


    /**
     * Generate Payment Plan Model For Apartment
     * Fetch data form Stock Payment Model in case Apartment Plan is null (Unsold apartment)
     * @param paymentPlanModel
     * @param isSold check if sold
     * @return
     */
    private PaymentPlanModel setPaymentInternalListApartment(PaymentPlanModel paymentPlanModel, boolean isSold) {

        List<PlanInternal> downPaymentList = new ArrayList<>();
        Date saleDate;
        if(isSold) {
            saleDate = paymentPlanModel.getApartment().getDate();
        }else{
            saleDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        }
        LocalDate dueDate;
        Double saleAmount = paymentPlanModel.getApartment().getSellingPrice();
        Double downPaymentAmount = 0.0;
        int paymentIndex = 1;

        List<PaymentPlanDown> paymentPlanDownList;
        if(isSold) {
            paymentPlanDownList = paymentPlanModel.getApartment().getPaymentPlan().getPaymentPlanDownList();
        }else{
            paymentPlanDownList = paymentPlanModel.getApartment().getStock().getPaymentPlan().getPaymentPlanDownList();
        }
        Collections.sort(paymentPlanDownList); // Sort By Sort Order

        if (paymentPlanDownList.size() > 0) {
            dueDate = saleDate.toLocalDate().plusDays(15);
        } else {
            dueDate = saleDate.toLocalDate();
        }
        for (PaymentPlanDown paymentPlanDown : paymentPlanDownList) {

            double paymentAmount = saleAmount * (paymentPlanDown.getRate() / 100);
            downPaymentList.add(new PlanInternal("DOWN PAYMENT " + Integer.toString(paymentIndex), "", java.sql.Date.valueOf(dueDate), Math.ceil(paymentAmount)));
            dueDate = dueDate.plusMonths(1);
            downPaymentAmount += paymentAmount;
            paymentIndex++;
        }
        paymentPlanModel.setDownPaymentList(downPaymentList);

        paymentIndex = 1;
        List<PlanInternal> installmentList = new ArrayList<>();
        double installmentAmount = 0.0;
        if(isSold) {
            installmentAmount = saleAmount * (paymentPlanModel.getApartment().getPaymentPlan().getInstallmentRate() / 100);
        }else{
            installmentAmount = saleAmount * (paymentPlanModel.getApartment().getStock().getPaymentPlan().getInstallmentRate() / 100);
        }

        dueDate = getNextQuarterDate(dueDate.minusMonths(1),10); // subtract because loop adds one after the last Down payment due
        while (saleAmount - downPaymentAmount> 0) {

            installmentList.add(new PlanInternal("INSTALLMENT  " + Integer.toString(paymentIndex), "", java.sql.Date.valueOf(dueDate), Math.ceil(installmentAmount)));
            dueDate = dueDate.plusMonths(3);
            saleAmount -= installmentAmount;
            paymentIndex++;
        }
        paymentPlanModel.setInstallmentMessage("14 Quarterly installments @ Rs. " + installmentAmount + "/= starting from next Quarter" );
        paymentPlanModel.setInstallmentTotal(installmentAmount * 14);
        paymentPlanModel.setImageUrl(paymentPlanModel.getApartment().getStock().getImageUrl());
        paymentPlanModel.setInstallmentStartDate(installmentList.get(0).getDueDate());
        paymentPlanModel.setInstallmentList(installmentList);
        return paymentPlanModel;
    }


    /**
     * Generate Payment Plan Model For Stock (Model)
     * Fetch Data for Stock Plan
     * @param paymentPlanModel
     * @return
     */
    private PaymentPlanModel setPaymentInternalListModel(PaymentPlanModel paymentPlanModel){
        List<PlanInternal> downPaymentList = new ArrayList<>();
        Date saleDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        LocalDate dueDate;
        Double saleAmount = paymentPlanModel.getStock().getSellingPrice();
        Double downPaymentAmount = 0.0;
        int paymentIndex = 1;

        List<PaymentPlanDown> paymentPlanDownList = paymentPlanModel.getStock().getPaymentPlan().getPaymentPlanDownList();
        Collections.sort(paymentPlanDownList); // Sort By Sort Order

        if (paymentPlanDownList.size() > 0) {
            dueDate = saleDate.toLocalDate().plusDays(15);
        } else {
            dueDate = saleDate.toLocalDate();
        }
        for (PaymentPlanDown paymentPlanDown : paymentPlanDownList) {

            double paymentAmount = saleAmount * (paymentPlanDown.getRate() / 100);
            downPaymentList.add(new PlanInternal("DOWN PAYMENT " + Integer.toString(paymentIndex), "", java.sql.Date.valueOf(dueDate), Math.ceil(paymentAmount)));
            dueDate = dueDate.plusMonths(1);
            downPaymentAmount += paymentAmount;
            paymentIndex++;
        }
        paymentPlanModel.setDownPaymentList(downPaymentList);

        paymentIndex = 1;
        List<PlanInternal> installmentList = new ArrayList<>();
        double installmentAmount = saleAmount * (paymentPlanModel.getStock().getPaymentPlan().getInstallmentRate() / 100);
        dueDate = getNextQuarterDate(dueDate.minusMonths(1),10); // subtract because loop adds one after the last Down payment due
        while (saleAmount - downPaymentAmount > 0) {

            installmentList.add(new PlanInternal("INSTALLMENT  " + Integer.toString(paymentIndex), "", java.sql.Date.valueOf(dueDate), Math.ceil(installmentAmount)));
            dueDate = dueDate.plusMonths(3);
            saleAmount -= installmentAmount;
            paymentIndex++;
        }
        paymentPlanModel.setInstallmentList(installmentList);
        return paymentPlanModel;
    }

    /**
     * Get next Quarter date
     * @param lastDownPaymentDate Source Date
     * @param dayOfMonth required day of returned Month
     * @return date of next Quarter
     */
     static LocalDate getNextQuarterDate(LocalDate lastDownPaymentDate,int dayOfMonth) {

        if (lastDownPaymentDate.getMonthValue() <= 3) {
            return LocalDate.of(lastDownPaymentDate.getYear(), Month.APRIL, dayOfMonth);
        } else if (lastDownPaymentDate.getMonthValue() <= 6) {
            return LocalDate.of(lastDownPaymentDate.getYear(), Month.JULY, dayOfMonth);
        } else if (lastDownPaymentDate.getMonthValue() <= 9) {
            return LocalDate.of(lastDownPaymentDate.getYear(), Month.OCTOBER, dayOfMonth);
        } else {
            return LocalDate.of(lastDownPaymentDate.getYear() + 1, Month.JANUARY, dayOfMonth);
        }
    }
}
