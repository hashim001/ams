package com.hellokoding.account.service.plan;

import com.hellokoding.account.model.Apartment;
import com.hellokoding.account.model.PaymentPlan;
import com.hellokoding.account.model.PaymentPlanDown;
import com.hellokoding.account.reportModel.PaymentPlanModel;
import com.hellokoding.account.repository.ApartmentRepository;
import com.hellokoding.account.repository.PaymentPlanRepository;
import com.hellokoding.account.repository.PlanDownPaymentRepository;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@Service
public class PaymentPlanServiceImpl implements PaymentPlanService {

    @Autowired
    private PaymentPlanRepository paymentPlanRepository;

    @Autowired
    private PlanDownPaymentRepository planDownPaymentRepository;

    @Autowired
    private ApartmentRepository apartmentRepository;

    @Autowired
    private PaymentPlanSupport paymentPlanSupport;

    @Autowired
    private PaymentDueService paymentDueService;

    @Override
    public PaymentPlan findByPlanId(int planId) {
        return paymentPlanRepository.findByPlanId(planId);
    }

    @Override
    public PaymentPlan savePlan(PaymentPlan paymentPlan) {
        return paymentPlanRepository.save(paymentPlan);
    }

    @Override
    public boolean removePaymentPlan(int planId) {
        paymentPlanRepository.remove(planId);
        return true;
    }

    @Override
    public PaymentPlanDown findByDownId(int downId) {
        return planDownPaymentRepository.findByDownId(downId);
    }

    @Override
    public PaymentPlanDown saveDownPayment(PaymentPlanDown paymentPlanDown) {
        return planDownPaymentRepository.save(paymentPlanDown);
    }

    @Override
    public boolean removeDownPayment(int downId) {
        planDownPaymentRepository.remove(downId);
        return true;
    }


    @Override
    public void removePaymentPlan(PaymentPlan paymentPlan) {
        paymentPlanRepository.delete(paymentPlan);
    }

    /**
     * Performs clone of Payment Plan
     *
     * @param apartment Target Apartment
     * @return
     */
    @Override
    public boolean attachPaymentPlan(Apartment apartment) {
        PaymentPlan paymentPlan = apartment.getStock().getPaymentPlan();
        if (paymentPlan == null) {
            return false;
        }
        PaymentPlan paymentPlanClone = new PaymentPlan();
        try {
            BeanUtils.copyProperties(paymentPlanClone, paymentPlan);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return false;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return false;
        }

        List<PaymentPlanDown> paymentPlanDownList = new ArrayList<>();// to save new plan internals
        for (PaymentPlanDown paymentPlanDown : paymentPlan.getPaymentPlanDownList()) {
            PaymentPlanDown planDown = new PaymentPlanDown();
            try {
                BeanUtils.copyProperties(planDown, paymentPlanDown);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            planDown.setDownId(null);
            planDown.setPaymentPlan(paymentPlanClone);
            paymentPlanDownList.add(planDown);
        }
        paymentPlanClone.setPlanId(null);
        paymentPlanClone.setPlanStock(null);
        paymentPlanClone.setPlanApartment(apartment);
        paymentPlanClone.setPaymentPlanDownList(paymentPlanDownList);
        paymentPlanClone.setUpdated(apartment.getDate());
        paymentPlanRepository.save(paymentPlanClone);
        return paymentDueService.generatePaymentDues(paymentPlanClone);
    }


    @Override
    public PaymentPlanModel getPlanModel(Integer stockId, String apartmentNo) {
        if (stockId != 0) {
            return paymentPlanSupport.getPlanForStock(stockId);
        }
        if (!apartmentNo.equals("0")) {
            return paymentPlanSupport.getPlanForApartment(apartmentNo);
        }
        return null;
    }

    @Override
    public List<PaymentPlanModel> getFloorPlan(Integer stockId, String floor) {

        List<String> apartmentNumberList;
        if (stockId != 0) {
            apartmentNumberList = apartmentRepository.findApartmentNoByStockIdAndFloor(stockId, floor);
        } else {
            apartmentNumberList = apartmentRepository.findApartmentNoByFloor(floor);
        }
        List<PaymentPlanModel> paymentPlanModelList = new ArrayList<>();

        for (String apartmentNumber : apartmentNumberList) {
            paymentPlanModelList.add(paymentPlanSupport.getPlanForApartment(apartmentNumber));
        }
        Collections.sort(paymentPlanModelList);
        return paymentPlanModelList;
    }
}
