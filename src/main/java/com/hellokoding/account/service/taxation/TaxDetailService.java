package com.hellokoding.account.service.taxation;


import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.TaxDetail;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface TaxDetailService {

    /**
     * Save TaxDetail (account_tax)
     * @param taxDetail Target
     */
    void save(TaxDetail taxDetail);

    /**
     * Remove TaxDetail based on Account
     * @param account account (Parent Account )
     */
    void remove(Account account);

    /**
     * Find All Tax Details
     * @return List of TaxDetails
     */
    List<TaxDetail> findAll();

    /**
     * Find TaxDetail Based on Account
     * @param account account
     * @return TaxDetail
     */
    TaxDetail findDetail(Account account);

    /**
     * Get Tax Map
     * Key -> accountCode-amount
     * Value -> title-amount%
     * @return Map with Configuration
     */
    Map<String,String> getTaxMap();
}
