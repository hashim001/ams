package com.hellokoding.account.service.ledger;

/**
 * Trial Balance Service
 * Details and Calculations of Trial Balance
 */

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.reportModel.TrialBalanceSet;
import com.hellokoding.account.service.voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static com.hellokoding.account.service.ledger.LedgerCalculationService.round;

@Service
public class TrialBalanceService {


    @Autowired
    private AccountService accountService;

    @Autowired
    private VoucherService voucherService;


    /**
     * Calculate Basic Trial balance
     *
     * @param accountLevel     Account Level (List of Account are fetched by level)
     * @param toDate           Trial Balance End date (inclusive)
     * @param sourceAccount    in case of single account Trial Balance Calculation (null in other cases)
     * @param accountListParam in case of Custom List of Account as Source List
     * @return List Of Trial Balance Model
     */
    public List<TrialBalanceSet> getTrailBalanceBasic(int accountLevel, Date toDate, Account sourceAccount, List<Account> accountListParam) {
        List<Account> accountList = new ArrayList<>();
        if (sourceAccount != null) {
            accountList.add(sourceAccount);
        } else if (accountListParam != null) {
            accountList = accountListParam;
        } else {
            accountList = accountService.findAccountsByLevel(accountLevel);
        }
        List<TrialBalanceSet> trialBalanceSetList = new ArrayList<>();
        if (accountLevel != 0 || sourceAccount != null) {

            for (Account account : accountList) {
                TrialBalanceSet trialBalanceSet = new TrialBalanceSet();
                trialBalanceSet.setAccountTitle(account.getTitle());
                Double trialAmountDebit = 0.0;
                Double trialAmountCredit = 0.0;


                List<Account> internalAccountList = getInternalAccountList(account, new ArrayList<>());
                for (Account accountInternal : internalAccountList) {

                    Double openingAmount = accountInternal.getOpeningDebit() - accountInternal.getOpeningCredit();
                    double calculatedAmount = round(getAmount(voucherService.getVouchersByAccount(accountInternal.getAccountCode())
                            , openingAmount, null, toDate, 1), 2);
                    if (calculatedAmount > 0) {
                        trialAmountDebit += calculatedAmount;
                    } else {
                        trialAmountCredit += calculatedAmount;
                    }

                }
                trialBalanceSet.setCredit(trialAmountCredit);
                trialBalanceSet.setDebit(trialAmountDebit);
                if (trialAmountCredit != 0 || trialAmountDebit != 0) {
                    trialBalanceSetList.add(trialBalanceSet);
                }
            }
        }
        return trialBalanceSetList;
    }


    /**
     * Get Detailed Trial Balance
     * having the Trial during Period
     * NOTE : View dimensions should be catered on back end
     * @param accountLevel level of Account
     * @param fromDate     Trial Start Date (inclusive)
     * @param toDate       Trial End Date (inclusive)
     * @return List Of Trial Balance Model
     */
    public List<TrialBalanceSet> getTrailBalanceDetailed(int accountLevel, Date fromDate, Date toDate) {

        List<TrialBalanceSet> trialBalanceSetList = new ArrayList<>();

        // List of Account By level selected (External Parent Accounts)
        List<Account> accountList = accountService.findAccountsByLevel(accountLevel);
        for (Account account : accountList) {
            TrialBalanceSet trialBalanceSet = new TrialBalanceSet();
            trialBalanceSet.setAccountTitle(account.getTitle());
            double trialDebit = 0.0;
            double trialCredit = 0.0;
            double trialDebitCalculated = 0.0;
            double trialCreditCalculated = 0.0;
            double trialDebitPeriod = 0.0;
            double trialCreditPeriod = 0.0;

            // List of 4th level accounts (Working Accounts)
            List<Account> internalAccountList = getInternalAccountList(account, new ArrayList<>());
            for (Account accountInternal : internalAccountList) {

                Double openingAmount = accountInternal.getOpeningDebit() - accountInternal.getOpeningCredit();
                Double trialAmount = round(getAmount(voucherService.getVouchersByAccount(accountInternal.getAccountCode())
                        , openingAmount, null, fromDate, 2), 2);

                if (trialAmount < 0) {
                    trialBalanceSet.setCredit(trialAmount);
                    trialBalanceSet.setCreditCalculated(round(getAmount(voucherService.getVouchersByAccount(accountInternal.getAccountCode())
                            , trialBalanceSet.getCredit(), fromDate, toDate, 3), 2));
                    trialCredit += trialBalanceSet.getCredit();
                    trialCreditCalculated += trialBalanceSet.getCreditCalculated();

                    if (trialBalanceSet.getCreditCalculated() > trialBalanceSet.getCredit()) {
                        trialBalanceSet.setCreditDuringPeriod(round(trialBalanceSet.getCreditCalculated() - trialBalanceSet.getCredit(), 2));
                        trialCreditPeriod += trialBalanceSet.getCreditDuringPeriod();

                    } else if (trialBalanceSet.getCreditCalculated() < trialBalanceSet.getCredit()) {
                        trialBalanceSet.setDebitDuringPeriod(round(trialBalanceSet.getCredit() - trialBalanceSet.getCreditCalculated(), 2));
                        trialDebitPeriod += trialBalanceSet.getDebitDuringPeriod();
                    }

                } else {
                    trialBalanceSet.setDebit(trialAmount);
                    trialBalanceSet.setDebitCalculated(round(getAmount(voucherService.getVouchersByAccount(accountInternal.getAccountCode())
                            , trialBalanceSet.getDebit(), fromDate, toDate, 3), 2));
                    trialDebit += trialBalanceSet.getDebit();
                    trialDebitCalculated += trialBalanceSet.getDebitCalculated();

                    if (trialBalanceSet.getDebitCalculated() > trialBalanceSet.getDebit()) {
                        trialBalanceSet.setDebitDuringPeriod(round(trialBalanceSet.getDebitCalculated() - trialBalanceSet.getDebit(), 2));
                        trialDebitPeriod += trialBalanceSet.getDebitDuringPeriod();

                    } else if (trialBalanceSet.getDebitCalculated() < trialBalanceSet.getDebit()) {
                        trialBalanceSet.setCreditDuringPeriod(round(trialBalanceSet.getDebit() - trialBalanceSet.getDebitCalculated(), 2));
                        trialCreditPeriod += trialBalanceSet.getCreditDuringPeriod();
                    }
                }

            }
            trialBalanceSet.setDebit(trialDebit);
            trialBalanceSet.setCredit(trialCredit);
            trialBalanceSet.setCreditCalculated(trialCreditCalculated);
            trialBalanceSet.setDebitCalculated(trialDebitCalculated);
            trialBalanceSet.setCreditDuringPeriod(trialCreditPeriod);
            trialBalanceSet.setDebitDuringPeriod(trialDebitPeriod);
            // Validating Set for Trial Balance effect
            if (trialBalanceSet.isValidForTrial()) {
                trialBalanceSetList.add(trialBalanceSet);
            }

        }
        return trialBalanceSetList;
    }

    /**
     * Calculate ledger amount of provided voucher List(of Account )
     *
     * @param allVouchers   List of Account Vouchers
     * @param openingAmount Account opening Amount
     * @param fromDate      Trial Start Date
     * @param toDate        Trial End Date
     * @param checkType     for different Manipulations of fromDate and toDate (case : opening ,during, ending)
     * @return amount
     */
    private double getAmount(List<Voucher> allVouchers, double openingAmount, Date fromDate, Date toDate, int checkType) {

        for (Voucher voucher : allVouchers) {
            if (validateDate(voucher.getVoucherDate(), fromDate, toDate, checkType)) {

                openingAmount += voucher.getDebit();
                openingAmount -= voucher.getCredit();
            }
        }
        return openingAmount;

    }

    /**
     * Validate Date
     * Case : Start 1, End 2 , During 3
     *
     * @param voucherDate Target Date
     * @param fromDate    start Date
     * @param toDate      end Date
     * @param checkType   case type
     * @return check
     */
    public boolean validateDate(Date voucherDate, Date fromDate, Date toDate, int checkType) {
        if (checkType == 1) {
            if (!voucherDate.after(toDate))
                return true;
        } else if (checkType == 2) {
            if (voucherDate.before(toDate))
                return true;
        } else if (checkType == 3) {
            if (voucherDate.after(fromDate) && !voucherDate.after(toDate))
                return true;

        }
        return false;
    }

    /**
     * Recursive Fuction to generate
     * list of all accounts falling in source account hierarchy (account inclusive)
     *
     * @param account   source account
     * @param finalList list of currently add accounts (Passed for recursive call)
     * @return List of falling Accounts
     */
    private List<Account> getInternalAccountList(Account account, List<Account> finalList) {

        if (account.getLevel() == 4) {
            finalList.add(account);
        }
        for (Account accountL1 : account.getChildList()) {
            if (accountL1.getLevel() == 4) {
                finalList.add(accountL1);
            } else {
                getInternalAccountList(accountL1, finalList);
            }
        }
        return finalList;
    }

}
