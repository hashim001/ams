package com.hellokoding.account.service.ledger;

/**
 * Ledger Reporting Service
 * Detailed calculations Of Ledger
 */

import com.hellokoding.account.model.*;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.plan.PaymentDueService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.service.voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class LedgerCalculationService {

    @Autowired
    private AccountService accountService;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private PaymentDueService paymentDueService;

    @Autowired
    private StaticInfoService staticInfoService;
    @Autowired
    private DistributorService distributorService;

    @Autowired
    private ChassisService chassisService;

    private static Logger LOGGER = Logger.getLogger("InfoLogging");


    /**
     * Get Vouchers for Ledger Calculation
     * Transient Field "balance" in Voucher is used
     * for setting the sequential balance
     * Vouchers from start to end date are fetched and
     * Ledger calculated accordingly
     *
     * @param accountCode      Account Code for target Account
     * @param costCentre       CostCentre Code if ledger is for specific costCentre
     * @param fromDate         ledger Start Date
     * @param toDate           ledger End Date
     * @param forBank          if account is a bank account (Vouchers are fetched by bank_account from Voucher)
     * @param forProductLedger if for product ledger (itemQuantity is ledgered from Voucher)
     * @return List of Voucher having their balance set
     */
    public List<Voucher> getSortedVouchers(int accountCode, Long costCentre, Date fromDate, Date toDate, boolean forBank, boolean forProductLedger, boolean forRawMaterial, boolean isChassis, List<Integer> plotList) {

        List<Voucher> sortedVoucherList = new ArrayList<>();
        Account account = new Account();
        Double openingAmount = 0.0;
        if (!forRawMaterial && !isChassis) {
            account = accountService.findAccount(accountCode);
            openingAmount = account.getOpeningDebit() - account.getOpeningCredit();
        }
        List<Voucher> voucherList;
        if (isChassis) {
            voucherList = voucherService.findByChassisId(plotList);

        } else if (forBank) {
            voucherList = voucherService.findByBankAccountCode(accountCode);
        } else {
            if (forRawMaterial) {
                voucherList = voucherService.getVouchersByItemAccount(accountCode);
            } else {
                voucherList = voucherService.getVouchersByAccount(accountCode);
            }
        }
        List<Voucher> genericVoucherList;
        if (forProductLedger) {

            genericVoucherList = productLedger(voucherList, account.getAccountStock().getOpeningQuantity(), "");

        } else {
            genericVoucherList = generateLedger(voucherList, openingAmount, forBank);
        }
        for (Voucher voucher : genericVoucherList) {
            if (costCentre != null) {
                if (voucher.getCostCentre() != null) {
                    if (voucher.getCostCentre().equals(costCentre) && validateDate(fromDate, toDate, voucher.getVoucherDate())) {
                        voucher.setVoucherType(voucher.getVoucherNumber().substring(0, 2));

                        sortedVoucherList.add(voucher);
                    }
                }
            } else {
                if (!isChassis) {
                    if (validateDate(fromDate, toDate, voucher.getVoucherDate())) {
                        voucher.setVoucherType(voucher.getVoucherNumber().substring(0, 2));
                        sortedVoucherList.add(voucher);

                    }
                } else {
                    sortedVoucherList.add(voucher);
                }

            }
        }

        return getLedgerVoucher(sortedVoucherList);
    }

    /**
     * Get sorted voucher for project wise report
     * @param projectCode selected project code
     * @param accountCode select account code list
     * @param fromDate from date
     * @param toDate to date
     * @return list of sorted vouchers by project and account code list
     */
    public List<Voucher> getSortedVouchers(Integer projectCode, List<Integer> accountCode, Date fromDate, Date toDate) {

        List<Voucher> sortedVoucherList = new ArrayList<>();
        Account account = new Account();
        Double openingAmount = 0.0;
        List<Voucher> voucherList;
        voucherList = voucherService.findByProjectCodeAndAccountCode(projectCode,accountCode);
        List<Voucher> genericVoucherList;

        genericVoucherList = generateLedger(voucherList, openingAmount, false);
        for (Voucher voucher : genericVoucherList) {
            if (validateDate(fromDate, toDate, voucher.getVoucherDate())) {
                voucher.setVoucherType(voucher.getVoucherNumber().substring(0, 2));
                sortedVoucherList.add(voucher);

            }
        }

        return getLedgerVoucher(sortedVoucherList);
    }

    /**
     * Set Voucher balance for Vouchers
     *
     * @param allVouchers   List of Vouchers on which calculations are done
     * @param openingAmount account Opening balance
     * @param isBank        if bank account (Calculations are inverted)
     * @return List of Voucher with balance set
     */
    private List<Voucher> generateLedger(List<Voucher> allVouchers, Double openingAmount, Boolean isBank) {

        List<Voucher> generatedListForAll = new ArrayList<>();

        for (Voucher voucher : allVouchers) {

            if (isBank) {
                openingAmount += voucher.getCredit();
                openingAmount -= voucher.getDebit();
            } else {
                openingAmount -= voucher.getCredit();
                openingAmount += voucher.getDebit();
            }

            voucher.setBalance(round(openingAmount, 2));
            generatedListForAll.add(voucher);

        }
        return generatedListForAll;
    }

    /**
     * Validate Date
     *
     * @param fromDate    starting date (inclusive)
     * @param toDate      ending date (inclusive)
     * @param voucherDate Target date
     * @return check
     */
    private boolean validateDate(Date fromDate, Date toDate, Date voucherDate) {

        if (voucherDate.before(fromDate)) {
            return false;
        }
        if (voucherDate.after(toDate)) {
            return false;
        }

        return true;
    }

    /**
     * Round Decimal to n places
     *
     * @param value  Target Value
     * @param places number of places to round
     * @return rounded value
     */
    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    /**
     * Get details of internal Vouchers
     * Sets Credit voucher list to Debit master Voucher and Vice versa
     * Explanation : Every Debit Voucher has 1 (or n) credit Vouchers (and Vice Versa)
     *
     * @param voucherList List of Target Vouchers
     * @return List of Vouchers with internal Voucher List Set (Transient Field)
     */
    private List<Voucher> getLedgerVoucher(List<Voucher> voucherList) {
        List<Voucher> masterList = voucherService.findByVoucherNumberIn(voucherList.stream()
                .map(Voucher::getVoucherNumber)
                .collect(Collectors.toList()));
        for (Voucher voucher : voucherList) {
            List<Voucher> internalList = new ArrayList<>();
            for (Voucher internalVoucher : masterList) {
                if (voucher.getDebit() > voucher.getCredit() || voucher.getVoucherNumber().contains("GI")) {
                    if (voucher.getVoucherNumber().equals(internalVoucher.getVoucherNumber()) && (internalVoucher.getCredit() >= 0)) {
                        internalList.add(internalVoucher);
                    }
                } else {
                    if (voucher.getVoucherNumber().equals(internalVoucher.getVoucherNumber()) && internalVoucher.getDebit() >= 0) {
                        internalList.add(internalVoucher);
                    }
                }
            }
            voucher.setInternalVoucherList(internalList);
        }
        return voucherList;

    }

    /**
     * Set Voucher balance to item Quantity Ledger
     * used balance, debit , credit fields
     *
     * @param allVouchers     List of Vouchers
     * @param openingQuantity opening balance of Quantity
     * @return List of Voucher with balance, debit , credit set
     */
    private List<Voucher> productLedger(List<Voucher> allVouchers, Double openingQuantity, String name) {

        List<Voucher> generatedListForAll = new ArrayList<>();

        for (Voucher voucher : allVouchers) {
            double creditQuantity = 0.0;
            double debitQuantity = 0.0;
            if (voucher.getCredit() > voucher.getDebit() || voucher.getVoucherNumber().contains("GI")) {
                creditQuantity += voucher.getItemQuantity();
                openingQuantity -= voucher.getItemQuantity();
            } else if (voucher.getDebit() > voucher.getCredit()) {
                debitQuantity += voucher.getItemQuantity();
                openingQuantity += voucher.getItemQuantity();
            }
            voucher.setDebit(round(debitQuantity, 2));
            voucher.setCredit(round(creditQuantity, 2));

            voucher.setBalance(round(openingQuantity, 2));
            voucher.setItemName(name);
            generatedListForAll.add(voucher);

        }
        return generatedListForAll;
    }

    /**
     * Get total cash in hand amount
     *
     * @param bankAccount Cash In Hand Account Code
     * @return current amount
     */
    public double getCashInHandAmount(int bankAccount) {

        double amount = 0.0;
        for (Voucher voucher : voucherService.findByBankAccountCode(bankAccount)) {

            if (voucher.getCredit() > 0) {
                amount += voucher.getCredit();
            } else
                amount -= voucher.getDebit();

        }
        return amount;

    }

    /**
     * Get remaining Stock Quantity
     *
     * @param itemAccount
     * @return
     */
    public double getRemaingStockQuantity(int itemAccount, double openingQuantity, double cost) {
        for (Voucher voucher : voucherService.getVouchersByItemAccount(itemAccount)) {

            if (voucher.getCredit() > voucher.getDebit() || voucher.getVoucherNumber().contains("GI")) {
                cost -= voucher.getCredit();
                openingQuantity -= voucher.getItemQuantity();
            } else if (voucher.getDebit() > voucher.getCredit()) {
                cost += voucher.getDebit();
                openingQuantity += voucher.getItemQuantity();
            }
        }
        return cost / openingQuantity;

    }

    /**
     * @param accountList     account list for report
     * @param forCurrentStock current report for stock
     * @return voucher list
     */
    public List<Voucher> currentAccountsReport(List<Account> accountList, boolean forCurrentStock, boolean isBank) {
        List<Voucher> voucherList = new ArrayList<>();
        Double openingAmount;
        for (Account account : accountList) {
            Voucher voucher = new Voucher();
            List<Voucher> vouchers;
            openingAmount = account.getOpeningDebit() - account.getOpeningCredit();
            if (isBank) {
                vouchers = generateLedger(voucherService.findByBankAccountCode(account.getAccountCode()), openingAmount, isBank);
            } else {
                vouchers = generateLedger(voucherService.getVouchersByAccount(account.getAccountCode()), openingAmount, isBank);
            }
            if (vouchers.size() > 0) {
                voucher.setBalance(vouchers.get(vouchers.size() - 1).getBalance());
            } else {
                voucher.setBalance(openingAmount);
            }
            if (voucher.getBalance() == 0) {
                continue;
            }
            voucher.setItemName(account.getTitle());
            List<String> chassisList = new ArrayList<>();
            if (forCurrentStock) {
                vouchers = productLedger(voucherService.getVouchersByItemAccount(account.getAccountCode()), account.getAccountStock().getOpeningQuantity(), "");
                if (vouchers.size() > 0) {
                    voucher.setItemQuantity(vouchers.get(vouchers.size() - 1).getBalance());
                } else {
                    voucher.setItemQuantity(account.getAccountStock().getOpeningQuantity());
                }
                for (Apartment apartment : account.getAccountStock().getApartmentList()) {
                    if (!apartment.isSold()) {
                        chassisList.add(apartment.getApartmentNo() + " === > " + apartment.getSellingPrice());
                    }
                }
            }
            voucher.setChassisList(chassisList);
            voucherList.add(voucher);
        }
        return voucherList;
    }

    /**
     * Set payment pending report for pak suzuki account
     *
     * @param apartmentS
     * @return
     */
    public List<Apartment> setPaymentPendingReport(List<Apartment> apartmentS, Date date) {
        List<Apartment> apartmentList = new ArrayList<>();
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        for (Apartment apartment : apartmentS) {
            double pendingAmount = 0;
            if (apartment.isSold()) {
                apartment.setDays(Duration.between(apartment.getDate().toLocalDate().atStartOfDay(), date.toLocalDate().atStartOfDay()).toDays());

            } else {
                apartment.setDays(0);
            }
            double amountPaid = 0;
            for (Voucher voucher : voucherService.findByChassisNumber(apartment.getApartmentNo())) {
                if (voucher.getAccountCode() == apartment.getCustomerAccount() && voucher.getCredit() > 0) {
                    if (voucher.getVoucherNumber().contains("BR")) {
                        amountPaid += voucher.getCredit();
                    } else if (voucher.getVoucherNumber().contains("BP") && voucher.getDebit() > 0) {
                        amountPaid -= voucher.getDebit();
                    }
                }
                if (voucher.getVoucherNumber().contains("AV")) {
                    pendingAmount += voucher.getCredit();

                }
                if (voucher.getVoucherNumber().contains("LP")) {
                    pendingAmount += voucher.getDebit();
                }
                if (voucher.getVoucherNumber().contains("BR") || voucher.getVoucherNumber().contains("BP")) {
                    if (voucher.getAccountCode().equals(staticInfo.getCompanyPaymentAccount())) {
                        pendingAmount -= voucher.getCredit();
                        pendingAmount -= voucher.getDebit();
                    }
                }
            }

            if (pendingAmount != 0) {
                apartment.setAmountPaid(amountPaid);
                apartment.setSellingPrice(pendingAmount);
                apartmentList.add(apartment);
            }
        }

        return apartmentList;

    }

    /**
     * set method for hold apartment report
     *
     * @param apartmentHoldList
     * @return
     */
    public List<ApartmentHold> setholdapartmentreport(List<ApartmentHold> apartmentHoldList, Date date) {
        for (ApartmentHold apartmentHold : apartmentHoldList) {
            LocalDate localDate = date.toLocalDate().minusDays(apartmentHold.getOnHoldDays());
            apartmentHold.setDays(Duration.between(localDate.atStartOfDay(), apartmentHold.getHoldDate().toLocalDate().atStartOfDay()).toDays());
            apartmentHold.setSalesmanName(distributorService.findByDistributorId(apartmentHold.getSalesmanId()).getName());
        }
        return apartmentHoldList;
    }


    /**
     * Get ledger for Payments of Apartment
     *
     * @param apartmentNumber
     * @param fromDate
     * @param toDate
     * @return
     */
    public List<Voucher> getSortedDueVouchers(String apartmentNumber, Date fromDate, Date toDate, boolean isLate) {

        List<Voucher> sortedVoucherList = new ArrayList<>();
        Double openingAmount = 0.0;
        Integer accountCode = chassisService.findByChassisNumber(apartmentNumber).getCustomerAccount();
        List<Voucher> voucherList = new ArrayList<>();
        if (!isLate) {
            voucherList = voucherService.getPaymentDueVoucher(apartmentNumber, accountCode);
        }
        voucherList.addAll(paymentDueService.mapToVoucher(apartmentNumber, accountCode, fromDate, toDate, isLate));


        Collections.sort(voucherList);
        voucherList = generateLedger(voucherList, openingAmount, false);

        for (Voucher voucher : voucherList) {

            if (validateDate(fromDate, toDate, voucher.getVoucherDate())) {
                voucher.setVoucherType(voucher.getVoucherNumber().substring(0, 2));
                sortedVoucherList.add(voucher);

            }
        }
        return getLedgerVoucher(sortedVoucherList);
    }

}
