package com.hellokoding.account.service.location;

import com.hellokoding.account.model.Location;
import com.hellokoding.account.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    public Location findByLocationId(int locationId) {
        return locationRepository.findByLocationId(locationId);
    }

    public List<Location> findAll() {
        return locationRepository.findAll();
    }

    public void save(Location location) {
        locationRepository.save(location);
    }

    public void remove(int locationId) {
        locationRepository.removeLocation(locationId);
    }
}
