package com.hellokoding.account.service.location;

import com.hellokoding.account.model.Location;

import java.util.List;

public interface LocationService {
    /**
     * Find Location by locationId
     *
     * @param locationId location identity
     * @return Location
     */
    Location findByLocationId(int locationId);

    /**
     * Get all Location
     *
     * @return List of location
     */
    List<Location> findAll();

    /**
     * Save location
     *
     * @param location location to save
     * @return
     */
    void save(Location location);

    /**
     * Remove Location with locationId
     *
     * @param locationId location identity
     */
    void remove(int locationId);
}
