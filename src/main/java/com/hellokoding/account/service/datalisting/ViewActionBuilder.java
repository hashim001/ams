package com.hellokoding.account.service.datalisting;


import com.hellokoding.account.service.security.RoleSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;


@Service
public class ViewActionBuilder {

    @Autowired
    private HttpServletRequest request;

    /**
     * Method to get Project Name for resource mapping.
     * Alternate for @Resource
     *
     * @return Project name String
     */

    public String getProjectName() {
        return request.getContextPath();
    }


    /**
     * Account Listing Action HTML Provider
     *
     * @param accountCode
     * @return
     */
    public String getAccountAction(String accountCode) {


        return "<form method=\"post\" id = \"accountRemove" + accountCode + "\" action=\"" + getProjectName() + "/Account/removeAccount\">\n" +

                "                                    <input  type=\"hidden\" name=\"accountCode\" value=\"" + accountCode + "\"/>\n" +
                "                                </form>\n" +
                "\n" +
                "                                    <form method=\"post\" id = \"accountEdit" + accountCode + "\" action=\"" + getProjectName() + "/Account/editAccount\">\n" +

                "                                        <input  type=\"hidden\" name=\"accountCode\" value=\"" + accountCode + "\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "                                        <li><a href=\"#\" onclick=\"edit(" + accountCode + ");\"><img src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\"></a></li>\n" +
                "                                        <li><a href=\"#\" onclick=\"remove(" + accountCode + ");\"><img src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "\n" +
                "                                    </ul>";
    }

    /**
     * Stock Listing Action HTML Provider
     *
     * @param accountCode
     * @return
     */
    public String getStockAction(String accountCode, String stockId) {

        return "                                    <form method=\"post\" id=\"stockRemove" + accountCode + "\" action=\"" + getProjectName() + "/Stock/removeStock\" />\n" +
                "                                        <input type=\"hidden\" name=\"accountCode\"\n" +
                "                                               value=\"" + accountCode + "\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "\n" +
                "                                    <li><a title=\"Edit\" href=\"#\" onclick=\"edit(" + accountCode + ")\" class=\"editForm\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
/*
                "                                    <li><a title=\"Manage Plan\" href=\"" + getProjectName() + "/Plan/paymentPlan?stockId=" + stockId + "\"  ><img src=\"" + getProjectName() + "/resources/img/bill.png\" alt=\"\"></a></li>\n" +
*/
                "                                        <li><a href=\"#\" onclick=\"remove(" + accountCode + ");\"><img\n" +
                "                                                src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                    </ul>";
    }

    /**
     * Sale voucher Listing Action HTML Provider
     *
     * @param voucherNumber
     * @param index
     * @return
     */
    public String getSaleVoucherAction(String voucherNumber, String index, String apartmentNo) {

        return "                                            <form method=\"post\" id=\"voucherRemove" + index + "\" action=\"" + getProjectName() + "/Account/deleteVoucher\">\n" +

                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\"" + voucherNumber + "\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Sales/viewSalesVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Edit\" href=\"" + getProjectName() + "/Sales/updateSaleVoucher?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Print\" href=\"" + getProjectName() + "/Sales/printSaleVoucher?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/print.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
/*
                "                                                <li><a title=\"View Payment Plan\" href=\"" + getProjectName() + "/Plan/viewPaymentPlan?apartmentNo=" + apartmentNo + "\"><img src=\"" + getProjectName() + "/resources/img/bill.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
*/
                "                                            </ul>";
    }


    /**
     * Sale voucher Listing Action HTML Provider
     *
     * @param voucherNumber
     * @param index
     * @return
     */
    public String getReturnVoucherAction(String voucherNumber, String index) {

        return "                                            <form method=\"post\" id=\"voucherRemove" + index + "\" action=\"" + getProjectName() + "/PurchaseReturn/deleteVoucher\">\n" +

                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\"" + voucherNumber + "\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"PurchaseReturn/viewReturnVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Edit\" href=\"" + getProjectName() + "//PurchaseReturn/updateReturn?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                            </ul>";
    }

    /**
     * Actions for Purchase Request listing
     * Actions for GM , Store , Maker
     *
     * @param index
     * @param status
     * @return
     */
    public String getRequestAction(String index, String status) {

        String responseHtml = "";
        // Normal Request Makers Actions
        if (RoleSupport.hasRole("ROLE_REQUEST_MAKER")) {
            if (status.equals("PENDING")) {
                responseHtml += " <form method=\"post\" id=\"requestRemove" + index + "\" action=\"" + getProjectName() + "/Request/removeRequest\">\n" +
                        "<input type=\"hidden\" name=\"requestId\" value=\"" + index + "\"/>\n" +
                        "</form>\n" +
                        "<form method=\"post\" id=\"requestClose" + index + "\" action=\"" + getProjectName() + "/Request/closeRequest\">\n" +
                        "<input type=\"hidden\" name=\"requestId\" value=\"" + index + "\"/>\n" +
                        "</form>\n";

            }
            responseHtml +=
                    "<ul class=\"list-inline\">\n" +
                            "<li><a title=\"Print\" target=\"_blank\" href=\"" + getProjectName() + "/Request/printRequestDetail?requestId=" + index + "\"><img src=\"" + getProjectName() + "/resources/img/viewIcon.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n";

            if (status.equals("PENDING")) {
                responseHtml += "<li><a title=\"Edit\"  href=\"" + getProjectName() + "/Request/request?requestId=" + index + "\"><img src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                        "<li><a title=\"Close\" href=\"#\" onclick=\"closeRequest(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                        " <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n";
            }
            responseHtml += " </ul>";
            // Role Master Admin , Store Or GM
        } else if (RoleSupport.hasRole("ROLE_MASTER_ADMIN")
                || RoleSupport.hasRole("ROLE_GM")
                || RoleSupport.hasRole("ROLE_STORE")) {

            if (!status.equals("APPROVED")) {
                responseHtml += " <form method=\"post\" id=\"requestRemove" + index + "\" action=\"" + getProjectName() + "/Request/removeRequest\">\n" +
                        "<input type=\"hidden\" name=\"requestId\" value=\"" + index + "\"/>\n" +
                        "</form>\n";

            }
            // if processed by store and not yet approved b y GM
            if (status.equals("PROCESSED") && RoleSupport.hasRole("ROLE_GM")) {
                responseHtml += " <form method=\"post\" id=\"approve" + index + "\" action=\"" + getProjectName() + "/Request/approve\">\n" +
                        "<input type=\"hidden\" name=\"requestId\" value=\"" + index + "\"/>\n" +
                        "</form>\n";

            }
            responseHtml +=
                    "<ul class=\"list-inline\">\n" +
                            "<li><a title=\"Print\" target=\"_blank\" href=\"" + getProjectName() + "/Request/printRequestDetail?requestId=" + index + "\"><img src=\"" + getProjectName() + "/resources/img/viewIcon.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n";

            // if processed by store and not yet approved b y GM
            if (status.equals("PROCESSED") && RoleSupport.hasRole("ROLE_GM")) {
                responseHtml += " <li><a title=\"Approve\" href=\"#\" onclick=\"approve(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/recycle.png\" alt=\"\"></a></li>\n";
            }
            if (!(status.equals("APPROVED") || status.equals("PO-STAGE"))) {
                responseHtml += "<li><a title=\"Edit\"  href=\"" + getProjectName() + "/Request/request?requestId=" + index + "\"><img src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                        " <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n";
            }
            responseHtml += " </ul>";
        }

        return responseHtml;
    }

    /**
     * Store Order Action List
     *
     * @param index  store Order id
     * @param status PENDING/ LOCKED >> PENDING CURRENTLY SURPASSED
     *               Icons View on basis of Status
     * @return HTML
     */
    public String getStoreOrderActionList(String index, String status) {

        String responseHTML = "";
        if (status.equals("PENDING")) {
            responseHTML += "<form method=\"post\" id=\"finalize" + index + "\" action=\"" + getProjectName() + "/Store/finalize\">\n" +
                    "       <input type=\"hidden\" name=\"orderId\" value=\"" + index + "\"/>\n" +
                    "       </form>\n";
        }


        responseHTML += " <ul class=\"list-inline\">\n" +
                "<li><a title=\"View\" target=\"_blank\" href=\"" + getProjectName() + "/Store/viewOrder?orderId=" + index + "\"><img src=\"" + getProjectName() + "/resources/img/viewIcon.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n";

        if (status.equals("LOCKED")) {
            responseHTML += "<li><a title=\"Update\"  href=\"" + getProjectName() + "/Store/order?orderId=" + index + "\"><img src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n"; //+
            /*"<li><a title=\"Finalize\" href=\"#\" onclick=\"finalize(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/recycle.png\" alt=\"\"></a></li>\n";*/

        }
        return responseHTML + "</ul>";
    }


    /**
     * Purchase voucher Action Provider
     *
     * @param voucherNumber
     * @param index
     * @return
     */
    public String getPurchaseVoucherAction(String voucherNumber, String index) {

        return "                                            <form method=\"post\" id=\"voucherRemove" + index + "\" action=\"" + getProjectName() + "/Account/deleteVoucher\">\n" +

                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\"" + voucherNumber + "\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Purchase/viewPurchaseVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
/*
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
*/
                "                                                <li><a title=\"Edit\" href=\"" + getProjectName() + "/Purchase/updatePurchaseVoucher?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                            </ul>";
    }


    /**
     * Payment voucher Action Provider
     *
     * @param voucherNumber
     * @param index
     * @return HTML
     */
    public String getPaymentVoucherAction(String voucherNumber, String index) {

        return "                                            <form method=\"post\" id=\"voucherRemove" + index + "\" action=\"" + getProjectName() + "/Account/deleteVoucher\">\n" +
                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\"" + voucherNumber + "\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Account/createPaymentVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Edit\" href=\"#\" onclick=\"edit(" + index + ")\" class=\"editForm\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/deleteIcon.png\" alt=\"\"></a>\n" +
                "                                                <li><a title=\"Print\" target=\"_blank\" href=\"" + getProjectName() + "/Account/printPaymentVoucher?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/print.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                "                                                </li>\n" +
                "                                            </ul>";
    }

    /**
     * Receipt voucher Action Provider
     *
     * @param voucherNumber
     * @param index
     * @return
     */
    public String getReceiptVoucherAction(String voucherNumber, String index) {

        return "                                            <form method=\"post\" id=\"voucherRemove" + index + "\" action=\"" + getProjectName() + "/Account/deleteVoucher\">\n" +
                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\"" + voucherNumber + "\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Account/createReceiptVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Edit\" href=\"#\" onclick=\"edit(" + index + ")\" class=\"editForm\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/deleteIcon.png\" alt=\"\"></a>\n" +
                "                                                <li><a title=\"Print\" target=\"_blank\" href=\"" + getProjectName() + "/Account/printReceiptVoucher?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/print.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                "                                                </li>\n" +
                "                                            </ul>";
    }

    /**
     * Sale Prep List Actions
     *
     * @param orderId
     * @return
     */

    public String getSalePrepAction(String orderId) {

        return "<ul class=\"list-inline\">\n" +
                "<li>\n" +
                "<a href=\"" + getProjectName() + "/SaleTrading/createDelivery?orderid=" + orderId + "\"><img\n" +
                "                                                    src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\"></a></li>\n" +
                "</ul>";
    }

    /**
     * Purchase Prep List Actions
     *
     * @param orderId
     * @return
     */

    public String getPurchasePrepAction(String orderId) {

        return "   <ul class=\"list-inline\">\n<li>\n<a href=\"" + getProjectName() + "/Trading/createDelivery?orderid=" + orderId + "\"><img\n" +
                "    src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\"></a></li>\n</ul>";
    }

    /**
     * Store GRN List Actions
     *
     * @param orderId
     * @return
     */

    public String getStorePurchasePrepAction(String orderId) {

        return "   <ul class=\"list-inline\">\n<li>\n<a href=\"" + getProjectName() + "/Store/receive?orderid=" + orderId + "\"><img\n" +
                "    src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\"></a></li>\n</ul>";
    }


    /**
     * Quality List Actions
     *
     * @param orderId
     * @return
     */

    public String getQualityPrepAction(String orderId) {

        return "   <ul class=\"list-inline\">\n<li>\n<a href=\"" + getProjectName() + "/Quality/qualityCheck?orderid=" + orderId + "\"><img\n" +
                "    src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\"></a></li>\n</ul>";
    }

    /**
     * Get Sale Challan List Actions
     *
     * @param challanId
     * @return HTML
     */

    public String getSaleDCAction(String challanId) {

        return "                                    <form id=\"challanUpdate" + challanId + "\" method=\"post\" action=\"" + getProjectName() + "/Challan/updateChallan\">\n" +
                "                                        <input type=\"hidden\" name=\"challanid\" value=\"" + challanId + "\"/>\n" +
                "                                        <input type=\"hidden\" name=\"redirectUrl\" value=\"/Challan/viewSaleChallanList\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "                                        <li>\n" +
                "                                            <a title=\"bill\" target=\"_blank\" href=\"" + getProjectName() + "/Challan/viewSaleChallan?challanid=" + challanId + "\"><img src=\"" + getProjectName() + "/resources/img/bill.png\" alt=\"\"></a>\n" +
                "                                        </li>\n" +
                "                                        <li>\n" +
                "                                            <a href=\"" + getProjectName() + "/Challan/updateChallan?challanid=" + challanId + "\"><img src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\"></a>\n" +
                "                                        </li>\n" +
                "                                    </ul>";
    }

    /**
     * Get Purchase Challan List Actions
     *
     * @param challanId
     * @return HTML
     */

    public String getPurchaseDCAction(String challanId) {

        return "<form id = \"challanUpdate" + challanId + "\" method=\"post\" action=\"" + getProjectName() + "/Challan/updateChallan\" >\n" +
                "<input  type=\"hidden\" name=\"challanid\" value=\"" + challanId + "\"/>\n" +
                "<input  type=\"hidden\" name=\"redirectUrl\" value=\"/Challan/viewChallanList\"/>\n" +
                "</form>\n" +
                "<ul class=\"list-inline\">\n" +
                "<li><a target=\"_blank\" href=\"" + getProjectName() + "/Challan/viewChallan?challanid=" + challanId + "\"><img src=\"" + getProjectName() + "/resources/img/viewIcon.png\" alt=\"\"></a>\n" +
                "</li>\n" +
                "</ul>";
    }

    /**
     * Get Reject Challan List Actions
     *
     * @param challanId
     * @return HTML
     */

    public String getChallanRejectAction(String challanId) {

        return "<form id = \"challanUpdate" + challanId + "\" method=\"post\" action=\"" + getProjectName() + "/Challan/updateChallan\" >\n" +
                "<input  type=\"hidden\" name=\"challanid\" value=\"" + challanId + "\"/>\n" +
                "<input  type=\"hidden\" name=\"redirectUrl\" value=\"/Challan/viewChallanList\"/>\n" +
                "</form>\n" +
                "<ul class=\"list-inline\">\n" +
                "<li><a target=\"_blank\" href=\"" + getProjectName() + "/Reject/rejectChallan?challanid=" + challanId + "\"><img src=\"" + getProjectName() + "/resources/img/viewIcon.png\" alt=\"\"></a>\n" +
                "</li>\n" +
                "</ul>";
    }

    /**
     * Get raw material list Actions
     *
     * @param accountCode
     * @return
     */
    public String getRawMaterialAction(String accountCode) {

        return "                                    <form method=\"post\" id=\"stockRemove" + accountCode + "\" action=\"" + getProjectName() + "/RawMaterialStock/removeRawMaterial\" />\n" +
                "                                        <input type=\"hidden\" name=\"rawId\"\n" +
                "                                               value=\"" + accountCode + "\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "\n" +
                "                                    <li><a title=\"Edit\" href=\"#\" onclick=\"edit(" + accountCode + ")\" class=\"editForm\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                        <li><a href=\"#\" onclick=\"remove(" + accountCode + ");\"><img\n" +
                "                                                src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                    </ul>";
    }

    /**
     * Get petty cash voucher list Actions
     *
     * @param voucherNumber
     * @param index
     * @return
     */
    public String getPettyCashVoucherAction(String voucherNumber, String index) {

        return "                                            <form method=\"post\" id=\"voucherRemove" + index + "\" action=\"" + getProjectName() + "/Account/deleteVoucher\">\n" +

                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\"" + voucherNumber + "\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Account/viewPettyCashVouchers\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Edit\" href=\"" + getProjectName() + "/Account/updatePettyCashVoucher?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Print\" target=\"_blank\" href=\"" + getProjectName() + "/Account/printPaymentPettyCash?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/print.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                "                                            </ul>";
    }

    /**
     * Get purchase petty cash voucher list Actions
     *
     * @param voucherNumber
     * @param index
     * @return
     */
    public String getPurchasePettyCashVoucherAction(String voucherNumber, String index) {

        return "                                            <form method=\"post\" id=\"voucherRemove" + index + "\" action=\"" + getProjectName() + "/Account/deleteVoucher\">\n" +

                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\"" + voucherNumber + "\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Account/viewPurchasePettyCashVouchers\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Edit\" href=\"" + getProjectName() + "/Account/updatePurchasePettyCashVoucher?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Print\" target=\"_blank\" href=\"" + getProjectName() + "/Account/printPurchasePettyCash?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/print.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                "                                            </ul>";
    }

    /**
     * Get Journal voucher list Actions
     *
     * @param voucherNumber
     * @param index
     * @return
     */
    public String getJournalVoucherAction(String voucherNumber, String index) {

        return "                                        <td>\n" +
                "                                      <form method=\"post\" id=\"voucherRemove" + index + "\"\n" +
                "                                                  action=\"" + getProjectName() + "/Account/deleteVoucher\">\n" +
                "                                                <input type=\"hidden\" name=\"voucherNumber\" value=\"" + voucherNumber + "\"/>\n" +
                "                                                <input type=\"hidden\" name=\"redirectUrl\" value=\"Account/viewJournalVoucher\"/>\n" +
                "                                            </form>\n" +
                "                                            <ul class=\"list-inline\">\n" +
                "                                                <li><a title=\"Delete\" href=\"#\" onclick=\"remove(" + index + ");\"><img src=\"" + getProjectName() + "/resources/img/deleteIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Edit\" href=\"" + getProjectName() + "/Account/editJournalVoucher?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                                <li><a title=\"Print\" target=\"_blank\" href=\"" + getProjectName() + "/Account/printJournalVoucher?voucherNumber=" + voucherNumber + "\"><img src=\"" + getProjectName() + "/resources/img/print.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                "                                            </ul>\n" +
                "                                        </td>";
    }

    /**
     * Stock Customer Listing Action HTML Provider
     *
     * @param accountCode
     * @return
     */
    public String getStockCustomerAction(String accountCode) {

        return "                                    <form method=\"post\" id=\"customerRemove" + accountCode + "\" action=\"" + getProjectName() + "/Customer/removeCustomer\" />\n" +
                "                                        <input type=\"hidden\" name=\"accountCode\"\n" +
                "                                               value=\"" + accountCode + "\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "\n" +
                "                                    <li><a title=\"Edit\" href=\"#\" onclick=\"edit(" + accountCode + ")\" class=\"editForm\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
               "                                        <li><a href=\"#\" onclick=\"remove(" + accountCode + ");\"><img\n" +
                "                                                src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                    </ul>";
    }

    /**
     * Stock Vendor Listing Action HTML Provider
     *
     * @param accountCode
     * @return
     */
    public String getStockVendorAction(String accountCode) {

        return "                                    <form method=\"post\" id=\"vendorRemove" + accountCode + "\" action=\"" + getProjectName() + "/StockVendor/removeVendor\" />\n" +
                "                                        <input type=\"hidden\" name=\"accountCode\"\n" +
                "                                               value=\"" + accountCode + "\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "\n" +
                "                                    <li><a title=\"Edit\" href=\"#\" onclick=\"edit(" + accountCode + ")\" class=\"editForm\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                        <li><a href=\"#\" onclick=\"remove(" + accountCode + ");\"><img\n" +
                "                                                src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                    </ul>";
    }

    /**
     * Get GIN Listing Action HTML Provider
     *
     * @param ginId
     * @return
     */
    public String getGinAction(String ginId) {

        return "                                    <form method=\"post\" id=\"ginRemove" + ginId + "\" action=\"" + getProjectName() + "/Gin/removeGinRequest\" />\n" +
                "                                        <input type=\"hidden\" name=\"ginId\"\n" +
                "                                               value=\"" + ginId + "\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "\n" +
                "                                    <li><a title=\"Edit\" href=\"" + getProjectName() + "/Gin/updateGinRequest?ginId=" + ginId + "\" class=\"editForm\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                    <li><a title=\"Print\" target=\"_blank\" href=\"" + getProjectName() + "/Gin/printGinRequest?requestId=" + ginId + "\"><img src=\"" + getProjectName() + "/resources/img/viewIcon.png\" alt=\"\" width=\"20\" height=\"20\"></a></li>\n" +
                "                                        <li><a href=\"#\" onclick=\"remove(" + ginId + ");\"><img\n" +
                "                                                src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +
                "                                    </ul>";
    }

    /**
     * Store Rejection List Actions
     *
     * @param orderId
     * @return
     */

    public String getStoreProductRejectionAction(String orderId) {

        return "   <ul class=\"list-inline\">\n<li>\n<a href=\"" + getProjectName() + "/Store/reject?orderid=" + orderId + "\"><img\n" +
                "    src=\"" + getProjectName() + "/resources/img/edit.png\" alt=\"\"></a></li>\n</ul>";
    }


    public String getEmployeeAction(String employeeId) {
        return "                                    <ul class=\"list-inline\">\n" +
                "                                    <li><a title=\"View\" href=\"" + getProjectName() + "/PayRoll/employeeDetail?employeeid=" + employeeId + "\" class=\"editForm\"><img src=\"" + getProjectName() + "/resources/img/viewIcon.png\" alt=\"\"></a></li>\n" +
                "                                    <li><a title=\"Edit\" href=\"#\" onclick=\"edit(" + employeeId + ");\"><img\n" +
                "                                                src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                          </ul>";
    }

    /**
     * Recipe Listing Action HTML Provider
     *
     * @param accountCode
     * @return
     */
    public String getRecipeAction(String accountCode) {

        return "                                    <form method=\"post\" id=\"recipeRemove" + accountCode + "\" action=\"" + getProjectName() + "/Recipe/removeRecipe\" />\n" +
                "                                        <input type=\"hidden\" name=\"recipeId\"\n" +
                "                                               value=\"" + accountCode + "\"/>\n" +
                "                                    </form>\n" +
                "                                    <ul class=\"list-inline\">\n" +
                "\n" +
                "                                    <li><a title=\"Edit\" href=\"#\" onclick=\"edit(" + accountCode + ")\" class=\"editForm\"><img src=\"" + getProjectName() + "/resources/img/editIcon.png\" alt=\"\"></a></li>\n" +
                "                                        <li><a href=\"#\" onclick=\"remove(" + accountCode + ");\"><img\n" +
                "                                                src=\"" + getProjectName() + "/resources/img/remove.png\" alt=\"\"></a></li>\n" +

                "                                    </ul>";
    }

}
