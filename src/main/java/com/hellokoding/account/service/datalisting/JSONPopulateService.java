package com.hellokoding.account.service.datalisting;

import com.hellokoding.account.model.*;
import com.hellokoding.account.model.jsonentity.JSONDataModel;
import com.hellokoding.account.service.chassis.ChassisService;
import com.hellokoding.account.service.distributor.DistributorService;
import com.hellokoding.account.service.stock.StockService;
import com.hellokoding.account.service.subDepartment.SubDepartmentService;
import com.hellokoding.account.service.voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.lang.Math.toIntExact;

@Service
public class JSONPopulateService {


    @Autowired
    private ViewActionBuilder viewActionBuilder;

    @Autowired
    private AccountService accountService;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private SubDepartmentService subDepartmentService;

    @Autowired
    private ChassisService chassisService;

    @Autowired
    private DistributorService distributorService;

    @Autowired
    private StockService stockService;

    /**
     * Map for Account Titles in voucher
     * Used in Voucher data listing
     * For optimization and single db hit per page
     *
     * @param voucherList List of vouchers
     * @return Map key -> accountCode, value -> title
     */
    public Map<Integer, String> getAccountTitleMap(List<Voucher> voucherList) {

        List<Integer> requestAccountCodeList = new ArrayList<>();
        for (Voucher voucher : voucherList) {
            // CASE BANK ACCOUNT CODE
            if (voucher.getBankAccount() != 0) {
                if (!requestAccountCodeList.contains(voucher.getBankAccount()))
                    requestAccountCodeList.add(voucher.getBankAccount());
            }
            // CASE ACCOUNT CODE
            if (voucher.getAccountCode() != null) {
                if (voucher.getAccountCode() != 0) {
                    if (!requestAccountCodeList.contains(voucher.getAccountCode()))
                        requestAccountCodeList.add(voucher.getAccountCode());
                }
            }
            // CASE ITEM ACCOUNT CODE
            // STOCK ITEMS HAVE CODE > 999999999
            if (voucher.getItemAccount() != null) {
                if (voucher.getItemAccount() > 99999999) {
                    if (!requestAccountCodeList.contains(voucher.getItemAccount()))
                        requestAccountCodeList.add(voucher.getItemAccount());
                }
            }
            // CASE TAX ACCOUNT CODE
            if (voucher.getTaxAccount() != null) {
                if (voucher.getTaxAccount() != 0) {
                    if (!requestAccountCodeList.contains(voucher.getTaxAccount()))
                        requestAccountCodeList.add(voucher.getTaxAccount());
                }
            }
        }

        return accountService.getTitleMap(requestAccountCodeList);
    }


    /**
     * Account List Populate
     *
     * @param accountPage
     * @param draw
     * @return JSONDataModel
     */
    public JSONDataModel populateAccountList(Page<Account> accountPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(accountPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(accountPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        for (Account account : accountPage.getContent()) {
            Account parentAccount = account.getParentAccount();
            String parentAccountTitle = "-";
            if (parentAccount != null) {
                parentAccountTitle = parentAccount.getTitle();
            }
            dataString.add(new String[]{Integer.toString(account.getAccountCode())
                    , df.format(account.getOpeningDate())
                    , account.getTitle()
                    , Double.toString(account.getOpeningDebit() + account.getOpeningCredit())
                    , parentAccountTitle,
                    viewActionBuilder.getAccountAction(Integer.toString(account.getAccountCode()))
            });
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    /**
     * Stock Listing Populate
     *
     * @param stockPage
     * @param draw
     * @return
     */
    public JSONDataModel populateStockList(Page<Stock> stockPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(stockPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(stockPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();

        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setGroupingSize(3);

        for (Stock stock : stockPage.getContent()) {

            dataString.add(new String[]{
                    stock.getName()
                    , stock.getDescription()
                    , stock.getStockCategory().getTitle()
                    , decimalFormat.format(stock.getSellingPrice())
                    /*, stock.getUnitMeasure()
                    , Double.toString(stock.getOpeningQuantity())*/
                    , viewActionBuilder.getStockAction(Integer.toString(stock.getStockAccount().getAccountCode()), Integer.toString(stock.getStockId()))

            });
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    /**
     * Sale voucher Listing Populate
     *
     * @param voucherPage
     * @param draw
     * @return
     */
    public JSONDataModel populateSaleVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Map<Integer, String> titleMap = getAccountTitleMap(voucherPage.getContent());
        for (Voucher voucher : voucherPage.getContent()) {

            String secondaryAccount = "";
            String itemName = "";
            String distributorName = "N/A";
            if (voucher.getCredit() > voucher.getDebit()) {
                secondaryAccount = voucherService.getSecondaryName(voucher.getVoucherNumber(), true);
            }
            if (voucher.getItemAccount() > 0) {
                itemName = titleMap.get(voucher.getItemAccount());
            }
            if (voucher.getCredit() > 0) {

                NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
                decimalFormat.setGroupingUsed(true);
                decimalFormat.setGroupingSize(3);
                Apartment apartment = chassisService.findByChassisId(voucher.getChassisId());

                Distributor distributor = distributorService.findByDistributorId(voucher.getSalesmanId());
                if(distributor != null){
                    distributorName = distributor.getName();
                }
                dataString.add(new String[]{
                        voucher.getVoucherNumber()
                        , df.format(voucher.getVoucherDate())
                        , secondaryAccount
                        , itemName
                        , apartment.getApartmentNo()
                        , decimalFormat.format(voucher.getItemRate())
                        , distributorName
                        , decimalFormat.format(voucher.getCredit())
                        , viewActionBuilder.getSaleVoucherAction(voucher.getVoucherNumber(), Integer.toString(voucher.getVoucherId()), voucher.getChassisNo())

                });
            }
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }


    /**
     * Purchase Returen voucher Listing Populate
     *
     * @param voucherPage
     * @param draw
     * @return
     */
    public JSONDataModel populateReturnVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Map<Integer, String> titleMap = getAccountTitleMap(voucherPage.getContent());

        for (Voucher voucher : voucherPage.getContent()) {

            String secondaryAccount = "";
            String itemName = "";
            if (voucher.getCredit() > voucher.getDebit()) {
                secondaryAccount = voucherService.getSecondaryName(voucher.getVoucherNumber(), true);
            }
            if (voucher.getItemAccount() > 0) {
                itemName = titleMap.get(voucher.getItemAccount());
            }
            if (voucher.getCredit() > 0) {

                // Refactor why null values generated
                double itemQuantity = 0.0;
                double itemRate = 0.0;
                if (voucher.getItemQuantity() != null) {
                    itemQuantity = voucher.getItemQuantity();
                }
                if (voucher.getItemRate() != null) {
                    itemRate = voucher.getItemRate();
                }
                NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
                decimalFormat.setGroupingUsed(true);
                decimalFormat.setGroupingSize(3);
                dataString.add(new String[]{
                        voucher.getVoucherNumber()
                        , df.format(voucher.getVoucherDate())
                        , secondaryAccount
                        , itemName
                        , Double.toString(itemQuantity)
                        , Double.toString(itemRate)
                        , decimalFormat.format(voucher.getCredit())
                        , viewActionBuilder.getReturnVoucherAction(voucher.getVoucherNumber(), Integer.toString(voucher.getVoucherId()))

                });
            }
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    /**
     * Purchase voucher Populate
     *
     * @param voucherPage
     * @param draw
     * @return
     */
    public JSONDataModel populatePurchaseVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        for (Voucher voucher : voucherPage.getContent()) {

            String secondaryAccount = "";
            String itemName = "";
            if (voucher.getCredit() < voucher.getDebit()) {
                secondaryAccount = voucherService.getSecondaryName(voucher.getVoucherNumber(), false);
            }
            if (voucher.getItemAccount() > 0) {
                itemName = accountService.getAccountTitle(voucher.getItemAccount());
            }
            if (voucher.getDebit() > 0) {

                // Refactor why null values generated
                double itemQuantity = 0.0;
                double itemRate = 0.0;
                if (voucher.getItemQuantity() != null) {
                    itemQuantity = voucher.getItemQuantity();
                }
                if (voucher.getItemRate() != null) {
                    itemRate = voucher.getItemRate();
                }

                NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
                decimalFormat.setGroupingUsed(true);
                decimalFormat.setGroupingSize(3);
                Apartment apartment = chassisService.findByChassisId(voucher.getChassisId());
                dataString.add(new String[]{
                        voucher.getVoucherNumber()
                        , df.format(voucher.getVoucherDate())
                        , secondaryAccount
                        , itemName
                        , apartment.getApartmentNo()
                        , "-"//apartment.getEngineNo()
                        , Double.toString(itemRate)
                        , decimalFormat.format(voucher.getDebit())
                        , viewActionBuilder.getPurchaseVoucherAction(voucher.getVoucherNumber(), Integer.toString(voucher.getVoucherId()))

                });
            }
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    /**
     * Payment voucher Listing Populate
     *
     * @param voucherPage
     * @param draw
     * @return
     */
    public JSONDataModel populatePaymentVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Map<Integer, String> titleMap = getAccountTitleMap(voucherPage.getContent());

        for (Voucher voucher : voucherPage.getContent()) {
            String taxAccount = "";
            String costCentre = "";
            if (voucher.getTaxAccount() != null) {
                taxAccount = titleMap.get(voucher.getTaxAccount());
            }
            if (voucher.getCostCentre() != null) {
                costCentre = Long.toString(voucher.getCostCentre());
            }
            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
            decimalFormat.setGroupingUsed(true);
            decimalFormat.setGroupingSize(3);

            dataString.add(new String[]{
                    voucher.getVoucherNumber() + "</br>" + df.format(voucher.getVoucherDate())
                    , titleMap.get(voucher.getBankAccount()) + "</br>" + voucher.getChequeNumber()
                    , titleMap.get(voucher.getAccountCode()) + "</br>" + costCentre
                    , taxAccount + "</br>" + voucher.getRemarks()
                    , decimalFormat.format(voucher.getBillAmount()) + "</br>" + decimalFormat.format(voucher.getTaxAmount())
                    , decimalFormat.format(voucher.getDebit())
                    , viewActionBuilder.getPaymentVoucherAction(voucher.getVoucherNumber(), Integer.toString(voucher.getVoucherId()))

            });
        }


        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }


    /**
     * Receipt voucher Listing Populate
     *
     * @param voucherPage
     * @param draw
     * @return
     */
    public JSONDataModel populateReceiptVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Map<Integer, String> titleMap = getAccountTitleMap(voucherPage.getContent());

        for (Voucher voucher : voucherPage.getContent()) {
            String taxAccount = "";
            String costCentre = "";
            if (voucher.getTaxAccount() != null) {
                taxAccount = titleMap.get(voucher.getTaxAccount());
            }
            if (voucher.getCostCentre() != null) {
                costCentre = Long.toString(voucher.getCostCentre());
            }
            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
            decimalFormat.setGroupingUsed(true);
            decimalFormat.setGroupingSize(3);

            dataString.add(new String[]{
                    voucher.getVoucherNumber() + "</br>" + df.format(voucher.getVoucherDate())
                    , titleMap.get(voucher.getBankAccount()) + "</br>" + voucher.getChequeNumber()
                    , titleMap.get(voucher.getAccountCode()) + "</br>" + costCentre
                    , taxAccount + "</br>" + voucher.getRemarks()
                    , decimalFormat.format(voucher.getBillAmount()) + "</br>" + decimalFormat.format(voucher.getTaxAmount())
                    , decimalFormat.format(voucher.getCredit())
                    , viewActionBuilder.getReceiptVoucherAction(voucher.getVoucherNumber(), Integer.toString(voucher.getVoucherId()))

            });
        }


        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }


    /**
     * Json populate for petty cash vouchers
     *
     * @param voucherPage
     * @param draw
     * @return
     */
    public JSONDataModel populatePettyCashVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Map<Integer, String> titleMap = getAccountTitleMap(voucherPage.getContent());

        for (Voucher voucher : voucherPage.getContent()) {
            if (voucher.getCredit() > 0) {

                dataString.add(new String[]{
                        voucher.getVoucherNumber()
                        , df.format(voucher.getVoucherDate())
                        , titleMap.get(voucher.getAccountCode())
                        , new BigDecimal(voucher.getCredit()).setScale(2, BigDecimal.ROUND_UP).toString()
                        , viewActionBuilder.getPettyCashVoucherAction(voucher.getVoucherNumber(), Integer.toString(voucher.getVoucherId()))

                });
            }
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    /**
     * Json populate for purchase petty cash vouchers
     *
     * @param voucherPage
     * @param draw
     * @return
     */
    public JSONDataModel populatePurchasePettyCashVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Map<Integer, String> titleMap = getAccountTitleMap(voucherPage.getContent());

        for (Voucher voucher : voucherPage.getContent()) {
            if (voucher.getCredit() > 0) {

                dataString.add(new String[]{
                        voucher.getVoucherNumber()
                        , df.format(voucher.getVoucherDate())
                        , titleMap.get(voucher.getAccountCode())
                        , new BigDecimal(voucher.getCredit()).setScale(2, BigDecimal.ROUND_UP).toString()
                        , viewActionBuilder.getPurchasePettyCashVoucherAction(voucher.getVoucherNumber(), Integer.toString(voucher.getVoucherId()))

                });
            }
        }

        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    /**
     * JSON populate for Journal Vouchers
     *
     * @param voucherPage
     * @param draw
     * @return
     */
    public JSONDataModel populateJournalVoucherList(Page<Voucher> voucherPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(voucherPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(voucherPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Map<Integer, String> titleMap = getAccountTitleMap(voucherPage.getContent());
        Map<Integer, String> categoryMap = stockService.categoryMap();
        for (Voucher voucher : voucherPage.getContent()) {
            String debitAccountTitle = "";
            String creditAccountTitle = "";
            if (voucher.getCredit() > 0) {
                creditAccountTitle = titleMap.get(voucher.getAccountCode());
            } else {
                debitAccountTitle = titleMap.get(voucher.getAccountCode());
            }

            dataString.add(new String[]{
                    voucher.getVoucherNumber()
                    , df.format(voucher.getVoucherDate())
                    , categoryMap.get(voucher.getProjectCode())
                    , debitAccountTitle
                    , creditAccountTitle
                    , new BigDecimal(voucher.getCredit() + voucher.getDebit()).setScale(2, BigDecimal.ROUND_UP).toString()
                    , viewActionBuilder.getJournalVoucherAction(voucher.getVoucherNumber(), Integer.toString(voucher.getVoucherId()))

            });

        }

        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    /**
     * Stock Customer Listing Populate
     *
     * @param stockCustomerPage
     * @param draw
     * @return
     */
    public JSONDataModel populateStockCustomerList(Page<StockCustomer> stockCustomerPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(stockCustomerPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(stockCustomerPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();

        for (StockCustomer stockCustomer : stockCustomerPage.getContent()) {

            dataString.add(new String[]{
                    stockCustomer.getCustomerAccount().getTitle()
                    , stockCustomer.getAddress()
                    , stockCustomer.getMobile()
                    , stockCustomer.getCnic()
                    , stockCustomer.getLandline()
                    , stockCustomer.getSaletaxNumber()
                    , viewActionBuilder.getStockCustomerAction(Integer.toString(stockCustomer.getCustomerAccount().getAccountCode()))

            });
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }

    /**
     * Stock Vendor Listing Populate
     *
     * @param stockVendorPage
     * @param draw
     * @return
     */
    public JSONDataModel populateStockVendorList(Page<StockVendor> stockVendorPage, int draw) {

        JSONDataModel JSONDataModel = new JSONDataModel();
        JSONDataModel.setDraw(draw);
        JSONDataModel.setRecordsTotal(toIntExact(stockVendorPage.getTotalElements()));
        JSONDataModel.setRecordsFiltered(toIntExact(stockVendorPage.getTotalElements()));
        List<String[]> dataString = new ArrayList<>();

        for (StockVendor stockVendor : stockVendorPage.getContent()) {

            dataString.add(new String[]{
                    stockVendor.getVendorAccount().getTitle()
                    , stockVendor.getAddress()
                    , stockVendor.getMobile()
                    , stockVendor.getCnic()
                    , stockVendor.getNationalTaxNumber()
                    , stockVendor.getLandline()
                    , stockVendor.getSaletaxNumber()
                    , viewActionBuilder.getStockVendorAction(Integer.toString(stockVendor.getVendorAccount().getAccountCode()))

            });
        }
        JSONDataModel.setData(dataString);

        return JSONDataModel;
    }


}

