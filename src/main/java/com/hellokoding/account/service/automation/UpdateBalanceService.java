package com.hellokoding.account.service.automation;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.reportModel.TrialBalanceSet;
import com.hellokoding.account.service.voucher.VoucherService;
import com.hellokoding.account.service.account.AccountService;
import com.hellokoding.account.service.ledger.TrialBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class UpdateBalanceService {

    @Autowired
    private TrialBalanceService trialBalanceService;
    @Autowired
    private VoucherService voucherService;
    @Autowired
    private AccountService accountService;


    /**
     * Calculate Ledger balance for every account
     * Used for optimization(update once) and setting limits to different sales
     */
    public void calculateDailyBalance() {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        for (Account account : accountService.findAccountsByLevel(4)) {
            List<TrialBalanceSet> trailBalanceCustomerList = trialBalanceService.getTrailBalanceBasic(0, date, account,null);
            if (trailBalanceCustomerList.size() > 0) {
                if (trailBalanceCustomerList.get(0).getDebit() > 0) {
                    account.setLedgerBalance(trailBalanceCustomerList.get(0).getDebit());
                } else {
                    account.setLedgerBalance(trailBalanceCustomerList.get(0).getCredit());
                }
                accountService.update(account);
            }
        }
    }

    /**
     * Calculate Ledger balance of account
     * @param account Target Account
     * @return Account with total Balance Amount Set
     */
    public Account calculateLedgerBalance(Account account) {
        Double totalAmount;
        Double ledgerBalance;
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
                ledgerBalance = account.getLedgerBalance();
                List<Voucher> voucherList = voucherService.getVoucherByCurrentDate(date, date, account.getAccountCode());
                totalAmount = calculateBalanceAmount(voucherList, ledgerBalance);
                account.setTotalBalanceAmount(totalAmount);

        return account;
    }


    /**
     * Calculate Balance based on List
     * @param allVouchers Voucher List of target Account
     * @param ledgerBalance ledger Balance Amount
     * @return balance Amount
     */
    private Double calculateBalanceAmount(List<Voucher> allVouchers, Double ledgerBalance) {

        for (Voucher voucher : allVouchers) {
            ledgerBalance -= voucher.getCredit();
            ledgerBalance += voucher.getDebit();
        }
        return ledgerBalance;
    }


}
