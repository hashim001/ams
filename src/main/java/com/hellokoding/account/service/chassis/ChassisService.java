package com.hellokoding.account.service.chassis;

import com.hellokoding.account.model.Apartment;
import com.hellokoding.account.model.ApartmentHold;
import com.hellokoding.account.model.Stock;

import java.util.List;

public interface ChassisService {
    void save(Apartment apartment);

    Apartment findByChassisId(int chassisId);

    Apartment findByChassisNumber(String chassisNumber);

    List<Apartment> findByCustomerAccount(int customerAccount);

    List<Apartment> findByCustomerAccountAndDistributorId(int customerAccount, int distributorId);

    List<Apartment> getChassisForSpecificCustomer(int customerAccount, int distributorId, Stock stock);

    List<Apartment> findByDistributorId(int distributorId);

    List<Apartment> findAll();

    List<Apartment> findSoldApartment();

    List<Apartment> findAllByChassisDate();

    List<Apartment> findAllByDistributorNotNull();

    void remove(int chassisId);

    ApartmentHold findByApartment(Apartment apartment);

    void saveApartmentHold(ApartmentHold apartmentHold);

    /**
     * Get List of Distinct Floors from Apartment
     *
     * @return
     */
    List<String> getFloorList();

    List<ApartmentHold> findAllHoldApartments();

    List<ApartmentHold> findHoldApartmentBySalesmanId(int salesmanId);

    List<Apartment> findAllAvailableApartments();

    List<Apartment> findByCommissionStatus(String commissionStatus);

    List<Apartment> findAllAvailableApartmentsByStock(Stock stock);

    void removeHoldApartment(ApartmentHold apartmentHold);

    void updateCommissionStatus(String apartmentNumber, String commissionStatus);
}
