package com.hellokoding.account.service.chassis;

import com.hellokoding.account.model.Apartment;
import com.hellokoding.account.model.ApartmentHold;
import com.hellokoding.account.model.Stock;
import com.hellokoding.account.repository.ApartmentHoldRepository;
import com.hellokoding.account.repository.ApartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChassisServiceImpl implements ChassisService {
    @Autowired
    private ApartmentRepository apartmentRepository;
    @Autowired
    private ApartmentHoldRepository apartmentHoldRepository;

    @Override
    public void save(Apartment apartment) {
        apartmentRepository.save(apartment);
    }

    @Override
    public Apartment findByChassisId(int chassisId) {
        return apartmentRepository.findByApartmentId(chassisId);
    }

    @Override
    public List<Apartment> findByDistributorId(int distributorId) {
        return apartmentRepository.findByDistributorId(distributorId);
    }

    @Override
    public List<Apartment> findAll() {
        return apartmentRepository.findAll();
    }

    @Override
    public List<Apartment> findSoldApartment() {
        return apartmentRepository.findByIsSold(true);
    }

    @Override
    public List<Apartment> findAllByChassisDate() {
        return apartmentRepository.findAllByDate();
    }

    @Override
    public void remove(int chassisId) {
        apartmentRepository.removeApartment(chassisId);
    }

    @Override
    public Apartment findByChassisNumber(String chassisNumber) {
        return apartmentRepository.findByApartmentNo(chassisNumber);
    }

    @Override
    public List<Apartment> findByCustomerAccount(int customerAccount) {
        return apartmentRepository.findByCustomerAccount(customerAccount);
    }

    @Override
    public List<Apartment> findByCustomerAccountAndDistributorId(int customerAccount, int distributorId) {
        return apartmentRepository.findByCustomerAccountAndDistributorId(customerAccount, distributorId);
    }

    @Override
    public List<Apartment> getChassisForSpecificCustomer(int customerAccount, int distributorId, Stock stock) {
        return apartmentRepository.findByCustomerAccountAndDistributorIdAndStock(customerAccount, distributorId, stock);
    }

    @Override
    public List<Apartment> findAllByDistributorNotNull() {
        return apartmentRepository.findByDistributorIdNot(0);
    }

    @Override
    public ApartmentHold findByApartment(Apartment apartment) {
        return apartmentHoldRepository.findByApartment(apartment);
    }

    @Override
    public void saveApartmentHold(ApartmentHold apartmentHold) {
        apartmentHoldRepository.save(apartmentHold);
    }

    @Override
    public List<String> getFloorList() {
        return apartmentRepository.getFloorList();
    }

    @Override
    public List<ApartmentHold> findAllHoldApartments() {
        return apartmentHoldRepository.findAll();
    }

    @Override
    public List<ApartmentHold> findHoldApartmentBySalesmanId(int salesmanId) {
        return apartmentHoldRepository.findBySalesmanId(salesmanId);
    }

    @Override
    public List<Apartment> findAllAvailableApartments() {
        return apartmentRepository.findAllAvailableApartments();
    }

    @Override
    public List<Apartment> findByCommissionStatus(String commissionStatus) {
        return apartmentRepository.findByCommissionStatus(commissionStatus);
    }

    @Override
    public List<Apartment> findAllAvailableApartmentsByStock(Stock stock) {
        return apartmentRepository.findAllAvailableApartmentsByStock(stock);
    }

    @Override
    public void removeHoldApartment(ApartmentHold apartmentHold) {
        apartmentHoldRepository.delete(apartmentHold);
    }

    public void updateCommissionStatus(String apartmentNumber , String commissionStatus){
        apartmentRepository.changeCommissionStatus(apartmentNumber, commissionStatus);
    }
}
