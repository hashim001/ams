package com.hellokoding.account.service.note;


import com.hellokoding.account.model.AccountNote;
import com.hellokoding.account.model.NoteSubHeading;
import com.hellokoding.account.model.ReportStaging;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface NoteService {

    void saveNote(AccountNote accountNote);
    void saveNoteSubHead(NoteSubHeading noteSubHeading);
    AccountNote findByNoteId(int noteId);
    NoteSubHeading findBySubId(Integer subId);
    List<NoteSubHeading> findAllSubHead();
    List<AccountNote> findAll();
    List<AccountNote> findAllByReportStaging(ReportStaging reportStaging);
    List<AccountNote> findByNoteType(String noteType, Integer status);
    void removeNote (AccountNote accountNote);
    void removeNoteSubHead (NoteSubHeading noteSubHeading);
 }
