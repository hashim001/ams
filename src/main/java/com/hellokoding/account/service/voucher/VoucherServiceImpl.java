package com.hellokoding.account.service.voucher;


import com.hellokoding.account.model.Apartment;
import com.hellokoding.account.model.StaticInfo;
import com.hellokoding.account.model.Voucher;
import com.hellokoding.account.repository.BankDetailRepository;
import com.hellokoding.account.repository.ApartmentRepository;
import com.hellokoding.account.repository.VoucherRepository;
import com.hellokoding.account.service.ledger.LedgerCalculationService;
import com.hellokoding.account.service.staticInfo.StaticInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
public class VoucherServiceImpl implements VoucherService {

    @Autowired
    private VoucherRepository voucherRepository;

    @Autowired
    private BankDetailRepository bankDetailRepository;

    @Autowired
    private ApartmentRepository apartmentRepository;

    @Autowired
    private StaticInfoService staticInfoService;

    public Voucher savePayment(Voucher voucher) {

        // insertion of

        voucher.setIsActive(1);
        voucher.setDebit(LedgerCalculationService.round(voucher.getDebit(), 2));
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
/*        if (voucher.getChassisId() > 0) {
            Apartment apartment = apartmentRepository.findByApartmentId(voucher.getChassisId());
            if (!voucher.getAccountCode().equals(staticInfo.getCompanyPaymentAccount())) {
                apartment.setAmountPaid(apartment.getAmountPaid() - voucher.getBillAmount());
            }
            voucher.setRemarks(apartment.getStock().getName() + " - " + apartment.getApartmentNo() + " " + voucher.getRemarks());
            voucher.setChassisNo(apartment.getApartmentNo());
        }*/
        String jtVoucherNumber = "";
        List<Voucher> voucherList = voucherRepository.findByVoucherNumber(voucher.getVoucherNumber());
        if (voucherList.size() > 0) {
            Optional<Voucher> voucherForChassis = voucherList.stream().filter(e -> e.getChassisId() > 0).findAny();
            if (voucherForChassis.isPresent()) {
                if (!voucherForChassis.get().getAccountCode().equals(staticInfo.getCompanyPaymentAccount())) {
                    Apartment apartment = apartmentRepository.findByApartmentId(voucherForChassis.get().getChassisId());
                    apartment.setAmountPaid(apartment.getAmountPaid() + voucherForChassis.get().getBillAmount());
                }
            }
        }

        for (Voucher jtVoucher : voucherRepository.findByReferenceVoucher(voucher.getVoucherNumber())) {
            voucherList.add(jtVoucher);
            jtVoucherNumber = jtVoucher.getVoucherNumber();
        }
        if (voucherList.size() > 0) {
            voucherRepository.delete(voucherList);
        }

        Voucher baseVoucher = voucherRepository.save(voucher);

        //insertion of Bank Credit voucher
        Voucher bankVoucher = new Voucher();
        bankVoucher.setVoucherNumber(voucher.getVoucherNumber());
        if (bankDetailRepository.isBank(voucher.getAccountCode()) != 0) {
            bankVoucher.setBankAccount(voucher.getAccountCode());
        }
        bankVoucher.setVoucherDate(voucher.getVoucherDate());
        bankVoucher.setAccountCode(voucher.getBankAccount());
        bankVoucher.setChassisId(voucher.getChassisId());
        bankVoucher.setChassisNo(voucher.getChassisNo());
        bankVoucher.setCredit(LedgerCalculationService.round(voucher.getDebit(), 2));
        bankVoucher.setIsActive(1);
        if (bankDetailRepository.isBank(voucher.getAccountCode()) != 0) {
            bankVoucher.setBankAccount(voucher.getAccountCode());
        }
        bankVoucher.setRemarks(baseVoucher.getRemarks());
        voucherRepository.save(bankVoucher);

        // if tax exists
        if (voucher.getTaxAmount() > 0) {
            String innerVoucherNumber = "";
            int maxVoucherID = 0;
            if (voucherRepository.findMaxIdForChild() == null) {
                maxVoucherID++;
                innerVoucherNumber = Integer.toString(maxVoucherID);
            } else {
                maxVoucherID = Integer.parseInt(voucherRepository.findMaxIdForChild().replaceAll("[^0-9]+", ""));
                maxVoucherID++;
                innerVoucherNumber = Integer.toString(maxVoucherID);
            }
            if (maxVoucherID <= 9) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 99) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 9999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 99999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 999999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }

            innerVoucherNumber = "JT" + innerVoucherNumber;

            //insertion of debit tax voucher for Vendor
            Voucher debitVoucher = new Voucher();
            Voucher creditVoucher = new Voucher();

            if (jtVoucherNumber.equals("")) {
                debitVoucher.setVoucherNumber(innerVoucherNumber);
                creditVoucher.setVoucherNumber(innerVoucherNumber);
            } else {
                debitVoucher.setVoucherNumber(jtVoucherNumber);
                creditVoucher.setVoucherNumber(jtVoucherNumber);
            }
            debitVoucher.setVoucherDate(voucher.getVoucherDate());
            debitVoucher.setAccountCode(voucher.getAccountCode());
            debitVoucher.setChassisId(voucher.getChassisId());
            debitVoucher.setChassisNo(voucher.getChassisNo());
            debitVoucher.setDebit(LedgerCalculationService.round(voucher.getTaxAmount(), 2));
            debitVoucher.setReferenceVoucher(voucher.getVoucherNumber());
            debitVoucher.setIsActive(1);

            //insertion of credit voucher for Tax Account

            creditVoucher.setVoucherDate(voucher.getVoucherDate());
            creditVoucher.setAccountCode(voucher.getTaxAccount());
            creditVoucher.setChassisId(voucher.getChassisId());
            creditVoucher.setChassisNo(voucher.getChassisNo());
            creditVoucher.setCredit(LedgerCalculationService.round(voucher.getTaxAmount(), 2));
            creditVoucher.setReferenceVoucher(voucher.getVoucherNumber());
            creditVoucher.setIsActive(1);

            debitVoucher.setRemarks(baseVoucher.getRemarks());
            creditVoucher.setRemarks(baseVoucher.getRemarks());
            voucherRepository.save(debitVoucher);
            voucherRepository.save(creditVoucher);

        }
        return baseVoucher;
    }

    public Integer getSecondaryAccount(List<Voucher> voucherList, String voucherNumber) {
        int accountCode = 0;
        for (Voucher voucher : voucherList) {
            if (voucher.getVoucherNumber().substring(0, 2).equalsIgnoreCase("SL")) {
                if (voucher.getDebit() > voucher.getCredit() && voucher.getVoucherNumber().equals(voucherNumber)) {
                    accountCode = voucher.getAccountCode();
                    break;
                }
            } else if (voucher.getVoucherNumber().substring(0, 2).equalsIgnoreCase("LP")) {
                if (voucher.getDebit() < voucher.getCredit() && voucher.getVoucherNumber().equals(voucherNumber)) {
                    accountCode = voucher.getAccountCode();
                    break;
                }
            }

        }
        return accountCode;

    }

    public Voucher saveReceipt(Voucher voucher) {

        voucher.setIsActive(1);
        voucher.setCredit(LedgerCalculationService.round(voucher.getCredit(), 2));
        StaticInfo staticInfo = staticInfoService.findStaticInfo();
        int apartId = 0;
        if (!voucher.getApartmentNo().equals("0")) {
            Apartment apartment = apartmentRepository.findByApartmentNo(voucher.getApartmentNo());
            if (!voucher.getAccountCode().equals(staticInfo.getCompanyPaymentAccount())) {
                apartment.setAmountPaid(apartment.getAmountPaid() + voucher.getBillAmount());

            }
            voucher.setRemarks(apartment.getStock().getName() + " - " + apartment.getApartmentNo() + " " + voucher.getRemarks());
            voucher.setChassisNo(apartment.getApartmentNo());
            apartId = apartment.getApartmentId();
        }
        String jtVoucherNumber = "";
        List<Voucher> voucherList = voucherRepository.findByVoucherNumber(voucher.getVoucherNumber());
        if (voucherList.size() > 0) {
            Optional<Voucher> voucherForChassis = voucherList.stream().filter(e -> e.getChassisId() > 0).findAny();
            if (voucherForChassis.isPresent()) {
                if (!voucherForChassis.get().getAccountCode().equals(staticInfo.getCompanyPaymentAccount())) {
                    Apartment apartment = apartmentRepository.findByApartmentId(voucherForChassis.get().getChassisId());
                    apartment.setAmountPaid(apartment.getAmountPaid() - voucherForChassis.get().getDebit());
                }
            }
        }

        for (Voucher jtVoucher : voucherRepository.findByReferenceVoucher(voucher.getVoucherNumber())) {
            voucherList.add(jtVoucher);
            jtVoucherNumber = jtVoucher.getVoucherNumber();
        }
        if (voucherList.size() > 0) {
            voucherRepository.delete(voucherList);
        }

        Voucher baseVoucher = voucherRepository.save(voucher);

        //insertion of Bank voucher
        Voucher bankVoucher = new Voucher();
        bankVoucher.setVoucherNumber(voucher.getVoucherNumber());
        bankVoucher.setVoucherDate(voucher.getVoucherDate());
        bankVoucher.setAccountCode(voucher.getBankAccount());
        bankVoucher.setChassisId(apartId);
        bankVoucher.setChassisNo(voucher.getChassisNo());
        bankVoucher.setDebit(LedgerCalculationService.round(voucher.getCredit(), 2));
        bankVoucher.setIsActive(1);
        if (bankDetailRepository.isBank(voucher.getAccountCode()) != 0) {
            bankVoucher.setBankAccount(voucher.getAccountCode());
        }
        bankVoucher.setRemarks(baseVoucher.getRemarks());
        voucherRepository.save(bankVoucher);

        // if tax exists
        if (voucher.getTaxAmount() > 0) {
            String innerVoucherNumber = "";
            int maxVoucherID = 0;
            if (voucherRepository.findMaxIdForChild() == null) {
                maxVoucherID++;
                innerVoucherNumber = Integer.toString(maxVoucherID);
            } else {
                maxVoucherID = Integer.parseInt(voucherRepository.findMaxIdForChild().replaceAll("[^0-9]+", ""));
                maxVoucherID++;
                innerVoucherNumber = Integer.toString(maxVoucherID);
            }
            if (maxVoucherID <= 9) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 99) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 9999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 99999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }
            if (maxVoucherID <= 999999) {
                innerVoucherNumber = "0" + innerVoucherNumber;
            }

            innerVoucherNumber = "JT" + innerVoucherNumber;
            Voucher debitVoucher = new Voucher();
            Voucher creditVoucher = new Voucher();
            if (jtVoucherNumber.equals("")) {
                debitVoucher.setVoucherNumber(innerVoucherNumber);
                creditVoucher.setVoucherNumber(innerVoucherNumber);
            } else {
                debitVoucher.setVoucherNumber(jtVoucherNumber);
                creditVoucher.setVoucherNumber(jtVoucherNumber);
            }
            //insertion of debit tax  voucher
            debitVoucher.setVoucherDate(voucher.getVoucherDate());
            debitVoucher.setAccountCode(voucher.getAccountCode());
            debitVoucher.setChassisId(voucher.getChassisId());
            debitVoucher.setChassisNo(voucher.getChassisNo());
            debitVoucher.setDebit(LedgerCalculationService.round(voucher.getTaxAmount(), 2));

            debitVoucher.setReferenceVoucher(voucher.getVoucherNumber());
            debitVoucher.setIsActive(1);

            //insertion of credit tax voucher
            creditVoucher.setVoucherDate(voucher.getVoucherDate());
            creditVoucher.setAccountCode(voucher.getTaxAccount());
            creditVoucher.setChassisNo(voucher.getChassisNo());
            creditVoucher.setChassisId(voucher.getChassisId());
            creditVoucher.setCredit(LedgerCalculationService.round(voucher.getTaxAmount(), 2));
            creditVoucher.setReferenceVoucher(voucher.getVoucherNumber());
            creditVoucher.setIsActive(1);

            debitVoucher.setRemarks(baseVoucher.getRemarks());
            creditVoucher.setRemarks(baseVoucher.getRemarks());
            voucherRepository.save(debitVoucher);
            voucherRepository.save(creditVoucher);

        }
        return baseVoucher;
    }

    public void saveJournal(Voucher voucher) {
        voucher.setIsActive(1);
        voucherRepository.save(voucher);
    }


    @Override
    public void saveVoucherList(List<Voucher> voucherList) {
        voucherRepository.save(voucherList);
    }

    public String generateVoucherNumber(String prefix) {
        String voucherNumber = "";
        int maxVoucherID = 0;
        String responseVoucherNumber = voucherRepository.findMaxVoucherNumber(prefix + "%");
        if (responseVoucherNumber == null) {
            maxVoucherID++;
            voucherNumber = Integer.toString(maxVoucherID);
        } else {
            maxVoucherID = Integer.parseInt(responseVoucherNumber.replaceAll("[^0-9]+", ""));
            maxVoucherID++;
            voucherNumber = Integer.toString(maxVoucherID);
        }
        if (maxVoucherID <= 9) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 99) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 999) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 9999) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 99999) {
            voucherNumber = "0" + voucherNumber;
        }
        if (maxVoucherID <= 999999) {
            voucherNumber = "0" + voucherNumber;
        }

        voucherNumber = prefix + voucherNumber;

        return voucherNumber;
    }

    @Override
    public String getVoucherNumberByReference(String voucherReference) {
        return voucherRepository.getVoucherNumberByReference(voucherReference);
    }

    public List<Voucher> getVouchersByBankAccount(int bankAccount, Date date, boolean nullStatus) {

        if (nullStatus) {
            return voucherRepository.findPendingVouchers(bankAccount, date);
        } else {
            return voucherRepository.findClearedVouchers(bankAccount, date);
        }
    }

    @Override
    public Voucher getVoucher(int voucherId) {
        return voucherRepository.findByVoucherId(voucherId);
    }

    @Override
    public String getSecondaryName(String voucherNumber, boolean forCredit) {
        if (forCredit) {
            return voucherRepository.nameForCreditNull(voucherNumber);
        }
        return voucherRepository.nameForDebitNull(voucherNumber);
    }

    public List<Voucher> findByVoucherNumber(String voucherNumber) {
        return voucherRepository.findByVoucherNumber(voucherNumber);
    }

    public List<Voucher> findByReferenceVoucher(String voucherNumber) {
        return voucherRepository.findByReferenceVoucher(voucherNumber);
    }

    public List<Voucher> getVouchersByItemAccount(int itemAccount) {
        return voucherRepository.findByItemAccount(itemAccount);
    }

    public List<Voucher> getAllItemVouchers() {

        return voucherRepository.findAllItemVouchers();
    }

    @Override
    public void save(Voucher voucher) {
        voucherRepository.save(voucher);
    }

    public List<Voucher> findByPrefix(Date fromDate, Date toDate, String prefix) {
        return voucherRepository.findVouchersByPrefix(fromDate, toDate, prefix);
    }

    public List<String> findJournalByPrefix(Date fromDate, Date toDate, String prefix) {
        return voucherRepository.findJournalVouchersByPrefix(fromDate, toDate, prefix);
    }

    public List<String> getVoucherNumbers(String prefix) {
        return voucherRepository.findDistinctVoucherNumbers(prefix);
    }

    public List<Voucher> findByPrefixAndVoucher(String fromVoucher, String toVoucher, String prefix) {
        return voucherRepository.findVouchersByPrefixAndNumber(fromVoucher, toVoucher, prefix);
    }

    public List<Voucher> getVouchersByAccount(int accountCode) {
        return voucherRepository.findByAccountCodeOrderByVoucherDateAsc(accountCode);
    }


    public List<String> findJournalVouchersByPrefixAndNumber(String fromVoucher, String toVoucher, String prefixString) {
        return voucherRepository.findJournalVouchersByPrefixAndNumber(fromVoucher, toVoucher, prefixString);
    }

    public List<Voucher> findJournalVouchersByPdcBankAccount(Date fromDate, Date toDate, int bankAccount) {
        return voucherRepository.findJournalVouchersByBankAccount(fromDate, toDate, bankAccount);
    }


    public List<Voucher> findByBankAccountCode(int bankAccountCode) {
        return voucherRepository.findByBankAccountOrderByVoucherDateAsc(bankAccountCode);
    }

    public List<Voucher> findBankAccountByDate(Date fromDate, Date toDate) {
        return voucherRepository.findBankAccountByDateOrder(fromDate, toDate);
    }

    public List<Voucher> findByVoucherType(String prefix) {
        return voucherRepository.findByVoucherType(prefix);
    }

    public void deleteVouchers(String voucherNumber) {
        voucherRepository.removeVouchersByReference(voucherNumber);
        voucherRepository.removeVouchersByNumber(voucherNumber);
    }

    public void removeVouchersByReference(String voucherNumber) {
        voucherRepository.removeVouchersByReference(voucherNumber);
    }

    public Voucher getVoucherForClosing(Integer accountCode, Integer voucherId, Date toDate) {
        return voucherRepository.getVoucherForClosing(accountCode, voucherId, toDate);
    }

    public List<Voucher> findByVoucherNumberIn(List<String> voucherNumbers) {
        return voucherRepository.findByVoucherNumberIn(voucherNumbers);
    }

    public void remove(List<Voucher> voucherList) {
        voucherRepository.delete(voucherList);
    }

    public List<Voucher> getVoucherByCurrentDate(Date fromDate, Date toDate, int accountCode) {
        return voucherRepository.findVouchersByCurrentDate(fromDate, toDate, accountCode);
    }

    public Page<Voucher> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString, String prefix) {


        if (searchString.startsWith(prefix) || searchString.startsWith(prefix.toLowerCase())) {
            prefix = searchString.toUpperCase();
        } else {
            prefix += searchString;
        }
        Sort sort = null;
        if (isAsc) {
            sort = new Sort(Sort.Direction.ASC, orderColumn);
        } else {
            sort = new Sort(Sort.Direction.DESC, orderColumn);
        }

        if (prefix.startsWith("SL") || prefix.startsWith("RT")) {
            return voucherRepository.findByVoucherNumberLikeAndCreditGreaterThan("%" + prefix + "%", 0.0, new PageRequest(pageIndex, size, sort));
        } else if (prefix.startsWith("LP")) {
            return voucherRepository.findByVoucherNumberLikeAndDebitGreaterThan("%" + prefix + "%", 0.0, new PageRequest(pageIndex, size, sort));
        } else if (prefix.startsWith("BR")) {
            return voucherRepository.findByVoucherNumberLikeAndCreditGreaterThanAndBankAccountGreaterThan("%" + prefix + "%", -0.1, 0, new PageRequest(pageIndex, size, sort));
        } else if (prefix.startsWith("CP")) {
            return voucherRepository.findByVoucherNumberLikeAndCreditGreaterThan("%" + prefix + "%", 0.0, new PageRequest(pageIndex, size, sort));

        } else if (prefix.startsWith("PP")) {
            return voucherRepository.findByVoucherNumberLikeAndCreditGreaterThan("%" + prefix + "%", 0.0, new PageRequest(pageIndex, size, sort));

        } else if (prefix.startsWith("JV")) {
            return voucherRepository.findJoucherVoucherList("%" + searchString + "%", "JV%", new PageRequest(pageIndex, size, sort));

        }

        return voucherRepository.findByVoucherNumberLikeAndDebitGreaterThanAndBankAccountGreaterThan("%" + prefix + "%", -0.1, 0, new PageRequest(pageIndex, size, sort));

    }

    public List<Voucher> findAllCashInHandVouchers(int bankAccount) {
        return voucherRepository.findByBankAccount(bankAccount);
    }

    public List<Voucher> findByChassisId(List<Integer> chassisIdList) {
        return voucherRepository.findByChassisIdIn(chassisIdList);
    }


    public List<Voucher> findByChassisId(int chassisId) {
        return voucherRepository.findByChassisId(chassisId);
    }

    public List<Voucher> findByChassisNumber(String chassisNumber) {
        return voucherRepository.findByChassisNo(chassisNumber);
    }

    @Override
    public List<Voucher> getPaymentDueVoucher(String apartmentNumber, int accountCode) {
        return voucherRepository.getPaymentDueVoucher(apartmentNumber, accountCode);
    }

    @Override
    public double getPaidAmount(String apartmentNumber) {
        return voucherRepository.getPaidAmount(apartmentNumber);
    }

    @Override
    public List<Voucher> findByProjectCodeAndAccountCode(Integer projectCode, List<Integer> accountList) {
        return voucherRepository.findByProjectCodeAndAccountCodeInOrderByVoucherDateAsc(projectCode, accountList);
    }
}

