package com.hellokoding.account.service.voucher;


import com.hellokoding.account.model.Voucher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public interface VoucherService {

    Voucher savePayment(Voucher voucher);

    Voucher saveReceipt(Voucher voucher);

    void saveJournal(Voucher voucher);

    void saveVoucherList(List<Voucher> voucherList);

    Voucher getVoucher(int voucherId);

    String getSecondaryName(String voucherNumber, boolean forCredit);

    List<Voucher> getVouchersByAccount(int accountCode);

    List<Voucher> getVouchersByItemAccount(int itemAccount);

    List<Voucher> getAllItemVouchers();

    List<Voucher> findByVoucherNumber(String voucherNumber);

    List<Voucher> findByReferenceVoucher(String voucherNumber);

    List<Voucher> findByBankAccountCode(int bankAccountCode);

    List<Voucher>   getVouchersByBankAccount(int bankAccount, Date date, boolean nullStatus);

    void save(Voucher voucher);

    List<Voucher> findByPrefix(Date fromDate, Date toDate, String prefix);

    List<Voucher> findBankAccountByDate(Date fromDate, Date toDate);

    List<Voucher> findByVoucherType(String prefix);

    List<Voucher> findByPrefixAndVoucher(String fromVoucher, String toVoucher, String prefix);

    List<String> findJournalByPrefix(Date fromDate, Date toDate, String prefix);

    List<String> findJournalVouchersByPrefixAndNumber(String fromVoucher, String toVoucher, String prefixString);

    List<Voucher> findJournalVouchersByPdcBankAccount(Date fromDate, Date toDate, int bankAccount);

    List<String> getVoucherNumbers(String prefix);

    String generateVoucherNumber(String prefix);

    void deleteVouchers(String voucherNumber);

    void removeVouchersByReference(String referenceNumber);

    /**
     * To retain same voucher number in case of Salary Update
     *
     * @param voucherReference reference Number
     * @return voucher number
     */
    String getVoucherNumberByReference(String voucherReference);

    Voucher getVoucherForClosing(Integer accountCode, Integer voucherId, Date toDate);

    Integer getSecondaryAccount(List<Voucher> voucherList, String voucherNumber);

    List<Voucher> findByVoucherNumberIn(List<String> voucherNumbers);

    void remove(List<Voucher> voucherList);

    List<Voucher> getVoucherByCurrentDate(Date fromDate, Date toDate, int accountCode);

    Page<Voucher> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString, String prefix);

    List<Voucher> findAllCashInHandVouchers(int bankAccount);

    List<Voucher> findByChassisId(List<Integer> chassisIdList);

    List<Voucher> findByChassisId(int chassisId);

    List<Voucher> findByChassisNumber(String chassisNumber);

    /**
     * Get Bank Payment Vouchers
     * @param apartmentNumber
     * @param accountCode
     * @return
     */
    List<Voucher> getPaymentDueVoucher(String apartmentNumber, int accountCode);

    /**
     * Get Amount Paid for an Apartment
     * @param apartmentNumber
     * @return
     */
    double getPaidAmount(String apartmentNumber);

    List<Voucher> findByProjectCodeAndAccountCode(Integer projectCode, List<Integer> accountList);


}
