package com.hellokoding.account.service.security;

import com.hellokoding.account.model.CustomAuth;
import com.hellokoding.account.model.UserAuth;
import org.springframework.security.core.context.SecurityContextHolder;

public class RoleSupport {

    /**
     * Check if current User has Role.
     * @param authority Name of Role.
     * @return boolean
     */
    public static boolean hasRole(String authority) {

        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .anyMatch(e -> e.getAuthority().equals(authority));

    }

    /**
     * Get Logged in username
     * @return username
     */
    public static String getUserName(){

        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();

    }

    /**
     * GET UserAuth loggedIn
     * @return UserAuth
     */
    public static UserAuth getUserAuth(){

        CustomAuth customAuth = (CustomAuth)SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();


        return customAuth.getUserAuth();




    }

}
