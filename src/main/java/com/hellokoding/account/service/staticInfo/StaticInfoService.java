package com.hellokoding.account.service.staticInfo;

import com.hellokoding.account.model.StaticInfo;

import java.io.File;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface StaticInfoService {


     /**
      * Save Static Info
      * @param staticInfo Target Data
      * @return Persisted StaticInfo
      */
     StaticInfo save(StaticInfo staticInfo);

     /**
      * Find StaticInfo by Id
      * @param infoId identity
      * @return StaticInfo
      */
     StaticInfo findByInfoId(int infoId);

     /**
      * Get First Occurrence of Static Info From List
      * @return StaticInfo
      */
     StaticInfo findStaticInfo();

     /**
      * Image Upload For Logo
      * @param multipartFile Image File Converted
      */
     void uploadImage(MultipartFile multipartFile);

}
