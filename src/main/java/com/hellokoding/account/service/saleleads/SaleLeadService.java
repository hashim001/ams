package com.hellokoding.account.service.saleleads;

import com.hellokoding.account.model.SaleLead;

import java.util.List;

public interface SaleLeadService {

    /**
     * Find by Lead Id
     * @param Id
     * @return
     */
    SaleLead findByLeadId(int Id);

    /**
     * List of sale leads for specific Salesman
     * @param salesmanId
     * @return
     */
    List<SaleLead> findBySalesmanId(int salesmanId);

    /**
     * List of all sale leads
     * Used by admin
     * @return
     */
    List<SaleLead> findAll();


    void save(SaleLead saleLead);

    /**
     * Remove sale lead
     * Only for admin use
     * @param id
     */
    void removeLead(int id);
}
