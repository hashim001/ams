package com.hellokoding.account.service.saleleads;


import com.hellokoding.account.model.SaleLead;
import com.hellokoding.account.repository.SaleLeadRepository;
import com.hellokoding.account.service.security.RoleSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleLeadServiceImpl implements SaleLeadService{

    @Autowired
    private SaleLeadRepository saleLeadRepository;

    @Override
    public SaleLead findByLeadId(int Id) {
        return saleLeadRepository.findByLeadId(Id);
    }

    @Override
    public List<SaleLead> findAll() {
        return saleLeadRepository.findAllByOrderByCreatedDesc();
    }

    @Override
    public List<SaleLead> findBySalesmanId(int salesmanId) {
        return saleLeadRepository.findBySalesmanIdOrderByCreatedDesc(salesmanId);
    }


    @Override
    public void save(SaleLead saleLead) {
        saleLeadRepository.save(saleLead);
    }

    @Override
    public void removeLead(int id) {
        // Currently only admin can remove Sale Lead
        if(RoleSupport.hasRole("ROLE_MASTER_ADMIN")){
            saleLeadRepository.removeLead(id);
        }
    }
}
