package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.BankDetails;
import com.hellokoding.account.repository.AccountRepository;
import com.hellokoding.account.repository.BankDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class BankDetailServiceImpl implements BankDetailService {

    @Autowired
    private BankDetailRepository bankDetailRepository;


    public void save(BankDetails bankDetails) {
        bankDetailRepository.save(bankDetails);

    }

    public BankDetails findBankDetail(Account account) {

        return bankDetailRepository.findByAccount(account);
    }

    public List<BankDetails> findAll() {
        return bankDetailRepository.findAll();
    }

    public void delete(Account account) {
        bankDetailRepository.deleteByAccount(account);
    }

    public Integer isBank(Integer accountCode) {

        return bankDetailRepository.isBank(accountCode);
    }


}
