package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.BankDetails;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface BankDetailService {

    void save(BankDetails bankDetails);

    BankDetails findBankDetail(Account account);

    List<BankDetails> findAll();

    void delete(Account account);

    Integer isBank(Integer accountCode);

}
