package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import com.hellokoding.account.model.Distributor;
import com.hellokoding.account.model.StockCustomer;
import com.hellokoding.account.model.StockVendor;
import com.hellokoding.account.repository.AccountRepository;
import com.hellokoding.account.repository.TaxDetailRepository;
import com.hellokoding.account.repository.VoucherRepository;
import com.hellokoding.account.service.security.RoleSupport;
import com.hellokoding.account.service.user.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TaxDetailRepository taxDetailRepository;

    @Autowired
    private VoucherRepository voucherRepository;

    @Autowired
    private UserAuthService userAuthService;


    /**
     * Save new Account
     * Generates New Account Code
     *
     * @param account Account to save
     * @return Persisted Account
     */
    public Account save(Account account) {

        if (account.getParentCode() == null) {
            int maxAccountCode = accountRepository.findMaxControlAccount();
            if (maxAccountCode > 0) {
                String currentMax = "";
                currentMax += Integer.toString(maxAccountCode).charAt(0);
                currentMax += Integer.toString(maxAccountCode).charAt(1);
                int max = Integer.parseInt(currentMax);
                if (max <= 99) {
                    max++;
                    currentMax = Integer.toString(max);
                    currentMax += "00000000";
                    account.setAccountCode(Integer.parseInt(currentMax));
                    account.setLevel(1);
                    account.setIsActive(1);
                    return accountRepository.save(account);
                }
            } else {
                account.setAccountCode(1000000000);
                account.setIsActive(1);
                account.setLevel(1);
                return accountRepository.save(account);
            }
        } else {
            if (Integer.toString(account.getParentCode()).charAt(2) == '0' && Integer.toString(account.getParentCode()).charAt(3) == '0') {
                String currentMax = "";
                int maxAccountCode = accountRepository.findMaxInnerAccount(accountRepository.findByAccountCode(account.getParentCode()));
                if (maxAccountCode > 0) {
                    currentMax = Integer.toString(maxAccountCode).substring(2, 4);
                    int max = Integer.parseInt(currentMax);
                    if (max <= 99) {
                        max++;
                        currentMax = "";
                        if (max <= 9) {
                            currentMax += "0";
                        }
                        currentMax += Integer.toString(max);
                        String parentCode = Integer.toString(account.getParentCode());
                        String accountCode = parentCode.substring(0, 2) + currentMax.charAt(0) + currentMax.charAt(1) + parentCode.substring(4);
                        account.setAccountCode(Integer.parseInt(accountCode));
                        account.setIsActive(1);
                        account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                        account.setLevel(2);
                        return accountRepository.save(account);
                    }
                } else {
                    String parentCode = Integer.toString(account.getParentCode());
                    String accountCode = parentCode.substring(0, 2) + "01000000";
                    account.setAccountCode(Integer.parseInt(accountCode));
                    account.setIsActive(1);
                    account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                    account.setLevel(2);
                    return accountRepository.save(account);
                }

            } else if (Integer.toString(account.getParentCode()).charAt(4) == '0' && Integer.toString(account.getParentCode()).charAt(5) == '0') {
                String currentMax = "";
                int maxAccountCode = accountRepository.findMaxInnerAccount(accountRepository.findByAccountCode(account.getParentCode()));
                if (maxAccountCode > 0) {
                    currentMax = Integer.toString(maxAccountCode).substring(4, 6);
                    int max = Integer.parseInt(currentMax);
                    if (max <= 99) {
                        max++;
                        currentMax = "";
                        if (max <= 9) {
                            currentMax += "0";
                        }
                        currentMax += Integer.toString(max);
                        String parentCode = Integer.toString(account.getParentCode());
                        String accountCode = parentCode.substring(0, 4) + currentMax.charAt(0) + currentMax.charAt(1) + parentCode.substring(6);
                        account.setAccountCode(Integer.parseInt(accountCode));
                        account.setIsActive(1);
                        account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                        account.setLevel(3);
                        return accountRepository.save(account);
                    }
                } else {
                    String parentCode = Integer.toString(account.getParentCode());
                    String accountCode = parentCode.substring(0, 4) + "010000";
                    account.setAccountCode(Integer.parseInt(accountCode));
                    account.setIsActive(1);
                    account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                    account.setLevel(3);
                    return accountRepository.save(account);
                }
            } else {
                String currentMax = "";
                int maxAccountCode = accountRepository.findMaxForthInnerAccount(accountRepository.findByAccountCode(account.getParentCode()));
                if (maxAccountCode > 0) {
                    currentMax = Integer.toString(maxAccountCode).substring(6);
                    int max = Integer.parseInt(currentMax);
                    if (max <= 9999) {
                        max++;
                        currentMax = "";
                        if (max <= 999) {
                            currentMax += "0";
                        }
                        if (max <= 99) {
                            currentMax += "0";
                        }

                        if (max <= 9) {
                            currentMax += "0";
                        }

                        currentMax += Integer.toString(max);
                        String parentCode = Integer.toString(account.getParentCode());
                        String accountCode = parentCode.substring(0, 6) + currentMax;
                        account.setAccountCode(Integer.parseInt(accountCode));
                        account.setIsActive(1);
                        account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                        account.setLevel(4);
                        return accountRepository.save(account);
                    }
                } else {
                    String parentCode = Integer.toString(account.getParentCode());
                    String accountCode = parentCode.substring(0, 6) + "0001";
                    account.setAccountCode(Integer.parseInt(accountCode));
                    account.setIsActive(1);
                    account.setLevel(4);
                    account.setParentAccount(accountRepository.findByAccountCode(account.getParentCode()));
                    return accountRepository.save(account);
                }
            }


        }
        return null;

        //accountRepository.save(account);
    }


    public void update(Account account) {
        accountRepository.save(account);
    }

    public void remove(Account account) {
        accountRepository.delete(account);
    }

    public List<Account> findAll() {

        return accountRepository.findByIsActive(1);
    }

    public String getAccountTitle(int accountCode) {

        return accountRepository.getAccountTitle(accountCode);
    }

    @Override
    public List<Account> findAccountFor(int id, boolean isRider) {

        return null;
    }

    public List<Account> getDetailAccounts() {

        return accountRepository.findByBankDetailsListNotNull();
    }

    public List<Account> getStockAccounts() {
        return accountRepository.findByStockListNotNull();
    }


    public List<Account> getCustomerAccounts() {

        return accountRepository.findByStockCustomerListNotNull();
    }

    public List<Account> getVendorAccounts() {

        return accountRepository.findByStockVendorListNotNull();
    }


    public List<Account> getAccounts() {

        return null;
        //return accountRepository.findByBankDetails(null);
    }

    public List<Account> getTaxAccounts() {
        List<Object[]> objectList = accountRepository.getTaxMap(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        List<Account> accountList = new ArrayList<>();
        for (Object[] objectArray : objectList) {
            Account account = new Account();
            account.setAccountCode((int) objectArray[0]);
            account.setTitle((String) objectArray[1]);
            accountList.add(account);
        }
        return accountList;
    }

    public Account findAccount(int accountCode) {
        return accountRepository.findByAccountCode(accountCode);
    }

    public List<Account> getAccountNames(List<Integer> accountCodeList) {
        return accountRepository.getAccountNames(accountCodeList);
    }

    public List<Account> findWithCostCentres() {
        return accountRepository.findDistinctByCostCentresNotNull();
    }

    ;

    public List<Account> findAllFiltered() {
        List<Account> accountList = new ArrayList<>();
        for (Account account : accountRepository.findAllAccount()) {
            String accountCode = Integer.toString(account.getAccountCode());
            if (accountCode.substring(accountCode.length() - 4).equals("0000")) {
                accountList.add(account);
            }
        }
        return accountList;
    }


    public List<Account> findAccountsByLevel(int level) {
        return accountRepository.findAccountByLevel(level);
    }

    public List<Account> findAccountByLevelAlphabetically(int level) {
        return accountRepository.findAccountByLevelAlphabetically(level);
    }

    public boolean accountDeletable(Account account) {

        return account.getChildList().size() < 1
                && account.getNoteSubHeadings().size() < 1
                && account.getStockCustomer() == null
                && account.getStockVendor() == null
                && account.getAccountStock() == null
                && voucherRepository.findByAccountCodeOrderByVoucherDateAsc(account.getAccountCode()).size() < 1;
    }

    public void delete(Integer accountCode) {
        accountRepository.deleteAccount(accountCode);
    }

    public List<String> getInfo(Account account, boolean check) {
        List<String> infoList = new ArrayList<>();
        if (check) {
            StockVendor stockVendor = account.getStockVendor();
            infoList.add((account.getTitle()));
            infoList.add(stockVendor.getAddress());
            infoList.add(stockVendor.getMobile());
            infoList.add(stockVendor.getCnic());
            infoList.add(stockVendor.getLandline());
            infoList.add(account.getTotalBalanceAmount().toString());
            infoList.add(account.getAmountLimit().toString());
        } else {
            StockCustomer stockCustomer = account.getStockCustomer();
            infoList.add(account.getTitle());
            infoList.add(stockCustomer.getAddress());
            infoList.add(stockCustomer.getMobile());
            infoList.add(stockCustomer.getCnic());
            infoList.add(stockCustomer.getLandline());
            infoList.add(account.getTotalBalanceAmount().toString());
            infoList.add(account.getAmountLimit().toString());
        }
        return infoList;
    }


    // ACCOUNT MAPS FOR OPTIMIZATION
    @Override
    public List<Account> getDistributorAccountList() {
        List<Object[]> objectList = accountRepository.getDistributorMap();
        List<Account> accountList = new ArrayList<>();
        for (Object[] objectArray : objectList) {
            Account account = new Account();
            account.setAccountCode((int) objectArray[0]);
            account.setTitle((String) objectArray[1]);
            accountList.add(account);
        }
        return accountList;
    }

    @Override
    public List<Account> getVendorAccountList() {
        List<Object[]> objectList = accountRepository.getVendorMap();
        List<Account> accountList = new ArrayList<>();
        for (Object[] objectArray : objectList) {
            Account account = new Account();
            account.setAccountCode((int) objectArray[0]);
            account.setTitle((String) objectArray[1]);
            accountList.add(account);
        }
        return accountList;
    }

    /**
     * Return Account Map to Avoid EAGER Load
     *
     * @return List of Account with Title, and accountCode
     * Currently Working 28-FEB-2018
     */
    @Override
    public List<Account> getCustomerAccountList() {

        List<Object[]> objectList = accountRepository.getCustomerMap();
        List<Account> accountList = new ArrayList<>();
        for (Object[] objectArray : objectList) {
            Account account = new Account();
            account.setAccountCode((int) objectArray[0]);
            account.setTitle((String) objectArray[1]);
            accountList.add(account);
        }
        return accountList;
    }

    /**
     * Get Account Title and AccountCode
     * To avoid EAGER loading if whole Account Object is Fetched
     *
     * @return List Of Account with title and AccountCode
     */
    @Override
    public List<Account> getAccountList(int userType) {
        List<Object[]> objectList = accountRepository.getAccountMap();
        List<Account> accountList = new ArrayList<>();
        for (Object[] objectArray : objectList) {
            Account account = new Account();
            account.setAccountCode((int) objectArray[0]);
            account.setTitle((String) objectArray[1]);
            account.setParentName((String) objectArray[2]);
            accountList.add(account);
        }
        return accountList;
    }

    /**
     * Get Account Title and AccountCode For Stock Accounts
     * To avoid EAGER loading if whole Account Object is Fetched
     *
     * @return List Of Account with title and AccountCode
     */
    @Override
    public List<Account> getStockAccountList() {
        List<Object[]> objectList = accountRepository.getStockMap();
        List<Account> accountList = new ArrayList<>();
        for (Object[] objectArray : objectList) {
            Account account = new Account();
            account.setAccountCode((int) objectArray[0]);
            account.setTitle((String) objectArray[1]);
            account.setOpeningBalance((Double) objectArray[2]);
            accountList.add(account);
        }
        return accountList;
    }

    /**
     * Get Account Title and AccountCode For Bank Accounts
     * To avoid EAGER loading if whole Account Object is Fetched
     *
     * @return List Of Account with title and AccountCode
     */
    @Override
    public List<Account> getBankAccountList() {
        List<Object[]> objectList = accountRepository.getBankMap();
        List<Account> accountList = new ArrayList<>();
        for (Object[] objectArray : objectList) {
            Account account = new Account();
            account.setAccountCode((int) objectArray[0]);
            account.setTitle((String) objectArray[1]);
            account.setOpeningDebit((double) objectArray[2]);
            account.setOpeningCredit((double) objectArray[3]);
            accountList.add(account);
        }
        return accountList;
    }

    /**
     * Get Account Title and AccountCode For Accounts of Specific Levels
     * To avoid EAGER loading if whole Account Object is Fetched
     *
     * @return List Of Account with title and AccountCode
     */
    @Override
    public List<Account> getAccountListByLevel(int level) {
        List<Object[]> objectList = accountRepository.getAccountMapByLevel(level);
        List<Account> accountList = new ArrayList<>();
        for (Object[] objectArray : objectList) {
            Account account = new Account();
            account.setAccountCode((int) objectArray[0]);
            account.setTitle((String) objectArray[1]);
            account.setParentName((String) objectArray[2]);
            accountList.add(account);
        }
        return accountList;
    }

    @Override
    public Map getTitleMap(List<Integer> accountCodeList) {

        // To avoid query Format Exception
        if (accountCodeList.size() < 1) {
            accountCodeList.add(0);
        }
        List<Object[]> objectList = accountRepository.getTitleList(accountCodeList);
        Map<Integer, String> titleMap = new HashMap<>();
        for (Object[] objectArray : objectList) {
            titleMap.put((int) objectArray[0], (String) objectArray[1]);
        }
        return titleMap;
    }

    public Page<Account> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString) {

        Account exampleAccount = new Account();
        exampleAccount.setTitle(searchString);

        //exampleAccount.setAccountCode(Integer.parseInt(searchString));
        ExampleMatcher columnMappingMatcher = ExampleMatcher.matchingAny()
                .withIgnorePaths("accountCode", "isActive", "level", "openingCredit", "lastOpeningCredit", "lastOpeningDebit", "openingDebit")
                .withIgnoreNullValues()
                .withMatcher("title", ExampleMatcher.GenericPropertyMatchers.startsWith().ignoreCase());

        Example<Account> accountExample = Example.of(exampleAccount, columnMappingMatcher);

        return accountRepository.findAll(accountExample, new PageRequest(pageIndex, size));
    }

    @Override
    public List<Account> findAccountsByParentCode(int parentCode) {
        return accountRepository.findAccountsByParentCode(parentCode);
    }

    @Override
    public List<Account> findAccountsByLevelNot(int level) {
        return accountRepository.findAccountByLevelNot(4);
    }

    @Override
    public List<Account> findAccountsByProjectCode(int projectCode) {
        return accountRepository.getProjectAccounts(projectCode);
    }
}
