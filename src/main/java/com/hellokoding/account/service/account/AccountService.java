package com.hellokoding.account.service.account;

import com.hellokoding.account.model.Account;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * NOTE: List Functions do not return whole Accounts
 * They are an optimization to avoid EAGER loading Form Account Model
 */

@Service
public interface AccountService {

    /**
     * Save Account
     * @param account Account to save
     * @return persisted Account
     */
    Account save(Account account);

    /**
     * Update Account
     * Skip Account Number Generation
     * @param account Account to Update
     */
    void update(Account account);

    /**
     * Delete Account
     * @param account Target Account
     */
    void remove(Account account);

    /**
     * Get Account Name (Title)
     * @param accountCode Target Account accountCode
     * @return Title
     */
    String getAccountTitle(int accountCode);

    /**
     * Get Account List by Multiple accountCodes
     * @param accountCodeList Account Code List
     * @return List of Accounts
     */
    List<Account> getAccountNames(List<Integer> accountCodeList);

    /**
     * Get all Accounts
     * @return List of Accounts
     */
    List<Account> findAll();

    /**
     * Get Accounts For Roles
     * Rider, Distributor
     * @param id id for distributor or rider
     * @param isRider check if rider
     * @return List of Accounts
     */
    List<Account> findAccountFor(int id, boolean isRider);

    /**
     * Get Account With CostCentres
     * @return List of Account
     */
    List<Account> findWithCostCentres();

    /**
     * GET 4th level Accounts
     * DEPRECIATED
     * @return List Of Account
     */
    List<Account> findAllFiltered();

    /**
     * Get Accounts By Level
     * @param level level
     * @return List Of Accounts
     */
    List<Account> findAccountsByLevel(int level);

    /**
     * Get Accounts By Level Not
     * @param level level
     * @return List Of Accounts
     */
    List<Account> findAccountsByLevelNot(int level);
    /**
     * Get Accounts By Level
     * Order by Account title Asc
     * @param level account level
     * @return List Of Accounts
     */
    List<Account> findAccountByLevelAlphabetically(int level);

    /**
     * Get Account With BankDetails (account_bank)
     * @return List Of Account
     */
    List<Account> getDetailAccounts();

    /**
     * Get Accounts with Stock (account_stock)
     * @return List of Accounts
     */
    List<Account> getStockAccounts();

    /**
     * Get Accounts with Customer (account_customer)
     * @return List of Account
     */
    List<Account> getCustomerAccounts();

    /**
     * Get Accounts with Vendor (account_vendor)
     * @return List of Account
     */
    List<Account> getVendorAccounts();

    /**
     * Get Accounts other than bank Detail accounts
     * @return List Of Accounts
     */
    List<Account> getAccounts();

    /**
     * Get Account with Tax Details (account_tax)
     * @return List Of Accounts
     */
    List<Account> getTaxAccounts();

    /**
     * Get All Account Optimized
     * @param userType Type Of User (Currently Unused)
     * @return List Of Account with account Code , title (and/or) parent account Title
     */
    List<Account> getAccountList(int userType);
    /**
     * Get All Customer Account Optimized
     * @return List Of Account with account Code , title (and/or) parent account Title
     */
    List<Account> getCustomerAccountList();
    /**
     * Get All Vendor Account Optimized
     * @return List Of Account with account Code , title (and/or) parent account Title
     */
    List<Account> getVendorAccountList();
    /**
     * Get All Distributor Account Optimized
     * @return List Of Account with account Code , title (and/or) parent account Title
     */
    List<Account> getDistributorAccountList();
    /**
     * Get All Stock Account Optimized
     * @return List Of Account with account Code , title (and/or) parent account Title
     */
    List<Account> getStockAccountList();
    /**
     * Get All Bank Account Optimized
     * @return List Of Account with account Code , title (and/or) parent account Title
     */
    List<Account> getBankAccountList();
    /**
     * Get All Account Optimized by level
     * @param level level
     * @return List Of Account with account Code , title (and/or) parent account Title
     */
    List<Account> getAccountListByLevel(int level);

    /**
     * Get MAP for titles of account
     * Used for multiple data fetch
     * use: Listing of vouchers
     * @param accountCodeList list of AccountCodes
     * @return Map key -> account code , value  -> title
     */
    Map getTitleMap(List<Integer> accountCodeList);

    /**
     * Get Account By AccountCode
     * @param accountCode Target account Code
     * @return Account
     */
    Account  findAccount(int accountCode);

    /**
     * Check if Account is Deletable
     * If it does not contain ledger effecting data
     * @param account Source Account
     * @return check
     */
    boolean accountDeletable(Account account);

    /**
     * Delete Account By Account Code
     * @param accountCode accountCode
     */
    void delete(Integer accountCode);

    /**
     * Get Account Info in String
     * For Ajax Data
     * @param account Source Account
     * @param check for Customer , Vendor
     * @return List of String with data
     */
    List<String> getInfo(Account account, boolean check);

    /**
     * Account List Ajax Call
     * @param pageIndex
     * @param size
     * @param orderColumn
     * @param isAsc
     * @param searchString
     * @return
     */
    Page<Account> findAllPaged(int pageIndex, int size, String orderColumn, boolean isAsc, String searchString);

    List<Account> findAccountsByParentCode(int parentCode);

    List<Account> findAccountsByProjectCode(int projectCode);
}
