package com.hellokoding.account.service.subDepartment;

import com.hellokoding.account.model.SubDepartment;

import java.util.List;

public interface SubDepartmentService {


    /**
     * Find SubDepartment by subDepartmentId
     * @param id subDept Identity
     * @return SubDepartment
     */
    SubDepartment findBySubDeptId(int id);


    /**
     * Get all SubDepartments
     * @return List of Departments
     */
    List<SubDepartment> findAll();

    /**
     * Save SubDepartment
     * @param subDepartment To save
     * @return Persisted SubDepartment
     */
    SubDepartment save(SubDepartment subDepartment);

    /**
     * Remove Sub Dept with DeptId
     * @param subDeptId Sub department Identity
     */
    void remove(int subDeptId);


}
