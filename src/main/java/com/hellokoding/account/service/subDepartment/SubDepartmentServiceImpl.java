package com.hellokoding.account.service.subDepartment;

import com.hellokoding.account.model.SubDepartment;
import com.hellokoding.account.repository.SubDepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubDepartmentServiceImpl implements SubDepartmentService {
    @Autowired
    private SubDepartmentRepository subDepartmentRepository;

    @Override
    public List<SubDepartment> findAll() {
        return subDepartmentRepository.findAll();
    }

    @Override
    public SubDepartment findBySubDeptId(int id) {
        return subDepartmentRepository.findBySubDepId(id);
    }

    @Override
    public SubDepartment save(SubDepartment subDepartment) {
        return subDepartmentRepository.save(subDepartment);
    }

    @Override
    public void remove(int subDeptId) {
        subDepartmentRepository.removeSubDept(subDeptId);
    }

}
