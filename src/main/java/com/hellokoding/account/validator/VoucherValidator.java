package com.hellokoding.account.validator;


import com.hellokoding.account.model.CostCentre;
import com.hellokoding.account.model.Voucher;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class VoucherValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return Voucher.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Voucher voucher = (Voucher) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billAmount", "NotEmpty");
        if(voucher.getBillAmount() !=null) {
            if (voucher.getBillAmount() < 0) {
                errors.rejectValue("billAmount", "Size.voucher.billAmount");
            }
        }


    }
}
